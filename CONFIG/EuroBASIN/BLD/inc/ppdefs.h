! Preprocessor definitions
#define PPWRITEDBGALLPRCS write(eDbgU,*)
#define PPWRLOG write(eLogU,*)
#ifdef PARALLELERSEM
#define PPWRITEDBG if (IOProcess) PPWRITEDBGALLPRCS
#define PPWRITELOG if (IOProcess) PPWRLOG
#else
#define PPWRITEDBG PPWRITEDBGALLPRCS
#define PPWRITELOG PPWRLOG
#endif
