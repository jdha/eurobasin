

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: {\em ERSEM} variables
!
! !DESCRIPTION:
! This module holds a small number of variables that are used throughout
! the {\em ERSEM} source code, including both the pelagic and benthic sub-
! components.\\[\baselineskip]
!
! !INTERFACE:
   module ersem_variables
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is public
   public
!
! !PUBLIC DATA MEMBERS:
! Time step
   real(fp8) :: timestep

! For data logging
   logical :: ncdfInstOut=.true.
   logical :: ncdfDailyOut=.true.
   logical :: ncdfWeeklyOut=.true.
   logical :: ncdfMonthlyOut=.true.


   logical :: IOProcess=.false.





   integer :: isubstart,jsubstart,ksubstart
   integer :: iglobdom,jglobdom,kglobdom


   integer :: pidErsem=0
   character(len=5) :: strpidErsem='0'

! File units for I/O
   integer :: eDbgU=10, eLogU=6

! Debugging
! Ersem switches
! Switch for maintainance of positive, close to 0 biomasses
   integer :: iswZeroControl=0
! Switch for computation of biogeochemical rates
   integer :: iswecolX
! Switch for pH impact on nitrification
   integer :: iswPHX
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
! James Clark (PML) Jan 2014 - Created module ersem_variables, and
! moved data members in from module global_declarations."
!
!EOP
!-----------------------------------------------------------------------
   end module ersem_variables
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
