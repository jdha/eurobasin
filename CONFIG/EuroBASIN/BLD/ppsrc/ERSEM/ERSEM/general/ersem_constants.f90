

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: {\em ERSEM} constants
!
! !DESCRIPTION:
! This module holds fixed constants used throughout the ersem source
! code. It defines the mol masses of the chemical elements used in the model and some conversion constants.\\[\baselineskip]
!
! !INTERFACE:
   module ersem_constants
!
! !USES:
   implicit none

! Default all is public
   public
!
! !PUBLIC DATA MEMBERS:
! Real kind type parameter
   integer, parameter :: fp8 = selected_real_kind(13,99)

! Epsilon
   real(fp8), parameter :: zeroX = 1.0e-8_fp8

! Molar Masses of constituents:
   real(fp8), parameter :: CMass = 12.011_fp8 ! Carbon
   real(fp8), parameter :: PMass = 30.9738_fp8 ! Phosphorous
   real(fp8), parameter :: NMass = 14.0067_fp8 ! Nitrogen
   real(fp8), parameter :: SMass = 28.0855_fp8 ! Silica
   real(fp8), parameter :: OMass = 15.9994_fp8 ! Oxygen
   real(fp8), parameter :: FeMass = 55.845_fp8 ! Iron

! Unit conversions
   real(fp8), parameter :: seconds_per_day = 86400._fp8
   real(fp8), parameter :: milli_to_mega = 1.e-9_fp8
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team

! James Clark (PML) Jan 2014 - Created module ersem_constants, and
! moved data members in from module global_declarations."
!
!EOP
!-----------------------------------------------------------------------

   end module ersem_constants

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
