

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Allocation helpers
!
! !DESCRIPTION:
! Reports an error for allocation of variable cvar, with up to
! five dimensions n1, n2, n3, n4, n5.\\[\baselineskip]
!
! !INTERFACE:
   module allocation_helpers
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

   interface allocerr
      module procedure allocerr1D,allocerr2D,allocerr3D,allocerr4D,allocerr5D
   end interface allocerr
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
! !TO DO:
! Document this module.
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Allocation error (1D)
!
! !DESCRIPTION:
! Reports an error when allocating 1D variable, cvar.\\[\baselineskip]
!
! !INTERFACE:
   subroutine allocerr1D ( cvar, n1, ierror )
!
! !USES:

   use ersem_variables, only: IOProcess, eLogU, eDbgU



!
! !INPUT PARAMETERS:
! Number of elements in dim 1
   integer,intent(in) :: n1

! Error status returned by allocate
   integer,intent(in) :: ierror

! Variable name
   character*(*),intent(in) :: cvar


   integer :: ierr

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Report error
   if ( ierror.ne.0 ) then

      if (IOProcess) write(eLogU,*) &
      ' Failed to allocate space for ',cvar &
      ,' with size of ',n1 &
      ,' stat= ',ierror
!

      call flush(eDbgU)
      call flush(eLogU)
      call MPI_abort(ierr)



!
   end if
!
   return
!
   end subroutine allocerr1D
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Allocation error (2D)
!
! !DESCRIPTION:
! Reports an error when allocating 2D variable, cvar.\\[\baselineskip]
!
! !INTERFACE:
   subroutine allocerr2D ( cvar, n1, n2, ierror )
!
! !USES:

   use ersem_variables, only: IOProcess, eLogU, eDbgU



!
! !INPUT PARAMETERS:
! Number of elements in dims 1,2
   integer,intent(in) :: n1, n2

! Error status returned by allocate
   integer,intent(in) :: ierror

! Variable name
   character*(*),intent(in) :: cvar


   integer :: ierr

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Report error
   if ( ierror.ne.0 ) then

      if (IOProcess) write(eLogU,*) &
      ' Failed to allocate space for ',cvar &
      ,' with size of ',n1,' x ',n2 &
      ,' stat= ',ierror
!

      call flush(eDbgU)
      call flush(eLogU)
      call MPI_abort(ierr)



!
   end if
!
   return
!
   end subroutine allocerr2D
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Allocation error (3D)
!
! !DESCRIPTION:
! Reports an error when allocating 3D variable, cvar.\\[\baselineskip]
!
! !INTERFACE:
   subroutine allocerr3D ( cvar, n1, n2, n3, ierror )
!
! !USES:

   use ersem_variables, only: IOProcess, eLogU, eDbgU



!
! !INPUT PARAMETERS:
! Number of elements in dims 1,2,3
   integer,intent(in) :: n1, n2, n3

! Error status returned by allocate
   integer,intent(in) :: ierror

! Variable name
   character*(*),intent(in) :: cvar


   integer :: ierr

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Report error
   if ( ierror.ne.0 ) then

      if (IOProcess) write(eLogU,*) &
      ' Failed to allocate space for ',cvar &
      ,' with size of ',n1,' x ',n2,' x ',n3 &
      ,' stat= ',ierror
!

      call flush(eDbgU)
      call flush(eLogU)
      call MPI_abort(ierr)



!
   end if
!
   return
!
   end subroutine allocerr3D
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Allocation error (4D) \label{sec:allocerr4D}
!
! !DESCRIPTION:
! Reports an error when allocating 4D variable, cvar.\\[\baselineskip]
!
! !INTERFACE:
   subroutine allocerr4D ( cvar, n1, n2, n3, n4, ierror )
!
! !USES:

   use ersem_variables, only: IOProcess, eLogU, eDbgU



!
! !INPUT PARAMETERS:
! Number of elements in dims 1,2,3,4
   integer,intent(in) :: n1, n2, n3, n4

! Error status returned by allocate
   integer,intent(in) :: ierror

! Variable name
   character*(*),intent(in) :: cvar


   integer :: ierr

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Report error
   if ( ierror.ne.0 ) then

      if (IOProcess) write(eLogU,*) &
      ' Failed to allocate space for ',cvar &
      ,' with size of ',n1,' x ',n2,' x ',n3,' x ',n4 &
      ,' stat= ',ierror
!

      call flush(eDbgU)
      call flush(eLogU)
      call MPI_abort(ierr)



!
   end if
!
   return
!
   end subroutine allocerr4D
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Allocation error (5D) \label{sec:allocerr5D}
!
! !DESCRIPTION:
! Reports an error when allocating 5D variable, cvar.\\[\baselineskip]
!
! !INTERFACE:
   subroutine allocerr5D ( cvar, n1, n2, n3, n4, n5, ierror )
!
! !USES:

   use ersem_variables, only: IOProcess, eLogU, eDbgU



!
! !INPUT PARAMETERS:
! Number of elements in dims 1,2,3,4,5
   integer,intent(in) :: n1, n2, n3, n4, n5

! Error status returned by allocate
   integer,intent(in) :: ierror

! Variable name
   character*(*),intent(in) :: cvar


   integer :: ierr

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Report error
   if ( ierror.ne.0 ) then

      if (IOProcess) write(eLogU,*) &
      ' Failed to allocate space for ',cvar &
      ,' with size of ',n1,' x ',n2,' x ',n3,' x ',n4,' x ',n5 &
      ,' stat= ',ierror
!

      call flush(eDbgU)
      call flush(eLogU)
      call MPI_abort(ierr)



!
      endif
!
      return
!
   end subroutine allocerr5D
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Allocate ersem variables (real)
!
! !DESCRIPTION:
! Allocates {\em ERSEM} variables of type real and initialises to zero.\\[\baselineskip]
!
! !INTERFACE:
   subroutine allocateErsemDouble ( var, varname, istart, iend)
!
! !INPUT PARAMETERS:
! Start/end indices
   integer,intent(in) :: istart,iend

! Variable name
   character(len=*),intent(in) :: varname
!
! !INPUT/OUTPUT PARAMETERS:
! Variable array
   real(fp8),dimension(:),pointer :: var
!
! !LOCAL VARIABLES:
   integer :: ialloc
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   allocate(var(istart:iend),stat=ialloc)
   call allocerr(varname,iend-istart+1,0,0,0,ialloc)
   var=0.0e0_fp8
!
   end subroutine allocateErsemDouble
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Allocate ersem variables (integer)
!
! !DESCRIPTION:
! Allocates {\em ERSEM} variables of type integer and initialieses to zero.\\[\baselineskip]
!
! !INTERFACE:
   subroutine allocateErsemInteger ( var, varname, istart, iend)
!
! !USES:
!
! !INPUT PARAMETERS:
! Start/end indices
   integer,intent(in) :: istart,iend

! Variable name
   character(len=*),intent(in) :: varname
!
! !INPUT/OUTPUT PARAMETERS:
! Variable array
   integer,dimension(:),pointer :: var
!
! !LOCAL VARIABLES:
   integer :: ialloc
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC

   allocate(var(istart:iend),stat=ialloc)
   call allocerr(varname,iend-istart+1,0,0,0,ialloc)
   var=0
!
   end subroutine allocateErsemInteger
!
!EOC
!-----------------------------------------------------------------------

   end module allocation_helpers

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
