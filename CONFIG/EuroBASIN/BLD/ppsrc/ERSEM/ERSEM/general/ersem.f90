

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: {\em ERSEM} main
!
! !DESCRIPTION:
! This module acts as the main interface to the {\em ERSEM} library.
! It contains the main ersem routine calling the various sub-components
! to compute all biogeochemical rates of change, the routine for the
! allocation of the main variables and the routine for the initialisation
! of the {\em ERSEM} library.
! \\[\baselineskip]
!
! !INTERFACE:
   module ersem
!
! !USES:
   use ersem_variables, ONLY: IOProcess
   implicit none

! Default all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public init_ersem, ersem_loop, allocate_ersem, close_ersem
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Allocate {\em ERSEM} variables
!
! !DESCRIPTION:
! This routine sets the dimension of the ersem state and rate variables
! for pelagic and benthos and sets the pointers to optional states,
! that can be activated via the preprocessing switches \texttt{1},
! \texttt{1}, \texttt{MASSTRACER}, \texttt{1}, \texttt{DOC\_DYN}
! and allocates the main variables by calling the routines \texttt{global\_allocations},
! \texttt{benthos\_alloc} and \texttt{bacteria\_alloc}.
! \begin{description}
! \item [{\texttt{I\_TOT}}] Total number of pelagic state variables for concentrations
! (second dimension of the \texttt{ccc} array).
! \item [{\texttt{1\_STATE}}] Position of the pelagic calcification state
! variables in the \texttt{ccc},\texttt{ ccc0},\texttt{ cccstr},\texttt{
! cccP},\texttt{ sccc},\texttt{ sdccc},\texttt{ wsiccc},\texttt{ wsoccc
! }and \texttt{itrccc} arrays.
! \item [{\texttt{1\_STATEBEN}}] Position of the benthic calcification
! state variables in the \texttt{ccc},\texttt{ ccc0},\texttt{ cccstr},\texttt{
! cccP},\texttt{ sccc},\texttt{ sdcc}b,\texttt{ wsiccc},\texttt{ wsoccc
! }and \texttt{itrccc} arrays.
! \item [{\texttt{1\_STATE}}] Position of the pelagic iron state variables
! in the \texttt{ccc},\texttt{ ccc0},\texttt{ cccstr},\texttt{ cccP},\texttt{
! sccc},\texttt{ sdccc},\texttt{ wsiccc},\texttt{ wsoccc }and \texttt{itrccc}
! arrays.
! \item [{\texttt{1\_STATEBEN}}] Position of the benthic iron state variables
! in the \texttt{ccc},\texttt{ ccc0},\texttt{ cccstr},\texttt{ cccP},\texttt{
! sccc},\texttt{ sdcc}b,\texttt{ wsiccc},\texttt{ wsoccc }and \texttt{itrccc}
! arrays.
! \item [{\texttt{MASSTRACER\_STATE}}] Position of the masstracer state variables
! in the \texttt{ccc},\texttt{ ccc0},\texttt{ cccstr},\texttt{ cccP},\texttt{
! sccc},\texttt{ sdccc},\texttt{ wsiccc},\texttt{ wsoccc }and \texttt{itrccc}
! arrays.
! \item [{\texttt{DOC\_DYN}}] Position of the extra state variables for dynamic
! recycling of organic matter through bacteria in the \texttt{ccc},\texttt{
! ccc0},\texttt{ cccstr},\texttt{ cccP},\texttt{ sccc},\texttt{ sdccc},\texttt{
! wsiccc},\texttt{ wsoccc }and \texttt{itrccc} arrays.
! \item [{\texttt{ADY\_STATE}}] Position of the optically active variables
! for gelbstoff adsorption in the \texttt{ccc},\texttt{ ccc0},\texttt{
! cccstr},\texttt{ cccP},\texttt{ sccc},\texttt{ sdccc},\texttt{ wsiccc},\texttt{
! wsoccc }and \texttt{itrccc} arrays.
! \end{description}
! The number of spatial points in pelagic (\texttt{N\_COMP}) and benthos
! (\texttt{N\_COMPBEN}) must be provided through the coupling interface
! with the physical driver.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine allocate_ersem()
!
! !USES:
   use ersem_variables, only: eLogU, eDbgU
   use pelagic_variables, only: i_state, &
      do_pelagic_variable_allocations
   use benthic_variables, only: i_stateben, &
      do_benthic_variable_allocations
   use benthic_zone, only: do_benthic_zone_allocations

   use pelagic_variables, only: calc_state
   use benthic_variables, only: calc_stateben


   use pelagic_variables, only: iron_state
   use benthic_variables, only: iron_stateben





   use pelagic_variables, only: ADY_STATE


   use pelagic_variables, only: DOCDYN_STATE

!
! !LOCAL VARIABLES:
! Total number of pelagic state variables
   integer :: I_TOT

! Total number of benthic state variables
   integer :: I_TOTBEN
! Unit of file indicating status of run
   integer :: statunit
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! UPDATE STATUS FILE
   IF (IOProcess) call update_status("Allocating")
   ! DEFINE THE NUMBER OF STATE VARIABLES
   I_TOT = 49
   I_TOTBEN = 34


   CALC_STATE = I_TOT + 1
   I_TOT = I_TOT + 1
   CALC_STATEBEN = I_TOTBEN + 1
   I_TOTBEN = I_TOTBEN + 1


   ! add 7 pelagic and 1 benthic iron state variable to the total numbers:
   IRON_STATE = I_TOT + 1
   I_TOT = I_TOT +7
   IRON_STATEBEN = I_TOTBEN + 1
   I_TOTBEN = I_TOTBEN + 1






   ADY_STATE = I_TOT + 1
   I_TOT = I_TOT + 1


   DOCDYN_STATE = I_TOT + 1
   I_TOT = I_TOT + 1

   I_STATE = I_TOT
   I_STATEBEN = I_TOTBEN
!
   if (IOProcess) write(eLogU,*) 'Total pelagic states: ',I_STATE
   if (IOProcess) write(eLogU,*) 'Total bethic states: ',I_STATEBEN
!
   call do_pelagic_variable_allocations()
   call do_benthic_variable_allocations()
   call do_benthic_zone_allocations()
!
   return

   end subroutine allocate_ersem
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise {\em ERSEM} variables
!
! !DESCRIPTION:
! Initialise the ecological model and allocate the budget variables.\\[\baselineskip]
!
! !INTERFACE:
   subroutine init_ersem()
!
! !USES:
   use pelagic_initialisations, only: do_pelagic_initialisations
   use benthic_initialisation, only: do_benthic_initialisations
   use budget, only: allocate_budget
   use reset, only: reset_ersem
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! UPDATE STATUS FILE
   IF (IOProcess) call update_status("Initialising")

   call do_pelagic_initialisations()
   call do_benthic_initialisations()
   call allocate_budget() ! allocation of budget vars. needs switches
   call reset_ersem()

   ! UPDATE STATUS FILE
   IF (IOProcess) call update_status("Running")
!
   return

   end subroutine init_ersem
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Main {\em ERSEM} loop
!
! !DESCRIPTION:
! This routine computes all rates of change in the general partial differential
! equation system for biogeochemical tracers, advection and diffusion
! excluded.
!
! First the available biomasses \texttt{cccP} for consumption processes
! are computed.
!
! Subsequently the routines to evaluate the pelagic (\texttt{subpel})
! and benthic (\texttt{subben}) processes are called, followed by the
! computation of sedimentation%
! \footnote{In this document (and in the code) we use the terms ``sedimentation''
! for the gravitational sinking of particles in the water column and
! ``settling'' for the absorption of pelagic material into the benthos
! in order to clearly distiguish between the two processes.%
! } (\texttt{calc\_sedimentation}) in the water column.
!
! In a final step all computed rates are aggregated into the \texttt{sccc}
! and \texttt{sccb} arrays, which need to be passed to the physical
! driver for integration.
!
! The entire dynamical cycle of ERSEM can be switched off by setting
! \texttt{ISWECOLX} to $0$.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine ersem_loop
!
! !USES:
   use ersem_constants, only: fp8
   use ersem_variables, only: iswZeroControl, iswECOLX




   use pelagic_variables, only: n_comp, i_state, ccc, cccP, ccc0, &
      sccc, wsiccc, wsoccc, &
      iswbioalk, iBioalk, iN1p, iN3n, iN4n
   use benthic_variables, only: n_compben, i_stateben, ccb, &
      ccbP, ccb0, sccb, wsiccb, wsoccb
   use pelagic_zone, only : pelagic_zone_dynamics
   use benthic_zone, only: benthic_zone_dynamics
   use sedimentation, only: do_sedimentation
!
! !LOCAL VARIABLES:
! Grid indices
   integer :: i,m,k
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if ( iswZeroControl .NE. 0 ) then
      do m=1,I_STATE
         do i=1,N_COMP
            cccP(i,m)=max(ccc(i,m)-ccc0(m),0._fp8)
         end do
      end do
      do m=1,I_STATEBEN
         do k=1,N_COMPBEN
            ccbP(k,m)=max(ccb(k,m)-ccb0(m),0._fp8)
         end do
      end do
   end if




   if ( iswECOLX .eq. 1 ) then





      call pelagic_zone_dynamics()





      call benthic_zone_dynamics()





   end if





   call do_sedimentation()

   ! The following deals with vertical fluxes across the benthic pelagic interface.
   do m=1,i_state
      do I=1,n_comp
         sccc(i,m)=sccc(i,m)-wSOCCC(i,m)+wSICCC(i,m)
      end do
   end do
   do m=1,i_stateben
      do k=1,n_compben
         sccb(k,m)=sccb(k,m)-wSOCCb(k,m)+wSICCb(k,m)
      end do
   end do

   ! YA: bioalk changes are due to nutrient uptake / release
   if (iswbioalk.eq.1) then
      sccc(:,ibioalk)=sccc(:,ibioalk)-sccc(:,iN1p)-sccc(:,iN3n)+sccc(:,iN4n)
   end if




!
   return
!
   end subroutine ersem_loop
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Close {\em ERSEM}
!
! !DESCRIPTION:
! Close {\em ERSEM} and update status of run.\\[\baselineskip]
!
! !INTERFACE:
   subroutine close_ersem()
!
! !USES:
   use pelagic_initialisations, only: do_pelagic_initialisations
   use benthic_initialisation, only: do_benthic_initialisations
   use budget, only: allocate_budget
   use reset, only: reset_ersem
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! UPDATE STATUS FILE
   IF (IOProcess) call update_status("Initialising")

   call do_pelagic_initialisations()
   call do_benthic_initialisations()
   call allocate_budget() ! allocation of budget vars. needs switches
   call reset_ersem()

   ! UPDATE STATUS FILE
   IF (IOProcess) call update_status("Done")
!
   return

   end subroutine close_ersem
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise {\em ERSEM} variables
!
! !DESCRIPTION:
! Updates entry in fine indicating the status of the simulation.
!
! !INTERFACE:
   subroutine update_status(msg)
!
! !USES:
!
! !LOCAL VARIABLES:
! Grid indices
   character(len=*) , intent(in) ::msg
   integer :: statunit
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! open status file, write new message and close
   open(newunit=statunit,file="ERSEM.status",status="REPLACE")
   write(statunit,*) msg
   close(statunit)
!
   return

   end subroutine update_status
!
!EOC
!-----------------------------------------------------------------------

   end module ersem

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
