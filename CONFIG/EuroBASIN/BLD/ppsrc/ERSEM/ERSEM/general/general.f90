

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: General helper functions
!
! !DESCRIPTION:
! Collection of general, frequently applied routines and functions.
! \\[\baselineskip]
!
! !INTERFACE:
   module general
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public eMM,eMM2,eMM3,eramp,eramp_,eramp__,eTq,FoodPartition4, &
      Combine,Decomp,CombineN,DecompN,NDiffusion, &
      Adjust_fixed_nutrients,YMD2YD
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
! !TO DO:
! Document this module.
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Michaelis-Menten kinetics
!
! !DESCRIPTION:
! Michaelis-Menten kinetics.\\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function eMM(x,m)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: x, m
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (x .ge. 0._fp8) then
      if (m.gt.0._fp8) then
         eMM=x/(m+x)
      else
         eMM = 1._fp8
      end if
   else
      eMM = 0._fp8
   end if

   return

   end function eMM
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Quadratic Michaelis-Menten kinetics
!
! !DESCRIPTION:
! Quadratic Michaelis-Menten kinetics.\\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function eMM2(x,m)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: x, m
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (x .gt. 0._fp8) then
      eMM2 = eMM(x*x,m*m)
   else
      eMM2 = 0._fp8
   end if

   return

   end function eMM2
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Cubic Michaelis-Menten kinetics
!
! !DESCRIPTION:
! Cubic Michaelis-Menten kinetics.\\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function eMM3(x,m)
!
! !USES:
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: x,m
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (x.gt.0._fp8) then
      eMM3 = eMM(x*x*x,m*m*m)
   else
      eMM3 = 0._fp8
   end if

   return

   end function eMM3
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Ratio constrained to $[0,1]$
!
! !DESCRIPTION:
! Ratio of two variables constrained to the interval $[0,1]$.\\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function eramp(x,m)
!
! !USES:
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: x,m
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (x.lt.0._fp8) then
      eramp = 0._fp8
   else
      if (x.lt.m) then
         eramp = x/m
      else
         eramp = 1._fp8
      end if
   end if

   return

   end function eramp
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Remainder of ratio constrained to $[0,1]$
!
! !DESCRIPTION:
! Remainder of the ratio of two variables constrained to the interval $[0,1]$.
! Uses eramp function.
! \\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function eramp_(x,m)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: x,m
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   eramp_ = 1._fp8 - eramp(x,m)

   return

   end function eramp_
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Remainder of ratio constrained to $[0,1]$ (explicit)
!
! !DESCRIPTION:
! Remainder of the ratio of two variables constrained to the interval $[0,1]$.
! Explicit version.
! \\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function eramp__(x,m)
!
! !USES:
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: x,m
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (x.le.0._fp8) then
      eramp__ = 1._fp8
   else
      eramp__ = 1._fp8 - x/m
   end if

   return

   end function eramp__
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Q10 function of temperature
!
! !DESCRIPTION:
! Temperature regulating function with q10 value.\\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function eTq(T,q10)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: T, q10
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   eTq = EXP( LOG(q10)*(T-10._fp8)/10._fp8 )

   return

   end function eTq
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Food partition of four components
!
! !DESCRIPTION:
! This Subroutine distributes the uptake among four food components. \\[\baselineskip]
!
! !INTERFACE:
   subroutine FoodPartition4(rate,mfood,hfood,eF,food1,flux1, &
       & food2,flux2,food3,flux3,food4,flux4)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: rate,mfood,hfood
   real(fp8), intent(in) :: food1,food2,food3,food4
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8), intent(inout) :: eF ,flux1,flux2,flux3,flux4
!
! !LOCAL VARIABLES:
   real(fp8) :: f1,f2,f3,f4,foodsum
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   f1 = food1*eMM(food1,mfood)
   f2 = food2*eMM(food2,mfood)
   f3 = food3*eMM(food3,mfood)
   f4 = food4*eMM(food4,mfood)

   foodsum = f1 + f2 + f3 + f4

   eF = eMM(foodsum,hfood)

   if (foodsum .gt. 0._fp8) then
      flux1 = rate * eF * f1/foodsum
      flux2 = rate * eF * f2/foodsum
      flux3 = rate * eF * f3/foodsum
      flux4 = rate * eF * f4/foodsum
   else
      flux1 = 0._fp8
      flux2 = 0._fp8
      flux3 = 0._fp8
      flux4 = 0._fp8
   end if

   return

   end subroutine FoodPartition4
!
!EOC
!------------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Combine four components
!
! !DESCRIPTION:
! Combination of four components to a /cnps/ record f.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Combine(f,fc,fn,fp,fs)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: fc,fn,fp,fs
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8), intent(inout) :: f(4)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   f(1) = fc
   f(2) = fn
   f(3) = fp
   f(4) = fs

   return

   end subroutine COMBINE
!
!EOC
!------------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Decomposition of four components
!
! !DESCRIPTION:
! Decomposition of a /cnps/ record f into its four components.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Decomp(f,fc,fn,fp,fs)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: f(4)
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8), intent(inout) :: fc,fn,fp,fs
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   fc = f(1)
   fn = f(2)
   fp = f(3)
   fs = f(4)

   return

   end subroutine Decomp
!
!EOC
!--------------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Combine seven components
!
! !DESCRIPTION:
! Combination of seven components to a /npso/ record f.\\[\baselineskip]
!
! !INTERFACE:
   subroutine CombineN(f,fn,fp,fs,fo2,fco2,fno3,fn2)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: fn,fp,fs,fo2,fco2,fno3,fn2
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8), intent(inout) :: f(7)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   f(1) = fn
   f(2) = fp
   f(3) = fs
   f(4) = fo2
   f(5) = fco2
   f(6) = fno3
   f(7) = fn2

   return

   end subroutine CombineN
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Decomposition of seven components
!
! !DESCRIPTION:
! Decomposition of a /npso/ record f into its seven components.\\[\baselineskip]
!
! !INTERFACE:
   subroutine DecompN(f,fn,fp,fs,fo2,fco2,fno3,fn2)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: f(7)
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8), intent(inout) :: fn,fp,fs,fo2,fco2,fno3,fn2
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   fn = f(1)
   fp = f(2)
   fs = f(3)
   fo2 = f(4)
   fco2 = f(5)
   fno3 = f(6)
   fn2 = f(7)

   return

   end subroutine DecompN
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Diffusion through sea floor
!
! !DESCRIPTION:
! Computes the diffusion of dissolved inorganic chemical components through the sea floor.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine NDiffusion(I,K,jMN)
!
! !USES:
   use benthic_variables
   use pelagic_variables, only: wsiN1p, wsiN3n, wsiN4n, wsiN5s, &
                                wsiO2o, wsiO3c, pdepth



!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: jMN(7)
   integer, intent(in) :: i,k
!
! !LOCAL VARIABLES:
   real(fp8) :: uBP
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   uBP = 1._fp8/pdepth(I)

   SK4n(k) = SK4n(k) - jMN(1)
   wSIN4n(I) = wSIN4n(I) + jMN(1)*uBP
   SK1p(k) = SK1p(k) - jMN(2)
   wSIN1p(I) = wSIN1p(I) + jMN(2)*uBP
   SK5s(k) = SK5s(k) - jMN(3)
   wSIN5s(I) = wSIN5s(I) + jMN(3)*uBP
   SG2o(k) = SG2o(k) - jMN(4)
   wSIO2o(I) = wSIO2o(I) + jMN(4)*uBP
   SG3c(k) = SG3c(k) - jMN(5)
   wSIO3c(I) = wSIO3c(I) + jMN(5)*uBP
   SK3n(k) = SK3n(k) - jMN(6)
   wSIN3n(I) = wSIN3n(I) + jMN(6)*uBP
   SG4n(k) = SG4n(k) - jMN(7)
   return
   end subroutine NDiffusion
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Adjust fixed nutrients
!
! !DESCRIPTION:
! This routine determines the amount of excess c,n or p in the
! source terms and excretes the appropriate amount(s) to Q6 and
! nutrients, so that the fixed nutrient ratio is re-established.\\[\baselineskip]
! !INTERFACE:
   subroutine Adjust_fixed_nutrients ( SXc, SXn, SXp,qn, qp, SKn, SKp, SQc )
!
! !USES:
   use ersem_constants, only: zeroX
!
! !INPUT/OUTPUT PARAMETERS:
! Source terms of fixed quota bio-state.(cnp)
   real(fp8), intent(inout) :: SXc, SXn, SXp, SKn, SKp, SQc
! Fixed quota values
   real(fp8), intent(inout) :: qn, qp
!
! !LOCAL VARIABLES:
! Excess nutrient in bio-state (np)
   real(fp8) :: ExcessN, ExcessP, ExcessC
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ExcessC = max(max(SXc - SXp/qp,SXc - SXn/qn),0._fp8)
!
   if ( ExcessC .gt. ZeroX ) then
      SXc = SXc - ExcessC
      SQc = SQc + ExcessC
   end if
   ExcessN = max(SXn - SXc*qn,0._fp8)
   ExcessP = max(SXp - SXc*qp,0._fp8)
   if ( ExcessN .gt. ZeroX ) then
      SXn = SXn - ExcessN
      SKn = SKn + ExcessN
   end if
   if ( ExcessP .gt. ZeroX ) then
      SXp = SXp - EXcessP
      SKp = SKp + ExcessP
   end if
   return
   end subroutine Adjust_fixed_nutrients
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Day number computation
!
! !DESCRIPTION:
! Compute number of a day in the year from date.\\[\baselineskip]
!
! !INTERFACE:
   subroutine YMD2YD(yyyy,mm,dd,yd)
!
! !INPUT PARAMETERS:
   integer, intent(in) :: yyyy ! Year
   integer, intent(in) :: mm ! Month
   integer, intent(in) :: dd ! Day
!
! !OUTPUT PARAMETERS:
   integer,intent(out) :: yd ! day in the year
!
! !LOCAL VARIABLES:
   integer :: m
   integer,dimension(12),parameter :: &
         dom=(/31,28,31,30,31,30,31,31,30,31,30,31/)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   m=1
   yd=0
   do while (m<mm)
      yd=yd+m*dom(m)
      m=m+1
   end do
   yd=yd+dd
   end subroutine YMD2YD
!
!EOC
!-----------------------------------------------------------------------
   end module general
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
