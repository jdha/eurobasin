

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Restart {\em ERSEM} in netcdf\label{sec:restart}
!
! !DESCRIPTION:
! This module contains the infrastructure to create, write and read
! {\em ERSEM} restart files in netcdf format. To enhance protability
! between different set-ups these restart files can hold only a sub-set of
! the full {\em ERSEM} state, not included state variables will then be
! initialised from namelist or the physical driver.
! \\[\baselineskip]
!
! !INTERFACE:
   module ncdfRestartErsem
!
! !USES:
   use ersem_constants, only: fp8
   use ersem_variables, only: IOProcess, eLogU, eDbgU
   use pelagic_variables, only: n_comp, i_state, ccc, cccstr, ccc0, water, &
                                boxdepth
   use benthic_variables, only: n_compben, i_stateben, ccb, ccbstr, ccb0
   use netcdf
   use allocation_helpers, only: allocerr
   implicit none
! Default all is private
   private
   logical :: putZ
   integer :: zid
!
! !PUBLIC DATA MEMBERS:
   logical, public :: writeErsemRestart=.false.,readErsemRestart=.false., &
                      readRestartGlobal=.false.
   integer,public,parameter :: nfrealkind=kind(NF90_FILL_REAL)
   integer,public :: nrste
   character(50),public :: oErsemRestartName,iErsemRestartName
   real(fp8),pointer,dimension(:,:),public ::ccrst,cbrst
   type,public :: ncdfRVar
      character(20) :: varname
      integer :: ncdfid
   end type ncdfRVar
   type,public :: ncdfRFile
      character(50) :: fn
      integer :: nid,nbuf,isize,jsize,ksize
      type(ncdfRVar),dimension(:),pointer :: ncdfP,ncdfB
   end type ncdfRFile
   type(ncdfRFile),public :: ncdfR
!
! NB:
! NC_FILE constants missing in F90 Library:
! integer,public,parameter :: NF90_MPIIO=8192,NF90_MPIPOSIX=16384
!
! !PUBLIC MEMBER FUNCTIONS:
   public ncdfCreateRestart,ncdfReadRestart,ncdfWriteRestart, &
          ncdfCloseRestart,checkNCDFErsemError,ncdfDumpErsem
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
! !TO DO:
! Document this module. Tidy it up?
!
!EOP
!-----------------------------------------------------------------------
   contains
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Create restart file \label{sec:ncdfCreateRestart}
!
! !DESCRIPTION:
! Create netcdf-ERSEM restart file and object.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine ncdfCreateRestart(fname,title,ncdfOutFile,lon,lat,zflag)
!
! !USES:
   use ersem_variables, only: strpidersem
   use ersem_variables, only: isubstart,jsubstart,ksubstart, &
                              iglobdom,jglobdom,kglobdom
! !INPUT PARAMETERS:
   character(len=*),intent(in) :: fname, title
   type(ncdfRFile),intent(out) :: ncdfOutFile
   real(kind=nfrealkind),optional,intent(in),dimension(:,:) :: lon,lat
   logical,intent(in),optional :: zflag
!
! !LOCAL VARIABLES:
   integer :: iret,lonid,latid,bid,incd,jncd,kncd,bncd,n
   integer :: isize,jsize,ksize,nid
   type(ncdfRVar),dimension(:),pointer :: ncdfRPelagic
   type(ncdfRVar),dimension(:),pointer :: ncdfRBenthic
   character(len=255) :: history,inst
   character(len=50) :: fn
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! set-up dimensions
   isize=size(Water,1)
   jsize=size(Water,2)
   ksize=size(Water,3)
   ! create restart output file:
   fn=trim(fname)//'.'//trim(strpidErsem)//'.nc'
   if (IOProcess) write(eLogU,*) 'Creating new ERSEM restart file: ',fn
   iret = NF90_CREATE(fn,NF90_CLOBBER,nid)
   call checkNCDFErsemError(iret,5,'creating '//fn)
   ! create dimensions:
   iret = NF90_DEF_DIM(nid,'x',isize,incd)
   call checkNCDFErsemError(iret,5,'defining x')
   iret = NF90_DEF_DIM(nid,'y',jsize,jncd)
   call checkNCDFErsemError(iret,5,'defining y')
   iret = NF90_DEF_DIM(nid,'z',ksize,kncd)
   call checkNCDFErsemError(iret,5,'defining z')
   iret = NF90_DEF_DIM(nid,'buffer',NF90_UNLIMITED,bncd)
   call checkNCDFErsemError(iret,5,'defining buffer')
   ! create coordinate variables:
   if (present(lon)) then
      iret = NF90_DEF_VAR(nid,'lon',NF90_REAL,(/incd,jncd/),lonid)
      call checkNCDFErsemError(iret,1,'lon',lonid)
      iret=NF90_PUT_ATT(nid,lonid,'units','degrees_east')
      call checkNCDFErsemError(iret,2,'lon',lonid)
      iret=iret+NF90_PUT_ATT(nid,lonid,'long_name','longitude')
      call checkNCDFErsemError(iret,2,'lon',lonid)
      iret=iret+NF90_PUT_ATT(nid,lonid,'standard_name','longitude')
      call checkNCDFErsemError(iret,2,'lon',lonid)
   end if
   if (present(lat)) then
      iret = NF90_DEF_VAR(nid,'lat',NF90_REAL,(/incd,jncd/),latid)
      call checkNCDFErsemError(iret,1,'lat',latid)
      iret=NF90_PUT_ATT(nid,latid,'units','degrees_north')
      call checkNCDFErsemError(iret,2,'lat',latid)
      iret=iret+NF90_PUT_ATT(nid,lonid,'long_name','latitude')
      call checkNCDFErsemError(iret,2,'lat',latid)
      iret=iret+NF90_PUT_ATT(nid,lonid,'standard_name','latitude')
      call checkNCDFErsemError(iret,2,'lat',latid)
   end if
   if (present(zflag)) then
       putZ=zflag
   else
       putZ=.false.
   end if
   if (putZ) then
      iret = NF90_DEF_VAR(nid,'depth',NF90_REAL,(/incd,jncd,kncd,bncd/),zid)
      call checkNCDFErsemError(iret,1,'depth',zid)
      iret=NF90_PUT_ATT(nid,zid,'units','meters')
      iret=iret+NF90_PUT_ATT(nid,zid,'positive','up')
      iret=iret+NF90_PUT_ATT(nid,zid,'long_name','cell centre depth')
      call checkNCDFErsemError(iret,2,'depth',zid)
   end if
  ! Global attributes:
   iret= NF90_PUT_ATT(nid,NF90_GLOBAL,'title',title)
   history = 'Restart file created by PML-ERSEM'
   iret=iret+NF90_PUT_ATT(nid,NF90_GLOBAL,'history',history)
   inst = "Plymouth Marine Laboratory - UK"
   iret=iret+NF90_PUT_ATT(nid,NF90_GLOBAL,'institution',inst)
   iret=iret+NF90_PUT_ATT(nid,NF90_GLOBAL,'source','ERSEM model')
   call checkNCDFErsemError(iret,5,'setting global attributes')
   ! Only for parallel ncdfErsem
   iret = NF90_PUT_ATT(nid,NF90_GLOBAL,'Global Dimensions',(/iglobdom,jglobdom,kglobdom/))
   call checkNCDFErsemError(iret,5,'setting global dimensions')
   iret = NF90_PUT_ATT(nid,NF90_GLOBAL,'Global Position',(/isubstart,jsubstart,ksubstart/))
   call checkNCDFErsemError(iret,5,'setting global position')
   ! define state variables:
   allocate(ncdfRPelagic(I_STATE),stat=iret)
   call allocerr('ncdfRBethic',I_STATE,iret)
   allocate(ncdfRBenthic(I_STATEBEN),stat=iret)
   call allocerr('ncdfRBenthic',I_STATEBEN,iret)
   do n=1,I_STATE
      ncdfRPelagic(n) = ncdfRVar(cccstr(n),0)
      iret = NF90_DEF_VAR(nid,trim(ncdfRPelagic(n)%varname),NF90_DOUBLE &
               ,(/incd,jncd,kncd,bncd/),ncdfRPelagic(n)%ncdfid)
      call checkNCDFErsemError(iret,1,ncdfRPelagic(n)%varname,ncdfRPelagic(n)%ncdfid)
      iret = NF90_PUT_ATT(nid,ncdfRPelagic(n)%ncdfid &
           ,'coordindates','lon lat depth')
      call checkNCDFErsemError(iret,2,ncdfRPelagic(n)%varname,ncdfRPelagic(n)%ncdfid)
   end do
   do n=1,I_STATEBEN
      ncdfRBenthic(n) = ncdfRVar(ccbstr(n),0)
      iret = NF90_DEF_VAR(nid,trim(ncdfRBenthic(n)%varname),NF90_DOUBLE &
               ,(/incd,jncd,bncd/),ncdfRBenthic(n)%ncdfid)
      call checkNCDFErsemError(iret,1,ncdfRBenthic(n)%varname,ncdfRBenthic(n)%ncdfid)
      iret = NF90_PUT_ATT(nid,ncdfRBenthic(n)%ncdfid &
           ,'coordindates','lon lat')
      call checkNCDFErsemError(iret,2,ncdfRBenthic(n)%varname,ncdfRBenthic(n)%ncdfid)
   end do
   iret = NF90_ENDDEF(nid)
   call checkNCDFErsemError(iret,5,'leaving define mode')
   ncdfOutFile = ncdfRFile(fn,nid,0,isize,jsize,ksize,ncdfRPelagic,ncdfRBenthic)
   ! Fill coordinate variables if present:
   if (present(lon)) then
      iret = NF90_PUT_VAR(nid,lonid,lon)
      call checkNCDFErsemError(iret,3,'lon',lonid)
   end if
   if (present(lat)) then
      iret = NF90_PUT_VAR(nid,latid,lat)
      call checkNCDFErsemError(iret,3,'lon',latid)
   end if
   end subroutine ncdfCreateRestart
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Write restart file \label{sec:ncdfWriteRestart}
!
! !DESCRIPTION:
! Write netcdf-ERSEM restart file.
! \\[\baselineskip]
!
! !INTERFACE:
  subroutine ncdfWriteRestart(ncdfOutFile,ccrst,cbrst)
! !INPUT PARAMETERS:
   type(ncdfRFile), intent(inout) :: ncdfOutFile
   real(fp8),pointer,dimension(:,:) :: ccrst,cbrst
!
! !LOCAL VARIABLES:
   integer :: iret,n
   integer :: start(4),edges(4)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) 'Writing data into restart file ',ncdfOutFile%fn
   ncdfOutFile%nbuf=ncdfOutFile%nbuf+1
   ! Pelagic states:
   start(1) = 1
   start(2) = 1
   start(3) = 1
   start(4) = ncdfOutFile%nbuf
   edges(1) = ncdfOutFile%isize
   edges(2) = ncdfOutFile%jsize
   edges(3) = ncdfOutFile%ksize
   edges(4) = 1
   if (putZ) then
      iret = NF90_PUT_VAR(ncdfOutFile%nid,zid,REAL(BoxDepth,nfrealkind),start,edges)
      call checkNCDFErsemError(iret,3,'z',zid)
   end if
   do n=1,I_STATE
      iret = NF90_PUT_VAR(ncdfOutFile%nid,ncdfoutFile%ncdfP(n)%ncdfid, &
      unpack(ccrst(1:n_comp,n),Water(:,:,:),NF90_FILL_DOUBLE), &
             start,edges)
   end do
   ! Benthic states:
   start(1) = 1
   start(2) = 1
   start(3) = ncdfOutFile%nbuf
   edges(1) = ncdfOutFile%isize
   edges(2) = ncdfOutFile%jsize
   edges(3) = 1
   do n=1,I_STATEBEN
      iret = NF90_PUT_VAR(ncdfOutFile%nid,ncdfOutFile%ncdfB(n)%ncdfid, &
      unpack(cbrst(1:n_compben,n),Water(:,:,1),NF90_FILL_DOUBLE), &
             start(1:3),edges(1:3))
   end do
   iret = NF90_SYNC(ncdfOutFile%nid)
   call checkNCDFErsemError(iret,5,'syncing '//ncdfOutFile%fn)
  end subroutine ncdfWriteRestart
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Close restart file \label{sec:ncdfCloseRestart}
!
! !DESCRIPTION:
! Close netcdf-ERSEM restart file.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine ncdfCloseRestart(ncdfOutFile)
!
! !USES:
!
! !INPUT PARAMETERS:
   type(ncdfRFile), intent(in) :: ncdfOutFile
!
! !LOCAL VARIABLES:
   integer :: iret
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! closing restart output file:
   if (IOProcess) write(eLogU,*) 'Closing ',ncdfOutFile%fn,'...'
   iret = NF90_CLOSE(ncdfOutFile%nid)
   call checkNCDFErsemError(iret,ncdfOutFile%nid)
   end subroutine ncdfCloseRestart
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Read restart file \label{sec:ncdfReadRestart}
!
! !DESCRIPTION:
! Read netcdf-ERSEM restart file.
! \\[\baselineskip]
!
! !INTERFACE:
  subroutine ncdfReadRestart(fn,nbuf,ccrst,cbrst)
!
! !USES:
   use ersem_variables, only: strpidersem
   use ersem_variables, only: isubstart,jsubstart,ksubstart, &
                              iglobdom,jglobdom,kglobdom
! !INPUT PARAMETERS:
   character(len=*),intent(in) :: fn
   integer :: nbuf
   real(fp8),pointer,dimension(:,:) :: ccrst
   real(fp8),pointer,dimension(:,:),optional :: cbrst
!
! !LOCAL VARIABLES:
   real(fp8),allocatable,dimension(:,:,:) :: buffer3D
   character(50) :: fname
   integer :: nid,varid,n,m,iret,isize,jsize,ksize
   integer,dimension(4) :: start,edges
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   if (readRestartGlobal) then
      fname=TRIM(fn)//'.nc'
   else
      fname=TRIM(fn)//'.'//trim(strpidErsem)//'.nc'
   end if
   write(eDbgU,*) "Reading ERSEM restart file ",fname," on " &
                     ,trim(strpidErsem),"..."
   iret = NF90_OPEN(TRIM(fname),NF90_NOWRITE,nid)
   call checkNCDFErsemError(iret,5,'opening '//fname)
   ! set-up dimensions
   isize=size(Water,1)
   jsize=size(Water,2)
   ksize=size(Water,3)
   allocate(buffer3D(isize,jsize,ksize),stat=iret)
   call allocerr('3D buffer',isize,jsize,ksize,0,iret)
   if (readRestartGlobal) then
      start(1) = isubstart
      start(2) = jsubstart
      start(3) = ksubstart
      isize=min(isize,iglobdom-isubstart+1)
      jsize=min(jsize,jglobdom-jsubstart+1)
      ksize=min(ksize,kglobdom-ksubstart+1)
   else
      start(1:3) = 1
   end if
   start(4) = nbuf
   edges(1) = isize
   edges(2) = jsize
   edges(3) = ksize
   edges(4) = 1
   do m=1,I_STATE
      iret = NF90_INQ_VARID(nid,TRIM(cccstr(m)(1:4)),varid)
      if (iret.eq.0) then
         iret=NF90_GET_VAR(nid,varid,buffer3D,start,edges)
         call checkNCDFErsemError(iret,5,'in reading in '//TRIM(cccstr(m)))
         ccrst(:,m)=pack(buffer3D,Water)
         where (ccrst(:,m)<ccc0(m))
            ccrst(:,m)=ccc0(m)
         end where
      else if (iret.NE.NF90_ENOTVAR) then
         call checkNCDFErsemError(iret,5,'in inquiring '//TRIM(cccstr(m)))
      end if
   end do
   if (present(cbrst)) then
      if (readRestartGlobal) then
         start(1) = isubstart
         start(2) = jsubstart
      else
         start(1:2) = 1
      end if
      start(3) = nbuf
      edges(1) = isize
      edges(2) = jsize
      edges(3) = 1
      do m=1,I_STATEBEN
         iret = NF90_INQ_VARID(nid,TRIM(ccbstr(m)),varid)
         if (iret.eq.0) then
            iret=NF90_GET_VAR(nid,varid,buffer3D(:,:,1),start(1:3),edges(1:3))
            cbrst(:,m)=pack(buffer3D(:,:,1),Water(:,:,1))
            call checkNCDFErsemError(iret,5,'in reading in '//TRIM(ccbstr(m)))
            where (ccrst(:,m)<ccc0(m))
               cbrst(:,m)=ccb0(m)
            end where
         else if (iret.NE.NF90_ENOTVAR) then
            call checkNCDFErsemError(iret,5,'in inquiring '//TRIM(ccbstr(m)))
         end if
      end do
   end if
  iret = NF90_CLOSE(nid)
  call checkNCDFErsemError(iret,nid)
  end subroutine ncdfReadRestart
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Dump current state \label{sec:ncdfDumpErsem}
!
! !DESCRIPTION:
! Write a netcdf-ERSEM dump file.
! (Creates, writes and closes, for use in catching errors to
! dump current state.)
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine ncdfDumpErsem
!
! !USES:
   use ersem_variables, only: strpidersem
!
! !LOCAL VARIABLES:
   type(ncdfRFile) :: ncdfD
   integer,save :: nd=1
   character(len=5) :: strnd
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   write(strnd,'(i0)') nd
   call ncdfCreateRestart("dump."//TRIM(strnd)//"."//TRIM(strpidErsem)// &
      ".nc","NCDF ERSEM dump file",ncdfD)
   ccrst=>ccc
   cbrst=>ccb
   call ncdfWriteRestart(ncdfD,ccrst,cbrst)
   call ncdfCloseRestart(ncdfD)
   nd = nd + 1
  end subroutine ncdfDumpErsem
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Check netcdf error \label{sec:checkNCDFErsemError}
!
! !DESCRIPTION:
! Collective routine for providing information on errors.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine checkNCDFErsemError(iret,errflag,varid,varno)
!
! !INPUT PARAMETERS:
   integer,intent(in) :: errflag,iret
   integer,intent(in),optional :: varno
   character(*),optional,intent(in) :: varid
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   if (iret .ne. NF90_NOERR) then
      write(eDbgU,*) 'NCDF Error!!!'
      write(eDbgU,*) nf90_strerror(iret)
      select case (errflag)
         case(1)
            write(eDbgU,*) 'in defining variable ',varid,' number ',varno,'!'
         case(2)
            write(eDbgU,*) 'in setting attributes to variable ',varid,'!'
         case(3)
            write(eDbgU,*) 'in writing variable ',varid,' number ',varno,'!'
         case(4)
            write(eDbgU,*) 'in closing netcdf file',trim(varid),'!'
         case(5)
            write(eDbgU,*) 'in ',trim(varid),'!'
         case default
      end select
      call flush(eDbgU)
      call flush(eLogU)
      call MPI_abort(iret)
   end if
   end subroutine checkNCDFErsemError
!
!EOC
!-----------------------------------------------------------------------
  end module ncdfRestartErsem
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
