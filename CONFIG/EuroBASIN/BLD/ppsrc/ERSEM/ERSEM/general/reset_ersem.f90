

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Reset
!
! !DESCRIPTION:
! Contains routine to reset {\em ERSEM} after each interation.
! \\[\baselineskip]
!
! !INTERFACE:
   module reset
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public reset_ersem
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Reset {\em ERSEM}
!
! !DESCRIPTION:
! Subroutine to reset the source terms and fluxes to zero in the model.
! Needs to be called after ersem\_loop, but before budget.\\[\baselineskip]
!
! !INTERFACE:
   subroutine reset_ersem
!
! !USES:
   use pelagic_variables
   use benthic_variables

   implicit none
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   SCCC= 0._fp8
   WSICCC=0._fp8
   WSOCCC=0._fp8
   SDCCC=0._fp8

   SCCb= 0._fp8
   WSICCb=0._fp8
   WSOCCb=0._fp8
   SDCCb=0._fp8

   fO3L2c=0._fp8


   fPXZXc=0._fp8
   fBXZXc=0._fp8
   fRXZXc=0._fp8
   netB=0._fp8
   ZResp=0._fp8
   fBXRDc=0._fp8
   BResp=0._fp8
   nitrN=0._fp8
   fPXRPc=0._fp8
   fPXRDc=0._fp8
   fZXRPc=0._fp8
   fZXRDc=0._fp8
   fRDBXc=0._fp8

   fRPBXc=0._fp8



   fN1PXp=0._fp8
   fN3PXn=0._fp8
   fN4PXn=0._fp8
   fN5PXs=0._fp8
   fN5PXs=0._fp8
   fBXN1p=0._fp8
   fBXNIn=0._fp8
   fRDN1p=0._fp8
   fRDNIn=0._fp8

   fN7PXf=0._fp8



   return

   end subroutine reset_ersem
!
!EOC
!-----------------------------------------------------------------------

   end module reset

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
