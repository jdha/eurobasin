

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Netcdf output for {\em ERSEM} \label{sec:ncdfersem}
!
! !DESCRIPTION:
! This module provides routines for saving the {\em ERSEM} results using
! the netcdf format.
! Note, that the 1D {\em ERSEM} state variable has to be unpacked in the
! order of lon, lat, zlev first to last index!
! In addition, this module requires, that the total dimension of the
! {\em ERSEM} state \texttt{N\_COMP} is equal to the wet points in
! lon*lat*nlev and \texttt{N\_COMPBEN} is equal to the wet points in
! lon*lat !
! \\[\baselineskip]
!
! !INTERFACE:
   module ncdfersem
!






! !USES:
   use pelagic_variables
   use benthic_variables
   use ersem_variables, only: ncdfInstOut, ncdfDailyOut, &
                              ncdfWeeklyOut, ncdfMonthlyOut
   use bacteria
   use budget
   use pelagic_zone
   use netcdf
   use ncdfRestartErsem, ONLY: nfrealkind,checkNCDFErsemError
   use allocation_helpers, ONLY: allocerr
!
   implicit none
!
! Default all is private.
   private
!
! !PUBLIC DATA MEMBERS:
   integer, public :: nceid,nceidd,nceidw,nceidm
!
! !PUBLIC MEMBER FUNCTIONS:
   public init_ncdfersem, write_to_ncdfersem, close_ncdfersem
!
! !PRIVATE DATA MEMBERS:
! dimension ids
   integer :: i_dim,j_dim,k_dim,nf_dim
   integer :: i_len,j_len,k_len
   integer, parameter :: nf_len=4,nzf_len=2
   integer :: time_dim
! variable ids
   integer :: lon_id,lat_id,bath_id
   integer :: ncdf_time_unit
   character(len=128) :: ncdf_time_str

   type, public :: ncdfvar
      character(20) :: varname
      integer :: ncdfid
      character(20) :: ncdfunit
      character(50) :: ncdfdescription
      character(50) :: ncdfstandard
      real(fp8), dimension(:), pointer :: ersemvar
   end type ncdfvar

   type, public :: ncdffile
      character(50) :: fn
      type(ncdfvar), dimension(:), pointer :: nv0D
      type(ncdfvar), dimension(:), pointer :: nv2D
      type(ncdfvar), dimension(:), pointer :: nv3D
      integer :: time_dim,nzf_dim,n0D,n2D,n3D,nid,time_id,tlev
   end type ncdffile

   type(ncdffile), public :: ncdfI,ncdfD,ncdfW,ncdfM
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
! !TO DO:
! Document this module.
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Init netcdf output \label{sec:initncdfersem}
!
! !DESCRIPTION:
! Opens and creates the netcdf file - initialize all dimensions and
! variables for the core {\em ERSEM} model.
! Call this once in the initialisation phase of the model.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine init_ncdfersem(fname,title,ilen,jlen,nlev, &
              lon,lat,lonbnd,latbnd, &
              start_time,time_unit)
!
! !USES:
   use ersem_variables, only: strpidersem, IOProcess, eLogU, eDbgU
!
! time_unit:
! 0 : seconds
! 1 : minutes
! 2 : hours
!
! !INPUT PARAMETERS:
   character(len=*), intent(in) :: title,start_time,fname
   integer, intent(in) :: nlev,time_unit,ilen,jlen
   real(kind=nfrealkind), intent(in), dimension(:,:), target :: lon,lat
   real(kind=nfrealkind), intent(in), dimension(:,:), target :: lonbnd,latbnd
!
! !LOCAL VARIABLES:
   integer :: iret
   character(len=50) :: fn
   real(kind=nfrealkind), pointer, dimension(:,:) :: lonp,latp,lonbndp,latbndp
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Set here your total number of pelagic and benthic output parameters:

   ncdfI%n0D=0
   ncdfD%n0D=0
   ncdfW%n0D=0
   ncdfM%n0D=0
   ncdfI%n2D=0
   ncdfD%n2D=0
   ncdfW%n2D=0
   ncdfM%n2D=0
   ncdfI%n3D=0
   ncdfD%n3D=0
   ncdfW%n3D=0
   ncdfM%n3D=0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!

   fn=trim(fname)//'.'//trim(strpidErsem)//'.nc'



   i_len=ilen
   j_len=jlen
   k_len=nlev
   ncdf_time_unit = time_unit
   lonp => lon
   latp => lat
   lonbndp => lonbnd
   latbndp => latbnd
   select case (ncdf_time_unit)
      case(0) ! seconds
         write(ncdf_time_str,100) 'seconds',trim(start_time)
      case(1) ! minutes
         write(ncdf_time_str,100) 'minutes',trim(start_time)
      case(2) ! hours
         write(ncdf_time_str,100) 'hours',trim(start_time)
      case default
         write(ncdf_time_str,100) 'seconds',trim(start_time)
   end select
100 format(A,' since ',A)
!
   if (IOProcess) write(eLogU,*) 'Creating ncdfersem files ',trim(fn),': ',trim(title),' ...'
!
   if (ncdfInstOut) then
      allocate(ncdfI%nv0D(ncdfI%n0D),stat=iret)
      call allocerr('ncdfI%nv0D',ncdfI%n0D,iret)
      allocate(ncdfI%nv2D(ncdfI%n2D),stat=iret)
      call allocerr('ncdfI%nv2D',ncdfI%n2D,iret)
      allocate(ncdfI%nv3D(ncdfI%n3D),stat=iret)
      call allocerr('ncdfI%nv3D',ncdfI%n3D,iret)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your scalar output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e.: Modify n0Ddim to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your 2D output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e.: Modify n2Ddim to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your 3D output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e. Modify n3Ddim to the required value) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ncdfI%fn=trim(fn)
      call ncdfErsemHeader(ncdfI,title,lonp,latp,lonbndp,latbndp)
   end if

   if (ncdfDailyOut) then
      allocate(ncdfD%nv0D(ncdfD%n0D),stat=iret)
      call allocerr('ncdfD%nv0D',ncdfD%n0D,iret)
      allocate(ncdfD%nv2D(ncdfD%n2D),stat=iret)
      call allocerr('ncdfD%nv2D',ncdfD%n2D,iret)
      allocate(ncdfD%nv3D(ncdfD%n3D),stat=iret)
      call allocerr('ncdfD%nv3D',ncdfD%n3D,iret)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your daily scalar output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e.: Modify ncdfD%n0D to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your daily 2D output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e.: Modify ncdfD%n2D to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your daily 3D output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e. Modify ncdfD%n3D to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ncdfD%fn='Daily.'//trim(fn)
      call ncdfErsemHeader(ncdfD,trim(title)//" - Daily Budgets",lonp,latp,lonbndp,latbndp)
   end if

   if (ncdfWeeklyOut) then
      allocate(ncdfW%nv0D(ncdfW%n0D),stat=iret)
      call allocerr('ncdfW%nv0D',ncdfW%n0D,iret)
      allocate(ncdfW%nv2D(ncdfW%n2D),stat=iret)
      call allocerr('ncdfW%nv2D',ncdfW%n2D,iret)
      allocate(ncdfW%nv3D(ncdfW%n3D),stat=iret)
      call allocerr('ncdfW%nv3D',ncdfW%n3D,iret)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your weekly scalar output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e.: Modify ncdfW%n0D to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your weekly 2D output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e.: Modify ncdfW%n2D to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your weekly 3D output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e. Modify ncdfW%n3D to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ncdfW%fn='Weekly.'//trim(fn)
      call ncdfErsemHeader(ncdfW,trim(title)//" - Weekly Budgets",lonp,latp,lonbndp,latbndp)
   end if

   if (ncdfMonthlyOut) then
      allocate(ncdfM%nv0D(ncdfM%n0D),stat=iret)
      call allocerr('ncdfM%nv0D',ncdfM%n0D,iret)
      allocate(ncdfM%nv2D(ncdfM%n2D),stat=iret)
      call allocerr('ncdfM%nv2D',ncdfM%n2D,iret)
      allocate(ncdfM%nv3D(ncdfM%n3D),stat=iret)
      call allocerr('ncdfM%nv3D',ncdfM%n3D,iret)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your monthly scalar output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e.: Modify ncdfM%n0D to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your monthly 2D output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e.: Modify ncdfM%n2D to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Define here your monthly 3D output variables !!
!! Must total the number given above in the module declaration! !!
!! (i.e. Modify ncdfM%n3D to the required value!) !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ncdfM%fn='Monthly.'//trim(fn)
      call ncdfErsemHeader(ncdfM,trim(title)//" - Monthly Budgets",lonp,latp,lonbndp,latbndp)
   end if

   return

   end subroutine init_ncdfersem
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Define netcdf header \label{sec:ncdfErsemHeader}
!
! !DESCRIPTION:
! Writes the header to netcdf-ERSEM output files.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine ncdfErsemHeader(ncf,title,lon,lat,lonbnd,latbnd)
!
! !USES:

   use ersem_variables, only: isubstart,jsubstart,ksubstart, &
                              iglobdom,jglobdom,kglobdom

!
! !INPUT PARAMETERS:
   real(kind=nfrealkind),pointer,dimension(:,:) :: lon,lat,lonbnd,latbnd
   character(len=*), intent(in) :: title
!
! !INPUT/OUTPUT PARAMETERS:
   type(ncdffile) :: ncf
!
! !LOCAL VARIABLES:
   integer :: iret,lonbnd_id,latbnd_id,n
   character(len=128) :: history,inst
   real(kind=nfrealkind), dimension(i_len,j_len) :: dummy2D
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   iret = NF90_CREATE(ncf%fn,NF90_CLOBBER,ncf%nid)
   call checkNCDFErsemError(iret,5,'creating '//ncf%fn)

   ! Define dimensions
   iret = NF90_DEF_DIM(ncf%nid, 'n_faces', nf_len, nf_dim)
   call checkNCDFErsemError(iret,5,'defining n_faces')

   iret = NF90_DEF_DIM(ncf%nid, 'n_zfaces', nzf_len, ncf%nzf_dim)
   call checkNCDFErsemError(iret,5,'defining n_zfaces')

   iret = NF90_DEF_DIM(ncf%nid, 'x', i_len, i_dim)
   call checkNCDFErsemError(iret,5,'defining x')

   iret = NF90_DEF_DIM(ncf%nid, 'y', j_len, j_dim)
   call checkNCDFErsemError(iret,5,'defining y')

   iret = NF90_DEF_DIM(ncf%nid, 'z', k_len, k_dim)
   call checkNCDFErsemError(iret,5,'defining z')

   iret = NF90_DEF_DIM(ncf%nid, 'time', NF90_UNLIMITED, time_dim)
   call checkNCDFErsemError(iret,5,'defining time')
   ncf%tlev=1

   ! Define coordinates
   iret = NF90_DEF_VAR(ncf%nid,'lon',NF90_REAL,(/i_dim,j_dim/),lon_id)
   call checkNCDFErsemError(iret,1,'lon',lon_id)
   iret = NF90_DEF_VAR(ncf%nid,'lonbnd',NF90_REAL,(/nf_dim,i_dim,j_dim/),lonbnd_id)
   call checkNCDFErsemError(iret,1,'lonbnd',lonbnd_id)

   iret = NF90_DEF_VAR(ncf%nid,'lat',NF90_REAL,(/i_dim,j_dim/),lat_id)
   call checkNCDFErsemError(iret,1,'lat',lat_id)
   iret = NF90_DEF_VAR(ncf%nid,'latbnd',NF90_REAL,(/nf_dim,i_dim,j_dim/),latbnd_id)
   call checkNCDFErsemError(iret,1,'latbnd',latbnd_id)

   iret = NF90_DEF_VAR(ncf%nid,'bathymetry',NF90_REAL,(/i_dim,j_dim/),bath_id)
   call checkNCDFErsemError(iret,1,'bathymetry',bath_id)

   iret = NF90_DEF_VAR(ncf%nid,'time',NF90_INT,time_dim,ncf%time_id)
   call checkNCDFErsemError(iret,1,'time',ncf%time_id)

   ! Define variables

   !type: t
   do n=1,ncf%n0D
      iret = NF90_DEF_VAR(ncf%nid,trim(ncf%nv0D(n)%varname),NF90_REAL &
            ,time_dim,ncf%nv0D(n)%ncdfid)
      call checkNCDFErsemError(iret,1,ncf%nv0D(n)%varname,ncf%nv0D(n)%ncdfid)
      iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv0D(n)%ncdfid &
        ,'units',ncf%nv0D(n)%ncdfunit)
      iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv0D(n)%ncdfid &
        ,'long_name',ncf%nv0D(n)%ncdfdescription)
      if (ncf%nv0D(n)%ncdfstandard.ne.'None') &
         iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv0D(n)%ncdfid &
          ,'standard_name',ncf%nv0D(n)%ncdfstandard)
      call checkNCDFErsemError(iret,2,ncf%nv0D(n)%varname,ncf%nv0D(n)%ncdfid)
   end do

   ! type: x,y,t
   do n=1,ncf%n2D
      iret = NF90_DEF_VAR(ncf%nid,trim(ncf%nv2D(n)%varname),NF90_REAL &
            ,(/i_dim,j_dim,time_dim/),ncf%nv2D(n)%ncdfid)
      call checkNCDFErsemError(iret,1,ncf%nv2D(n)%varname,ncf%nv2D(n)%ncdfid)
      iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv2D(n)%ncdfid &
        ,'units',ncf%nv2D(n)%ncdfunit)
      iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv2D(n)%ncdfid &
        ,'long_name',ncf%nv2D(n)%ncdfdescription)
      iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv2D(n)%ncdfid &
        ,'coordindates','lon lat')
      if (ncf%nv2D(n)%ncdfstandard.ne.'None') &
         iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv2D(n)%ncdfid &
          ,'standard_name',ncf%nv2D(n)%ncdfstandard)
      call checkNCDFErsemError(iret,2,ncf%nv2D(n)%varname,ncf%nv2D(n)%ncdfid)
   end do

   ! type: x,y,z,t
   do n=1,ncf%n3D
      iret = NF90_DEF_VAR(ncf%nid,trim(ncf%nv3D(n)%varname),NF90_REAL &
            ,(/i_dim,j_dim,k_dim,time_dim/),ncf%nv3D(n)%ncdfid)
      call checkNCDFErsemError(iret,1,ncf%nv3D(n)%varname,ncf%nv3D(n)%ncdfid)
      iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv3D(n)%ncdfid &
        ,'units',ncf%nv3D(n)%ncdfunit)
      iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv3D(n)%ncdfid &
        ,'long_name',ncf%nv3D(n)%ncdfdescription)
      iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv3D(n)%ncdfid &
        ,'coordindates','lon lat depth')
      if (ncf%nv3D(n)%ncdfstandard.ne.'None') &
         iret = iret + NF90_PUT_ATT(ncf%nid,ncf%nv3D(n)%ncdfid &
          ,'standard_name',ncf%nv3D(n)%ncdfstandard)
      call checkNCDFErsemError(iret,2,ncf%nv3D(n)%varname,ncf%nv3D(n)%ncdfid)
   end do

   ! Assign attributes

   ! Coordinates
   iret=NF90_PUT_ATT(ncf%nid,lon_id,'units','degrees_east')
   iret=iret+NF90_PUT_ATT(ncf%nid,lon_id,'long_name','longitude')
   iret=iret+NF90_PUT_ATT(ncf%nid,lon_id,'standard_name','longitude')
   iret=iret+NF90_PUT_ATT(ncf%nid,lon_id,'bounds','lonbnd')
   call checkNCDFErsemError(iret,2,'lon',lon_id)

   iret=NF90_PUT_ATT(ncf%nid,lat_id,'units','degrees_north')
   iret=iret+NF90_PUT_ATT(ncf%nid,lat_id,'long_name','latitude')
   iret=iret+NF90_PUT_ATT(ncf%nid,lat_id,'standard_name','latitude')
   iret=iret+NF90_PUT_ATT(ncf%nid,lat_id,'bounds','latbnd')
   call checkNCDFErsemError(iret,2,'lat',lat_id)

   ! Bathymetry
   iret=NF90_PUT_ATT(ncf%nid,bath_id,'units','meters')
   iret=iret+NF90_PUT_ATT(ncf%nid,bath_id,'long_name','bathymetry')
   iret=iret+NF90_PUT_ATT(ncf%nid,bath_id,'coords','lon lat')
   call checkNCDFErsemError(iret,2,'bathymetry',bath_id)

   ! Time
   iret=NF90_PUT_ATT(ncf%nid,ncf%time_id,'units',trim(ncdf_time_str))
   iret=iret+NF90_PUT_ATT(ncf%nid,ncf%time_id,'long_name','time')
   iret=iret+NF90_PUT_ATT(ncf%nid,ncf%time_id,'calendar','standard')
   call checkNCDFErsemError(iret,2,'time',ncf%time_id)

   ! Global attributes
   iret= NF90_PUT_ATT(ncf%nid,NF90_GLOBAL,'title',title)
   history = 'Created by PML-ERSEM v. '//'2009'
   iret=iret+NF90_PUT_ATT(ncf%nid,NF90_GLOBAL,'history',history)
   iret=iret+NF90_PUT_ATT(ncf%nid,NF90_GLOBAL,'Conventions','CF-1.4')
   inst = "Plymouth Marine Laboratory - UK"
   iret=iret+NF90_PUT_ATT(ncf%nid,NF90_GLOBAL,'institution',inst)
   iret=iret+NF90_PUT_ATT(ncf%nid,NF90_GLOBAL,'source','PML-ERSEM model')
   call checkNCDFErsemError(iret,5,'setting global attributes')


   ! Only for parallel ncdfErsem
   iret = NF90_PUT_ATT(ncf%nid,NF90_GLOBAL,'Global Dimensions',(/iglobdom,jglobdom,kglobdom/))
   call checkNCDFErsemError(iret,5,'setting global dimensions')
   iret = NF90_PUT_ATT(ncf%nid,NF90_GLOBAL,'Global Position',(/isubstart,jsubstart,ksubstart/))
   call checkNCDFErsemError(iret,5,'setting global position')


   ! Leave define mode
   iret = NF90_ENDDEF(ncf%nid)
   call checkNCDFErsemError(iret,5,'leaving define mode')

   ! Save latitude and logitude
   iret = NF90_PUT_VAR(ncf%nid, lon_id, lon)
   call checkNCDFErsemError(iret,3,'lon',lon_id)
   iret = NF90_PUT_VAR(ncf%nid, lonbnd_id, lonbnd(1:i_len,1:j_len) &
        ,(/1,1,1/),(/1,i_len,j_len/) )
   call checkNCDFErsemError(iret,3,'lonbnd',lonbnd_id)
   iret = NF90_PUT_VAR(ncf%nid, lonbnd_id, lonbnd(2:i_len+1,1:j_len) &
        ,(/2,1,1/),(/1,i_len,j_len/) )
   call checkNCDFErsemError(iret,3,'lonbnd',lonbnd_id)
   iret = NF90_PUT_VAR(ncf%nid, lonbnd_id, lonbnd(2:i_len+1,2:j_len+1) &
        ,(/3,1,1/),(/1,i_len,j_len/) )
   call checkNCDFErsemError(iret,3,'lonbnd',lonbnd_id)
   iret = NF90_PUT_VAR(ncf%nid, lonbnd_id, lonbnd(1:i_len,2:j_len+1) &
        ,(/4,1,1/),(/1,i_len,j_len/) )
   call checkNCDFErsemError(iret,3,'lonbnd',lonbnd_id)

   iret = NF90_PUT_VAR(ncf%nid, lat_id, lat)
   call checkNCDFErsemError(iret,3,'lat',lat_id)
   iret = NF90_PUT_VAR(ncf%nid, latbnd_id, latbnd(1:i_len,1:j_len) &
        ,(/1,1,1/),(/1,i_len,j_len/) )
   call checkNCDFErsemError(iret,3,'latbnd',latbnd_id)
   iret = NF90_PUT_VAR(ncf%nid, latbnd_id, latbnd(2:i_len+1,1:j_len) &
        ,(/2,1,1/),(/1,i_len,j_len/) )
   call checkNCDFErsemError(iret,3,'latbnd',latbnd_id)
   iret = NF90_PUT_VAR(ncf%nid, latbnd_id, latbnd(2:i_len+1,2:j_len+1) &
        ,(/3,1,1/),(/1,i_len,j_len/) )
   call checkNCDFErsemError(iret,3,'latbnd',latbnd_id)
     iret = NF90_PUT_VAR(ncf%nid, latbnd_id, latbnd(1:i_len,2:j_len+1) &
          ,(/4,1,1/),(/1,i_len,j_len/) )
   call checkNCDFErsemError(iret,3,'latbnd',latbnd_id)

   ! Wave bathymetry
   where (Seafloor>0)
      dummy2D=REAL(Bathymetry,nfrealkind)
   else where
      dummy2D=NF90_FILL_REAL
   end where
   iret = NF90_PUT_VAR(ncf%nid, bath_id, dummy2D)
   call checkNCDFErsemError(iret,3,'bathymetry',bath_id)

   iret = NF90_SYNC(ncf%nid)
   call checkNCDFErsemError(iret,5,'syncing '//ncf%fn)

   end subroutine ncdfErsemHeader
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Write netcdf output \label{sec:writetoncdfersem}
!
! !DESCRIPTION:
! Write {\em ERSEM} variables to the netcdf-ERSEM file.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine write_to_ncdfersem(ncf,secs)
!
! !USES:
!
! !INPUT PARAMETERS:
   integer, intent(in) :: secs
!
! !INPUT/OUTPUT PARAMETERS:
   type(ncdffile) :: ncf
!
! !LOCAL VARIABLES:
   integer :: iret,i
   integer :: start(4),edges(4)
   integer :: ncdftime
   real(kind=nfrealkind) :: dum(1:max(1,n_comp)) ! r4-dummy to be filled with ersemstate
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   start(1) = ncf%tlev
   edges(1) = 1
   select case (ncdf_time_unit)
      case(0) ! seconds
         ncdftime = secs
      case(1) ! minutes
         ncdftime = secs/60
      case(2) ! hours
         ncdftime = secs/3600
      case default
         ncdftime = secs
   end select
   dum(1)=ncdftime
   iret = NF90_PUT_VAR(ncf%nid,ncf%time_id,dum(1:1),start=start(1:1))
   ! The weird vector slice notation for the scalar values to be put
   ! in the time series is due to the incapacity of NF90_PUT_VAR to treat
   ! scalar data to be written, they have to be 'masked' as vector slice of
   ! size 1. The netcdf manual says the function can handle scalars, but it
   ! can't. At least not when using the start argument. (momm)
   call checkNCDFErsemError(iret,3,'time',ncf%time_id)

! Time varying 0D data : t
   start(1) = ncf%tlev
   edges(1) = 1
!
   do i=1,ncf%n0D
      dum(1)=REAL(ncf%nv0D(i)%ersemvar(1),kind(dum(1))) ! converts r8 ot r4
      iret = NF90_PUT_VAR(ncf%nid,ncf%nv0D(i)%ncdfid,dum(1),start(1:1))
      call checkNCDFErsemError(iret,3,ncf%nv0D(i)%varname,ncf%nv0D(i)%ncdfid)
   end do

! Time varying 2D data : x,y,t
   start(1) = 1
   start(2) = 1
   start(3) = ncf%tlev
   edges(1) = i_len
   edges(2) = j_len
   edges(3) = 1
!
   do i=1,ncf%n2D
      dum(1:n_compben)=REAL(ncf%nv2D(i)%ersemvar(1:n_compben),kind(dum(1))) ! converts r8 ot r4
      iret = NF90_PUT_VAR(ncf%nid,ncf%nv2D(i)%ncdfid, &
         unpack(dum(1:n_compben),SeaFloor>0,NF90_FILL_REAL), &
         start(1:3),edges(1:3))
      call checkNCDFErsemError(iret,3,ncf%nv2D(i)%varname,ncf%nv2D(i)%ncdfid)
   end do

! Time varying profile 3D data : x,y,z,t
   start(1) = 1
   start(2) = 1
   start(3) = 1
   start(4) = ncf%tlev
   edges(1) = i_len
   edges(2) = j_len
   edges(3) = k_len
   edges(4) = 1
   do i=1,ncf%n3D
      dum(1:n_comp)=REAL(ncf%nv3D(i)%ersemvar(1:n_comp),kind(dum(1))) ! converts r8 to r4
      iret = NF90_PUT_VAR(ncf%nid,ncf%nv3D(i)%ncdfid, &
         unpack(dum,Water,NF90_FILL_REAL),start,edges)
      call checkNCDFErsemError(iret,3,ncf%nv3D(i)%varname,ncf%nv3D(i)%ncdfid)
   end do

   ncf%tlev = ncf%tlev + 1

   iret = NF90_SYNC(ncf%nid)
   call checkNCDFErsemError(iret,0)

   return

   end subroutine write_to_ncdfersem
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Close netcdf output \label{sec:closeNcdfersem}
!
! !DESCRIPTION:
! Closes the netcdf-ERSEM file.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine close_ncdfersem(ncf)
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU
!
! !INPUT PARAMETERS:
   type(ncdffile) :: ncf
!
! !LOCAL VARIABLES:
   integer :: iret
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) 'Closing ',ncf%fn

   iret = NF90_CLOSE(ncf%nid)

   call checkNCDFErsemError(iret,ncf%nid)

   return

   end subroutine close_ncdfersem


!
!EOC
!-----------------------------------------------------------------------

   end module ncdfersem

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
