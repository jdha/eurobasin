

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Budget calculations \label{sec:budget}
!
! !DESCRIPTION:
! Manages ersem logging budget calculations.
! \\[\baselineskip]
!
! !INTERFACE:
   module budget
!
! !USE:
   use ersem_constants, only: fp8
   use ersem_variables, only: ncdfInstOut, ncdfDailyOut, &
                              ncdfWeeklyOut, ncdfMonthlyOut
   use pelagic_variables
   use benthic_variables
   use allocation_helpers
!
   implicit none
!
! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_fixed_quota, allocate_budget, calc_budget
!
! !PUBLIC DATA MEMBERS:
   real(fp8), public, dimension(:), pointer :: TpelOC,TbenOC
   real(fp8), public, dimension(:), pointer :: TpelON,TbenON
   real(fp8), public, dimension(:), pointer :: TpelIN,TbenIN

   real(fp8), public, dimension(:), pointer :: fPelBenC,fBenPelC
   real(fp8), public, dimension(:), pointer :: fPelBenN,fBenPelN
   real(fp8), public, dimension(:), pointer :: fPelBenP,fBenPelP
   real(fp8), public, dimension(:), pointer :: fPelBenS,fBenPelS

   real(fp8), public, dimension(:), pointer :: fPelBenFe,fBenPelFe

   real(fp8), public, dimension(:), pointer :: wsoC,wsoN

   real(fp8), public, dimension(1), target :: TpelP,TbenP,TotP
   real(fp8), public, dimension(1), target :: TpelS,TbenS,TotS

   real(fp8), public, dimension(1), target :: TpelF,TbenF,TotF

   real(fp8), public, dimension(:), pointer :: opticalDepth
   real(fp8), public, dimension(:), pointer :: oChl
   real(fp8), public, dimension(:), pointer :: onetPP
   real(fp8), public, dimension(:), pointer :: grossPP
   real(fp8), public, dimension(:), pointer :: clipCount
!
!
! add manually declared budgets here:
! (daily,weekly,monthly variables need suffix D,W,M!)
   !1D:
   !real(fp8), public,dimension(1), target ::
   !2D,3D:
   !real(fp8), public, dimension(:),pointer ::
   integer, public :: nmonthly=0 ! Counter for monthly budgets
                                 ! 0=compute nudget
                                 ! 1=reset budgets
   integer, public :: nmoff=0 ! half month offset for budget time
   integer, public :: nwoff=0,ndoff=0 ! offsets for initial time
   integer, public :: ndaily=0,nweekly=0 ! switches for budgeting and output
   integer, public :: nPerMonth=1
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
   contains
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Set fixed quota
!
! !DESCRIPTION:
! Subroutine to set variables with fixed quota. Z4 does not have
! independent nutrient dynamics, but is set to a fixed ratio to carbon.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_fixed_quota
!
! !USES:
   use ersem_constants, only: fp8
   use pelagic_parameters, only: qnZIcX
   use pelagic_variables, only: Z4c, Z4n
   implicit none
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   Z4n = Z4c * qnZIcX
   end subroutine set_fixed_quota
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Allocate budget variables \label{sec:alocatebudget}
!
! !DESCRIPTION:
! Allocates budget variables.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine allocate_budget
!
! !LOCAL VARIABLES:
   integer :: lb, lbben
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   lb=min(1,N_COMP)
   lbben=min(1,N_COMPBEN)
   call allocateErsemDouble( opticalDepth, 'opticalDepth', lb, N_upperX )
   call allocateErsemDouble( oChl, 'oChl', lb, N_upperX )
   call allocateErsemDouble( onetPP, 'onetPP', lb, N_upperX )
   call allocateErsemDouble( grossPP, 'grossPP', lb, N_COMP )
   call allocateErsemDouble( TpelOC, 'TpelOC', lb, N_COMP )
   call allocateErsemDouble( TpelON, 'TpelON', lb, N_COMP )
   call allocateErsemDouble( TpelIN, 'TpelIN', lb, N_COMP )
   call allocateErsemDouble( TbenOC, 'TbenOC', lb, N_COMPBEN )
   call allocateErsemDouble( TbenON, 'TbenON', lb, N_COMPBEN )
   call allocateErsemDouble( fPelBenC, 'fPelBenC', lb, N_COMPBEN )
   call allocateErsemDouble( fBenPelC, 'fBenPelC', lb, N_COMPBEN )
   call allocateErsemDouble( fPelBenN, 'fPelBenN', lb, N_COMPBEN )
   call allocateErsemDouble( fBenPelN, 'fBenPelN', lb, N_COMPBEN )
   call allocateErsemDouble( wsoC, 'wsoC', lb, N_COMP)
   call allocateErsemDouble( wsoN, 'wsoN', lb, N_COMP)
   call allocateErsemDouble( clipCount, 'clipCount', lb, N_COMP )
   if (ncdfDailyOut) then
! add manually added daily 2D,3D variables here:
   !2D:
   !call allocateErsemDouble( nameD, 'nameD', lb, N_COMPBEN )
   !3D:
   !call allocateErsemDouble( nameD, 'nameD', lb, N_COMP )
      continue
   end if
   if (ncdfWeeklyOut) then
! add manually added weekly 2D,3D variables here:
   !2D:
   !call allocateErsemDouble( nameD, 'nameD', lb, N_COMPBEN )
   !3D:
   !call allocateErsemDouble( nameD, 'nameD', lb, N_COMP )
      continue
   end if
   if (ncdfMonthlyOut) then
! add manually added monthly 2D,3D variables here:
   !2D:
   !call allocateErsemDouble( nameD, 'nameD', lb, N_COMPBEN )
   !3D:
   !call allocateErsemDouble( nameD, 'nameD', lb, N_COMP )
      continue
   end if
   if (ncdfInstOut) then
! add manually added 2D,3D variables here:
   !2D:
   !call allocateErsemDouble( nameD, 'nameD', lb, N_COMPBEN )
   !3D:
   !call allocateErsemDouble( nameD, 'nameD', lb, N_COMP )
      continue
   end if
   end subroutine
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Calculate budget variables \label{sec:calcbudget}
!
! !DESCRIPTION:
! Budgeting the ecosystem's diagnostic and state variables,
! assumes a fixed timestep being a factor of the day.
!\!\! !INTERFACE:
   subroutine calc_budget(n,dt,nsave)
!
! !USES:
   use ersem_constants, only: milli_to_mega, seconds_per_day, &
                              CMass, PMass, NMass, SMass, OMass, &
                              FeMass
   use pelagic_parameters, only: qnZIcX, qpZIcX
   use benthic_parameters, only: qnyicx, qpyicx, qnhicx, qphicx
   use bacteria
!
! !INPUT PARAMETERS:
   integer,intent(in) :: n ! time step counter
   integer,intent(in) :: nsave ! interval for budgeting
   real(fp8),intent(in) :: dt ! time step in [1/d]
   real(fp8) :: dts,val
   integer :: i,j,nsecs,k
!
! !LOCAL VARIABLES:
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   !------------------------------
   ! set up the budget variables
   !------------------------------
   dts=dt*seconds_per_day
   nsecs = nint(n*dts)
   ndaily=MOD(n+nint(ndoff/dts),nint(1./dt)) ! no it. in current day
   nweekly=MOD(n+nint(nwoff/dts),nint(7./dt)) ! no it. in current week
   ! monthly flag is controlled by the coupler due to varying day
   ! numbers of the months
   ! Resetting Integrated biomasses
   TpelP = 0._fp8
   TbenP = 0._fp8
   TpelS = 0._fp8
   TbenS = 0._fp8
   TotF = 0._fp8
   TpelF=0._fp8
   call ResetTimeBudgets(N,dt,nsave)
   !-------------------------------------
   ! Calculate the integrated variables
   !-------------------------------------
   ! PELAGIC
   do I = 1,N_COMP
       if ( ibenX(I) .gt. 0 ) then
          K=benthicIndex(I)
          parea(K)=parea(I)*milli_to_mega
       end if
   end do
   Chl=Chl1+Chl2+Chl3+Chl4
   TpelOC=P1c+P2c+P3c+P4c+R1c+R2c+R3c+R4c+R6c+R8c+Z4c+Z5c+Z6c
   TpelON=P1n+P2n+P3n+P4n+R1n+R4n+R6n+R8n+Z4n+Z5n+Z6n
   TpelIN=N3n+N4n
   PResp=(fP1O3c+fP2O3c+fP3O3c+fP4O3c)*pvol
   grossPP=(fO3P1c+fO3P2c+fO3P3c+fO3P4c)*pvol
   netPP=(netP1+netP2+netP3+netP4)*pvol
   netB=netB1*pvol
   fPXZXc=(fP1Z5c+fP2Z5c+fP3Z5c+fP4Z5c+fP2Z6c+fP3Z6c+fP1Z4c+ &
          fP2Z4c+fP3Z4c+fP4Z4c)*pvol
   fBXZXc=(fB1Z5c+fB1Z6c+fB1Z4c)*pvol
   fRXZXc=fR6Z4c*pvol
   PResp=(fP1O3c+fP2O3c+fP3O3c+fP4O3c)*pvol
   ZResp=(fZ4O3c+fZ5O3c+fZ6O3c)*pvol
   BResp=fB1O3c*pvol
   nitrN=nitrN*pvol
   fPXRPc=(fP1R6c+fP2R4c+fP3R4c+fP4R6c)*pvol
   fZXRPc=(fZ6R4c+fZ5R6c+fZ4R8c)*pvol
   fPXRDc=(fP1RDc+fP2RDc+fP3RDc+fP4RDC)*pvol
   fZXRDc=fZXRDc*pvol
   fBXRDc=fB1RDc*pvol
   fRDBXc=fRDBXc*pvol
   fRPBXc=fRPBXc*pvol
   fN1PXp=(fN1P1p+fN1P2p+fN1P3p+fN1P4p)*pvol
   fN3PXn=(fN3P1n+fN3P2n+fN3P3n+fN3P4n)*pvol
   fN4PXn=(fN4P1n+fN4P2n+fN4P3n+fN4P4n)*pvol
   fN5PXs=fN5PXs*pvol
   fN7PXf=(fN7P1f+fN7P2f+fN7P3f+fN7P4f)*pvol
   fBXN1p=fB1N1p*pvol
   fBXNIn=fB1NIn*pvol
   fRDN1p=fR1N1p*pvol
   fRDNIn=fR1NIn*pvol
   wsoC=(wsoP1c+wsoP2c+wsoP3c+wsoP4c+wsoR4c+wsoR6c+wsoR8c)*pdepth
   wsoN=(wsoP1n+wsoP2n+wsoP3n+wsoP4n+wsoR4n+wsoR6n+wsoR8n)*pdepth
!
  ! PHOSPHATE
  ! Pelagic
  Tpelp = sum( (N1p+R1p+R4p+R6p+R8p+P1p+P2p+P3p+p4p+Z4c*qpZIcX+Z5p &
      +Z6p+B1p)*pvol )
  ! SILICATE
  ! Pelagic
  Tpels = sum( (R6s+R8s+P1s+N5s)*pvol )
   TpelF = sum( (N7f+P1f+P2f+P3f+P4f+R4f+R6f)*pvol )
!..BENTHIC
   TbenOC = Q1c+Q6c+Q7c
   TbenON = Q1n+Q6n+Q7n
   if (any(ibenX==2)) then
      TbenOC = TbenOC + H1c+H2c+Y2c+Y3c+Y4c+Q17c
      TbenON = TbenON + (H1c+H2c)*qpHIcX+(Y2c+Y3c+Y4c)*qpYIcX+Q17n
      TbenIN = G4n+K3n+K4n
   end if
   TbenF = sum( Q6f*parea )*FeMass
! Benthic-Pelagic Fluxes
   DO k=1,N_COMPBEN
       i=pelagicIndex(k)
       fPelBenC(k)=wsiQ1c(k)+wsiQ6c(k)+wsiQ7c(k)+fPXY3c(k)
       fPelBenN(k)=wsiQ1n(k)+wsiQ6n(k)+wsiQ7n(k)+fPXY3n(k)
       fBenPelC(k)=(wsiO3c(i)*CMass+wsiR6c(i))*pdepth(i)
       fBenPelN(k)=(wsiN3n(i)+wsiN4n(i)+wsiR6n(i))*pdepth(i)
   ENDDO
   ! Calculate optical depth
   ! approximated as inverse of the extinction koefficient, i.e. the depth,
   ! where the surface light has been reduced by the factor of e (e-folding
   ! depth).
   opticalDepth = 0._fp8
   oChl = 0._fp8
   onetPP = 0._fp8
   do i = 1,n_upperX !loop over surface
      j=i
      !val=exp(1._fp8) !e-folding depth
      val=100. !euphotic depth (extiction to 1% of surface light level
      do while (j.gt.0)
         val=val*exp(-pdepth(j)*xEPS(j)) ! value at cell bottom
         if (val.gt.1._fp8) then ! if lower face is above optical depth
            opticalDepth(i) = opticalDepth(i) + pdepth(j)
            oChl(i) = oChl(i) + Chl(j)*pdepth(j)
            onetPP(i) = onetPP(i) + (netP1(j)+netP2(j)+netP3(j) &
                  +netP4(j))*pdepth(j)
            j=lowerX(j)
         else ! if lower face is <= optical depth:
            opticalDepth(i)=opticalDepth(i) + pdepth(j) + 1/xEPS(j)*log(val)
            oChl(i)=oChl(i) + Chl(j)*(pdepth(j) + 1/xEPS(j)*log(val))
            j=0
         end if
      end do
   end do
   oChl=oChl/opticalDepth
   !-------------------------------------
   ! calculate the budget variables
   !-------------------------------------
   call ComputeTimeBudgets(N,dt,nsave)
346 format(3f10.5)
347 format(a30,f10.5)
348 format(a25,5F13.2)
349 format(25X,5A13)
   return
   end subroutine
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Reset time averages
!
! !DESCRIPTION:
! Reset daily, weekly, monthly and custome interval bugdet variables.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine ResetTimeBudgets(N,dt,nsave)
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU
!
! !INPUT PARAMETERS:
   integer, intent(in) :: N,nsave
   real(fp8),intent(in) :: dt
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Resetting budgets on nsave interval
   if (MOD(N,NSAVE).EQ.1) then
      if (ncdfInstOut) then
         continue
      end if
   end if
   ! Resetting daily budgets
   if (ndaily.eq.1) then
      if (ncdfDailyOut) then
         continue
      end if
   end if
   ! Resetting weekly budgets
   if (nweekly.eq.1) then
      if (ncdfWeeklyOut) then
         continue
      end if
   end if
   ! Resetting monthly budgets
   if (nmonthly.eq.1) then
      if (ncdfMonthlyOut) then
         nPerMonth = 1
         continue
      end if
      if (IOProcess) write(eLogU,*) 'nPerMonth reset: ',nPerMonth
   end if
   end subroutine ResetTimeBudgets
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Compute time averages
!
! !DESCRIPTION:
! Calculate daily, weekly, monthly and custome interval bugdet variables.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine ComputeTimeBudgets(N,dt,nsave)
!
! !INPUT PARAMETERS:
   integer,intent(in) :: N,nsave
   real(fp8),intent(in) :: dt
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! nsave budget control:
   if (ncdfInstOut) then
! varS = varS + var
      if (MOD(N,NSAVE).EQ.0) then
! varS=varS/nsave
         continue
      end if
   end if
   ! daily budget control:
   if (ncdfDailyOut) then
      if (ndaily.eq.0) then
         continue
      end if
   end if
   ! weekly budget control:
   if (ncdfWeeklyOut) then
      if (nweekly.eq.0) then
         continue
      end if
   end if
   ! monthly budgets control:
   if (ncdfMonthlyOut) then
      if(nmonthly.eq.0) then
         continue
      end if
   end if
   nmonthly=nmonthly+1
   nPerMonth=nmonthly
   end subroutine
!
!EOC
!-----------------------------------------------------------------------
   end module budget
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
