

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Pelagic variables
!
! !DESCRIPTION:
! Repository for state and anscillary variables specific to the pelagic
! component of {\em ERSEM}. It contains the definitions of the global pelagic
! array \texttt{ccc} and the corresponding auxilliary arrays (table
! \ref{tab:State-array-and}), and provides pointers to the
! individual pelagic states within the \texttt{ccc} array (tables \ref{tab:ERSEM-state-inorganic},
! \ref{tab:ERSEM-state-organic} and \ref{tab:ERSEM-state-matter})
! along with the respective auxilliary pointers. It contains
! abiotic variables describing the pyhsical environment, and diagnostic
! variables like the biogeochemical fluxes in between the model compartments,
! or derived variables like bacterial growth efficiency. A last set
! of variables defined in this module concern the model goemetry, i.e.
! the mapping of the actual (usually three dimensional) spatial structure
! into the contiguous memory structure, and the mapping of benthic to
! pelagic boxes, as well as switches for the model set-up. This module
! also contains routines for the allocation and initialisation of the
! defined arrays.
!
! \begin{center}
! \begin{table}
! \begin{centering}
! \begin{tabular}{|c|c|c|c|c|}
! \hline
! Symbol & Pointer ID & Description & Unit & Position\tabularnewline
! \hline
! \hline
! $G_{o_{2}}$ & \texttt{O2o} & Dissolved Oxygen & $\frac{\textit{mmol}}{m^{3}}$ & $1$\tabularnewline
! \hline
! $G_{\textit{co}_{2}}$ & \texttt{O3c} & Dissolved Inorganic Carbon & $\frac{\textit{mmol}}{m^{3}}$ & $2$\tabularnewline
! \hline
! $N_{\textit{phos}}$ & \texttt{N1p} & Phosphate & $\frac{\textit{mmol}}{m^{3}}$ & $3$\tabularnewline
! \hline
! $N_{\textit{nitr}}$ & \texttt{N3n} & Nitrate & $\frac{\textit{mmol}}{m^{3}}$ & $4$\tabularnewline
! \hline
! $N_{\textit{amm}}$ & \texttt{N4n} & Ammonium & $\frac{\textit{mmol}}{m^{3}}$ & $5$\tabularnewline
! \hline
! $N_{\textit{sil}}$ & \texttt{N5s} & Silicate & $\frac{\textit{mmol}}{m^{3}}$ & $6$\tabularnewline
! \hline
! \end{tabular}
! \par\end{centering}
!
! \centering{}\caption{ERSEM state variables - inorganic part\label{tab:ERSEM-state-inorganic}}
! \end{table}
!
! \par\end{center}
!
! \begin{center}
! \begin{table}
! \begin{centering}
! \begin{tabular}{|c|c|c|c|c|}
! \hline
! Symbol & Code & Description & Unit & Position\tabularnewline
! \hline
! \hline
! $\overset{c}{P}_{\textit{dia}}$ & \texttt{P1c} & Diatom Carbon & $\frac{\textit{mg}}{m^{3}}$ & $14$\tabularnewline
! \hline
! $\overset{n}{P}_{\textit{dia}}$ & \texttt{P1n} & Diatom Nitrogen & $\frac{\textit{mmol}}{m^{3}}$ & $15$\tabularnewline
! \hline
! $\overset{p}{P}_{\textit{dia}}$ & \texttt{P1p} & Diatom Phosphorus & $\frac{\textit{mmol}}{m^{3}}$ & $16$\tabularnewline
! \hline
! $\overset{s}{P}_{\textit{dia}}$ & \texttt{P1s} & Diatom Silicate & $\frac{\textit{mmol}}{m^{3}}$ & $17$\tabularnewline
! \hline
! $\overset{c}{P}_{\textit{nano}}$ & \texttt{P2c} & Nanophytoplankton Carbon & $\frac{\textit{mg}}{m^{3}}$ & $18$\tabularnewline
! \hline
! $\overset{n}{P}_{\textit{nano}}$ & \texttt{P2n} & Nanophytoplankton Nitrogen & $\frac{\textit{mmol}}{m^{3}}$ & $19$\tabularnewline
! \hline
! $\overset{p}{P}_{\textit{nano}}$ & \texttt{P2p} & Nanophytoplankton Phosphorus & $\frac{\textit{mmol}}{m^{3}}$ & $20$\tabularnewline
! \hline
! $\overset{c}{P}_{\textit{pico}}$ & \texttt{P3c} & Picophytoplankton Carbon & $\frac{\textit{mg}}{m^{3}}$ & $21$\tabularnewline
! \hline
! $\overset{n}{P}_{\textit{pico}}$ & \texttt{P3n} & Picophytoplankton Nitrogen & $\frac{\textit{mmol}}{m^{3}}$ & $22$\tabularnewline
! \hline
! $\overset{p}{P}_{\textit{pico}}$ & \texttt{P3p} & Picophytoplankton Phosphorus & $\frac{\textit{mmol}}{m^{3}}$ & $23$\tabularnewline
! \hline
! $\overset{c}{P}_{\textit{micro}}$ & \texttt{P4c} & Microphytoplankton Carbon & $\frac{\textit{mg}}{m^{3}}$ & $24$\tabularnewline
! \hline
! $\overset{n}{P}_{\textit{micro}}$~ & \texttt{P4n} & Microphytoplankton Nitrogen & $\frac{\textit{mmol}}{m^{3}}$ & $25$\tabularnewline
! \hline
! $\overset{p}{P}_{\textit{micro}}$ & \texttt{P4p} & Microphytoplankton Phosphorus & $\frac{\textit{mmol}}{m^{3}}$ & $26$\tabularnewline
! \hline
! $\overset{c}{Z}_{\textit{MESO}}$ & \texttt{Z4c} & Mesozooplankton Carbon & $\frac{\textit{mmol}}{m^{3}}$ & $27$\tabularnewline
! \hline
! $\overset{c}{Z}_{\textit{MICRO}}$ & \texttt{Z5c} & Microozooplankton Carbon & $\frac{\textit{mg}}{m^{3}}$ & $28$\tabularnewline
! \hline
! $\overset{n}{Z}_{\textit{MICRO}}$ & \texttt{Z5n} & Microozooplankton Nitrogen & $\frac{\textit{mmol}}{m^{3}}$ & $29$\tabularnewline
! \hline
! $\overset{p}{Z}_{\textit{MICRO}}$ & \texttt{Z5p} & Microozooplankton Phosphorus & $\frac{\textit{mmol}}{m^{3}}$ & $30$\tabularnewline
! \hline
! $\overset{c}{Z}_{\textit{HET}}$ & \texttt{Z6c} & Heteroflagellate Carbon & $\frac{\textit{mg}}{m^{3}}$ & $31$\tabularnewline
! \hline
! $\overset{n}{Z}_{\textit{HET}}$ & \texttt{Z6n} & Heteroflagellate Nitrogen & $\frac{\textit{mmol}}{m^{3}}$ & $32$\tabularnewline
! \hline
! $\overset{p}{Z}_{\textit{HET}}$ & \texttt{Z6p} & Heteroflagellate Phosphorus & $\frac{\textit{mmol}}{m^{3}}$ & $33$\tabularnewline
! \hline
! $\overset{c}{B}$ & \texttt{B1c} & Bacteria Carbon & $\frac{\textit{mg}}{m^{3}}$ & $34$\tabularnewline
! \hline :w

! $\overset{n}{B}$ & \texttt{B1n} & Bacteria Nitrogen & $\frac{\textit{mmol}}{m^{3}}$ & $35$\tabularnewline
! \hline
! $\overset{p}{B}$ & \texttt{B1p} & Bacteria Phosphorus & $\frac{\textit{mmol}}{m^{3}}$ & $36$\tabularnewline
! \hline
! $\overset{\mathscr{C}}{P}_{\textit{dia}}$ & \texttt{Chl1} & Diatom Chlorophyll-a & $\frac{\textit{mg}}{m^{3}}$ & $45$\tabularnewline
! \hline
! $\overset{\mathscr{C}}{P}_{\textit{nano}}$ & \texttt{Chl2} & Nanophytoplankton Chlorophyll-a & $\frac{\textit{mg}}{m^{3}}$ & $46$\tabularnewline
! \hline
! $\overset{\mathscr{C}}{P}_{\textit{pico}}$ & \texttt{Chl3} & Picophytoplankton Chlorophyll-a & $\frac{\textit{mg}}{m^{3}}$ & $47$\tabularnewline
! \hline
! $\overset{\mathscr{C}}{P}_{\textit{micro}}$ & \texttt{Chl4} & Microphytoplankton Chlorophyll-a & $\frac{\textit{mg}}{m^{3}}$ & $48$\tabularnewline
! \hline
! \end{tabular}
! \par\end{centering}
!
! \centering{}\caption{ERSEM state variables - living organic part\label{tab:ERSEM-state-organic}}
! \end{table}
!
! \par\end{center}
!
! \begin{center}
! \begin{table}
! \begin{centering}
! \begin{tabular}{|c|c|c|c|c|}
! \hline
! Symbol & Code & Description & Unit & Position\tabularnewline
! \hline
! \hline
! $\overset{c}{M}_{\textit{med}}$ & \texttt{R6c} & Medium Size Particulate Organic Carbon & $\frac{\textit{mg}}{m^{3}}$ & $7$\tabularnewline
! \hline
! $\overset{n}{M}_{\textit{med}}$ & \texttt{R6n} & Medium Size Particulate Organic Nitrogen & $\frac{\textit{mmol}}{m^{3}}$ & $8$\tabularnewline
! \hline
! $\overset{p}{M}_{\textit{med}}$ & \texttt{R6p} & Medium Size Particulate Organic Phophorus & $\frac{\textit{mmol}}{m^{3}}$ & $9$\tabularnewline
! \hline
! $\overset{s}{M}_{\textit{med}}$ & \texttt{R6s} & Medium Size Particulate Organic Silicate & $\frac{\textit{mmol}}{m^{3}}$ & $10$\tabularnewline
! \hline
! $\overset{c}{M}_{\textit{dis}}$ & \texttt{R1c} & Dissolved Organic Carbon & $\frac{\textit{mg}}{m^{3}}$ & $11$\tabularnewline
! \hline
! $\overset{n}{M}_{\textit{dis}}$ & \texttt{R1n} & Dissolved Organic Nitrogen & $\frac{\textit{mmol}}{m^{3}}$ & $12$\tabularnewline
! \hline
! $\overset{p}{M}_{\textit{dis}}$ & \texttt{R1p} & Dissolved Organic Phosphorus & $\frac{\textit{mmol}}{m^{3}}$ & $13$\tabularnewline
! \hline
! $\overset{c}{M}_{\textit{small}}$ & \texttt{R4c} & Small Particulate Organic Carbon & $\frac{\textit{mg}}{m^{3}}$ & $37$\tabularnewline
! \hline
! $\overset{cn}{M}_{\textit{small}}$ & \texttt{R4n} & Small Particulate Organic Nitrrogen & $\frac{\textit{mmol}}{m^{3}}$ & $38$\tabularnewline
! \hline
! $\overset{p}{M}_{\textit{small}}$ & \texttt{R4p} & Small Particulate Organic Phosphorus & $\frac{\textit{mmol}}{m^{3}}$ & $39$\tabularnewline
! \hline
! $\overset{c}{M}_{\textit{large}}$ & \texttt{R8c} & Large Particulate Organic Carbon & $\frac{\textit{mg}}{m^{3}}$ & $40$\tabularnewline
! \hline
! $\overset{n}{M}_{\textit{large}}$ & \texttt{R8n} & Large Particulate Organic Nitrogen & $\frac{\textit{mmol}}{m^{3}}$ & $41$\tabularnewline
! \hline
! $\overset{p}{M}_{\textit{large}}$ & \texttt{R8p} & Large Particulate Organic Phosphorus & $\frac{\textit{mmol}}{m^{3}}$ & $42$\tabularnewline
! \hline
! $\overset{s}{M}_{\textit{large}}$ & \texttt{R8s} & Large Particulate Organic Silicate & $\frac{\textit{mmol}}{m^{3}}$ & $43$\tabularnewline
! \hline
! $\overset{c}{M}_{\textit{slab}}$ & \texttt{R2c} & Semi-labile Organic Carbon & $\frac{\textit{mg}}{m^{3}}$ & $44$\tabularnewline
! \hline
! $\mathbb{A}_{\textit{bio}}$ & \texttt{bioalk} & Bioalkalinity & $\frac{\textit{\ensuremath{\mu}mol}}{\textit{kg}}$ & $49$\tabularnewline
! \hline
! \end{tabular}
! \par\end{centering}
!
! \centering{}\caption{ERSEM state variables - living organic part\label{tab:ERSEM-state-matter}}
! \end{table}
!
! \par\end{center}
!
! \begin{center}
! \begin{table}
! \begin{centering}
! \begin{tabular}{|c|c|c|c|}
! \hline
! Symbol & Code & Description & Unit\tabularnewline
! \hline
! \hline
! $\overset{c}{M}_{\textit{calc}}$ & \texttt{L2c} & Calcite & $\frac{\textit{mg}}{m^{3}}$\tabularnewline
! \hline
! $\overset{f}{P}_{\textit{dia}}$ & \texttt{P1f} & Diatom Iron & $\frac{\textit{mmol}}{m^{3}}$\tabularnewline
! \hline
! $\overset{f}{P}_{\textit{nano}}$ & \texttt{P2f} & Nanophytoplankton Iron & $\frac{\textit{mmol}}{m^{3}}$\tabularnewline
! \hline
! $\overset{f}{P}_{\textit{pico}}$ & \texttt{P3f} & Picophytoplankton Iron & $\frac{\textit{mmol}}{m^{3}}$\tabularnewline
! \hline
! $\overset{f}{P}_{\textit{micro}}$ & \texttt{P4f} & Microphytoplankton Iron & $\frac{\textit{mmol}}{m^{3}}$\tabularnewline
! \hline
! $\overset{f}{M}_{\textit{med}}$ & \texttt{R6f} & Medium Size Particulate Iron & $\frac{\textit{mmol}}{m^{3}}$\tabularnewline
! \hline
! $\overset{f}{M}_{\textit{small}}$ & \texttt{R4f} & Small Size Particulate Iron & $\frac{\textit{mmol}}{m^{3}}$\tabularnewline
! \hline
! $\mathbb{T}$ & \texttt{M1m} & Dummy Tracer & $1$\tabularnewline
! \hline
! $K_{\textit{ady}}$ & \texttt{ADY} & Gelbstoff Adsorption & $\frac{1}{m}$\tabularnewline
! \hline
! $\overset{c}{M}_{\textit{srefr}}$ & \texttt{R3c} & Semi-refractory Organic Matter & $\frac{\textit{mg}}{m^{3}}$\tabularnewline
! \hline
! \end{tabular}
! \par\end{centering}
!
! \centering{}\caption{Optional ERSEM state variables available through through extra features.\label{tab:Optional-ERSEM-states}}
! \end{table}
!
! \par\end{center}
!
!
! !INTERFACE:
   module pelagic_variables
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is public
   public
!
! !PUBLIC MEMBER FUNCTIONS:
   public do_pelagic_variable_allocations
!
! !PUBLIC DATA MEMBERS:
! Number of pelagic grid locations
   integer :: N_COMP=0

! Number of pelagic state variables
   integer :: I_STATE=0

! Extra state variables dependent on preprocessor defs

   integer :: CALC_STATE


   integer :: IRON_STATE





   integer :: ADY_STATE


   integer :: DOCDYN_STATE


! Lower bound used in allocations
   integer :: lb=0

! Integer identifiers for pelagic state variables
   integer :: iO2o,iO3c,iN1p,iN3n
   integer :: iN4n,iN5s,iR6c,iR6n,iR6p,iR6s,iR1c,iR1n
   integer :: iR1p,iP1c,iP1n,iP1p,iP1s,iP2c,iP2n,iP2p
   integer :: iP3c,iP3n,iP3p,iP4c,iP4n,iP4p,iZ4c,iZ5c
   integer :: iZ5n,iZ5p,iZ6c,iZ6n,iZ6p,iB1c,iB1n,iB1p
   integer :: iR4c,iR4n,iR4p
   integer :: iR8c,iR8n,iR8p,iR8s
   integer :: iR2c

   integer :: iR3c

   integer :: iChl1, iChl2, iChl3, iChl4
   integer :: ibioalk

   integer :: iN7f,iP1f,iP2f,iP3f,iP4f,iR6f,iR4f





   integer :: iADY


   integer :: iL2c


! Pelagic transport switches
   integer, pointer :: itrXo2o,itrXo3c,itrXn1p,itrXn3n
   integer, pointer :: itrXn4n,itrXn5s,itrXr6c,itrXr6n
   integer, pointer :: itrXr6p,itrXr6s,itrXr1c,itrXr1n
   integer, pointer :: itrXr1p,itrXp1c,itrXp1n,itrXp1p
   integer, pointer :: itrXp1s,itrXp2c,itrXp2n,itrXp2p
   integer, pointer :: itrXp3c,itrXp3n,itrXp3p,itrXp4c
   integer, pointer :: itrXp4n,itrXp4p,itrXz4c,itrXz5c
   integer, pointer :: itrXz5n,itrXz5p,itrXz6c,itrXz6n
   integer, pointer :: itrXz6p,itrXb1c,itrXb1n,itrXb1p
   integer, pointer :: itrXr4c,itrXr4n,itrXr4p
   integer, pointer :: itrXr8c,itrXr8n,itrXr8p,itrXr8s
   integer, pointer :: itrXr2c

   integer,pointer :: itrXr3c

   integer, pointer :: itrXchl1, itrXchl2, itrXchl3, itrXchl4
   integer, pointer :: itrXbioalk

   integer, pointer :: itrXN7f,itrXP1f,itrXP2f,itrXP3f
   integer, pointer :: itrXP4f,itrXR6f,itrXR4f





   integer, pointer :: itrXADY


   integer, pointer :: itrXl2c


! Pelagic state variables
   real(fp8), pointer, dimension(:) :: o2o,o3c
   real(fp8), pointer, dimension(:) :: n1p,n3n,n4n,n5s,r6c
   real(fp8), pointer, dimension(:) :: r6n,r6p,r6s,r1c,r1n
   real(fp8), pointer, dimension(:) :: r1p,p1c,p1n,p1p,p1s
   real(fp8), pointer, dimension(:) :: p2c,p2n,p2p,p3c,p3n
   real(fp8), pointer, dimension(:) :: p3p,p4c,p4n,p4p,z4c
   real(fp8), pointer, dimension(:) :: z5c,z5n,z5p,z6c,z6n
   real(fp8), pointer, dimension(:) :: z6p,b1c,b1n,b1p
   real(fp8), pointer, dimension(:) :: r4c,r4n,r4p
   real(fp8), pointer, dimension(:) :: r8c,r8n,r8p,r8s
   real(fp8), pointer, dimension(:) :: r2c

   real(fp8), pointer, dimension(:) :: r3c

   real(fp8), pointer, dimension(:) :: chl1, chl2, chl3, chl4
   real(fp8), pointer, dimension(:) :: bioalk

   real(fp8), pointer,dimension(:) :: N7f,P1f,P2f,P3f,P4f,R6f,R4f





   real(fp8), pointer,dimension(:) :: ADY


   real(fp8), pointer, dimension(:) :: L2c


! Processable concentration
   real(fp8), pointer, dimension(:) :: o2oP,o3cP
   real(fp8), pointer, dimension(:) :: n1pP,n3nP,n4nP,n5sP,r6cP
   real(fp8), pointer, dimension(:) :: r6nP,r6pP,r6sP,r1cP,r1nP
   real(fp8), pointer, dimension(:) :: r1pP,p1cP,p1nP,p1pP,p1sP
   real(fp8), pointer, dimension(:) :: p2cP,p2nP,p2pP,p3cP,p3nP
   real(fp8), pointer, dimension(:) :: p3pP,p4cP,p4nP,p4pP,z4cP
   real(fp8), pointer, dimension(:) :: z5cP,z5nP,z5pP,z6cP,z6nP
   real(fp8), pointer, dimension(:) :: z6pP,b1cP,b1nP,b1pP
   real(fp8), pointer, dimension(:) :: r4cP,r4nP,r4pP
   real(fp8), pointer, dimension(:) :: r8cP,r8nP,r8pP,r8sP
   real(fp8), pointer, dimension(:) :: r2cP

   real(fp8), pointer, dimension(:) :: r3cP

   real(fp8), pointer, dimension(:) :: chl1P, chl2P, chl3P, chl4P

   real(fp8), pointer, dimension(:) :: bioalkP

   real(fp8), pointer,dimension(:) :: N7fP,P1fP,P2fP,P3fP
   real(fp8), pointer,dimension(:) :: P4fP,R6fP,R4fP





   real(fp8), pointer,dimension(:) :: ADYP


   real(fp8), pointer,dimension(:) :: L2cP


! Asymptotic minimal concentration
   real(fp8),pointer :: o2o0,o3c0
   real(fp8),pointer :: n1p0,n3n0,n4n0,n5s0,r6c0
   real(fp8),pointer :: r6n0,r6p0,r6s0,r1c0,r1n0
   real(fp8),pointer :: r1p0,p1c0,p1n0,p1p0,p1s0
   real(fp8),pointer :: p2c0,p2n0,p2p0,p3c0,p3n0
   real(fp8),pointer :: p3p0,p4c0,p4n0,p4p0,z4c0
   real(fp8),pointer :: z5c0,z5n0,z5p0,z6c0,z6n0
   real(fp8),pointer :: z6p0,b1c0,b1n0,b1p0
   real(fp8),pointer :: r4c0,r4n0,r4p0
   real(fp8),pointer :: r8c0,r8n0,r8p0,r8s0
   real(fp8),pointer :: r2c0

   real(fp8),pointer :: r3c0

   real(fp8),pointer :: chl10, chl20, chl30, chl40

   real(fp8), pointer:: bioalk0

   real(fp8), pointer :: N7f0,P1f0,P2f0,P3f0,P4f0,R6f0,R4f0





   real(fp8), pointer :: ADY0


   real(fp8), pointer :: L2C0


! Sink/source terms
   real(fp8), pointer, dimension(:) :: so2o,so3c
   real(fp8), pointer, dimension(:) :: sn1p,sn3n,sn4n,sn5s,sr6c
   real(fp8), pointer, dimension(:) :: sr6n,sr6p,sr6s,sr1c,sr1n
   real(fp8), pointer, dimension(:) :: sr1p,sp1c,sp1n,sp1p,sp1s
   real(fp8), pointer, dimension(:) :: sp2c,sp2n,sp2p,sp3c,sp3n
   real(fp8), pointer, dimension(:) :: sp3p,sp4c,sp4n,sp4p,sz4c
   real(fp8), pointer, dimension(:) :: sz5c,sz5n,sz5p,sz6c,sz6n
   real(fp8), pointer, dimension(:) :: sz6p,sb1c,sb1n,sb1p
   real(fp8), pointer, dimension(:) :: sr4c,sr4n,sr4p
   real(fp8), pointer, dimension(:) :: sr8c,sr8n,sr8p,sr8s
   real(fp8), pointer, dimension(:) :: sr2c

   real(fp8), pointer, dimension(:) :: sr3c

   real(fp8), pointer, dimension(:) :: schl1, schl2, schl3, schl4
   real(fp8), pointer, dimension(:) :: sbioalk

   real(fp8), pointer,dimension(:) :: SN7f,SP1f,SP2f,SP3f
   real(fp8), pointer,dimension(:) :: SP4f,SR6f,SR4f





   real(fp8), pointer,dimension(:) :: SADY


   real(fp8), pointer, dimension(:) :: SL2C


! Vertical transport flux into each pelagic state variable
   real(fp8), pointer, dimension(:) :: wsio2o,wsio3c
   real(fp8), pointer, dimension(:) :: wsin1p,wsin3n,wsin4n
   real(fp8), pointer, dimension(:) :: wsin5s,wsir6c,wsir6n,wsir6p
   real(fp8), pointer, dimension(:) :: wsir6s,wsir1c,wsir1n,wsir1p
   real(fp8), pointer, dimension(:) :: wsip1c,wsip1n,wsip1p,wsip1s
   real(fp8), pointer, dimension(:) :: wsip2c,wsip2n,wsip2p,wsip3c
   real(fp8), pointer, dimension(:) :: wsip3n,wsip3p,wsip4c,wsip4n
   real(fp8), pointer, dimension(:) :: wsip4p,wsiz4c,wsiz5c,wsiz5n
   real(fp8), pointer, dimension(:) :: wsiz5p,wsiz6c,wsiz6n,wsiz6p
   real(fp8), pointer, dimension(:) :: wsib1c,wsib1n,wsib1p
   real(fp8), pointer, dimension(:) :: wsir4c,wsir4n,wsir4p
   real(fp8), pointer, dimension(:) :: wsir8c,wsir8n,wsir8p,wsir8s
   real(fp8), pointer, dimension(:) :: wsir2c

   real(fp8), pointer, dimension(:) :: wsir3c

   real(fp8), pointer, dimension(:) :: wsichl1, wsichl2
   real(fp8), pointer, dimension(:) :: wsichl3, wsichl4
   real(fp8), pointer, dimension(:) :: wsibioalk

   real(fp8), pointer,dimension(:) :: wsiN7f,wsiP1f,wsiP2f,wsiP3f
   real(fp8), pointer,dimension(:) :: wsiP4f,wsiR6f,wsiR4f





   real(fp8), pointer,dimension(:) :: wsiADY


   real(fp8), pointer, dimension(:) :: wsil2c


! Vertical transport flux out of each pelagic state variable
   real(fp8), pointer, dimension(:) :: wsoo2o,wsoo3c
   real(fp8), pointer, dimension(:) :: wson1p,wson3n,wson4n
   real(fp8), pointer, dimension(:) :: wson5s,wsor6c,wsor6n,wsor6p
   real(fp8), pointer, dimension(:) :: wsor6s,wsor1c,wsor1n,wsor1p
   real(fp8), pointer, dimension(:) :: wsop1c,wsop1n,wsop1p,wsop1s
   real(fp8), pointer, dimension(:) :: wsop2c,wsop2n,wsop2p,wsop3c
   real(fp8), pointer, dimension(:) :: wsop3n,wsop3p,wsop4c,wsop4n
   real(fp8), pointer, dimension(:) :: wsop4p,wsoz4c,wsoz5c,wsoz5n
   real(fp8), pointer, dimension(:) :: wsoz5p,wsoz6c,wsoz6n,wsoz6p
   real(fp8), pointer, dimension(:) :: wsob1c,wsob1n,wsob1p
   real(fp8), pointer, dimension(:) :: wsor4c,wsor4n,wsor4p
   real(fp8), pointer, dimension(:) :: wsor8c,wsor8n,wsor8p,wsor8s
   real(fp8), pointer, dimension(:) :: wsor2c

   real(fp8), pointer, dimension(:) :: wsor3c

   real(fp8), pointer, dimension(:) :: wsochl1, wsochl2
   real(fp8), pointer, dimension(:) :: wsochl3, wsochl4
   real(fp8), pointer, dimension(:) :: wsobioalk

   real(fp8), pointer, dimension(:) :: wsoN7f,wsoP1f,wsoP2f,wsoP3f
   real(fp8), pointer, dimension(:) :: wsoP4f,wsoR6f,wsoR4f





   real(fp8), pointer,dimension(:) :: wsoADY


   real(fp8), pointer, dimension(:) :: wsol2c


! Sedimentation fluxes
   real(fp8), pointer, dimension(:) :: sdo2o,sdo3c
   real(fp8), pointer, dimension(:) :: sdn1p,sdn3n,sdn4n
   real(fp8), pointer, dimension(:) :: sdn5s,sdr6c,sdr6n,sdr6p
   real(fp8), pointer, dimension(:) :: sdr6s,sdr1c,sdr1n,sdr1p
   real(fp8), pointer, dimension(:) :: sdp1c,sdp1n,sdp1p,sdp1s
   real(fp8), pointer, dimension(:) :: sdp2c,sdp2n,sdp2p,sdp3c
   real(fp8), pointer, dimension(:) :: sdp3n,sdp3p,sdp4c,sdp4n
   real(fp8), pointer, dimension(:) :: sdp4p,sdz4c,sdz5c,sdz5n
   real(fp8), pointer, dimension(:) :: sdz5p,sdz6c,sdz6n,sdz6p
   real(fp8), pointer, dimension(:) :: sdb1c,sdb1n,sdb1p
   real(fp8), pointer, dimension(:) :: sdr4c,sdr4n,sdr4p
   real(fp8), pointer, dimension(:) :: sdr8c,sdr8n,sdr8p,sdr8s
   real(fp8), pointer, dimension(:) :: sdr2c

   real(fp8), pointer, dimension(:) :: sdr3c

   real(fp8), pointer, dimension(:) :: sdchl1, sdchl2
   real(fp8), pointer, dimension(:) :: sdchl3, sdchl4
   real(fp8), pointer, dimension(:) :: sdbioalk

   real(fp8), pointer,dimension(:) :: sdN7f,sdP1f,sdP2f,sdP3f
   real(fp8), pointer,dimension(:) :: sdP4f,sdR6f,sdR4f





   real(fp8), pointer,dimension(:) :: sdADY


   real(fp8), pointer,dimension(:) :: sdL2c


! Equivalence arrays
   real(fp8), allocatable, target, dimension(:,:) :: ccc,sccc
   real(fp8), pointer, dimension(:,:) :: cccP
   real(fp8), allocatable, target, dimension(:,:) :: wsiccc,wsoccc
   real(fp8), allocatable, target, dimension(:,:) :: sdccc
   integer, allocatable, target, dimension(:) :: itrXccc
   real(fp8), allocatable, target, dimension(:) :: ccc0
   character(4),allocatable,target,dimension(:) :: cccstr

! Abiotic variables, typically set within the physical model
   real(fp8), pointer, dimension(:) :: wnd
   real(fp8), pointer, dimension(:) :: ess,etw,epw,erw
   real(fp8), pointer, dimension(:) :: osat
   real(fp8), pointer, dimension(:) :: eir,xeps
   real(fp8), pointer, dimension(:) :: x1x
   real(fp8), pointer, dimension(:) :: irr_enh
   real(fp8), pointer, dimension(:) :: SurfaceEIR

   real(fp8) :: pco2a
   real(fp8) :: eo2mo2

! Initial condition for suspended inorganic matter
   real(fp8) :: essxr

! Light extinction of (pure) sea-water [1/m]
   real(fp8),pointer, dimension(:) :: eps0X

! Diagnostic fluxes
   real(fp8), pointer, dimension(:) :: fptnin,fptnip
   real(fp8), pointer, dimension(:) :: fptnis,fniptn!,fptnis
   real(fp8), pointer, dimension(:) :: fniptp
   real(fp8), pointer, dimension(:) :: fp1r6c,fp2r4c
   real(fp8), pointer, dimension(:) :: fp3r4c,fp4r6c,fp1rdc,fp2rdc
   real(fp8), pointer, dimension(:) :: fp3rdc,fp4rdc
   real(fp8), pointer, dimension(:) :: fp1n5s
   real(fp8), pointer, dimension(:) :: fp1o3c,fp2o3c,fp3o3c,fp4o3c
   real(fp8), pointer, dimension(:) :: fo3p1c,fo3p2c,fo3p3c,fo3p4c
   real(fp8), pointer, dimension(:) :: fn4p1n,fn4p2n,fn4p3n,fn4p4n
   real(fp8), pointer, dimension(:) :: fn3p1n,fn3p2n,fn3p3n,fn3p4n
   real(fp8), pointer, dimension(:) :: fn1p1p,fn1p2p,fn1p3p,fn1p4p

   real(fp8), pointer, dimension(:) :: fn7p1f,fn7p2f,fn7p3f,fn7p4f

   real(fp8), pointer, dimension(:) :: fb1rdc
   real(fp8), pointer, dimension(:) :: fr1nin
   real(fp8), pointer, dimension(:) :: fr1n1p,fp1z5c
   real(fp8), pointer, dimension(:) :: fp2z5c,fp3z5c,fp4z5c,fz6z5c
   real(fp8), pointer, dimension(:) :: fb1z5c,fz5r6c
   real(fp8), pointer, dimension(:) :: fz5z5c
   real(fp8), pointer, dimension(:) :: fp2z6c
   real(fp8), pointer, dimension(:) :: fp3z6c
   real(fp8), pointer, dimension(:) :: fb1z6c,fz6r4c
   real(fp8), pointer, dimension(:) :: fz6z6c
   real(fp8), pointer, dimension(:) :: fp1z4c,fp2z4c,fp3z4c,fp4z4c
   real(fp8), pointer, dimension(:) :: fz6z4c,fb1z4c,fz5z4c,fz4z4c
   real(fp8), pointer, dimension(:) :: fr6z4c,fz4r8c
   real(fp8), pointer, dimension(:) :: fb1o3c
   real(fp8), pointer, dimension(:) :: fz4o3c,fz5o3c,fz6o3c,fb1n1p
   real(fp8), pointer, dimension(:) :: fz4n1p,fz5n1p,fz6n1p,fb1nin
   real(fp8), pointer, dimension(:) :: fz4nin,fz5nin,fz6nin

   real(fp8), pointer, dimension(:) :: fPXZXc,fBXZXc,fRXZXc,PResp
   real(fp8), pointer, dimension(:) :: netB,ZResp,fBXRDc,BResp,nitrN
   real(fp8), pointer, dimension(:) :: fPXRPc,fPXRDc,fZXRPc
   real(fp8), pointer, dimension(:) :: fZXRDc,fRDBXc

   real(fp8), pointer, dimension(:) :: fRPBXc



   real(fp8), pointer, dimension(:) :: fN1PXp,fN3PXn,fN4PXn,fN5PXs
   real(fp8), pointer, dimension(:) :: fBXN1p,fBXNIn,fRDN1p,fRDNIn

   real(fp8), pointer, dimension(:) :: fN7PXf



   real(fp8), pointer, dimension(:) :: fl2o3c, fo3l2c, rainR


! Diagnostic variables
   real(fp8), pointer, dimension(:) :: chl
   real(fp8), pointer, dimension(:) :: netp1,netp2,netp3,netPP
   real(fp8), pointer, dimension(:) :: netp4,netb1
   real(fp8), pointer, dimension(:) :: BGE
   real(fp8), pointer, dimension(:) :: n3nprod,n4nprod
   real(fp8), pointer, dimension(:) :: z4n

! Diagnostic rates (resting respiration)
   real(fp8) :: srsp4,srsp3,srsp2,srsp1

! Quatas
   real(fp8), pointer, dimension(:) :: qnp1c
   real(fp8), pointer, dimension(:) :: qpp1c,qsp1c,qnp2c,qpp2c
   real(fp8), pointer, dimension(:) :: qnp3c,qpp3c,qnp4c,qpp4c

   real(fp8), pointer, dimension(:) :: qfP1c,qfP2c,qfP3c,qfP4c

   real(fp8), pointer, dimension(:) :: qnz5c,qpz5c,qpz6c,qnz6c
   real(fp8), pointer, dimension(:) :: qnb1c,qpb1c,qnr6c,qpr6c
   real(fp8), pointer, dimension(:) :: qsr6c
   real(fp8), pointer, dimension(:) :: qnr4c,qpr4c
   real(fp8), pointer, dimension(:) :: qnr8c,qpr8c
   real(fp8), pointer, dimension(:) :: chlc1, chlc2, chlc3, chlc4

! Update switches
   integer :: iswp1X,iswp2X,iswp3X,iswp4X
   integer :: iswz4X,iswz5X,iswz6X,iswb1x
   integer :: iswco2X,iswbioalk
   integer :: iswcal

! Carbonate system (allocate as non transported ie in cco array)
   real(fp8), pointer, dimension(:) :: phx,fairmg,Cco2
   real(fp8), pointer, dimension(:) :: pco2w
   real(fp8), pointer, dimension(:) :: tota
   real(fp8), pointer, dimension(:) :: CarbA,Bicarb,Carb
   real(fp8), pointer, dimension(:) :: Om_cal,Om_arg

! Budgeting arrays benthic and pelagic for key variables
! Carbon, Nitrate, Phosphate, Silicate
   real(fp8) :: pel_budget(1:9,1:4)

! real(fp8),pointer, dimension(:) :: denitflx
   real(fp8),pointer, dimension(:) :: preyint

! JBasal bioturbation rate
   real(fp8) :: eturX

! For time varying co2 flux in POLCOMS (need to move this to coupler!!!)
! integer ::irec
! real(fp8) :: pco2a1,pco2a2

! Masks for mapping of ncdf variables on structured
! 3D ncdf-geometry and 3D arrays with Depth levels the grid:
! (have to be associated or allocated, usually in GCM)
   logical,dimension(:,:,:),pointer :: Water
   logical,dimension(:,:),pointer :: SeaSurface
   integer,dimension(:,:),pointer :: SeaFloor
   real(fp8),dimension(:,:,:),pointer :: BoxDepth ! cell centre depth
   real(fp8),dimension(:,:,:),pointer :: BoxFaces ! cell faces depth
   real(fp8),dimension(:,:),pointer :: Bathymetry ! absoulte depth
   integer,dimension(:,:,:),pointer :: ijk2n ! mapping of 3d to ERSEM-1d
   integer,dimension(:,:),pointer :: ij2nb ! mapping of 3d to benthic 1d
   integer,dimension(:),pointer :: n2i ! mapping of ERSEM-1d to 3d
   integer,dimension(:),pointer :: n2j ! mapping of ERSEM-1d to 3d
   integer,dimension(:),pointer :: n2k ! mapping of ERSEM-1d to 3d
   integer,dimension(:),pointer :: nb2i ! mapping of benthic 1d to 3d
   integer,dimension(:),pointer :: nb2j ! mapping of benthic 1d to 3d

   real(fp8),pointer, dimension(:) :: pvol,pdepth,parea

   real(fp8) :: fsecs ! day fraction in seconds

   integer,dimension(:),pointer :: benthicIndex

   integer, pointer, dimension(:) :: ibenX,lowerX

   integer :: n_upperX
   real(fp8),pointer, dimension(:) ::zenithA,elon,elat,iopADS,iopBBS
! For irradiance calculations
   integer :: yday
! Extra switches
! Switch for alkalinity parametrisation (1=TA(S)-old;
! 2=TA(S)-Millero,3=TA(T,S)-Millero,4=TA(T,S)-Lee,5=prognostic)
   integer :: iswTALK
! Redundant. Should be set equal to 2.
   integer :: iswASFLUX
! Zooplankton overwintering switch
   integer :: Z4_OW_SW
! real(fp8), pointer, dimension(:) :: afternoon_light, noon_light
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
! James Clark (PML) Jan 2014 - Created module pelagic_variables, and
! moved data members in from module global_declarations.
!
!EOP
!-----------------------------------------------------------------------
   contains
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Pelagic variable allocations
!
! !DESCRIPTION:
! This routine allocates and initialises all pelagic state variables
! and related arrays as well as pelagic diagnostics and arrays descriping
! the model geometry.\\[\baselineskip]
!
! !INTERFACE:
   subroutine do_pelagic_variable_allocations
!
! !USES:
   use allocation_helpers, only: allocerr, allocateErsemDouble, &
      allocateErsemInteger
!
! !LOCAL VARIABLES:
   integer :: ialloc
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
! James Clark (PML) Feb 2014: Moved pelagic allocation statements from
! global_allocations.
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Allocate main ERSEM arrays.
   allocate (itrXccc(i_state),stat=ialloc)
   call allocerr ('itrXccc',i_state,ialloc)
   if ( ialloc.ne.0 ) then
       !JCflush(eDbgU)
       !JCflush(eLogU)
      call MPI_abort(ialloc)
   end if
   ! Ensure, for the case that N_Comp is zero, that the lower bound
   ! is also zero, otherwise array bound checking generates errors.
   lb = min(1,N_Comp)
   allocate (ccc(lb:N_Comp,i_state),stat=ialloc)
   call allocerr ('ccc',N_Comp-lb+1,i_state,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then
      call MPI_abort(ialloc)
   end if
   ! Sink/source terms
   allocate (sccc(lb:N_Comp,i_state),stat=ialloc)
   call allocerr ('sccc',N_Comp-lb+1,i_state,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then
      call MPI_abort(ialloc)
   end if
   ! Asymptopic minimal concentration
   allocate (ccc0(i_state),stat=ialloc)
   call allocerr ('ccc0',i_state,0,0,0,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then
      call MPI_abort(ialloc)
   end if
   ! Vertical in fluxes
   allocate (wsiccc(lb:N_Comp,i_state),stat=ialloc)
   call allocerr ('wsiccc',N_Comp-lb+1,i_state,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then
      call MPI_abort(ialloc)
   end if
   ! Vertical out fluxes
   allocate (wsoccc(lb:N_Comp,i_state),stat=ialloc)
   call allocerr ('wsoccc',N_Comp-lb+1,i_state,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then
      call MPI_abort(ialloc)
   end if
   ! Sedimentation fluxes
   allocate (sdccc(lb:N_Comp,i_state),stat=ialloc)
   call allocerr ('sdccc',N_Comp-lb+1,i_state,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then
      call MPI_abort(ialloc)
   end if
   ! String identifiers
   allocate (cccstr(i_state),stat=ialloc)
   call allocerr ('cccstr',i_state,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then
      call MPI_abort(ialloc)
   end if
   ! Position ids
   iO2o = 1 ; cccstr( iO2o ) = 'O2o'
   iO3c = 2 ; cccstr( iO3c ) = 'O3c'
   iN1p = 3 ; cccstr( iN1p ) = 'N1p'
   iN3n = 4 ; cccstr( iN3n ) = 'N3n'
   iN4n = 5 ; cccstr( iN4n ) = 'N4n'
   iN5s = 6 ; cccstr( iN5s ) = 'N5s'
   iR6c = 7 ; cccstr( iR6c ) = 'R6c'
   iR6n = 8 ; cccstr( iR6n ) = 'R6n'
   iR6p = 9 ; cccstr( iR6p ) = 'R6p'
   iR6s = 10 ; cccstr( iR6s ) = 'R6s'
   iR1c = 11 ; cccstr( iR1c ) = 'R1c'
   iR1n = 12 ; cccstr( iR1n ) = 'R1n'
   iR1p = 13 ; cccstr( iR1p ) = 'R1p'
   iP1c = 14 ; cccstr( iP1c ) = 'P1c'
   iP1n = 15 ; cccstr( iP1n ) = 'P1n'
   iP1p = 16 ; cccstr( iP1p ) = 'P1p'
   iP1s = 17 ; cccstr( iP1s ) = 'P1s'
   iP2c = 18 ; cccstr( iP2c ) = 'P2c'
   iP2n = 19 ; cccstr( iP2n ) = 'P2n'
   iP2p = 20 ; cccstr( iP2p ) = 'P2p'
   iP3c = 21 ; cccstr( iP3c ) = 'P3c'
   iP3n = 22 ; cccstr( iP3n ) = 'P3n'
   iP3p = 23 ; cccstr( iP3p ) = 'P3p'
   iP4c = 24 ; cccstr( iP4c ) = 'P4c'
   iP4n = 25 ; cccstr( iP4n ) = 'P4n'
   iP4p = 26 ; cccstr( iP4p ) = 'P4p'
   iZ4c = 27 ; cccstr( iZ4c ) = 'Z4c'
   iZ5c = 28 ; cccstr( iZ5c ) = 'Z5c'
   iZ5n = 29 ; cccstr( iZ5n ) = 'Z5n'
   iZ5p = 30 ; cccstr( iZ5p ) = 'Z5p'
   iZ6c = 31 ; cccstr( iZ6c ) = 'Z6c'
   iZ6n = 32 ; cccstr( iZ6n ) = 'Z6n'
   iZ6p = 33 ; cccstr( iZ6p ) = 'Z6p'
   iB1c = 34 ; cccstr( iB1c ) = 'B1c'
   iB1n = 35 ; cccstr( iB1n ) = 'B1n'
   iB1p = 36 ; cccstr( iB1p ) = 'B1p'
   iR4c = 37 ; cccstr( iR4c ) = 'R4c'
   iR4n = 38 ; cccstr( iR4n ) = 'R4n'
   iR4p = 39 ; cccstr( iR4p ) = 'R4p'
   iR8c = 40 ; cccstr( iR8c ) = 'R8c'
   iR8n = 41 ; cccstr( iR8n ) = 'R8n'
   iR8p = 42 ; cccstr( iR8p ) = 'R8p'
   iR8s = 43 ; cccstr( iR8s ) = 'R8s'
   iR2c = 44 ; cccstr( iR2c ) = 'R2c'
   iChl1= 45 ; cccstr( iChl1) = 'Chl1'
   iChl2= 46 ; cccstr( iChl2) = 'Chl2'
   iChl3= 47 ; cccstr( iChl3) = 'Chl3'
   iChl4= 48 ; cccstr( iChl4) = 'Chl4'
   ibioalk=49; cccstr(ibioalk)= 'bioalk'
   iL2c = CALC_STATE; cccstr( iL2c ) = 'L2c'
   iN7f= IRON_STATE ; cccstr( iN7f ) = 'N7f'
   iP1f= IRON_STATE+1 ; cccstr( iP1f ) = 'P1f'
   iP2f= IRON_STATE+2 ; cccstr( iP2f ) = 'P2f'
   iP3f= IRON_STATE+3 ; cccstr( iP3f ) = 'P3f'
   iP4f= IRON_STATE+4 ; cccstr( iP4f ) = 'P4f'
   iR6f= IRON_STATE+5 ; cccstr( iR6f ) = 'R6f'
   iR4f= IRON_STATE+6 ; cccstr( iR4f ) = 'R4f'
   iADY= ADY_STATE ; cccstr( iADY ) = 'ADY'
   iR3c = DOCDYN_STATE ; cccstr( iR3c ) = 'R3c'
   ! Transport switches - pelagic
   itrXccc=0
   itrXO2o => itrXccc( iO2o )
   itrXO3c => itrXccc( iO3c )
   itrXN1p => itrXccc( iN1p )
   itrXN3n => itrXccc( iN3n )
   itrXN4n => itrXccc( iN4n )
   itrXN5s => itrXccc( iN5s )
   itrXR6c => itrXccc( iR6c )
   itrXR6n => itrXccc( iR6n )
   itrXR6p => itrXccc( iR6p )
   itrXR6s => itrXccc( iR6s )
   itrXR1c => itrXccc( iR1c )
   itrXR1n => itrXccc( iR1n )
   itrXR1p => itrXccc( iR1p )
   itrXP1c => itrXccc( iP1c )
   itrXP1n => itrXccc( iP1n )
   itrXP1p => itrXccc( iP1p )
   itrXP1s => itrXccc( iP1s )
   itrXP2c => itrXccc( iP2c )
   itrXP2n => itrXccc( iP2n )
   itrXP2p => itrXccc( iP2p )
   itrXP3c => itrXccc( iP3c )
   itrXP3n => itrXccc( iP3n )
   itrXP3p => itrXccc( iP3p )
   itrXP4c => itrXccc( iP4c )
   itrXP4n => itrXccc( iP4n )
   itrXP4p => itrXccc( iP4p )
   itrXZ4c => itrXccc( iZ4c )
   itrXZ5c => itrXccc( iZ5c )
   itrXZ5n => itrXccc( iZ5n )
   itrXZ5p => itrXccc( iZ5p )
   itrXZ6c => itrXccc( iZ6c )
   itrXZ6n => itrXccc( iZ6n )
   itrXZ6p => itrXccc( iZ6p )
   itrXB1c => itrXccc( iB1c )
   itrXB1n => itrXccc( iB1n )
   itrXB1p => itrXccc( iB1p )
   itrXR4c => itrXccc( iR4c )
   itrXR4n => itrXccc( iR4n )
   itrXR4p => itrXccc( iR4p )
   itrXR8c => itrXccc( iR8c )
   itrXR8n => itrXccc( iR8n )
   itrXR8p => itrXccc( iR8p )
   itrXR8s => itrXccc( iR8s )
   itrXR2c => itrXccc( iR2c )
   itrXChl1=> itrXccc( iChl1 )
   itrXChl2=> itrXccc( iChl2 )
   itrXChl3=> itrXccc( iChl3 )
   itrXChl4=> itrXccc( iChl4 )
   itrXL2c => itrXccc( iL2c )
   itrXbioalk=> itrXccc( ibioalk )
   itrXN7f=> itrXccc( iN7f )
   itrXP1f=> itrXccc( iP1f )
   itrXP2f=> itrXccc( iP2f )
   itrXP3f=> itrXccc( iP3f )
   itrXP4f=> itrXccc( iP4f )
   itrXR6f=> itrXccc( iR6f )
   itrXR4f=> itrXccc( iR4f )
   itrXADY=> itrXccc( iADY ) ; itrXADY = 22
   itrXR3c => itrXccc( iR3c ) ; itrXR3c = 22
   ! For access to individual state vars -- pelagic
   O2o => ccc(:, iO2o ) ; O2o0 => ccc0( iO2o )
   O3c => ccc(:, iO3c ) ; O3c0 => ccc0( iO3c )
   N1p => ccc(:, iN1p ) ; N1p0 => ccc0( iN1p )
   N3n => ccc(:, iN3n ) ; N3n0 => ccc0( iN3n )
   N4n => ccc(:, iN4n ) ; N4n0 => ccc0( iN4n )
   N5s => ccc(:, iN5s ) ; N5s0 => ccc0( iN5s )
   R6c => ccc(:, iR6c ) ; R6c0 => ccc0( iR6c )
   R6n => ccc(:, iR6n ) ; R6n0 => ccc0( iR6n )
   R6p => ccc(:, iR6p ) ; R6p0 => ccc0( iR6p )
   R6s => ccc(:, iR6s ) ; R6s0 => ccc0( iR6s )
   R1c => ccc(:, iR1c ) ; R1c0 => ccc0( iR1c )
   R1n => ccc(:, iR1n ) ; R1n0 => ccc0( iR1n )
   R1p => ccc(:, iR1p ) ; R1p0 => ccc0( iR1p )
   P1c => ccc(:, iP1c ) ; P1c0 => ccc0( iP1c )
   P1n => ccc(:, iP1n ) ; P1n0 => ccc0( iP1n )
   P1p => ccc(:, iP1p ) ; P1p0 => ccc0( iP1p )
   P1s => ccc(:, iP1s ) ; P1s0 => ccc0( iP1s )
   P2c => ccc(:, iP2c ) ; P2c0 => ccc0( iP2c )
   P2n => ccc(:, iP2n ) ; P2n0 => ccc0( iP2n )
   P2p => ccc(:, iP2p ) ; P2p0 => ccc0( iP2p )
   P3c => ccc(:, iP3c ) ; P3c0 => ccc0( iP3c )
   P3n => ccc(:, iP3n ) ; P3n0 => ccc0( iP3n )
   P3p => ccc(:, iP3p ) ; P3p0 => ccc0( iP3p )
   P4c => ccc(:, iP4c ) ; P4c0 => ccc0( iP4c )
   P4n => ccc(:, iP4n ) ; P4n0 => ccc0( iP4n )
   P4p => ccc(:, iP4p ) ; P4p0 => ccc0( iP4p )
   Z4c => ccc(:, iZ4c ) ; Z4c0 => ccc0( iZ4c )
   Z5c => ccc(:, iZ5c ) ; Z5c0 => ccc0( iZ5c )
   Z5n => ccc(:, iZ5n ) ; Z5n0 => ccc0( iZ5n )
   Z5p => ccc(:, iZ5p ) ; Z5p0 => ccc0( iZ5p )
   Z6c => ccc(:, iZ6c ) ; Z6c0 => ccc0( iZ6c )
   Z6n => ccc(:, iZ6n ) ; Z6n0 => ccc0( iZ6n )
   Z6p => ccc(:, iZ6p ) ; Z6p0 => ccc0( iZ6p )
   B1c => ccc(:, iB1c ) ; B1c0 => ccc0( iB1c )
   B1n => ccc(:, iB1n ) ; B1n0 => ccc0( iB1n )
   B1p => ccc(:, iB1p ) ; B1p0 => ccc0( iB1p )
   R4c => ccc(:, iR4c ) ; R4c0 => ccc0( iR4c )
   R4n => ccc(:, iR4n ) ; R4n0 => ccc0( iR4n )
   R4p => ccc(:, iR4p ) ; R4p0 => ccc0( iR4p )
   R8c => ccc(:, iR8c ) ; R8c0 => ccc0( iR8c )
   R8n => ccc(:, iR8n ) ; R8n0 => ccc0( iR8n )
   R8p => ccc(:, iR8p ) ; R8p0 => ccc0( iR8p )
   R8s => ccc(:, iR8s ) ; R8s0 => ccc0( iR8s )
   R2c => ccc(:, iR2c ) ; R2c0 => ccc0( iR2c )
   Chl1=> ccc(:, iChl1 ) ; Chl10 => ccc0( iChl1 )
   Chl2=> ccc(:, iChl2 ) ; Chl20 => ccc0( iChl2 )
   Chl3=> ccc(:, iChl3 ) ; Chl30 => ccc0( iChl3 )
   Chl4=> ccc(:, iChl4 ) ; Chl40 => ccc0( iChl4 )
   bioalk=>ccc(:,ibioalk); bioalk0 => ccc0( ibioalk )
   L2c => ccc(:, iL2c ) ; L2c0 => ccc0( iL2c )
   N7f=> ccc(:, iN7f ) ; N7f0 => ccc0( iN7f )
   P1f=> ccc(:, iP1f ) ; P1f0 => ccc0( iP1f )
   P2f=> ccc(:, iP2f ) ; P2f0 => ccc0( iP2f )
   P3f=> ccc(:, iP3f ) ; P3f0 => ccc0( iP3f )
   P4f=> ccc(:, iP4f ) ; P4f0 => ccc0( iP4f )
   R6f=> ccc(:, iR6f ) ; R6f0 => ccc0( iR6f )
   R4f=> ccc(:, iR4f ) ; R4f0 => ccc0( iR4f )
   ADY=> ccc(:, iADY ) ; ADY0 => ccc0( iADY )
   R3c => ccc(:, iR3c ) ; R3c0 => ccc0( iR3c )
   ! Initialised to 0
   ccc = 0._fp8
   ccc0 = 0._fp8
   ! Sink/source terms -- pelagic
   sO2o => sccc(:, iO2o )
   sO3c => sccc(:, iO3c )
   sN1p => sccc(:, iN1p )
   sN3n => sccc(:, iN3n )
   sN4n => sccc(:, iN4n )
   sN5s => sccc(:, iN5s )
   sR6c => sccc(:, iR6c )
   sR6n => sccc(:, iR6n )
   sR6p => sccc(:, iR6p )
   sR6s => sccc(:, iR6s )
   sR1c => sccc(:, iR1c )
   sR1n => sccc(:, iR1n )
   sR1p => sccc(:, iR1p )
   sP1c => sccc(:, iP1c )
   sP1n => sccc(:, iP1n )
   sP1p => sccc(:, iP1p )
   sP1s => sccc(:, iP1s )
   sP2c => sccc(:, iP2c )
   sP2n => sccc(:, iP2n )
   sP2p => sccc(:, iP2p )
   sP3c => sccc(:, iP3c )
   sP3n => sccc(:, iP3n )
   sP3p => sccc(:, iP3p )
   sP4c => sccc(:, iP4c )
   sP4n => sccc(:, iP4n )
   sP4p => sccc(:, iP4p )
   sZ4c => sccc(:, iZ4c )
   sZ5c => sccc(:, iZ5c )
   sZ5n => sccc(:, iZ5n )
   sZ5p => sccc(:, iZ5p )
   sZ6c => sccc(:, iZ6c )
   sZ6n => sccc(:, iZ6n )
   sZ6p => sccc(:, iZ6p )
   sB1c => sccc(:, iB1c )
   sB1n => sccc(:, iB1n )
   sB1p => sccc(:, iB1p )
   sR4c => sccc(:, iR4c )
   sR4n => sccc(:, iR4n )
   sR4p => sccc(:, iR4p )
   sR8c => sccc(:, iR8c )
   sR8n => sccc(:, iR8n )
   sR8p => sccc(:, iR8p )
   sR8s => sccc(:, iR8s )
   sR2c => sccc(:, iR2c )
   sChl1=> sccc(:, iChl1 )
   sChl2=> sccc(:, iChl2 )
   sChl3=> sccc(:, iChl3 )
   sChl4=> sccc(:, iChl4 )
   sBioalk=> sccc(:, ibioalk )
   sl2c => sccc(:, iL2c )
   sN7f=> sccc(:, iN7f )
   sP1f=> sccc(:, iP1f )
   sP2f=> sccc(:, iP2f )
   sP3f=> sccc(:, iP3f )
   sP4f=> sccc(:, iP4f )
   sR6f=> sccc(:, iR6f )
   sR4f=> sccc(:, iR4f )
   sADY=> sccc(:, iADY )
   sR3c => sccc(:, iR3c )
   sccc = 0._fp8
   ! Sedimentation rates - pelagic
   sdO2o => sdccc(:, iO2o )
   sdO3c => sdccc(:, iO3c )
   sdN1p => sdccc(:, iN1p )
   sdN3n => sdccc(:, iN3n )
   sdN4n => sdccc(:, iN4n )
   sdN5s => sdccc(:, iN5s )
   sdR6c => sdccc(:, iR6c )
   sdR6n => sdccc(:, iR6n )
   sdR6p => sdccc(:, iR6p )
   sdR6s => sdccc(:, iR6s )
   sdR1c => sdccc(:, iR1c )
   sdR1n => sdccc(:, iR1n )
   sdR1p => sdccc(:, iR1p )
   sdP1c => sdccc(:, iP1c )
   sdP1n => sdccc(:, iP1n )
   sdP1p => sdccc(:, iP1p )
   sdP1s => sdccc(:, iP1s )
   sdP2c => sdccc(:, iP2c )
   sdP2n => sdccc(:, iP2n )
   sdP2p => sdccc(:, iP2p )
   sdP3c => sdccc(:, iP3c )
   sdP3n => sdccc(:, iP3n )
   sdP3p => sdccc(:, iP3p )
   sdP4c => sdccc(:, iP4c )
   sdP4n => sdccc(:, iP4n )
   sdP4p => sdccc(:, iP4p )
   sdZ4c => sdccc(:, iZ4c )
   sdZ5c => sdccc(:, iZ5c )
   sdZ5n => sdccc(:, iZ5n )
   sdZ5p => sdccc(:, iZ5p )
   sdZ6c => sdccc(:, iZ6c )
   sdZ6n => sdccc(:, iZ6n )
   sdZ6p => sdccc(:, iZ6p )
   sdB1c => sdccc(:, iB1c )
   sdB1n => sdccc(:, iB1n )
   sdB1p => sdccc(:, iB1p )
   sdR4c => sdccc(:, iR4c )
   sdR4n => sdccc(:, iR4n )
   sdR4p => sdccc(:, iR4p )
   sdR8c => sdccc(:, iR8c )
   sdR8n => sdccc(:, iR8n )
   sdR8p => sdccc(:, iR8p )
   sdR8s => sdccc(:, iR8s )
   sdR2c => sdccc(:, iR2c )
   sdChl1=> sdccc(:, iChl1 )
   sdChl2=> sdccc(:, iChl2 )
   sdChl3=> sdccc(:, iChl3 )
   sdChl4=> sdccc(:, iChl4 )
   sdbioalk=> sdccc(:, ibioalk )
   sdl2c => sdccc(:, iL2c )
   sdN7f=> sdccc(:, iN7f )
   sdP1f=> sdccc(:, iP1f )
   sdP2f=> sdccc(:, iP2f )
   sdP3f=> sdccc(:, iP3f )
   sdP4f=> sdccc(:, iP4f )
   sdR6f=> sdccc(:, iR6f )
   sdR4f=> sdccc(:, iR4f )
   sdADY=> sdccc(:, iADY )
   sdR3c => sdccc(:, iR3c )
   sdccc=0._fp8
   ! "Into" transport fluxes -- pelagic
   wsiO2o => wsiccc(:, iO2o )
   wsiO3c => wsiccc(:, iO3c )
   wsiN1p => wsiccc(:, iN1p )
   wsiN3n => wsiccc(:, iN3n )
   wsiN4n => wsiccc(:, iN4n )
   wsiN5s => wsiccc(:, iN5s )
   wsiR6c => wsiccc(:, iR6c )
   wsiR6n => wsiccc(:, iR6n )
   wsiR6p => wsiccc(:, iR6p )
   wsiR6s => wsiccc(:, iR6s )
   wsiR1c => wsiccc(:, iR1c )
   wsiR1n => wsiccc(:, iR1n )
   wsiR1p => wsiccc(:, iR1p )
   wsiP1c => wsiccc(:, iP1c )
   wsiP1n => wsiccc(:, iP1n )
   wsiP1p => wsiccc(:, iP1p )
   wsiP1s => wsiccc(:, iP1s )
   wsiP2c => wsiccc(:, iP2c )
   wsiP2n => wsiccc(:, iP2n )
   wsiP2p => wsiccc(:, iP2p )
   wsiP3c => wsiccc(:, iP3c )
   wsiP3n => wsiccc(:, iP3n )
   wsiP3p => wsiccc(:, iP3p )
   wsiP4c => wsiccc(:, iP4c )
   wsiP4n => wsiccc(:, iP4n )
   wsiP4p => wsiccc(:, iP4p )
   wsiZ4c => wsiccc(:, iZ4c )
   wsiZ5c => wsiccc(:, iZ5c )
   wsiZ5n => wsiccc(:, iZ5n )
   wsiZ5p => wsiccc(:, iZ5p )
   wsiZ6c => wsiccc(:, iZ6c )
   wsiZ6n => wsiccc(:, iZ6n )
   wsiZ6p => wsiccc(:, iZ6p )
   wsiB1c => wsiccc(:, iB1c )
   wsiB1n => wsiccc(:, iB1n )
   wsiB1p => wsiccc(:, iB1p )
   wsiR4c => wsiccc(:, iR4c )
   wsiR4n => wsiccc(:, iR4n )
   wsiR4p => wsiccc(:, iR4p )
   wsiR8c => wsiccc(:, iR8c )
   wsiR8n => wsiccc(:, iR8n )
   wsiR8p => wsiccc(:, iR8p )
   wsiR8s => wsiccc(:, iR8s )
   wsiR2c => wsiccc(:, iR2c )
   wsiChl1=> wsiccc(:, iChl1 )
   wsiChl2=> wsiccc(:, iChl2 )
   wsiChl3=> wsiccc(:, iChl3 )
   wsiChl4=> wsiccc(:, iChl4 )
   wsibioalk => wsiccc(:, ibioalk )
   wsil2c => wsiccc(:, iL2c )
   wsiN7f => wsiccc(:, iN7f )
   wsiP1f => wsiccc(:, iP1f )
   wsiP2f => wsiccc(:, iP2f )
   wsiP3f => wsiccc(:, iP3f )
   wsiP4f => wsiccc(:, iP4f )
   wsiR6f => wsiccc(:, iR6f )
   wsiR4f => wsiccc(:, iR4f )
   wsiADY => wsiccc(:, iADY )
   wsiR3c => wsiccc(:, iR3c )
   wsiccc=0._fp8
   ! Out-of transport fluxes -- pelagic
   wsoO2o => wsoccc(:, iO2o )
   wsoO3c => wsoccc(:, iO3c )
   wsoN1p => wsoccc(:, iN1p )
   wsoN3n => wsoccc(:, iN3n )
   wsoN4n => wsoccc(:, iN4n )
   wsoN5s => wsoccc(:, iN5s )
   wsoR6c => wsoccc(:, iR6c )
   wsoR6n => wsoccc(:, iR6n )
   wsoR6p => wsoccc(:, iR6p )
   wsoR6s => wsoccc(:, iR6s )
   wsoR1c => wsoccc(:, iR1c )
   wsoR1n => wsoccc(:, iR1n )
   wsoR1p => wsoccc(:, iR1p )
   wsoP1c => wsoccc(:, iP1c )
   wsoP1n => wsoccc(:, iP1n )
   wsoP1p => wsoccc(:, iP1p )
   wsoP1s => wsoccc(:, iP1s )
   wsoP2c => wsoccc(:, iP2c )
   wsoP2n => wsoccc(:, iP2n )
   wsoP2p => wsoccc(:, iP2p )
   wsoP3c => wsoccc(:, iP3c )
   wsoP3n => wsoccc(:, iP3n )
   wsoP3p => wsoccc(:, iP3p )
   wsoP4c => wsoccc(:, iP4c )
   wsoP4n => wsoccc(:, iP4n )
   wsoP4p => wsoccc(:, iP4p )
   wsoZ4c => wsoccc(:, iZ4c )
   wsoZ5c => wsoccc(:, iZ5c )
   wsoZ5n => wsoccc(:, iZ5n )
   wsoZ5p => wsoccc(:, iZ5p )
   wsoZ6c => wsoccc(:, iZ6c )
   wsoZ6n => wsoccc(:, iZ6n )
   wsoZ6p => wsoccc(:, iZ6p )
   wsoB1c => wsoccc(:, iB1c )
   wsoB1n => wsoccc(:, iB1n )
   wsoB1p => wsoccc(:, iB1p )
   wsoR4c => wsoccc(:, iR4c )
   wsoR4n => wsoccc(:, iR4n )
   wsoR4p => wsoccc(:, iR4p )
   wsoR8c => wsoccc(:, iR8c )
   wsoR8n => wsoccc(:, iR8n )
   wsoR8p => wsoccc(:, iR8p )
   wsoR8s => wsoccc(:, iR8s )
   wsoR2c => wsoccc(:, iR2c )
   wsoChl1=> wsoccc(:, iChl1 )
   wsoChl2=> wsoccc(:, iChl2 )
   wsoChl3=> wsoccc(:, iChl3 )
   wsoChl4=> wsoccc(:, iChl4 )
   wsobioalk => wsoccc(:, ibioalk )
   wsol2c => wsoccc(:, iL2c )
   wsoN7f=> wsoccc(:, iN7f )
   wsoP1f=> wsoccc(:, iP1f )
   wsoP2f=> wsoccc(:, iP2f )
   wsoP3f=> wsoccc(:, iP3f )
   wsoP4f=> wsoccc(:, iP4f )
   wsoR6f=> wsoccc(:, iR6f )
   wsoR4f=> wsoccc(:, iR4f )
   wsoADY=> wsoccc(:, iADY )
   wsoR3c => wsoccc(:, iR3c )
   wsoccc =0._fp8
   ! Additional ERSEM state variables needed by the model:
   ! Flag for seafloor points and benthic sub-model
   call allocateErsemInteger(ibenX,'ibenX',lb,N_Comp)
   ! Number of grid cell below
   call allocateErsemInteger(lowerX,'lowerX',lb,N_Comp)
   ! Indices of benthic boxes connected to sea floor:
   call allocateErsemInteger(benthicIndex,'benthicIndex',lb,N_Comp)
   ! grid cell volume
   call allocateErsemDouble(pvol,'pvol',lb,N_Comp)
   ! grid cell area
   call allocateErsemDouble(parea,'parea',lb,N_Comp)
   ! grid cell thickness
   call allocateErsemDouble(pdepth,'pdepth',lb,N_Comp)
   ! Temeperature
   call allocateErsemDouble(etw,'ETW',lb,N_Comp)
   ! Salinity
   call allocateErsemDouble(x1x,'x1x',lb,N_Comp)
   ! Pressure (Pa)
   call allocateErsemDouble(epw,'epw',lb,N_Comp)
   ! Density (mg/m^3)
   call allocateErsemDouble(erw,'erw',lb,N_Comp)
   ! Light
   call allocateErsemDouble(EIR,'EIR',lb,N_Comp)
   ! silt
   call allocateErsemDouble(ESS,'ESS',lb,N_Comp)
   ! extinction coefficient
   call allocateErsemDouble(xeps,'xeps',lb,N_Comp)
   ! oxygen saturation
   call allocateErsemDouble(osat,'osat',lb,N_Comp)
   ! background extinction coefficient
   call allocateErsemDouble(eps0X,'eps0X',lb,N_Comp)
   ! pseudo state (fixed dependency on Z4c), for diagnostics and mass balance
   call allocateErsemDouble(z4n,'z4n',lb,N_Comp)
   ! for gas_dynamics
   ! Wind?, see gas_dynamics
   call allocateErsemDouble(wnd,'wnd',lb,n_upperX)
   ! Surface irradiation
   call allocateErsemDouble(SurfaceEIR,'SurfaceEIR',lb,n_upperX)
   ! Calite saturation
   call allocateErsemDouble(Om_cal,'Om_cal',lb,N_Comp)
   ! Om_arg
   call allocateErsemDouble(Om_arg,'Om_arg',lb,N_Comp)
   ! PH of water, used in bacteria,ben_nut_oldb and gas_dynamics
   call allocateErsemDouble(phx,'phx',lb,N_Comp)
   !Ratios
   ! P1 N to C ratio, used in benthos,ben_zoo,pel,mesozoo,microzoo and primary_producers
   call allocateErsemDouble(qNP1c,'qNP1c',lb,N_Comp)
   ! P1 P to C ratio, used in benthos,ben_zoo,pel,mesozoo,microzoo and primary_producers
   call allocateErsemDouble(qPP1c,'qPP1c',lb,N_Comp)
   ! P1 S to C ratio, used in benthos,ben_zoo,pel,mesozoo,microzoo and primary_producers
   call allocateErsemDouble(qSP1c,'qSP1c',lb,N_Comp)
   ! P2 N to C ratio, used in benthos,ben_zoo,pel,mesozoo,microzoo and primary_producers
   call allocateErsemDouble(qNP2c,'qNP2c',lb,N_Comp)
   ! P2 P to C ratio, used in benthos,ben_zoo,pel,mesozoo,microzoo and primary_producers
   call allocateErsemDouble(qPP2c,'qPP2c',lb,N_Comp)
   ! P3 N to C ratio, used in benthos,ben_zoo,pel,mesozoo,microzoo and primary_producers
   call allocateErsemDouble(qNP3c,'qNP3c',lb,N_Comp)
   ! P3 P to C ratio, used in benthos,ben_zoo,pel,mesozoo,microzoo and primary_producers
   call allocateErsemDouble(qPP3c,'qPP3c',lb,N_Comp)
   ! P4 N to C ratio, used in benthos,pel,mesozoo,microzoo and primary_producers
   call allocateErsemDouble(qNP4c,'qNP4c',lb,N_Comp)
   ! P4 P to C ratio, used in benthos,pel,mesozoo,microzoo and primary_producers
   call allocateErsemDouble(qPP4c,'qPP4c',lb,N_Comp)
   ! Z5 N to C ratio, used in pel,mesozoo and microzoo
   call allocateErsemDouble(qNZ5c,'qNZ5c',lb,N_Comp)
   ! Z5 P to C ratio, used in pel,mesozoo and microzoo
   call allocateErsemDouble(qPZ5c,'qPZ5c',lb,N_Comp)
   ! Z6 P to C ratio, used in pel,mesozoo and microzoo
   call allocateErsemDouble(qPZ6c,'qPZ6c',lb,N_Comp)
   ! Z6 N to C ratio, used in pel,mesozoo and microzoo`
   call allocateErsemDouble(qNZ6c,'qNZ6c',lb,N_Comp)
   ! B1 N to C ratio, used in bacteria,pel,mesozoo and microzoo
   call allocateErsemDouble(qNB1c,'qNB1c',lb,N_Comp)
   ! B1 P to C ratio, used in bacteria,pel,mesozoo and microzoo
   call allocateErsemDouble(qPB1c,'qPB1c',lb,N_Comp)
   ! R6 N to C ratio, used in bacteria,benthos,ben_zoo,mseozoo and pel
   call allocateErsemDouble(qNR6c,'qNR6c',lb,N_Comp)
   ! R6 P to C ratio, used in bacteria,benthos,ben_zoo,mseozoo and pel
   call allocateErsemDouble(qPR6c,'qPR6c',lb,N_Comp)
   ! R6 S to C ratio, used in benthos,ben_zoo and pel
   call allocateErsemDouble(qSR6c,'qSR6c',lb,N_Comp)
   ! R4 N to C ratio, used in bacteria, benthos and pel
   call allocateErsemDouble(qNR4c,'qNR4c',lb,N_Comp)
   ! R4 P to C ratio, used in bacteria, benthos and pel
   call allocateErsemDouble(qPR4c,'qPR4c',lb,N_Comp)
   ! R8 N to C ratio, used in bacteria, benthos and pel
   call allocateErsemDouble(qNR8c,'qNR8c',lb,N_Comp)
   ! R8 P to C ratio, used in bacteria, benthos and pel
   call allocateErsemDouble(qPR8c,'qPR8c',lb,N_Comp)
   ! Reaplaceable by scalars
   ! Z4 respiration, replaceable by scalar, used in mesozoo, pel and gas_dynamics, budget
   call allocateErsemDouble(fZ4O3c,'fZ4O3c',lb,N_Comp)
   ! P1 respiration, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(fP1O3c,'fP1O3c',lb,N_Comp)
   ! P2 respiration, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(fP2O3c,'fP2O3c',lb,N_Comp)
   ! P3 respiration, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(fP3O3c,'fP3O3c',lb,N_Comp)
   ! P4 respiration, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(fP4O3c,'fP4O3c',lb,N_Comp)
   ! CO2 uptake of p1, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(fO3P1c,'fO3P1c',lb,N_Comp)
   ! CO2 uptake of p2, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(fO3P2c,'fO3P2c',lb,N_Comp)
   ! CO2 uptake of p3, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(fO3P3c,'fO3P3c',lb,N_Comp)
   ! CO2 uptake of p4, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(fO3P4c,'fO3P4c',lb,N_Comp)
   ! B1 respiration, replaceable by scalar, used in bacteria and gas_dynamics, budget
   call allocateErsemDouble(fB1O3c,'fB1O3c',lb,N_Comp)
   ! Z5 respiration,replaceable by scalar, used in microzoo and gas_dynamics, budget
   call allocateErsemDouble(fZ5O3c,'fZ5O3c',lb,N_Comp)
   ! Z6 respiration, replaceable by scalar, used in microzoo and gas_dynamics, budget
   call allocateErsemDouble(fZ6O3c,'fZ6O3c',lb,N_Comp)
   ! P uptake of p1, replaceable by scalar, used in primary_producers and pel, budget
   call allocateErsemDouble(fN1P1p,'fN1P1p',lb,N_Comp)
   ! P uptake of p2, replaceable by scalar, used in primary_producers and pel, budget
   call allocateErsemDouble(fN1P2p,'fN1P2p',lb,N_Comp)
   ! P uptake of p3, replaceable by scalar, used in primary_producers and pel, budget
   call allocateErsemDouble(fN1P3p,'fN1P3p',lb,N_Comp)
   ! P uptake of p4, replaceable by scalar, used in primary_producers and pel, budget
   call allocateErsemDouble(fN1P4p,'fN1P4p',lb,N_Comp)
   ! C excretion of Z4 -> R8, replaceable by scalar, used in mesozoo and pel, budget
   call allocateErsemDouble(fZ4R8c,'fZ4R8c',lb,N_Comp)
   ! B1 -> N1p, replaceable by scalar, used in bacterira and pel, budget
   call allocateErsemDouble(fB1N1p,'fB1N1p',lb,N_Comp)
   ! Z5 -> N1p, replaceable by scalar, used in microzoo and pel, budget
   call allocateErsemDouble(fZ5N1p,'fZ5N1p',lb,N_Comp)
   ! Z6 -> N1p, replaceable by scalar, used in microzoo and pel, budget
   call allocateErsemDouble(fZ6N1p,'fZ6N1p',lb,N_Comp)
   ! N4n excretion of Z5, replaceable by scalar, used in microzoo and pel, budget
   call allocateErsemDouble(fZ5NIn,'fZ5NIn',lb,N_Comp)
   ! N4n excretion of Z6, replaceable by scalar, used in microzoo and pel, budget
   call allocateErsemDouble(fZ6NIn,'fZ6NIn',lb,N_Comp)
   ! N4n excretion of B1, replaceable by scalar, used in bacteria and pel, budget
   call allocateErsemDouble(fB1NIn,'fB1NIn',lb,N_Comp)
   ! N4 uptake of p1, replaceable by scalar, used in primary_producers and pel
   call allocateErsemDouble(fN4P1n,'fN4P1n',lb,N_Comp)
   ! N4 uptake of p2, replaceable by scalar, used in primary_producers and pel
   call allocateErsemDouble(fN4P2n,'fN4P2n',lb,N_Comp)
   ! N4 uptake of p3, replaceable by scalar, used in primary_producers and pel
   call allocateErsemDouble(fN4P3n,'fN4P3n',lb,N_Comp)
   ! N4 uptake of p4, replaceable by scalar, used in primary_producers and pel
   call allocateErsemDouble(fN4P4n,'fN4P4n',lb,N_Comp)
   ! N3 uptake of p1, replaceable by scalar, used in primary_producers and pel
   call allocateErsemDouble(fN3P1n,'fN3P1n',lb,N_Comp)
   ! N3 uptake of p2, replaceable by scalar, used in primary_producers and pel
   call allocateErsemDouble(fN3P2n,'fN3P2n',lb,N_Comp)
   ! N3 uptake of p3, replaceable by scalar, used in primary_producers and pel
   call allocateErsemDouble(fN3P3n,'fN3P3n',lb,N_Comp)
   ! N3 uptake of p4, replaceable by scalar, used in primary_producers and pel
   call allocateErsemDouble(fN3P4n,'fN3P4n',lb,N_Comp)
   ! L2 dissolution, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(fl2O3c,'fl2O3c',lb,N_Comp)
   ! CO2 uptake for calcification, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(fO3l2c,'fO3l2c',lb,N_Comp)
   ! CO2 uptake for calcification, replaceable by scalar, used in primary_producers and gasdynamics, budget
   call allocateErsemDouble(rainR,'rainR',lb,N_Comp)
! Iron ratios and p-fluxes:
   call allocateErsemDouble(qfP1c,'qfP1c',lb,N_Comp)
   call allocateErsemDouble(qfP2c,'qfP2c',lb,N_Comp)
   call allocateErsemDouble(qfP3c,'qfP3c',lb,N_Comp)
   call allocateErsemDouble(qfP4c,'qfP4c',lb,N_Comp)
   call allocateErsemDouble(fN7P1f,'fN7P1f',lb,N_Comp)
   call allocateErsemDouble(fN7P2f,'fN7P2f',lb,N_Comp)
   call allocateErsemDouble(fN7P3f,'fN7P3f',lb,N_Comp)
   call allocateErsemDouble(fN7P4f,'fN7P4f',lb,N_Comp)
   ! Surface irradiation
   call allocateErsemDouble(elon,'elon',lb,N_Comp)
   call allocateErsemDouble(elat,'elat',lb,N_Comp)
   call allocateErsemDouble(zenithA,'zenithA',lb,N_Comp)
   call allocateErsemDouble(iopADS,'iopADS',lb,N_Comp)
   call allocateErsemDouble(iopBBS,'iopBB',lb,N_Comp)
   ! Bio-irrigation, replaceable by scalar, used in benthos and ben_phys
   call allocateErsemDouble(irr_enh,'irr_enh',lb,N_Comp)
   ! p1 -> R6 flux, replaceable by scalar, used in primary_producers, budget
   call allocateErsemDouble(fP1R6c,'fP1R6c',lb,N_Comp)
   ! p2 -> R4 flux, replaceable by scalar, used in primary_producers, budget
   call allocateErsemDouble(fP2R4c,'fP2R4c',lb,N_Comp)
   ! p3 -> R4 flux, replaceable by scalar, used in primary_producers, budget
   call allocateErsemDouble(fP3R4c,'fP3R4c',lb,N_Comp)
   ! p4 -> R6 flux, replaceable by scalar, used in primary_producers, budget
   call allocateErsemDouble(fP4R6c,'fP4R6c',lb,N_Comp)
   ! p1 -> R1,R2 flux, replaceable by scalar, used in primary_producers, budget
   call allocateErsemDouble(fP1Rdc,'fP1Rdc',lb,N_Comp)
   ! p2 -> R1,R2 flux, replaceable by scalar, used in primary_producers, budget
   call allocateErsemDouble(fP2Rdc,'fP2Rdc',lb,N_Comp)
   ! p3 -> R1,R2 flux, replaceable by scalar, used in primary_producers, budget
   call allocateErsemDouble(fP3Rdc,'fP3Rdc',lb,N_Comp)
   ! p4 -> R1,R2 flux, replaceable by scalar, used in primary_producers, budget
   call allocateErsemDouble(fP4Rdc,'fP4Rdc',lb,N_Comp)
   ! Mortality B1, replacealbe by scalar, used in bacteria, budget
   call allocateErsemDouble(fB1Rdc,'fB1Rdc',lb,N_Comp)
   ! mineralisation DOP->N1p, replaceable by scalar, used in bacteria, budget
   call allocateErsemDouble(fR1N1p,'fR1N1p',lb,N_Comp)
   ! P1 grazing of Z5, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fP1Z5c,'fP1Z5c',lb,N_Comp)
   ! P2 grazing of Z5, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fP2Z5c,'fP2Z5c',lb,N_Comp)
   ! P3 grazing of Z5, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fP3Z5c,'fP3Z5c',lb,N_Comp)
   ! P4 grazing of Z5, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fP4Z5c,'fP4Z5c',lb,N_Comp)
   ! Z5 feeding on Z6, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fZ6Z5c,'fZ6Z5c',lb,N_Comp)
   ! Z5 feeding on B1, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fB1Z5c,'fB1Z5c',lb,N_Comp)
   ! C Excretion Z5 -> R6, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fZ5R6c,'fZ5R6c',lb,N_Comp)
   ! P2 grazing of Z6, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fP2Z6c,'fP2Z6c',lb,N_Comp)
   ! P3 grazing of Z6, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fP3Z6c,'fP3Z6c',lb,N_Comp)
   ! Z6 fedding on B1, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fB1Z6c,'fB1Z6c',lb,N_Comp)
   ! Z5 canibalism, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fZ5Z5c,'fZ5Z5c',lb,N_Comp)
   ! C excretion of Z6 -> R4, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fZ6R4c,'fZ6R4c',lb,N_Comp)
   ! P1 grazing of P4, replaceable by scalar, used in mesozoo, budget
   call allocateErsemDouble(fP1Z4c,'fP1Z4c',lb,N_Comp)
   ! P2 grazing of P4, replaceable by scalar, used in mesozoo, budget
   call allocateErsemDouble(fP2Z4c,'fP2Z4c',lb,N_Comp)
   ! P3 grazing of P4, replaceable by scalar, used in mesozoo, budget
   call allocateErsemDouble(fP3Z4c,'fP3Z4c',lb,N_Comp)
   ! P4 grazing of P4, replaceable by scalar, used in mesozoo, budget
   call allocateErsemDouble(fP4Z4c,'fP4Z4c',lb,N_Comp)
   ! Z4 fedding on Z6, replaceable by scalar, used in mesozoo, budget
   call allocateErsemDouble(fZ6Z4c,'fZ6Z4c',lb,N_Comp)
   ! Z4 fedding on B1, replaceable by scalar, used in mesozoo, budget
   call allocateErsemDouble(fB1Z4c,'fB1Z4c',lb,N_Comp)
   ! Z4 fedding on Z5, replaceable by scalar, used in mesozoo, budget
   call allocateErsemDouble(fZ5Z4c,'fZ5Z4c',lb,N_Comp)
   ! Z4 canibalism, replaceable by scalar, used in mesozoo, budget
   call allocateErsemDouble(fZ4Z4c,'fZ4Z4c',lb,N_Comp)
   ! Z4 feeding on Z6, replaceable by scalar, used in mesozoo, budget
   call allocateErsemDouble(fR6Z4c,'fR6Z4c',lb,N_Comp)
   ! Z6 canibalism, replaceable by scalar, used in microzoo, budget
   call allocateErsemDouble(fZ6Z6c,'fZ6Z6c',lb,N_Comp)
   ! Prey available to Mesozooplankton, replaceable by scalar, used in pel
   call allocateErsemDouble(preyINT,'preyINT',lb,N_Comp)
   call allocateErsemDouble(netB,'netB',lb,N_Comp)
   call allocateErsemDouble(fPXZXc,'fPXZXc',lb,N_Comp)
   call allocateErsemDouble(fBXZXc,'fBXZXc',lb,N_Comp)
   call allocateErsemDouble(fRXZXc,'fRXZXc',lb,N_Comp)
   call allocateErsemDouble(PResp,'PResp',lb,N_Comp)
   call allocateErsemDouble(ZResp,'ZResp',lb,N_Comp)
   call allocateErsemDouble(BResp,'BResp',lb,N_Comp)
   call allocateErsemDouble(nitrN,'nitrN',lb,N_Comp)
   call allocateErsemDouble(fPXRPc,'fPXRPc',lb,N_Comp)
   call allocateErsemDouble(fPXRDc,'fPXRDc',lb,N_Comp)
   call allocateErsemDouble(fZXRPc,'fZXRPc',lb,N_Comp)
   call allocateErsemDouble(fZXRDc,'fPXRDc',lb,N_Comp)
   call allocateErsemDouble(fBXRDc,'fBXRDc',lb,N_Comp)
   call allocateErsemDouble(fRDBXc,'fRDBXc',lb,N_Comp)
   call allocateErsemDouble(fRPBXc,'fRPBXc',lb,N_Comp)
   call allocateErsemDouble(fN1PXp,'fN1PXp',lb,N_Comp)
   call allocateErsemDouble(fN3PXn,'fN3PXn',lb,N_Comp)
   call allocateErsemDouble(fN4PXn,'fN4PXn',lb,N_Comp)
   call allocateErsemDouble(fN5PXs,'fN5PXs',lb,N_Comp)
   call allocateErsemDouble(fBXN1p,'fBXN1p',lb,N_Comp)
   call allocateErsemDouble(fBXNIn,'fBXNIn',lb,N_Comp)
   call allocateErsemDouble(fRDN1p,'fRDN1p',lb,N_Comp)
   call allocateErsemDouble(fRDNIn,'fRDNIn',lb,N_Comp)
   call allocateErsemDouble(fN7PXf,'fN7PXf',lb,N_Comp)
! N Excretion Z5 -> R6, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ5R6n,'fZ5R6n',lb,N_Comp)
! P Excretion Z5 -> R6, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ5R6p,'fZ5R6p',lb,N_Comp)
! DOC excretion of Z5, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ5Rdc,'fZ5Rdc',lb,N_Comp)
! DON excretion of Z5, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ5Rdn,'fZ5Rdn',lb,N_Comp)
! DOP excretion of Z5, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ5Rdp,'fZ5Rdp',lb,N_Comp)
! N excretion of Z5, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ5RIn,'fZ5RIn',lb,N_Comp)
! P excretion of Z5, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ5RIp,'fZ5RIp',lb,N_Comp)
! N excretion of Z6 -> R4, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ6R4n,'fZ6R4n',lb,N_Comp)
! P excretion of Z6 -> R4, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ6R4p,'fZ6R4p',lb,N_Comp)
! DOC excretion of Z6, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ6Rdc,'fZ6Rdc',lb,N_Comp)
! DON excretion of Z6, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ6Rdn,'fZ6Rdn',lb,N_Comp)
! DOP excretion of Z6, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ6Rdp,'fZ6Rdp',lb,N_Comp)
! N excretion of Z6, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ6RIn,'fZ6RIn',lb,N_Comp)
! P excretion of Z6, replaceable by scalar, used in microzoo
! call allocateErsemDouble(fZ6Rip,'fZ6RIp',lb,N_Comp)
! N excretion of Z4 -> R8, replaceable by scalar, used in mesozoo
! call allocateErsemDouble(fZ4R8n,'fZ4R8n',lb,N_Comp)
! P excretion of Z4 -> R8, replaceable by scalar, used in mesozoo
! call allocateErsemDouble(fZ4R8p,'fZ4R8p',lb,N_Comp)
! DOC excretion of Z4, replaceable by scalar, used in mesozoo
! call allocateErsemDouble(fZ4Rdc,'fZ4Rdc',lb,N_Comp)
! DON excretion of Z4, replaceable by scalar, used in mesozoo
! call allocateErsemDouble(fZ4Rdn,'fZ4Rdn',lb,N_Comp)
! DOP excretion of Z4, replaceable by scalar, used in mesozoo
! call allocateErsemDouble(fZ4Rdp,'fZ4Rdp',lb,N_Comp)
! p1 -> R6 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP1R6p,'fP1R6p',lb,N_Comp)
! p2 -> R4 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP2R4p,'fP2R4p',lb,N_Comp)
! p3 -> R4 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP3R4p,'fP3R4p',lb,N_Comp)
! p4 -> R6 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP4R6p,'fP4R6p',lb,N_Comp)
! p1 -> R1,R2 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP1Rdp,'fP1Rdp',lb,N_Comp)
! p2 -> R1,R2 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP2Rdp,'fP2Rdp',lb,N_Comp)
! p3 -> R1,R2 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP3Rdp,'fP3Rdp',lb,N_Comp)
! p4 -> R1,R2 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP4Rdp,'fP4Rdp',lb,N_Comp)
! p1 -> R6 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP1R6n,'fP1R6n',lb,N_Comp)
! p2 -> R1,R2 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP2R4n,'fP2R4n',lb,N_Comp)
! p3 -> R4 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP3R4n,'fP3R4n',lb,N_Comp)
! p4 -> R6 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP4R6n,'fP4R6n',lb,N_Comp)
! p1 -> R1,R2 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP1Rdn,'fP1Rdn',lb,N_Comp)
! p2 -> R1,R2 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP2Rdn,'fP2Rdn',lb,N_Comp)
! p3 -> R1,R2 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP3Rdn,'fP3Rdn',lb,N_Comp)
! p4 -> R1,R2 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP4Rdn,'fP4Rdn',lb,N_Comp)
! p1 -> R6 flux, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fP1R6s,'fP1R6s',lb,N_Comp)
! S uptake of p1, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fN5P1s,'fN5P1s',lb,N_Comp)
! N uptake of p1, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fNIP1n,'fNIP1n',lb,N_Comp)
! N uptake of p2, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fNIP2n,'fNIP2n',lb,N_Comp)
! N uptake of p3, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fNIP3n,'fNIP3n',lb,N_Comp)
! N uptake of p4, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(fNIP4n,'fNIP4n',lb,N_Comp)
! Nitrification, replacealbe by scalar, used in bacteria
! call allocateErsemDouble(fN4N3n,'fN4N3n',lb,N_Comp)
! DON uptake, replaceable by scalar, used in bacteria
! call allocateErsemDouble(fR1B1n,'fR1B1n',lb,N_Comp)
! DOP uptake, replaceable by scalar, used in bacteria
! call allocateErsemDouble(fR1B1p,'fR1B1p',lb,N_Comp)
! mineralisation DON->N4n, replaceable by scalar, used in bacteria
   call allocateErsemDouble(fR1NIn,'fR1NIn',lb,N_Comp)
! B1P -> DOP, replaceable by scalar, used in bacteria
! call allocateErsemDouble(fB1Rdp,'fB1Rdp',lb,N_Comp)
! B1N -> DOP, replaceable by scalar, used in bacteria
! call allocateErsemDouble(fB1Rdn,'fB1Rdn',lb,N_Comp)
! internal nutrient limitation P1, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(iniP1,'iniP1',lb,N_Comp)
! internal nutrient limitation P2, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(iniP2,'iniP2',lb,N_Comp)
! internal nutrient limitation P3, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(iniP3,'iniP3',lb,N_Comp)
! internal nutrient limitation P4, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(iniP4,'iniP4',lb,N_Comp)
! external silicate limitaion P1, replaceable by scalar, used in primary_producers
! call allocateErsemDouble(inP1s,'inP1s',lb,N_Comp)
! Bioturbation, replaceable by scalar, used in ben_phys
! call allocateErsemDouble(tur_enh,'tur_enh',lb,N_Comp)
! CO2 production, used in gas_dynamics, replaceable by scalar
! call allocateErsemDouble(fptO3c,'fptO3c',lb,N_Comp)
! CO2 consumption used in gas_dynamics, replaceable by scalar
! call allocateErsemDouble(fO3ptc,'fO3ptc',lb,N_Comp)
! Total Oxygen consumption, replaceable by scalar, used in gas_dynamics
! call allocateErsemDouble(fO2pto,'fO2pto',lb,N_Comp)
! Total Oxygen production, replaceable by scalar, used in gas_dynamics
! call allocateErsemDouble(fptO2o,'fptO2o',lb,N_Comp)
! Air to sea flux of CO2, replaceable by scalar, used in gas_dynamics
! call allocateErsemDouble(fairCO2,'fairCO2',lb,N_Comp)
! Diagnostic variables:
   ! chlorophyll, purely diagnostic(budget), can be computed in post-processing
   call allocateErsemDouble(Chl,'Chl',lb,N_Comp)
   ! Chlorophyll to Carbon ratio P1
   call allocateErsemDouble(ChlC1,'ChlC1',lb,N_Comp)
   ! Chlorophyll to Carbon ratio P2
   call allocateErsemDouble(ChlC2,'ChlC2',lb,N_Comp)
   ! Chlorophyll to Carbon ratio P3
   call allocateErsemDouble(ChlC3,'ChlC3',lb,N_Comp)
   ! Chlorophyll to Carbon ratio P4
   call allocateErsemDouble(ChlC4,'ChlC4',lb,N_Comp)
   ! net p1 production, purely diagnostic(budget)
   call allocateErsemDouble(netP1,'netP1',lb,N_Comp)
   ! net p2 production, purely diagnostic(budget)
   call allocateErsemDouble(netP2,'netP2',lb,N_Comp)
   ! net p3 production, purely diagnostic(budget)
   call allocateErsemDouble(netP3,'netP3',lb,N_Comp)
   ! net p4 production, purely diagnostic(budget)
   call allocateErsemDouble(netP4,'netP4',lb,N_Comp)
   ! net phytoplankton production,, purely diagnostic(budget)
   call allocateErsemDouble(netpp,'netpp',lb,N_Comp)
   ! net bacteria production, purely diagnostic(budget)
   call allocateErsemDouble(netB1,'netB1',lb,N_Comp)
   ! bacterial growth efficiency
   call allocateErsemDouble(BGE,'BGE',lb,N_Comp)
   ! N3 consumption by phytoplankton, purely diagnostic(budget)
   call allocateErsemDouble(N3nprod,'N3nprod',lb,N_Comp)
   ! N4 consumption by phytoplankton, purely diagnostic(budget)
   call allocateErsemDouble(N4nprod,'N4nprod',lb,N_Comp)
   ! B+Z->N flux, purely diagnostic(budget)
   call allocateErsemDouble(fptNIn,'fptNIn',lb,N_Comp)
   ! B+Z->P flux, purely diagnostic(budget)
   call allocateErsemDouble(fptNIp,'fptNIp',lb,N_Comp)
   ! N uptake by phytoplankton, purely diagnostic(budget)
   call allocateErsemDouble(fNIptn,'fNIptn',lb,N_Comp)
   ! P uptake by phytoplankton, purely diagnostic(budget)
   call allocateErsemDouble(fNIptp,'fNIptp',lb,N_Comp)
   ! Z4 -> N1p, purely diagnostic(budget)
   call allocateErsemDouble(fZ4N1p,'fZ4N1p',lb,N_Comp)
   ! N4n excretion of Z4, purely diagnostic(budget)
   call allocateErsemDouble(fZ4NIn,'fZ4NIn',lb,N_Comp)
   ! loss of excess silicate, replaceable by scalar, used in primary_producers
   call allocateErsemDouble(fP1N5s,'fP1N5s',lb,N_Comp)
! unused
! call allocateErsemDouble(fR6Z6c,'fR6Z6c',lb,N_Comp)
! R6 N to C ratio, unused
! call allocateErsemDouble(qNQ6c,'qNQ6c',lb,N_Comp)
! R6 P to C ratio, unused
! call allocateErsemDouble(qPQ6c,'qPQ6c',lb,N_Comp)
! R6 S to C ratio, unused
! call allocateErsemDouble(qSQ6c,'qSQ6c',lb,N_Comp)
! Q1 N to C ratiom, remove
! call allocateErsemDouble(qNQ1c,'qNQ1c',lb,N_Comp)
! Q1 P to C ratiom, remove
! call allocateErsemDouble(qPQ1c,'qPQ1c',lb,N_Comp)
! R8 S to C ratio, unused
! call allocateErsemDouble(qSR8c,'qSR8c',lb,N_Comp)
! noon_lightS, remove
! call allocateErsemDouble(noon_light,'noon_light',lb,N_Comp)
! afternoon_lightS, remove
! call allocateErsemDouble(afternoon_light,'afternoon_light',lb,N_Comp)
! remove
! call allocateErsemDouble(denitflx,'denitflx',lb,N_Comp)
!
   ! Point geometry masks and arrays to null, need to be assigned properly in
   ! Coupler, use of SeaSurface is optional, the other three are necessary when
   ! using ERSEMs netcdf output. All of them are useful for the passing of
   ! information between ERSEM and the GCM:
   if (.not. associated(Water)) Water => null()
   if (.not. associated(SeaFloor)) SeaFloor => null()
   if (.not. associated(SeaSurface)) SeaSurface => null()
   if (.not. associated(BoxDepth)) BoxDepth => null()
   if (.not. associated(BoxFaces)) BoxFaces => null()
   end subroutine do_pelagic_variable_allocations
!
!EOC
!-----------------------------------------------------------------------
   end module pelagic_variables
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
