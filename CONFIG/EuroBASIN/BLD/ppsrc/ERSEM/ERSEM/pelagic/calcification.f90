

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Calcification
!
! !DESCRIPTION:
! This module describes calcification when coccolithophores
! are not in the model.\\[\baselineskip]
!
! !INTERFACE:
   module calcification
!
! !USES:
   use ersem_constants, only: fp8
!

   use pelagic_variables

   implicit none

! Default all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_calcification_parameters, set_calcification, &
      do_calcification
!
! !PUBLIC DATA MEMBERS:
   real(fp8), public :: Cmax, CL_EH, Nmax, bendiss, KcalpX, alphacalX

! Half-saturation constant for calcification limitation from
! saturation state (MEECE module)
   real(fp8), public :: KcalomX

! dissolution rate in seawater from PISCES
   real(fp8), public :: kdiss

! Power of the dissolution law (taken from Keir 1980
   real(fp8), public :: ndiss

! Power of the calcification law (taken from Rigwell et
! al., 2007 , conisdering mineral calcite) used if iswcal=1
   real(fp8), public :: ncalc

! Dissolution of calcite in the zooplankton guts
   real(fp8), public :: gutdiss

! Sedimentation rate of free liths
   real(fp8), public :: sedL2

! Max rain ratio from PISCES
   real(fp8), public :: Rain0
!
! !LOCAL VARIABLES:
   real(fp8) :: fcalc, fdiss
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise parameters
!
! !DESCRIPTION:
! Initialise calcification parameters.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_calcification_parameters
!
! !USES:
   use ersem_variables, only: IOProcess,eLogU, eDbgU
!
! !LOCAL VARIABLES:
   namelist/calcParameters/Cmax,CL_EH,Nmax,bendiss,KcalpX, alphacalX,&
      KcalomX,kdiss,ndiss,ncalc,gutdiss,sedL2,Rain0
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading calcification parameters'
   open(99,file='BioParams/calc.nml',status='old')
   read(99,nml=calcParameters)

   close(99)

   return

   end subroutine set_calcification_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Set calcification \label{sec:setcalcification}
!
! !DESCRIPTION:
! This routine sets the rain ratio and computes the
! dissolution of calcified matter.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_calcification (I)
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (iswcal.eq.0) then
      fcalc=0._fp8
      fdiss=0._fp8
   else if (iswcal.eq.1) then
      fcalc=max(om_cal(I)-1._fp8,0._fp8)**ncalc
      fdiss=max(1._fp8-om_cal(I),0._fp8)**ndiss
   else if (iswcal.eq.2) then
      fcalc=max(0._fp8,(om_cal(I)-1._fp8)/(om_cal(I)-1._fp8+KcalomX))
      fdiss=max(0._fp8,(1._fp8-om_cal(I))/(1._fp8-om_cal(I)+KcalomX))
   else
      if (IOProcess) write(eLogU,*) 'ISWcal set to 1'
      iswcal=1
      fcalc=max(om_cal(I)-1._fp8,0._fp8)**ncalc
      fdiss=max(1._fp8-om_cal(I),0._fp8)**ndiss
   end if
   fL2O3c(I) = fdiss * L2c(I)
   RainR(I) = fcalc * Rain0

   end subroutine set_calcification
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Calcification dynamics \label{sec:docalcification}
!
! !DESCRIPTION:
! This routine assembles the calcification rates.\\[\baselineskip]
!
! !INTERFACE:
   subroutine do_calcification (I)
!
! !USES:
   use ersem_variables, only: timestep
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
! Calcification of sedimenting P2:
   fO3L2c(I) = fO3L2c(I) + &
      RainR(I)*min(sdP2c(I)/pdepth(I),1._fp8/timestep)*P2cP(I)
!
   sO3c(I) = sO3c(I) - fO3L2c(I)/12._fp8 + fL2O3c(I)/12._fp8
   if (iswbioalk.eq.1) then
       sbioalk(i) = sbioalk(i) - 2._fp8*fO3L2c(I)/12._fp8 + 2._fp8*fL2O3c(I)/12._fp8
   end if
   sL2c(I) = SL2c(I) + fO3L2c(I) - fL2O3c(I)
   SDL2c(I) = SDL2c(I) + sedL2

   return

   end subroutine do_calcification
!
!EOC
!-----------------------------------------------------------------------


   end module calcification

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
