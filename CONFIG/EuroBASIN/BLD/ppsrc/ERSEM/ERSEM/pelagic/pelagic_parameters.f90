

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Pelagic parameters
!
! !DESCRIPTION:
! This module holds a small subset of the parameters used in the pelagic
! subcomponent of {\em ERSEM}, includng food-matix parameters and parameters
! concerning internal nutrient quotas. It also contains routines for
! reading the oxygen and food-matrix parameters from their respective
! namelists.\\[\baselineskip]
!
! !INTERFACE:
   module pelagic_parameters
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is public
   public
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_oxygen_parameters, set_food_matrix_parameters
!
! !PUBLIC DATA MEMBERS:
! Partial pressure of CO2 [ppm]
   real(fp8) :: pco2a3

! Food preference of heteroflagellates for medium size phytoplankton
   real(fp8) :: sup2_z6X

! Food preference of heteroflagellates for small phytoplankton
   real(fp8) :: sup3_z6X

! Food preference of heteroflagellates for heteroflagellates
   real(fp8) :: suz6_z6X

! Food preference of heteroflagellates for bacteria
   real(fp8) :: sub1_z6X

! Food preference of microzooplankton for diatoms
   real(fp8) :: sup1_z5X

! Food preference of microzooplankton for medium size phytoplankton
   real(fp8) :: sup2_z5X

! Food preference of microzooplankton for small phytoplankton
   real(fp8) :: sup3_z5X

! Food preference of microzooplankton for large phytoplankton
   real(fp8) :: sup4_z5X

! Food preference of microzooplankton for microzooplankton
   real(fp8) :: suz5_z5X

! Food preference of microzooplankton for heteroflagellates
   real(fp8) :: suz6_z5X

! Food preference of microzooplankton for bacteria
   real(fp8) :: sub1_z5X

! Food preference of mesozooplankton for diatoms
   real(fp8) :: sup1_z4X

! Food preference of mesozooplankton for medium size phytoplankton
   real(fp8) :: sup2_z4X

! Food preference of mesozooplankton for small phytoplankton
   real(fp8) :: sup3_z4X

! Food preference of mesozooplankton for large phytoplankton
   real(fp8) :: sup4_z4X

! Food preference of mesozooplankton for microzooplanton
   real(fp8) :: suz5_z4X

! Food preference of mesozooplankton for heteroflagellates
   real(fp8) :: suz6_z4X

! Food preference of mesozooplankton for medium size POM
   real(fp8) :: sur6_z4X

! Food preference of mesozooplankton for bacteria
   real(fp8) :: sub1_z4X

! Food preference of mesozooplankton for mesozooplankton
   real(fp8) :: suz4_z4X

! Fixed nitrogen to carbon ratio of mesozooplankton [mmol N/mg C]
   real(fp8) :: qnzicX

! Fixed phosphate to carbon ratio of mesozooplankton [mmol P/mg C]
   real(fp8) :: qpzicX

! Phosphorus fraction in cytoplasm of DOM vs. structural components
   real(fp8) :: xr1pX

! Nitrogen fraction in cytoplasm of DOM vs. structural components
   real(fp8) :: xr1nX

! Phosphorus fraction in cytoplasm of POM vs. structural components
   real(fp8) :: xr7pX

! Nitrogen fraction in cytoplasm of POM vs. structural components
   real(fp8) :: xr7nX

! Labile fraction of DOM production
   real(fp8) :: r1r2x
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team

! James Clark (PML) Jan 2014 - Created module pelagic_parameters, and
! moved data members in from module global_declarations."
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise oxygen parameters
!
! !DESCRIPTION:
! Initialises oxygen parameters from namelist \texttt{oxParameters}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_oxygen_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU

   implicit none

! !LOCAL VARIABLES:
   namelist/oxParameters/pco2a3
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading oxygen parameters'
   open(99,file='BioParams/ox.nml',status='old')
   read(99,nml=oxParameters)

   close(99)

   return

   end subroutine set_oxygen_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initalise food matrix parameters
!
! !DESCRIPTION:
! Initialises food matrix coefficients from namelist \texttt{fmatrixParameters}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_food_matrix_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU

   implicit none

! !LOCAL VARIABLES:
   namelist/fmatrixParameters/suP2_Z6X,suP3_Z6X,suZ6_Z6X,suB1_Z6X,&
      suP1_Z5X,suP2_Z5X,suP3_Z5X,suP4_Z5X,suZ5_Z5X,suZ6_Z5X,&
      suB1_Z5X,suP1_Z4X,suP2_Z4X,suP3_Z4X,suP4_Z4X,suZ5_Z4X,&
      suB1_Z4X,suR6_Z4X,suZ4_Z4X,suZ6_Z4X
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading food matrix'
   open(99,file='BioParams/fmatrix.nml',status='old')
   read(99,nml=fmatrixParameters)

   close (99)

   return

   end subroutine set_food_matrix_parameters
!
!EOC
!-----------------------------------------------------------------------

   end module pelagic_parameters

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
