

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Heterotrophic nanoflagellates \label{sec:hetflagdynamics}
!
! !DESCRIPTION:
! This module contains the parametrisation for heterotrophic nanoflagellates
! and the routines to initialse the respective parameters from namelist
! and launch the heterotrophic nanoflagelate dynamics.\\[\baselineskip]
!
! !INTERFACE:
   module heterotrophic_nanoflagellates
!
! !USES:
   use ersem_constants, only: fp8
   use pelagic_variables
   use pelagic_parameters
   use gas_dynamics, only: ub1c_o2x,urB1_O2X

   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_heterotrophic_nanoflagellate_parameters, &
      heterotrophic_nanoflagellate_dynamics
!
! !PUBLIC DATA MEMBERS:
! Michaelis-Menten constant for food uptake by
! heteroflagellates [mg C/m^3]
   real(fp8), public :: chuz6cX

! Maximal specific uptake of heteroflagellates at
! reference temperature [1/d]
   real(fp8), public :: sumz6X

! Relative heteroflagellates assimilation efficiency
   real(fp8), public :: puz6X

! DOM-Fraction of heteroflagellates excretion
   real(fp8), public :: pe_r1z6X

! Regulating temperature factor Q10 for heteroflagellates
   real(fp8), public :: q10z6X

! Specific rest respiration of heteroflagellates at
! reference temperature [1/d]
   real(fp8), public :: srsz6X

! Michaelis-Menten constant for oxygen limitation of
! heteroflagellates
   real(fp8), public :: chrz6oX

! Excreted fraction of POM uptake by heteroflagellates
   real(fp8), public :: pu_eaz6X

! Specific mortality of heteroflagellates due to oxygen
! limitation [1/d]
   real(fp8), public :: sdz6oX

! Specific basal mortality of heteroflagellates [1/d]
   real(fp8), public :: sdz6X

! Maximal nitrogen to carbon ratio of heteroflagellates
! [mmol N/mg C]
   real(fp8), public :: qnz6cX

! Maximal phospherus to carbon ratio of heteroflagellates
! [mmol P/mg C]
   real(fp8), public :: qpz6cX

! Michaelis-Menten constant to perceive food for
! heteroflagellates [mg C/m^2]
   real(fp8), public :: minfoodz6X

! Specific ammonium excretion rate of microozooplankton [1/d]
   real(fp8), public :: stempz6nX

! Specific phosphate excretion rate of microozooplankton [1/d]
   real(fp8), public :: stempz6pX
!
! !LOCAL VARIABLES:
   real(fp8) :: fZ6R4n,fZ6R4p,fZ6Rdc,fZ6Rdn
   real(fp8) :: fZ6Rdp,fZ6RIp,fZ6RIn
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise parameters
!
! !DESCRIPTION:
! Initialises heterotrophic nanaflagellate parameters from namelist
! \texttt{heterotrophic\_nanoflagellates\_parameters}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_heterotrophic_nanoflagellate_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU

   implicit none
!
! !LOCAL VARIABLES:
   namelist/heterotrophic_nanoflagellate_parameters/chuZ6cX,sumZ6X,&
      puZ6X,pe_R1Z6X,q10Z6X,srsZ6X,chrZ6oX,pu_eaZ6X,sdZ6oX,sdZ6X,&
      qnZ6cX,qpZ6cX,minfoodZ6X,stempZ6nX,stempZ6pX
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading heterotrophic nanoflagellate parameters'
   open(99,file='BioParams/heterotrophic_nanoflagellate.nml',status='old')
   read(99,nml=heterotrophic_nanoflagellate_parameters)

   close(99)

   return

   end subroutine set_heterotrophic_nanoflagellate_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Heterotrophic nanoflagellate dynamics
!
! !DESCRIPTION:
! The predation of heterotrophic nanoflagellates on other states of
! the modelled ecosystem is based on the prey available in the current
! environment according to:
! \begin{eqnarray*}
! \overset{\textit{c,n,p}}{\textit{Pr}}_{\textit{HET}} & = & \left.f_{\textit{pr}}\right|_{B}^{Z_{\textit{HET}}}\frac{\overset{c}{B'}}{\overset{c}{B'}+f_{\textit{mHET}}}\overset{\textit{c,n,p}}{B'}+\left.f_{\textit{pr}}\right|_{P_{\textit{nano}}}^{Z_{\textit{HET}}}\frac{\overset{c}{P'}_{\textit{nano}}}{\overset{c}{P'}_{\textit{nano}}+f_{\textit{mHET}}}\overset{\textit{c,n,p}}{P'}_{\textit{nano}}\! & & +\left.f_{\textit{pr}}\right|_{P_{\textit{pico}}}^{Z_{\textit{HET}}}\frac{\overset{c}{P'}_{\textit{pico}}}{\overset{c}{P'}_{\textit{pico}}+f_{\textit{mHET}}}\overset{\textit{c,n,p}}{P'}_{\textit{pico}}+\left.f_{\textit{pr}}\right|_{Z_{\textit{HET}}}^{Z_{\textit{HET}}}\frac{\overset{c}{Z'}_{\textit{HET}}}{\overset{c}{Z'}_{\textit{HET}}+f_{\textit{mHET}}}\overset{\textit{c,n,p}}{Z'}_{\textit{HET}}\,,

! \end{eqnarray*}
! where $\left.f_{\textit{pr}}\right|_{X}^{Z_{\textit{HET}}}$ are the
! food preferences of heteroflagellates on state $X$ and $f_{\textit{mHET}}$
! is the minimum food constant limiting the uptake efficiency at low
! concentrations of a specific prey.
!
! Specific food uptake by heterotrophic
! nanoflagellates is a temperature regulated function of its own biomass
! and prey availability, additionally limited towards low concentrations
! of total prey:
! \begin{equation}
! \mathcal{\overset{\textit{HET}}{S}}_{\textit{growth}}=\overset{\textit{HET}}{g}_{\textit{max}}\overset{\textit{HET}}{l}_{T}\frac{\overset{c}{Z}_{\textit{HET}}}{\overset{\textit{c}}{\textit{Pr}}_{\textit{HET}}+\overset{\textit{HET}}{h}_{\textit{up}}}\,,
! \end{equation}
! leading to a total carbon uptake of:
! \begin{equation}
! \left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{growth}}=\mathcal{\overset{\textit{HET}}{S}}_{\textit{growth}}\overset{\textit{c}}{\textit{Pr}}_{\textit{HET}}\,,
! \end{equation}
! where $\overset{\textit{HET}}{h}_{\textit{up}}$ is the Michaelis-Menten
! constant for food uptake by heterotrophic nanoflagellates. The temperature
! response factor $\overset{B}{l}_{T}$ is given by the equation:
! \begin{equation}
! \overset{\textit{HET}}{l}_{T}=\left.\overset{\textit{HET}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-10}{10}}-\left.\overset{\textit{HET}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-32}{3}}.
! \end{equation}

! The gross uptake is subject to losses to organic matter and activity
! respiration. The losses to organic matter like excretion or sloppy
! feeding are computed as:
! \begin{equation}
! \left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{excr}}=\left(1-\overset{\textit{HET}}{p}_{\textit{eff}}\right)\overset{\textit{HET}}{p}_{\textit{excr}}\left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{growth}}
! \end{equation}
! with the parameter$\overset{\textit{HET}}{p}_{\textit{eff}}$ estimating
! the fraction of uptake being effectively synthesised and $\overset{\textit{HET}}{p}_{\textit{excr}}$
! giving the fraction of uptake loss directed to organic matter.

! Heterotrophic nanoflagellate respiration is computed according to:
! \begin{equation}
! \left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{resp}}=\left(1-\overset{\textit{HET}}{p}_{\textit{eff}}\right)\left(1-\overset{\textit{HET}}{p}_{\textit{excr}}\right)\left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{growth}}+\overset{\textit{HET}}{r}_{\textit{resp}}\overset{\textit{HET}}{l}_{T}\overset{c}{Z'}_{\textit{HET}}\,,
! \end{equation}
! where $\overset{\textit{HET}}{r}_{\textit{resp}}$ is the specific
! basal respiration rate at rest.

! Mortality is given as:
! \begin{equation}
! \left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{mort}}=\left(\left(1-\overset{\textit{HET}}{l}_{O_{2}}\right)\overset{\textit{HET}}{p}_{\textit{mort}O2}+\overset{\textit{HET}}{p}_{\textit{mort}}\right)\overset{\textit{c,p,n}}{Z'}_{\textit{HET}}
! \end{equation}
! with $\overset{\textit{HET}}{p}_{\textit{mort}}$ being the specific
! mortality rate of bacteria, $\overset{\textit{HET}}{p}_{\textit{mort}O2}$
! is the maximum specific rate of mortality due to oxygen limitation
! and the oxygen limitation state $\overset{\textit{HET}}{l}_{O_{2}}$
! is given by:
! \begin{equation}
! \overset{\textit{HET}}{l}_{O_{2}}=\frac{s_{\textit{relO}_{2}+}s_{\textit{relO}_{2}}\overset{\textit{HET}}{h}_{O_{2}}}{s_{\textit{relO}_{2}}+\overset{\textit{HET}}{h}_{O_{2}}}\,.
! \end{equation}
!
! The fluxes of prey to heterotrophic nanoflagellates are then given
! by the equations:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{B}}^{\overset{\textit{c,p,n}}{Z}_{\textit{HET}}} & = & \mathcal{\overset{\textit{HET}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{B'}\!  \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{P}_{\textit{nano}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{HET}}} & = & \mathcal{\overset{\textit{HET}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{P'}_{\textit{nano}}\! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{P}_{\textit{pico}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{HET}}} & = & \mathcal{\overset{\textit{HET}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{P'}_{\textit{pico}}\,.


! \end{eqnarray*}
!
! The respiration flux is given by:
! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{HET}}}^{G_{\textit{\textit{CO}}_{2}}}=\frac{1}{\mathcal{\mathbb{C}}}\left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{resp}}\,.
! \end{equation}
!
! The loss to organic matter is split between dissolved and particulate
! matter as:

! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{HET}}}^{\overset{c}{M}_{\textit{\textit{small}}}} & = & \left(1-\overset{\textit{HET}}{p}_{\textit{dis}}\right)\left(\left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{mort}}\right)\! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{HET}}}^{\overset{c}{M}_{\textit{\textit{dis}}}} & =\overset{\textit{HET}}{p}_{\textit{dis}} & \overset{\textit{HET}}{p}_{\textit{lab}}\left(\left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{mort}}\right)\! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{HET}}}^{\overset{c}{M}_{\textit{\textit{slab}}}} & = & \overset{\textit{HET}}{p}_{\textit{dis}}\left(1-\overset{\textit{HET}}{p}_{\textit{lab}}\right)\left(\left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{mort}}\right)\,,


! \end{eqnarray*}
! where $\overset{\textit{HET}}{p}_{\textit{dis}}$ is the fraction
! of uptake loss directed to dissolved organic matter and $\overset{\textit{HET}}{p}_{\textit{lab}}$
! is the fraction of this in labile state.
!
! The phosphate and nitrogen fluxes to organic matter are given by:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{p,n}{Z}_{\textit{HET}}}^{\overset{p,n}{M}_{\textit{\textit{dis}}}} & = & \overset{\textit{HET}}{p}_{\textit{dis}}p_{\textit{DPcyto}}\left(\left.\frac{\partial\overset{p,n}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{p,n}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{mort}}\right)\! \left.\mathcal{F}\right|_{\overset{p,n}{Z}_{\textit{HET}}}^{\overset{p,n}{M}_{\textit{\textit{small}}}} & = & \left(1-\overset{\textit{HET}}{p}_{\textit{dis}}p_{\textit{DPcyto}}\right)\left(\left.\frac{\partial\overset{p,n}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{p,n}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{mort}}\right)\,,

! \end{eqnarray*}
! with $p_{\textit{DNcyto}}$ being the phosphate/nitrogen fraction
! in cytoplasm of DOM vs. structural components.
!
! The internal stochiometric relation of phosporus and nitrogen is then
! balanced according to:
! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{HET}}}^{N_{\textit{phos,amm}}}=\textit{min}\left(0,\overset{\textit{p,n}}{Z'}_{\textit{HET}}-\overset{\textit{HET}}{q}_{\textit{p,n:c}}\overset{c}{Z'}_{\textit{HET}}\right)\overset{\textit{p,n}}{r}_{\textit{HETex}}\,,
! \end{equation}
! where $\overset{\textit{p,n}}{r}_{\textit{HETex}}$ is the reference
! specific phosphate excretion rate of heterotrophic flagellates.
!
! The respiration fluxes towards inorganic gasses are given as:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{HET}}}^{G_{\textit{\textit{CO}}_{2}}} & = & \frac{1}{\mathcal{\mathbb{C}}}\left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{resp}}\! \left.\mathcal{F}\right|_{G_{O_{2}}}^{\textit{sink}} & = & \overset{B}{p}_{O_{2}}\left.\frac{\partial\overset{c}{Z}_{\textit{HET}}}{\partial t}\right|_{\textit{resp}}\,.

! \end{eqnarray*}
!
! If iron is used (pre-preocessing option \texttt{1}), the iron contained
! in the prey fields is directly cycled to organic matter:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{nano}}}^{\overset{f}{M}_{\textit{small}}} & = & \mathcal{\overset{\textit{HET}}{S}}_{\textit{growth}}\overset{f}{P'}_{\textit{nano}}\!  \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{pico}}}^{\overset{f}{M}_{\textit{small}}} & = & \mathcal{\overset{\textit{HET}}{S}}_{\textit{growth}}\overset{f}{P'}_{\textit{pico}}\,.

! \end{eqnarray*}
!
! !INTERFACE:
   subroutine heterotrophic_nanoflagellate_dynamics(I)
!
! !USES:
   use ersem_constants, only: CMass
!
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in) :: i
!
! !LOCAL VARIABLES:
   real(fp8) :: etZ6,corrox,eO2Z6,rumZ6, put_uZ6
   real(fp8) :: rrsZ6,rraZ6,rugZ6,retZ6,rdZ6,sdZ6
   real(fp8) :: ruP2Z6c,ruP3Z6c
   real(fp8) :: ruZ6Z6c,ruB1Z6c
   real(fp8) :: sB1Z6,sP2Z6,sP3Z6,ineffZ6
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Temperature effect :
   etZ6 = q10Z6X**((ETW(I)-10._fp8)/10._fp8) - q10Z6X**((ETW(I)-32._fp8)/3._fp8)

   ! Oxygen limitation :
   CORROX = 1._fp8 + chrZ6oX
   eO2Z6 = MIN(1._fp8,CORROX*(eO2mO2/(chrZ6oX + eO2mO2)))

   ! Available food :
   sB1Z6 = suB1_Z6X*B1cP(I)/(B1cP(I)+minfoodZ6X)
   sP2Z6 = suP2_Z6X*P2cP(I)/(P2cP(I)+minfoodZ6X)
   sP3Z6 = suP3_Z6X*P3cP(I)/(P3cP(I)+minfoodZ6X)
   ruZ6Z6c = suZ6_Z6X*Z6cP(I)/(Z6cP(I)+minfoodZ6X)*Z6cP(I)
   ruB1Z6c = sB1Z6 * B1cP(I)
   ruP2Z6c = sP2Z6 * P2cP(I)
   ruP3Z6c = sP3Z6 * P3cP(I)

   rumZ6 = ruB1Z6c + ruP2Z6c + ruP3Z6c + ruZ6Z6c

   ! Uptake :
   put_uZ6 = sumZ6X/(rumZ6 + chuZ6cX)*etZ6*Z6c(I)
   rugZ6 = put_uZ6*rumZ6

   ! Fluxes into heteroflagellates:
   fB1Z6c(I) = put_uZ6*ruB1Z6c
   fP2Z6c(I) = put_uZ6*ruP2Z6c
   fP3Z6c(I) = put_uZ6*ruP3Z6c
   fZ6Z6c(I) = put_uZ6*ruZ6Z6c
   sB1Z6 = put_uZ6*sB1Z6
   sP2Z6 = put_uZ6*sP2Z6
   sP3Z6 = put_uZ6*sP3Z6

   ! Zooplankton Grazing
! Z6herb(I) = fP2Z6c(I) + fP3Z6c(I)
! Z6carn(I) = fB1Z6c(I)

   ! Fluxes from heteroflagellates:

   ! Mortality
   sdZ6 = ((1._fp8 - eO2Z6)*sdZ6oX + sdZ6X)
   rdZ6 = sdZ6*Z6cP(I)

   ! Assimilation inefficiency:
   ineffZ6 = (1._fp8 - puZ6X)

   ! Excretion
   retZ6 = ineffZ6 * rugZ6 * pu_eaZ6X
   fZ6RDc = (retZ6 + rdZ6)*pe_R1Z6X
   fZ6R4c(I) = (retZ6 + rdZ6)*(1._fp8 - pe_R1Z6X)

   fZXRDc(I) = fZXRDc(I)+fZ6RDc


   ! Rest respiration, corrected for prevailing temperature
   rrsZ6 = srsZ6X*etZ6*Z6cP(I)

   ! Activity respiration
   rraZ6 = ineffZ6 * rugZ6 - retZ6

   ! Total respiration
   fZ6O3c(I) = rrsZ6 + rraZ6

   ! Source equation
   SZ6c(I) = SZ6c(I) + rugZ6 &
             - fZ6O3c(I) - fZ6R4c(I) - fZ6RDc - fZ6Z6c(I)

   ! Flows from and to detritus (R6)
   SR1c(I) = SR1c(I) + (fZ6RDc * R1R2X)
   SR2c(I) = SR2c(I) + (fZ6RDc * (1._fp8-R1R2X))
   SR4c(I) = SR4c(I) + fZ6R4c(I)

   ! Grazing and predation
   SP2c(I) = SP2c(I) - fP2Z6c(I)
   SP3c(I) = SP3c(I) - fP3Z6c(I)
   SB1c(I) = SB1c(I) - fB1Z6c(I)
   SChl2(I) = SChl2(I) - sP2Z6*chl2P(I)
   SChl3(I) = SChl3(I) - sP3Z6*chl3P(I)

   ! Respiration
   SO3c(I) = SO3c(I) + fZ6O3c(I)/CMass
   SO2o(I) = SO2o(I) - fZ6O3c(I)*urB1_O2X

   ! Nutrient dynamics in heteroflagellates, derived from carbon flows

   ! Phosphorus dynamics
   fZ6RIp = retZ6*qpZ6c(I)+sdZ6*Z6pP(I)
   fZ6RDp = fZ6RIp*min(1._fp8, pe_R1Z6X*xR1pX)
   fZ6R4p = fZ6RIp - fZ6RDp
   fZ6N1p(I) = max(0._fp8, Z6pP(I) - qpZ6cX*Z6cP(I))*stempZ6pX

   ! Source equations
   SZ6p(I) = SZ6p(I) + sP2Z6*P2pP(I) &
                     + sP3Z6*P3pP(I) &
                     + sB1Z6*B1pP(I) &
                     - fZ6R4p - fZ6RDp - fZ6N1p(I)

   ! Iron dynamics
   ! following Vichi et al., 2007 it is assumed that the iron fraction of the ingested phytoplankton
   ! is egested as particulate detritus (Luca)

   SR4f(I)=SR4f(I)+ sP2Z6*P2fP(I)+sP3Z6*P3fP(I)

   SP2f(I)=SP2f(I)-sP2Z6*P2fP(I)
   SP3f(I)=SP3f(I)-sP3Z6*P3fP(I)


   ! P-flow to pool
   SN1p(I) = SN1p(I) + fZ6N1p(I)

   ! Phosphorus flux from/to detritus
   SR4p(I) = SR4p(I) + fZ6R4p
   SR1p(I) = SR1p(I) + fZ6RDp

   ! Phosphorus flux from prey
   SP2p(I) = SP2p(I) - sP2Z6*P2pP(I)
   SP3p(I) = SP3p(I) - sP3Z6*P3pP(I)
   SB1p(I) = SB1p(I) - sB1Z6*B1pP(I)

   ! Nitrogen dynamics
   fZ6RIn = retZ6*qnZ6c(I)+sdZ6*Z6nP(I)
   fZ6RDn = fZ6RIn*min(1._fp8, pe_R1Z6X*xR1nX)
   fZ6R4n = fZ6RIn - fZ6RDn

   fZ6NIn(I) = max(0._fp8, Z6nP(I) - qnZ6cX*Z6cP(I))*stempZ6nX
   SN4n(I) = SN4n(I) + fZ6NIn(I)
   SZ6n(I) = SZ6n(I) + sP3Z6*P3nP(I) &
                     + sP2Z6*P2nP(I) &
                     + sB1Z6*B1nP(I) &
                     - fZ6R4n - fZ6RDn - fZ6NIn(I)

   ! Nitrogen flux from/to detritus
   SR4n(I) = SR4n(I) + fZ6R4n
   SR1n(I) = SR1n(I) + fZ6RDn

   ! Nitrogen flux from prey
   SP2n(I) = SP2n(I) - sP2Z6*P2nP(I)
   SP3n(I) = SP3n(I) - sP3Z6*P3nP(I)
   SB1n(I) = SB1n(I) - sB1Z6*B1nP(I)

   return

   end subroutine heterotrophic_nanoflagellate_dynamics
!
!EOC
!-----------------------------------------------------------------------

   end module heterotrophic_nanoflagellates

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
