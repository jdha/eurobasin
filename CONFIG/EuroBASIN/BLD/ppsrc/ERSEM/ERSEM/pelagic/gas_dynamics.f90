

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Gas dynamics
!
! !DESCRIPTION:
! Calculate oxygen saturations corrected for temperature and
! salinity. Units: oxygen in mmol/m3, salinity in psu S,
! temp in C converted to absolute temp.\\[\baselineskip]
!
! !INTERFACE:
   module gas_dynamics
!
! !USES:
   use ersem_constants, only: fp8
   use pelagic_variables

   implicit none

! Default all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public oxygen_chemistry, carbon_chemistry

! !PUBLIC DATA MEMBERS:
! Conversion of carbon into oxygen produced [mg C/mmol O2]
   real(fp8), public :: ub1c_o2X

! Conversion of carbon into oxygen respired [mg C/mmol O2]
   real(fp8), public :: urb1_o2X

   integer, public :: iswo2X

   real (fp8) :: k0co2,k1co2,k2co2,kb
   real (fp8) :: CTOT,TA,pH,pCO2,H2CO3,HCO3,CO3

! Used to store previous CO2sys variable in case of non convergence
   real (fp8),dimension(7) :: CO2sys_memory
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Oxygen chemistry \label{sec:oxygenchemistry}
!
! !DESCRIPTION:
! Computes oxygen saturation states and oxygen flux at the sea surface.\\[\baselineskip]
!
! !INTERFACE:
   subroutine oxygen_chemistry(I)
!
! !LOCAL VARIABLES:
   real(fp8) :: ABT
   real(fp8) :: A1,A2,A3,A4,B1,B2,B3
   real(fp8) :: R,P,T,VIDEAL,ko2o
   integer :: i

   DATA A1/-173.4292_fp8/,A2/249.6339_fp8/,A3/143.3483_fp8/,A4/-21.8492_fp8/
   DATA B1/-0.033096_fp8/,B2/0.014259_fp8/,B3/-0.0017_fp8/
   DATA R/8.3145_fp8/,P/101325_fp8/,T/273.15_fp8/
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
! calc volume of an ideal gas at standard temp (25C) and
! pressure (1 atm)

   VIDEAL = (R * 298.15_fp8 / P) *1000._fp8

   ! Calc absolute temperature
   ABT = ETW(I) + T

   if (ISWO2X .eq. 1) then

      ! Old ERSEM saturation Formulation
      OSAT(I) = 31.25_fp8 * (475._fp8-2.65_fp8*X1X(I)) / (33.5_fp8+ETW(I))

   else if (iswO2X .eq. 2 ) then

      ! Calc theoretical oxygen saturation for temp + salinity
      ! From WEISS 1970 DEEP SEA RES 17, 721-735.
      ! units of ln(ml(STP)/l)
      OSAT(I) = A1 + A2 * (100._fp8/ABT) + A3 * log(ABT/100._fp8) &
      + A4 * (ABT/100._fp8) &
      + X1X(I) * ( B1 + B2 * (ABT/100._fp8) + B3 * ((ABT/100._fp8)**2))

      ! Convert units to ml(STP)/l then to mMol/m3
      OSAT(I) = exp( OSAT(I) )
      OSAT(I) = OSAT(I) * 1000._fp8 / VIDEAL

   end if

   ! Surface Oxygen Flux
   if (I.le.n_upperX) then

      if (wnd(i).gt.11._fp8) then
         ko2o = sqrt((1953.4_fp8-128._fp8*etw(i)+3.9918_fp8*etw(i)**2- &
            0.050091_fp8*etw(i)**3)/660._fp8) * (0.02383_fp8 * wnd(i)**3._fp8)
      else
         ko2o = sqrt((1953.4_fp8-128._fp8*etw(i)+3.9918_fp8*etw(i)**2- &
             0.050091_fp8*etw(i)**3)/660._fp8) * (0.31_fp8 * wnd(i)**2._fp8)
      end if

      ! units of ko2 converted from cm/hr to m/day
      ko2o = ko2o*(24._fp8/100._fp8)

      wsio2o(i) = wsio2o(i) + ko2o*(OSAT(I)-O2o(I)) &
                  / pdepth(I)

   end if

   ! Calculate Percentage Oxygen Saturation

   ! ESAT(I) = 100. * ( O2O(I) / OSAT(I) )

   ! Calculate Apparent Oxygen Utilisation mMol/m3
   ! +ve = utilisation -ve = production

   ! AOU(I) = OSAT(I) - O2O(I)

   return

   end subroutine oxygen_chemistry
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Carbon chemistry \label{sec:carbonchemistry}
!
! !DESCRIPTION:
! Sets-up the components of the carbonate system and air-sea flux of CO2.\\[\baselineskip]
!
! !INTERFACE:
   subroutine carbon_chemistry(I)
!
! !USES:
   use ersem_constants, only: CMass
   use ersem_variables, only: IOProcess, eLogU, eDbgU
   use pelagic_parameters, only: pco2a3



!
   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in) :: i
!
! !LOCAL VARIABLES:
   real(fp8) :: uptake, Fwind, T, S
   real(fp8) :: fairco2, sc
   real(fp8) :: a,b,c,density
   integer :: ierr
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Call carbonate cycle routines and air>water CO2 exchange...............

   if ( iswCO2X .eq. 1 ) then

      T = ETW(I)

      S = X1X(I)

      ! Calculate total alkalinity
      if (iswtalk .eq. 1) then !!!! Old formulation
         if (S .GE. 34.65_fp8) then
            TA = (66.96_fp8*S - 36.803_fp8) / 1.0e6_fp8 ! From Bellerby & Canoba
         else
            TA = (3887._fp8-46.25_fp8*S) / 1.0e6_fp8 ! From Borges & Frankignoulle & Canoba
         end if
      else if (iswtalk.eq.2) then
         TA = (520.1_fp8+51.24_fp8*S)/1.0e6_fp8 ! from Millero et al, MarChem, 1998
      else if (iswtalk.eq.3) then
         if (T.lt.20._fp8) then
            TA = (S/35._fp8*(2291._fp8-2.69_fp8*(T-20._fp8)-0.046_fp8*(T-20._fp8)**2))/1.0e6_fp8 ! from Millero et al, MarChem, 1998
         else
            TA = (520.1_fp8+51.24_fp8*S)/1.0e6_fp8 ! from Millero et al, MarChem, 1998
         end if
      else if (iswtalk.eq.4) then
         if (T.lt.20._fp8) then
            TA = (2305._fp8+53.97_fp8*(S-35._fp8)+2.74_fp8*(S-35._fp8)**2-1.16_fp8*(T-20._fp8)-0.04_fp8*(T-20._fp8)**2)/1.0e6_fp8 ! from Lee et al., Geophys REs Lett, 1998
         else
           TA = (2305._fp8+58.66_fp8*(S-35._fp8)+2.32_fp8*(S-35._fp8)**2-1.41_fp8*(T-20._fp8)+0.04_fp8*(T-20._fp8)**2)/1.0e6_fp8 ! from Lee et al., Geophys REs Lett, 1998
         end if
      else if (iswtalk.eq.5) then ! Alkalinity as full prognostic
         TA=0.0e0_fp8
      else
         if (IOProcess) write(eLogU,*) 'iswtalk out of bounds', iswtalk

         call flush(eDbgU)
         call flush(eLogU)
         call MPI_abort(ierr)



      end if

      if (iswbioalk.eq.1)then
         TA = TA + bioalk(I)/1.0e6_fp8
      end if

      ! sea-water density (Millero & Poisson, Deep-Sea research, 1981; UNESCO, 1981)
      ! density in kg/m3, equation valid for 0<T<40 and 0.5<S<42 at atmospheric pressure of 1 atm
      a= 8.24493e-1_fp8 - 4.0899e-3_fp8*T + 7.6438e-5_fp8*T**2 - 8.2467e-7_fp8*T**3 + 5.3875e-9_fp8*T**4
      b= -5.72466e-3_fp8 + 1.0227e-4_fp8*T - 1.6546e-6_fp8*T**2
      c= 4.8314e-4_fp8
      density= (999.842594_fp8 + 6.793952e-2_fp8*T- 9.095290e-3_fp8*T**2 + 1.001685e-4_fp8*T**3 &
                  - 1.120083e-6_fp8*T**4 + 6.536332e-9_fp8*T**5+a*S+b*S**1.5_fp8+c*S**2)


      ! Scale DIC for iteration - all the CO2 sys routines need umol/kg as unit
      Ctot = O3C(I) / 1.e3_fp8 / density

      ! This is the atmospheric value of PPM CO2.
      PCO2A = pco2a3 ! needs to be set from somewhere (=385ppm for ~2008)

      ! Initialise the CO2sys with the state at the previous timestep
      pH=pHx(i)
      PCO2= PCO2W(I) / 1.e6_fp8
      H2CO3 = CarbA(I) / 1.e3_fp8 / density
      HCO3 = Bicarb(I)/1.e3_fp8/density
      CO3=Carb(I) /1.e3_fp8/density

      call co2_dynamics (T,S,I)

      ! Scaled for output (mmols C m-3)
      pHx(I)=pH
      PCO2W(I) = PCO2*1.e6_fp8
      TOTA(I) = TA*1.e6_fp8 ! TOTA expressed as umol/kg
      CarbA(I)= H2CO3*1.e3_fp8*density ! conversion to mmol/m3
      Bicarb(I)=HCO3*1.e3_fp8*density ! conversion to mmol/m3
      Carb(I) = CO3*1.e3_fp8*density ! conversion to mmol/m3

      ! Call carbonate saturation state subroutine
      call caco3_saturation ( T, S, epw(I), CO3, Om_cal(I), Om_arg(I))

      ! For surface box only calculate air-sea flux
      ! Nightingale and Liss parameterisation

      ! Only call after 2 days, because the derivation of instability in the
      if (I .le. n_upperX .AND. iswASFLUX .gt. 0)then

         if (iswASFLUX .eq. 1)then !could ditch iswASFLUX??
            ! no longer valid
         else if (iswASFLUX .eq. 2)then
            sc=2073.1_fp8-125.62_fp8*T+3.6276_fp8*T**2._fp8-0.043219_fp8*T**3
            fwind = (0.222_fp8 * wnd(I)**2 + 0.333_fp8 * wnd(I))*(sc/660._fp8)**(-0.5_fp8)
            fwind=fwind*24._fp8/100._fp8 ! convert to m/day
            UPTAKE = fwind * k0co2 * ( PCO2A/1.e6_fp8 - PCO2 )
         end if

         FAIRCO2 = UPTAKE * 1.e3_fp8 * density
         FAIRMG(I) = FAIRCO2 * CMass







         SO3C(I) = SO3C(I) + FAIRCO2/pdepth(I)
!off if(I.eq.5) if (IOProcess) write(eLogU,*) PCO2A/1.0D6, PCO2WATER

      end if
   end if

   return

   end subroutine carbon_chemistry
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: CO2 dynamics
!
! !DESCRIPTION:
! This subroutine calculates the partial pressure of CO2 (pCO2) at
! the ambient salinity, temperature, alkalinity and total CO2 and
! hence all components of the carbonate system.\\[\baselineskip]
!
! !INTERFACE:
   subroutine co2_dynamics ( T, S, I)
!
! !LOCAL VARIABLES:
   real(fp8):: PRSS, T, S, Tmax, Btot
   integer :: ICALC, I
   logical :: BORON
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   PRSS = epw(I)*1.e-5_fp8 ! convert Pa to bar
   ICALC = 1 ! use DIC and TA to calculate CO2sys
   Tmax = max(T,0._fp8)

   BORON=.True.
   if (BORON) then
      BTOT=0.0004128_fp8*S/35._fp8
   end if

   call set_co2(PRSS,Tmax,S)
   call do_co2(ICALC,BORON,BTOT,I)

   return

   end subroutine co2_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Set CO2 constants
!
! !DESCRIPTION:
! Routine to calculate CO2 system constants under the conditions set by
! P,S,T (NOTE: PRESSURE <> 1ATM IS NOT YET CODED).
!
! I. Calculate constants at P=1 and S=0 using:
! \begin{equation}
! ln(K0) = A + B/TK + C ln TK (where TK is in Kelvin)
! \end{equation}
!
! II. Calculate constants at P=1 and salinity S using
! \begin{equation}
! ln K = ln K0 + (a0 + a1/TK + a2 ln TK) S**1/2
! + (b0 + b1TK + b2TK**2) S
! \end{equation}
!
! The sources of the coefficients are as follows:
!
! \begin{center}
! \footnotesize
! \begin{centering}
! \begin{tabular}{|c|c|c|c|}
! \hline
! IC= & 1 & 2 & 3 \tabularnewline
! & (NBS pH scale) & (SWS pH scale with no F) & (SWS pH scale) \tabularnewline
! \hline
! KP & WEISS (1974) & WEISS(1974) & WEISS(1974 \tabularnewline
! K1C ) & MEHRBACH ACC. TO & HANSSON ACC. TO & HANSSON AND MEH \tabularnewline
! K2C ) & MILLERO (1979) & MILLERO (1979) & ACC. TO DICKS \tabularnewline
! KB ) & & & AND MILLERO (1 \tabularnewline
! & & & (K1C AND K2C \tabularnewline
! & & & HANSSON ACC \tabularnewline
! & & & MILLERO (1 \tabularnewline
! & & & (KB ONLY \tabularnewline
! \hline
! \end{tabular}
! \\[\baselineskip]
! \par\end{centering}
! \end{center}
!
! !INTERFACE:
   subroutine set_co2(P,T,S)
!
! !USES:
!
! !LOCAL VARIABLES:
   integer :: IC
   real(fp8) :: P,T,S,VAL,TK, delta, kappa,Rgas
   real(fp8) :: dlogTK, S2, S15, sqrtS,TK100
   DATA Rgas/83.131_fp8/
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Derive simple terms used more than once
   TK=T+273.15_fp8
   dlogTK = log(TK)
   TK100=TK/100._fp8
   S2 = S*S
   sqrtS = sqrt(S)
   S15 = S**1.5_fp8

   ! Calculation of constants as used in the OCMIP process for ICONST = 3 or 6
   ! see http://www.ipsl.jussieu.fr/OCMIP/
   ! k0co2 = CO2/fCO2 from Weiss 1974
   k0co2 = exp(93.4517/tk100 - 60.2409 + 23.3585 * log(tk100) + &
      & s * (.023517 - 0.023656 * tk100 + 0.0047036 * tk100 ** 2._fp8))

   ! the commented definition of K0co2 herebelow is the one giving bit-identical output to the old CO2sys module
   ! VAL=-167.8108_fp8 + 9345.17_fp8/TK + 23.3585_fp8*LOG(TK)
   ! VAL=VAL + (0.023517_fp8-2.3656e-4_fp8*TK+4.7036e-7_fp8*TK*TK)*S
   ! k0co2=EXP(VAL)
   ! end of old definition

   ! Correction for high pressure of K0 (from Millero 1995)
   ! added YA 04/10/2010
   delta=-25.6_fp8+0.2324_fp8*T-0.0036246_fp8*T**2
   kappa=(-5.13_fp8+0.0794_fp8*T)/1000._fp8
   k0co2=k0co2*exp((-delta+0.5_fp8*kappa*P)*P/(Rgas*TK))

   ! k1co2 = [H][HCO3]/[H2CO3]
   ! Millero p.664 (1995) using Mehrbach et al. data on seawater scale
   k1co2=10._fp8**(-1._fp8*(3670.7_fp8/TK - 62.008_fp8 + 9.7944_fp8*dlogTK - &
      & 0.0118_fp8 * S + 0.000116_fp8*S2))
   ! Correction for high pressure (from Millero 1995)
   ! added YA 04/10/2010
   delta=-25.5_fp8+0.1271_fp8*T
   kappa=(-3.08_fp8+0.0877_fp8*T)/1000._fp8
   k1co2=k1co2*exp((-delta+0.5_fp8*kappa*P)*P/(Rgas*TK))

   ! k2co2 = [H][CO3]/[HCO3]
   ! Millero p.664 (1995) using Mehrbach et al. data on seawater scale
   k2co2=10._fp8**(-1._fp8*(1394.7_fp8/TK + 4.777_fp8 - &
      & 0.0184_fp8*S + 0.000118_fp8*S2))
   ! Correction for high pressure (from Millero 1995)
   ! added YA 04/10/2010
   delta=-15.82_fp8-0.0219_fp8*T
   kappa=(1.13_fp8-0.1475_fp8*T)/1000._fp8
   k2co2=k2co2*exp((-delta+0.5_fp8*kappa*P)*P/(Rgas*TK))

   ! kb = [H][BO2]/[HBO2]
   ! Millero p.669 (1995) using data from Dickson (1990)
   kb=exp((-8966.9_fp8 - 2890.53_fp8*sqrtS - 77.942_fp8*S + &
      & 1.728_fp8*S15 - 0.0996_fp8*S2)/TK + &
      & (148.0248_fp8 + 137.1942_fp8*sqrtS + 1.62142_fp8*S) + &
      & (-24.4344_fp8 - 25.085_fp8*sqrtS - 0.2474_fp8*S) * &
      & dlogTK + 0.053105_fp8*sqrtS*TK)
   ! Correction for high pressure (from Millero, 1995)
   ! added YA 04/10/2010
   delta=-29.48_fp8+0.1622_fp8*T-0.002608_fp8*T**2._fp8
   kappa=-2.84_fp8/1000._fp8
   kb=kb*exp((-delta+0.5_fp8*kappa*P)*P/(Rgas*TK))

   return

   end subroutine set_co2
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: CO2 calculation
!
! !DESCRIPTION:
! Routine to carry out CO2 calculations with 2 fixed parameters according to
! the equations given by parks(1969) and skirrow (1975)
! with additions for including boron if boron=.true.\\[\baselineskip]
!
! !INTERFACE:
   subroutine do_co2(ICALC,BORON,BTOT,I)
!
! !USES:
!
! !LOCAL VARIABLES:
   integer :: ICALC, KARL, LQ

! Put counter in to check duration in convergence loop
   integer :: COUNTER,C_CHECK,C_SW,I
   real(fp8) :: ALKC, ALKB,BTOT
   real(fp8) :: AKR,AHPLUS
   real(fp8) :: PROD,tol1,tol2,tol3,tol4,steg,fak
   real(fp8) :: STEGBY,Y,X,W,X1,Y1,X2,Y2,FACTOR,TERM,Z
   LOGICAL :: BORON,DONE
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! DERIVING PH REQUIRES FOLLOWING LOOP TO CONVERGE.
   ! THIS SUBROUTINE RELIES ON CONVERGENCE. If THE ENVIRONMENTAL
   ! CONDITIONS DO NOT ALLOW FOR CONVERGENCE (IN 3D MODEL THIS IS
   ! LIKELY TO OCCUR NEAR LOW SALINITY REGIONS) THE MODEL WILL
   ! BE STUCK IN THE LOOP. TO AVOID THIS A CONVERGENCE CONDITION
   ! IS PUT IN PLACE TO SET A FLAGG OF -99 IN THE PH VAR FOR NON CONVEGENCE.
   ! THE MODEL IS THEN ALLOWED TO CONTINUE. 'COUNTER, C_SW,C_CHECK' ARE
   ! THE LOCAL VARS USED.
   ! C_SW = condition of convergence 0=yes, 1= no
   ! COUNTER = number of iterations
   ! C_CHECK = maximum number of iterations

   ! SET COUNTER AND SWITCH TO ZERO AND OFF
   COUNTER=0
   C_SW=0
   ! FROM EXPERIENCE if THE ITERATIONS IN THE FOLLOWING DO LOOP
   ! EXCEEDS 15 CONVERGENCE WILL NOT OCCUR. THE OVERHEAD OF 25 ITERATIONS
   ! IS OK FOR SMALL DOMAINS WITH 1/10 AND 1/15 DEG RESOLUTION.
   ! I RECOMMEND A LOWER VALUE OF 15 FOR HIGHER RESOLUTION OR LARGER DOMAINS.
   C_CHECK=25
   ! If CONVERGENCE IS NOT ACHIEVED THE LOCAL ARRAY CONCS MUST BE STORED TO
   ! ALLOW THE MODEL TO CONTINUE. THEREFORE ....
   CO2sys_memory(1) = Ctot
   CO2sys_memory(2) = TA
   CO2sys_memory(3) = pH
   CO2sys_memory(4) = PCO2
   CO2sys_memory(5) = H2CO3
   CO2sys_memory(6) = HCO3
   CO2sys_memory(7) = CO3

! do II=1,NKVAL
! AKVAL2(II)=AKVAL(II)
! end do

   AKR = k1co2/k2co2
   AHPLUS=10._fp8**(-PH)
   PROD=AKR*k0co2*PCO2

   if(BORON) then

      if (ICALC .eq. 1 .OR. ICALC .eq. 4) then
! *** TA, BTOT AND CTOT OR PCO2 FIXED ***
! *** ITERATIVE CALCULATION NECESSARY HERE

! SET INITIAL GUESSES AND TOLERANCE
         H2CO3=PCO2*k0co2
         CO3=TA/10._fp8
         AHPLUS=1.e-8_fp8
         ALKB=BTOT
         TOL1=TA/1.e5_fp8
         TOL2=H2CO3/1.e5_fp8
         TOL3=CTOT/1.e5_fp8
         TOL4=BTOT/1.e5_fp8
         ! HALTAFALL iteration to determine CO3, ALKB, AHPLUS
         KARL=1
         STEG=2._fp8
         FAK=1._fp8
         STEGBY=0.4_fp8
         DONE=.FALSE.

         do while (.not.DONE)

            DONE=.true.

            ! SET COUNTER UPDATE.
            COUNTER=COUNTER+1

            ! CHECK if CONVERGENCE HAS OCCURED IN THE NUMBER OF
            ! ACCEPTABLE ITTERATIONS.
            if (counter .ge. c_check) then
               !! if(MASTER)THEN
               !!! LOG FILE TO SHOW WHEN AND WHERE NON CONVERGENCE OCCURS.
               !! if (IOProcess) write(eLogU,*) 'ERRORLOG ',III,' ',(CONCS2(II),II=1,NCONC)
               !! end if
               ! if NON CONVERGENCE, THE MODEL REQUIRES CONCS TO CONTAIN USABLE VALUES.
               ! BEST OFFER BEING THE OLD CONCS VALUES WHEN CONVERGENCE HAS BEEN
               ! ACHIEVED
               Ctot = CO2sys_memory(1)
               TA = CO2sys_memory(2)
               pH = CO2sys_memory(3)
               PCO2 = CO2sys_memory(4)
               H2CO3= CO2sys_memory(5)
               HCO3 = CO2sys_memory(6)
               CO3 = CO2sys_memory(7)

               ! RESET SWITCH FOR NEXT call TO THIS subroutine
               C_SW=0

               return

            end if

            if (ICALC .eq. 4) then
               ! *** PCO2 IS FIXED ***
               Y=AHPLUS*AHPLUS*CO3/(k1co2*k2co2)
               if (ABS(Y-H2CO3) .gt. TOL2) then
                  CO3=CO3*H2CO3/Y
                  DONE=.false.
               end if
            else if (ICALC.eq.1) then
               ! *** CTOT IS FIXED ***
               Y=CO3*(1._fp8+AHPLUS/k2co2+AHPLUS*AHPLUS/(k1co2*k2co2))
               if(ABS(Y-CTOT) .gt. TOL3) then
                  CO3=CO3*CTOT/Y
                  DONE=.false.
               end if
            end if

            Y=ALKB*(1._fp8+AHPLUS/kb)
            if (ABS(Y-BTOT) .gt. TOL4) then
               ALKB=ALKB*BTOT/Y
               DONE=.false.
            end if

            ! Alkalinity is equivalent to -(total H+), so the sign of W is opposite
            ! to that normally used

            Y=CO3*(2._fp8+AHPLUS/k2co2)+ALKB
            if (ABS(Y-TA) .gt. TOL1) then
               DONE=.false.
               X=LOG(AHPLUS)
               W=SIGN(1._fp8,Y-TA)
               if (W .ge. 0._fp8) then
                  X1=X
                  Y1=Y
               else
                  X2=X
                  Y2=Y
               end if
               LQ=KARL
               if (LQ .eq. 1) then
                  KARL=2*NINT(W)
               else if(IABS(LQ).eq.2.AND.(LQ*W).LT.0._fp8) then
                  FAK=0.5_fp8
                  KARL=3
               end if
               if (KARL .eq. 3 .AND. STEG .LT. STEGBY) then
                  W=(X2-X1)/(Y2-Y1)
                  X=X1+W*(TA-Y1)
               else
                  STEG=STEG*FAK
                  X=X+STEG*W
               end if
               AHPLUS=EXP(X)
            end if

            ! LOOP BACK UNTIL CONVERGENCE HAS BEEN ACHIEVED
            ! OR MAX NUMBER OF ITERATIONS (C_CHECK) HAS BEEN REACHED.
         end do

         HCO3=CO3*AHPLUS/k2co2

         if (ICALC .eq. 4) then
            CTOT=H2CO3+HCO3+CO3
         else if (ICALC .eq. 1) then
            H2CO3=HCO3*AHPLUS/k1co2
            PCO2=H2CO3/k0co2
         end if

         PH=-LOG10(AHPLUS)
         ALKC=TA-ALKB

      else if(ICALC.eq.2) then
         ! *** CTOT, PCO2, AND BTOT FIXED ***
         Y=SQRT(PROD*(PROD-4._fp8*k0co2*PCO2+4._fp8*CTOT))
         H2CO3=PCO2*k0co2
         HCO3=(Y-PROD)/2._fp8
         CO3=CTOT-H2CO3-HCO3
         ALKC=HCO3+2._fp8*CO3
         AHPLUS=k1co2*H2CO3/HCO3
         PH=-LOG10(AHPLUS)
         ALKB=BTOT/(1._fp8+AHPLUS/kb)
         TA=ALKC+ALKB
      else if(ICALC.eq.3) then
         ! *** CTOT, PH AND BTOT FIXED ***
         FACTOR=CTOT/(AHPLUS*AHPLUS+k1co2*AHPLUS+k1co2*k2co2)
         CO3=FACTOR*k1co2*k2co2
         HCO3=FACTOR*k1co2*AHPLUS
         H2CO3=FACTOR*AHPLUS*AHPLUS
         PCO2=H2CO3/k0co2
         ALKC=HCO3+2._fp8*CO3
         ALKB=BTOT/(1._fp8+AHPLUS/kb)
         TA=ALKC+ALKB
      else if(ICALC.eq.5) then
         ! *** TA, PH AND BTOT FIXED ***
         ALKB=BTOT/(1._fp8+AHPLUS/kb)
         ALKC=TA-ALKB
         HCO3=ALKC/(1._fp8+2._fp8*k2co2/AHPLUS)
         CO3=HCO3*k2co2/AHPLUS
         H2CO3=HCO3*AHPLUS/k1co2
         PCO2=H2CO3/k0co2
         CTOT=H2CO3+HCO3+CO3
      else if(ICALC.eq.6) then
! *** PCO2, PH AND BTOT FIXED ***
         ALKB=BTOT/(1._fp8+AHPLUS/kb)
         H2CO3=PCO2*k0co2
         HCO3=H2CO3*k1co2/AHPLUS
         CO3=HCO3*k2co2/AHPLUS
         CTOT=H2CO3+HCO3+CO3
         ALKC=HCO3+2._fp8*CO3
         TA=ALKC+ALKB
      end if

   else
      if (ICALC .eq. 1) then
         ! *** CTOT AND TA FIXED ***
         TERM=4._fp8*TA+CTOT*AKR-TA*AKR
         Z=SQRT(TERM*TERM+4._fp8*(AKR-4._fp8)*TA*TA)
         CO3=(TA*AKR-CTOT*AKR-4._fp8*TA+Z)/(2._fp8*(AKR-4._fp8))
         HCO3=(CTOT*AKR-Z)/(AKR-4._fp8)
         H2CO3=CTOT-TA+CO3
         PCO2=H2CO3/k0co2
         PH=-LOG10(k1co2*H2CO3/HCO3)
      else if (ICALC .eq. 2) then
         ! *** CTOT AND PCO2 FIXED ***
         Y=SQRT(PROD*(PROD-4._fp8*k0co2*PCO2+4._fp8*CTOT))
         H2CO3=PCO2*k0co2
         HCO3=(Y-PROD)/2._fp8
         CO3=CTOT-H2CO3-HCO3
         TA=HCO3+2._fp8*CO3
         PH=-LOG10(k1co2*H2CO3/HCO3)
      else if (ICALC .eq. 3) then
         ! *** CTOT AND PH FIXED ***
         FACTOR=CTOT/(AHPLUS*AHPLUS+k1co2*AHPLUS+k1co2*k2co2)
         CO3=FACTOR*k1co2*k2co2
         HCO3=FACTOR*k1co2*AHPLUS
         H2CO3=FACTOR*AHPLUS*AHPLUS
         PCO2=H2CO3/k0co2
         TA=HCO3+2._fp8*CO3
      else if (ICALC .eq. 4) then
         ! *** TA AND PCO2 FIXED ***
         TERM=SQRT((8._fp8*TA+PROD)*PROD)
         CO3=TA/2._fp8+PROD/8._fp8-TERM/8._fp8
         HCO3=-PROD/4._fp8+TERM/4._fp8
         H2CO3=PCO2*k0co2
         CTOT=CO3+HCO3+H2CO3
         PH=-LOG10(k1co2*H2CO3/HCO3)
      else if (ICALC .eq. 5) then
         ! *** TA AND PH FIXED ***
         HCO3=TA/(1._fp8+2._fp8*k2co2/AHPLUS)
         CO3=HCO3*k2co2/AHPLUS
         H2CO3=HCO3*AHPLUS/k1co2
         PCO2=H2CO3/k0co2
         CTOT=H2CO3+HCO3+CO3
      else if (ICALC .eq. 6) then
         ! *** PCO2 AND PH FIXED ***
         H2CO3=PCO2*k0co2
         HCO3=H2CO3*k1co2/AHPLUS
         CO3=HCO3*k2co2/AHPLUS
         CTOT=H2CO3+HCO3+CO3
         TA=HCO3+2._fp8*CO3
      end if

   end if

! do II=1,NCONC
! CONCS(II)=CONCS2(II)
! end do

   return

   end subroutine do_co2
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: CaCO3 Saturation
!
! !DESCRIPTION:
! Routine to calculate the saturation state of calcite and aragonite.
! Source: Zeebe and Wolf-Gladrow 2001 following Mucci (1983)
! with pressure corrections from Millero (1995). Code tested against
! reference values given in Z and W-G.\\[\baselineskip]
!
! !INTERFACE:
   subroutine caco3_saturation (Tc, S, Pr, CO3, Om_cal, Om_arg)
!
! !INPUT PARAMETERS:
! Tc Temperature (C)
! S Salinity
! Pr Pressure (Pa)
! CO3 Carbonate ion concentration (mol.-l ie /1D6)
   real(fp8) :: Tc, S, Pr, CO3
!
! !OUTPUT PARAMETERS:
! Om_cal Calite saturation
! Om_arg Aragonite saturation
   real(fp8) :: Om_cal
   real(fp8) :: Om_arg
!
! !LOCAL VARIABLES:
! K_cal Stoichiometric solubility product for calcite
! K_arg Stoichiometric solubility product for aragonite
! Ca Calcium 2+ concentration (mol.kg-1)
! P Pressure (bars)
   real(fp8) Tk, Kelvin, Ca
   real(fp8) logKspc, Kspc
   real(fp8) logKspa, Kspa
   real(fp8) tmp1, tmp2, tmp3
   real(fp8) dV, dK, P, R
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! setup
   Kelvin = 273.15_fp8
   Tk = Tc + Kelvin
   Ca = 0.01028_fp8 ! Currently oceanic mean value at S=25, needs refining)
   R = 83.131_fp8 !(cm3.bar.mol-1.K-1)
   P = Pr*1.e-5_fp8 !pressure in bars

   ! calculate K for calcite
   tmp1 = -171.9065_fp8 - (0.077993_fp8*Tk) + (2839.319_fp8/Tk) + 71.595_fp8*log10(Tk)
   tmp2 = + (-0.77712_fp8 + (0.0028426_fp8*Tk) + (178.34_fp8/Tk))*SQRT(S)
   tmp3 = - (0.07711_fp8*S) + (0.0041249_fp8*(S**1.5_fp8))
   logKspc = tmp1 + tmp2 + tmp3
   Kspc = 10._fp8**logKspc

   ! correction for pressure for calcite
   dV = -48.76_fp8 + 0.5304_fp8*Tc
   dK = -11.76_fp8/1.e3_fp8 + (0.3692_fp8/1.e3_fp8) * Tc
   tmp1 = -(dV/(R*Tk))*P + (0.5_fp8*dK/(R*Tk))*P*P
   Kspc = Kspc*exp(tmp1)
   logKspc = log10(Kspc)

   ! calculate K for aragonite
   tmp1 = -171.945_fp8 - 0.077993_fp8*Tk + 2903.293_fp8 / Tk + 71.595_fp8* log10(Tk)
   tmp2 = + (-0.068393_fp8 + 0.0017276_fp8*Tk + 88.135_fp8/Tk)*SQRT(S)
   tmp3 = - 0.10018_fp8*S + 0.0059415_fp8*S**1.5_fp8
   logKspa = tmp1 + tmp2 + tmp3
   Kspa = 10._fp8**logKspa

   ! correction for pressure for aragonite
   dV = -46._fp8 + 0.530_fp8*Tc
   dK = -11.76_fp8/1.e3_fp8 + (0.3692_fp8/1.e3_fp8) * Tc
   tmp1 = -(dV/(R*Tk))*P + (0.5_fp8*dK/(R*Tk))*P*P
   Kspa = Kspa*exp(tmp1)
   logKspa = log10(Kspa)

   ! calculate saturation states
   Om_cal = (CO3 * Ca) / Kspc
   Om_arg = (CO3 * Ca) / Kspa

   return

   end subroutine caco3_saturation
!
!EOC
!-----------------------------------------------------------------------

   end module gas_dynamics

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
