

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Sedimentation
!
! !DESCRIPTION:
! This module handles sedimentation rate calculations. (Calculations
! of elevated sedimentation rates in response to nutrient stress are
! handled separately in module primary\_producers.)\\[\baselineskip]
!
! !INTERFACE:
   module sedimentation
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_sedimentation_parameters, set_sedimentation_velocities, &
      do_sedimentation
!
! !LOCAL VARIABLES:
! sinking velocity of diatoms [m/d]
   real(fp8) :: rp1mX

! sinking velocity of medium size phytoplankton [m/d]
   real(fp8) :: rp2mX

! sinking velocity of small phytoplankton [m/d]
   real(fp8) :: rp3mX

! sinking velocity of large phytoplankton [m/d]
   real(fp8) :: rp4mX

! sinking velocity of small POM [m/d]
   real(fp8) :: rR4mX

! sinking velocity of medium size POM [m/d]
   real(fp8) :: rr6mX

! sinking velocity of large POM [m/d]
   real(fp8) :: rR8mX
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team

! James Clark (PML): Created module sedimentation based on original
! Fortran subroutines set_sedimentation and calc_subsidance.
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise parameters
!
! !DESCRIPTION:
! Initialise sedimentation parameters.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_sedimentation_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU
!
! !LOCAL VARIABLES:
   namelist/sedimentation_parameters/rR8mX,rR6mX,rR4mX,rP1mX,&
      rP2mX,rP3mX,rP4mX
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading sedimentation parameters'
   open(99,file='BioParams/sedimentation.nml',status='old')
   read(99,nml=sedimentation_parameters)

   close (99)

   return

   end subroutine set_sedimentation_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Set Sedimentation velocities \label{sec:setsedvel}
!
! !DESCRIPTION:
! Calculate sedimentation rates.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_sedimentation_velocities(I)
!
! !USES:
   use pelagic_variables, only: sdR4c, sdR4p, sdR4n, &
      sdR6c, sdR6p, sdR6n, sdR6s, &
      sdR8c, sdR8p, sdR8n, sdR8s, &
      sdP1c, sdP1p, sdP1n, sdP1s, sdChl1, &
      sdP2c, sdP2p, sdP2n, sdChl2, &
      sdP3c, sdP3p, sdP3n, sdChl3, &
      sdP4c, sdP4p, sdP4n, sdChl4

   use pelagic_variables, only: sdP1f, sdP2f, sdP3f, sdP4f

!
! !INPUT PARAMETERS:
   integer, intent(in) :: I
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   SDR6c(I) = rR6mX
   SDR6p(I) = rR6mX
   SDR6n(I) = rR6mX
   SDR6s(I) = rR6mX

   SDR4c(I) = rR4mX
   SDR4p(I) = rR4mX
   SDR4n(I) = rR4mX

   SDR8c(I) = rR8mX
   SDR8p(I) = rR8mX
   SDR8n(I) = rR8mX
   SDR8s(I) = rR8mX

   SDP1c(I) = SDP1c(I)+rP1mX
   SDP1p(I) = SDP1p(I)+rP1mX
   SDP1n(I) = SDP1n(I)+rP1mX
   SDP1s(I) = SDP1s(I)+rP1mX
   SDChl1(I) = SDChl1(I)+rP1mx

   SDP2c(I) = SDP2c(I)+rP2mX
   SDP2p(I) = SDP2p(I)+rP2mX
   SDP2n(I) = SDP2n(I)+rP2mX
   SDChl2(I) = SDChl2(I)+rP2mx

   SDP3c(I) = SDP3c(I)+rP3mX
   SDP3p(I) = SDP3p(I)+rP3mX
   SDP3n(I) = SDP3n(I)+rP3mX
   SDChl3(I) = SDChl3(I)+rP3mx

   SDP4c(I) = SDP4c(I)+rP4mX
   SDP4p(I) = SDP4p(I)+rP4mX
   SDP4n(I) = SDP4n(I)+rP4mX
   SDChl4(I) = SDChl4(I)+rP4mx


   SDP1f(I) = SDP1f(I) + rP1mX
   SDP2f(I) = SDP2f(I) + rP2mX
   SDP3f(I) = SDP3f(I) + rP3mX
   SDP4f(I) = SDP4f(I) + rP4mX


   return

   end subroutine set_sedimentation_velocities
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Compute sedimentation
!
! !DESCRIPTION:
! Calculate import/export to/from pelagic boxes due to
! sedimentation and save in wso/wsi pelagic variable arrays.\\[\baselineskip]
!
! !INTERFACE:
   subroutine do_sedimentation
!
! !USES:
   use ersem_constants, only: fp8
   use ersem_variables, only: timestep
   use pelagic_variables, only: n_comp, i_state, sdCCC, cccP, &
                                wsiccc, wsoccc, pdepth, lowerX
!
! !LOCAL VARIABLES:
   integer :: inbox, outbox
   integer :: i, j
   real(fp8) :: r
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   do j=1,N_COMP
      do i=1,I_STATE
         outbox = j
         inbox = lowerX(outbox)
         ! sinking velocity:
         r=sdCCC(outbox,i)
         if (r.gt.0._fp8) then
            ! limit subsidence with biomass available:
            r=cccP(outbox,i)*min(r,pdepth(outbox)/timestep)
            if (inbox.ne.0) then
               wSICCC(inbox,i)=wSICCC(inbox,i)+r/pdepth(inbox)
               wSOCCC(outbox,i)=wSOCCC(outbox,i)+r/pdepth(outbox)
            end if
         end if
      end do
   end do

   end subroutine do_sedimentation
!
!EOC
!-----------------------------------------------------------------------

   end module sedimentation

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
