

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: The pelagic zone
!
! !DESCRIPTION:
! This module contains procedures which coordinate the update of the
! pelagic system.\\[\baselineskip]
!
! !INTERFACE:
   module pelagic_zone
!
! !USES:
   use ersem_constants, only: fp8
   use pelagic_variables
   use gas_dynamics, only: carbon_chemistry,oxygen_chemistry

   implicit none

! Default all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_pelagic_zone_parameters, pelagic_zone_dynamics
!
! !PUBLIC DATA MEMBERS:
! Food treshold for overwintering state of zoolplankton [mg C/m^2]
   real(fp8), public :: MinpreyX

   real(fp8), public :: z4cINT
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise pelagic zone parameters
!
! !DESCRIPTION:
! Initialise pelagic zone parameters from namelist \texttt{pelParameters}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_pelagic_zone_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU
   use gas_dynamics, only: uB1c_O2X, urB1_O2X
   use pelagic_parameters

   implicit none
!
! !LOCAL VARIABLES:
   namelist/pelParameters/uB1c_O2X,urB1_O2X,&
      MinpreyX,qnZIcX,qpZIcX,xR1pX,xR1nX,xR7pX,xR7nX,R1R2X
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading general pelagic parameters'
   open(99,file='BioParams/pel.nml',status='old')
   read(99,nml=pelParameters)

   close (99)

   return

   end subroutine set_pelagic_zone_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Update the pelagic system
!
! !DESCRIPTION:
! In this routine all pelagic processes are computed successively to
! provide the rates of change $\left.\frac{\partial\mathbf{c}}{\partial t}\right|_{\textit{pel}}$:
! \begin{enumerate}
! \item The routine to compute the available vertically integrated mesozooplankton
! prey (\texttt{\ref{sec:preyintegration}}) is called, activated by the
! overwintering switch \texttt{Z4\_OW\_SW}.
! \item The routine to set the dynamic carbon ratios (\texttt{\ref{sec:carbonratios}})
! is called.
! \item The chemical environment is established through calling the routines
! \texttt{oxygen\_chemistry} (\ref{sec:oxygenchemistry}) and \texttt{carbon\_chemistry}
! (\ref{sec:carbonchemistry}) and computing the relative oxygen
! saturation state according to:
! \begin{equation}
! \mathbf{s}_{\textit{relO}_{2}}=\mathrm{min}(1,\mathbf{c}_{O_{2}}/\mathbf{s}_{O_{2}}).
! \end{equation}
!
! \item Rain ratio and dissolution of calcite are computed (\ref{sec:setcalcification}),
! if the carbonate system is activated (\texttt{ISWCO2X}$=1$).
! \item Nutrient limitation states are computed by calling \texttt{nutrient\_limitation}
! (\ref{sec:nutrientlimitation}).
! \item Phytoplankton processes (\texttt{diatom\_dynamics}, \texttt{nanophytoplankton\_dynamics},
! \texttt{picophytoplankton\_dynamics}, \texttt{microphytoplankton\_dynamics}
! \ref{sec:diatomdynamics}-\ref{sec:microphytdynamics}) and the processes
! for heteroflagellates (\ref{sec:hetflagdynamics}), microzooplankton
! (\ref{sec:microzoodynamics}), mesozooplankton (\ref{sec:mesozoodynamics}),
! and bacteria (\ref{sec:bacteriadynamics})
! are computed if they have been activated through their respective
! switches (\texttt{ISWP1X}, \texttt{ISWP2X}, \texttt{ISWP2X}, \texttt{ISWP4X},
! \texttt{ISWZ6X}, \texttt{ISWZ5X}, \texttt{ISWZ4X}, and \texttt{ISWB1X}).
! If the overwintering/hibernation switch for mesozooplankton is active
! and the threshold for vertically integrated prey availability (\ref{sec:preyintegration})
! is not reached, the routine for reduced activity (\ref{sec:mesozooow})
! is called, otherwise the standard routine for full dynamics (\ref{sec:mesozoodynamics}).
! \item If bacteria dynamics are included (pre-procssing key \texttt{NOBAC}
! is undefined) the biogeochemical fluxes mediated through bacteria
! are computed (\ref{sec:bacteriadynamics}).
! \item Remineralisation is computed (\ref{sec:remineralisation}).
! \item Aggregated diagnostics are computed (\texttt{derived\_vars}, \ref{sec:derivedvars}).
! \item If the carbonate system is active, calcification is computed (\ref{sec:docalcification}).
! \item Sedimentation velocities are set (\texttt{set\_sedimentation\_velocities}\ref{sec:setsedvel}).
! \end{enumerate}
!
! !INTERFACE:
   subroutine pelagic_zone_dynamics
!
! !USES:
   use primary_producers, only: nutrient_limitation, diatom_dynamics, &
      nanophytoplankton_dynamics, picophytoplankton_dynamics, &
      microphytoplankton_dynamics
   use heterotrophic_nanoflagellates, only: &
      heterotrophic_nanoflagellate_dynamics
   use microzooplankton, only: microzooplankton_dynamics
   use mesozooplankton, only: mesozooplankton_prey_integration, &
      mesozooplankton_over_wintering_dynamics, &
      mesozooplankton_dynamics
   use bacteria, only: bacteria_dynamics, do_remineralisation

   use calcification, only: set_calcification, do_calcification

   use sedimentation, only: set_sedimentation_velocities

   implicit none
!
! !LOCAL VARIABLES:
   integer :: I
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! jth zooplankton vertical prey integration
   if (Z4_OW_SW .eq. 1) call mesozooplankton_prey_integration

   ! Here the actual simulation for each compartment starts...............
   do I = 1, N_COMP

      call carbon_ratios(I)

      call oxygen_chemistry(I)

      call carbon_chemistry(i)

      ! The relative oxygen saturation is calculated.........................
      eO2mO2 = MIN(1._fp8,O2o(I)/OSAT(I))


      ! Set calcification fractions..........................................
      if (ISWCO2X.eq.1) call set_calcification (I)

      !..Total phytoplankton biomass in mgC/m3
      ! PIc(I) = P1c(I)+P2c(I)+P3c(I)+P4c(I)

      !..Total phytoplankton biomass less Diatoms in mgC/m3
      ! Pndc(I) = P2c(I)+P3c(I)+P4c(I)

      !..Total phytoplankton biomass in mmolN/m3
      ! PIn(I) = P1n(I)+P2n(I)+P3n(I)+P4n(I)

      !..Total phytoplankton biomass less Diatoms in mmolN/m3
      ! Pndn(I) = P2n(I)+P3n(I)+P4n(I)

      ! Call biological and associated modules...............................
      call nutrient_limitation(I)
      if (ISWP1X.eq.1) call diatom_dynamics(I)
      if (ISWP2X.eq.1) call nanophytoplankton_dynamics(I)
      if (ISWP3X.eq.1) call picophytoplankton_dynamics(I)
      if (ISWP4X.eq.1) call microphytoplankton_dynamics(I)
      if (ISWZ5X.eq.1) call microzooplankton_dynamics(I)
      if (ISWZ6X.eq.1) call heterotrophic_nanoflagellate_dynamics(I)

      ! Mesozoo over wintering condition
      if (ISWZ4X.eq.1) then

      !jth if (Z4_OW_SW .eq. 1) call mzoopreyint(I)

         if ((Z4_OW_SW .eq. 1) .and. (preyint(i) .lt. MinpreyX)) then
            call mesozooplankton_over_wintering_dynamics(I)
         else
            call mesozooplankton_dynamics(I)
         end if

      end if


      if (ISWB1X.eq.1) call bacteria_dynamics(I)


      call do_remineralisation(I)

      call derived_vars(I)


      if (ISWCO2X.eq.1) call do_calcification (I)


      call set_sedimentation_velocities(I)

   end do

   return

   end subroutine pelagic_zone_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Calculate carbon ratios \label{sec:carbonratios}
!
! !DESCRIPTION:
! Computes the internal stochiometric ratios with respect to carbon.
! Mind that these are in {\em ERSEM} units, i.e. carbon mass is given
! in $\textit{mg}$, while nutrient and gas masses are given in
! $\textit{mmol}$, so the unit for macronutrient to carbon ratios is
! $\frac{\textit{mmol}}{\textit{mg}}$. \\[\baselineskip]
!
! !INTERFACE:
   subroutine carbon_ratios( I )
!
! !INPUT PARAMETERS:
   integer, intent(in) :: I
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
!..calculate chlorophyll-a : carbon ratios for phtyoplankton variables....
! if (P1c(I) .gt. ZeroX ) then
   ChlC1(I) = Chl1(I)/P1c(I)
! else
! ChlC1(I) = 0d0
! end if

! if (P2c(I) .gt. ZeroX ) then
   ChlC2(I) = Chl2(I)/P2c(I)
! else
! ChlC2(I) = 0d0
! end if

! if (P3c(I) .gt. ZeroX ) then
   ChlC3(I) = Chl3(I)/P3c(I)
! else
! ChlC3(I) = 0d0
! end if

! if (P4c(I) .gt. ZeroX ) then
   ChlC4(I) = Chl4(I)/P4c(I)
! else
! ChlC4(I) = 0d0
! end if
!

!..calculate nutrient : carbon ratios for organic state variables.......

   ! For pelagic detritus
! if (R4c(I) .gt. ZeroX ) then
      qpR4c(I) = R4p(I)/R4c(I)
      qnR4c(I) = R4n(I)/R4c(I)
! else
! qpR4c(I) = 0d0
! qnR4c(I) = 0d0
! end if

   ! For pelagic detritus
! if (R8c(I) .gt. ZeroX ) then
      qpR8c(I) = R8p(I)/R8c(I)
      qnR8c(I) = R8n(I)/R8c(I)
! qsR8c(I) = R8s(I)/R8c(I)
! else
! qpR8c(I) = 0d0
! qnR8c(I) = 0d0
! qsR8c(I) = 0d0
! end if

   ! For pelagic detritus
! if (R6c(I) .gt. ZeroX ) then
      qpR6c(I) = R6p(I)/R6c(I)
      qnR6c(I) = R6n(I)/R6c(I)
      qsR6c(I) = R6s(I)/R6c(I)
! else
! qpR6c(I) = 0d0
! qnR6c(I) = 0d0
! qsR6c(I) = 0d0
! end if

   ! For microzooplankton
! if (Z5c(I) .gt. ZeroX ) then
      qpZ5c(I) = Z5p(I)/Z5c(I)
      qnZ5c(I) = Z5n(I)/Z5c(I)
! else
! qpZ5c(I) = 0d0
! qnZ5c(I) = 0d0
! end if

   ! For heteroflagellates
! if (Z6c(I) .gt. ZeroX ) then
      qpZ6c(I) = Z6p(I)/Z6c(I)
      qnZ6c(I) = Z6n(I)/Z6c(I)
! else
! qpZ6c(I) = 0d0
! qnZ6c(I) = 0d0
! end if

   ! For diatoms
! if (P1c(I) .gt. ZeroX ) then
      qpP1c(I) = P1p(I)/P1c(I)
      qnP1c(I) = P1n(I)/P1c(I)
      qsP1c(I) = P1s(I)/P1c(I)
! else
! qpP1c(I) = 0d0
! qnP1c(I) = 0d0
! qsP1c(I) = 0d0
! end if

   ! For flagellate phytoplankton
! if (P2c(I) .gt. ZeroX ) then
      qpP2c(I) = P2p(I)/P2c(I)
      qnP2c(I) = P2n(I)/P2c(I)
! else
! qpP2c(I) = 0d0
! qnP2c(I) = 0d0
! end if

   ! For picophytoplankton
! if (P3c(I) .gt. ZeroX ) then
      qpP3c(I) = P3p(I)/P3c(I)
      qnP3c(I) = P3n(I)/P3c(I)
! else
! qpP3c(I) = 0d0
! qnP3c(I) = 0d0
! end if

   ! For dino flagellates
! if (P4c(I) .gt. ZeroX ) then
      qpP4c(I) = P4p(I)/P4c(I)
      qnP4c(I) = P4n(I)/P4c(I)
! else
! qpP4c(I) = 0d0
! qnP4c(I) = 0d0
! end if

   ! For pelagic bacteria
! if (B1c(I) .gt. ZeroX ) then
      qpB1c(I) = B1p(I)/B1c(I)
      qnB1c(I) = B1n(I)/B1c(I)
! else
! qpB1c(I) = 0d0
! qnB1c(I) = 0d0
! end if


   qfP1c(I)= P1f(I)/P1c(I)
   qfP2c(I)= P2f(I)/P2c(I)
   qfP3c(I)= P3f(I)/P3c(I)
   qfP4c(I)= P4f(I)/P4c(I)


   return

   end subroutine carbon_ratios
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Calculate derived variables \label{sec:derivedvars}
!
! !DESCRIPTION:
! Computes some aggregated fluxes for diagnostic purposes.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine derived_vars( I )
!
! !INPUT PARAMETERS:
   integer, intent(in) :: I
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC

!..The fractional biomass of each group in the phytoplankton
! if (PIc(I) .gt. ZeroX) then
! pP1PI(I) = P1c(I)/PIc(I)
! pP2PI(I) = P2c(I)/PIc(I)
! pP3PI(I) = P3c(I)/PIc(I)
! pP4PI(I) = P4c(I)/PIc(I)
! else
! pP1PI(I) = 0d0
! pP2PI(I) = 0d0
! pP3PI(I) = 0d0
! pP4PI(I) = 0d0
! end if

!..phytoplankton biomass expressed as chl-a (mg/m3)

! CHL(I) = Chl1(I)+Chl2(I)+Chl3(I)+Chl4(I)

!..POC is the sum of all particulate carbon state variables (mgC/M3)
! POC(I) = B1c(I)+Z4c(I)+Z5c(I)+Z6c(I) &
! & +P1c(I)+P2c(I)+P3c(I)+P4c(I) &
! & +R4c(I)+R6c(I)+R8c(I)

!..ZI is the sum of the biomass of micro- and mesozooplankton
! ZIc(I) = Z4c(I)+Z5c(I)+Z6c(I)

!..dominant nutrient limitation
! dindip1(I) = (N3n(I) + N4n(I)) / N1p(I)
! dindip2(I) = N3n(I) / N1p(I)

   ! Net production
   netpp(I) = netP1(I) + netP2(I) + netP3(I) + netP4(I)
! netPPnD(i) = netP2(I) + netP3(I) + netP4(I)

!..f-ratio
! N3nprod(I) = fN3P1n(I) + fN3P2n(I) + fN3P3n(I) + fN3P4n(I)
! N4nprod(I) = fN4P1n(I) + fN4P2n(I) + fN4P3n(I) + fN4P4n(I)

! if ((N3nprod(I).gt. ZeroX).AND.(N4nprod(I) .gt. ZeroX)) then
! fratio(I) = N3nprod(I) / (N3nprod(I) + N4nprod(I))
! else if ( N3nprod(I).gt. ZeroX ) then
! fratio(I) = 1.0d0
! else if ( N4nprod(I).gt. ZeroX ) then
! fratio(I) = 0d0
! else
! fratio(I) = -1d0
! end if

!..calculate budget variables...........................................

! oxygen and co2 budget is dealt with in the call to Oxdyn

   fNIPTn(I) = (fN4P1n(I)+fN4P2n(I)+fN4P3n(I)+fN4P4n(I) &
              +fN3P1n(I)+fN3P2n(I)+fN3P3n(I)+fN3P4n(I))
   fNIPTp(I) = (fN1P1p(I)+fN1P2p(I)+fN1P3p(I)+fN1P4p(I))

! fPTbNIn(I) = fB1NIn(I)
! fPTzNIn(I) = (fZ5NIn(I)+fZ6NIn(I)+fZ4NIn(I))


   fPTNIn(I) = (fB1NIn(I)+fZ5NIn(I)+fZ6NIn(I) &
              +fZ4NIn(I))
   fPTNIp(I) = (fB1N1p(I)+fZ5N1p(I)+fZ6N1p(I) &
              +fZ4N1p(I))

! PTn(I) = PTn(I)+qnZIcX*Z4c(I)
! PTp(I) = PTp(I)+qpZIcX*Z4c(I)

   return

   end subroutine derived_vars
!
!EOC
!-----------------------------------------------------------------------

   end module pelagic_zone

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
