

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Pelagic initialisations
!
! !DESCRIPTION:
! This module manages all initialisations within the pelagic components
! of ERSEM.\\[\baselineskip]
!
! !INTERFACE:
   module pelagic_initialisations
!
! !USES:
   use pelagic_variables

   implicit none

! Default all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public do_pelagic_initialisations
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team

! James Clark (PML) Jan 2014 - Created module pelagic_initialisations, and
! moved data members in from module set_initial_conditions.
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise the pelagic system
!
! !DESCRIPTION:
! Initialise all parameters and variables specific to the pelagic
! components of {\em ERSEM}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine do_pelagic_initialisations
!
! !USES:
   use bacteria, only: set_bacteria_parameters
   use primary_producers, only: set_primary_producer_parameters
   use heterotrophic_nanoflagellates, only: &
      set_heterotrophic_nanoflagellate_parameters
   use microzooplankton, only: set_microzooplankton_parameters
   use mesozooplankton, only: set_mesozooplankton_parameters
   use pelagic_zone, only: set_pelagic_zone_parameters
   use pelagic_parameters, only: set_food_matrix_parameters, &
      set_oxygen_parameters
   use light_extinction, only: set_light_extinction_parameters

   use calcification, only: set_calcification_parameters

   use sedimentation, only: set_sedimentation_parameters
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Initialise pelagic switches
   call set_pelagic_switches()

   ! Initialise module parameters
   call set_bacteria_parameters()
   call set_primary_producer_parameters()
   call set_heterotrophic_nanoflagellate_parameters()
   call set_microzooplankton_parameters()
   call set_mesozooplankton_parameters()
   call set_pelagic_zone_parameters()
   call set_oxygen_parameters()

   call set_calcification_parameters()

   call set_sedimentation_parameters()
   call set_light_extinction_parameters()
   call set_food_matrix_parameters

   ! Initialise pelagic state variables
   call set_initial_pelagic_conditions()

   end subroutine do_pelagic_initialisations
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Set pelagic switches
!
! !DESCRIPTION:
! Set-up all switches specific to the pelagic components of {\em ERSEM}
! from namelist \texttt{ersem\_pelagic\_switches}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_pelagic_switches
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU, iswZeroControl, &
      iswECOLX, iswPHx
   use allocation_helpers, only:allocerr,allocateErsemDouble, &
      allocateErsemInteger
   use gas_dynamics, only: iswO2x
   use primary_producers, only: LimnutX
   use bacteria, only: iswBlimX

   implicit none
!
! !LOCAL VARIABLES:
  integer :: ialloc
  integer :: ITRNO2o,ITRNO3c,ITRNN1p,ITRNN3n,ITRNN4n,ITRNN5s,ITRNR6c, &
             ITRNR6p,ITRNR6n,ITRNR6s,ITRNR1c,ITRNR1p,ITRNR1n,ITRNR2c, &
             ITRNB1c,ITRNB1p,ITRNB1n,ITRNP1c,ITRNP1p, &
             ITRNP1n,ITRNP1s,ITRNP2c,ITRNP2p,ITRNP2n,ITRNP3c,ITRNP3p, &
             ITRNP3n,ITRNP4c,ITRNP4p,ITRNP4n,ITRNZ4c,ITRNZ5c,ITRNZ5p, &
             ITRNZ5n,ITRNZ6c,ITRNZ6p,ITRNZ6n,ITRNr4c,ITRNr4p,ITRNr4n, &
             ITRNr8c,ITRNr8p,ITRNr8n,ITRNr8s,ITRNchl1,ITRNchl2, &
             ITRNchl3,ITRNchl4, &
             ITRNL2c, &
             itrNbioalk,itrNady,itrNR3c

  integer :: itrNN7f,itrNP1f,itrNP2f,itrNP3f,itrNP4f,itrNR6f,itrNR4f
  namelist/ersem_pelagic_switches/ISWZeroControl,iswECOLX, &
         LimnutX,iswO2X,&
         iswBlimX,ISWP1X,ISWP2X,ISWP3X,ISWP4X,ISWZ4X,&
         ISWZ5X,ISWZ6X,ISWB1X, &
         Z4_OW_SW,ITRNO2o,ITRNO3c,ITRNN1p,&
         ITRNN3n,ITRNN4n,ITRNN5s,ITRNR6c,&
         ITRNR6p,ITRNR6n,ITRNR6s,ITRNR1c,&
         ITRNR1p,ITRNR1n,ITRNR2c,&
         ITRNB1c,ITRNB1p,ITRNB1n,ITRNP1c,&
         ITRNP1p,ITRNP1n,ITRNP1s,&
         ITRNP2c,ITRNP2p,ITRNP2n,ITRNP3c,&
         ITRNP3p,ITRNP3n,ITRNP4c,ITRNP4p,&
         ITRNP4n,ITRNZ4c,ITRNZ5c,ITRNZ5p,&
         ITRNZ5n,ITRNZ6c,ITRNZ6p,ITRNZ6n,&
         ITRNr4c,ITRNr4p,ITRNr4n,ITRNr8c,&
         ITRNr8p,ITRNr8n,ITRNr8s,ITRNchl1,&
         ITRNchl2,ITRNchl3,ITRNchl4, &
         iswCO2X,iswTALK,iswASFLUX,iswPHX, &
         itrNN7f,itrNP1f,itrNP2f,itrNP3f,&
         itrNP4f,itrNR6f,itrNR4f, &
         ITRNL2c,iswcal, &
         iswbioalk,itrNbioalk,itrNady,itrNR3c
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading ersem pelagic switches'

   ! Default: no bioalk
   iswbioalk=0
   iswtalk=1

   ! Read in switches
   open(99,file="include/ersem_pelagic_switches.nml",status='old')
   read(99,nml=ersem_pelagic_switches)

   ! Exclude old equation for alkalinity when the bioALK switch is on, force it if the switch is off
   if (iswbioalk.eq.1) then
      if (iswtalk.lt.2) then
         if (IOProcess) write(eLogU,*) "WARNING!!! bioalkalinity is switched on"
         if (IOProcess) write(eLogU,*) "ISWTALK needs to be 2 or higher (now forced equal to 2)"
         iswtalk=2
      end if
   else
      if (iswtalk.ne.1) then
         if (IOProcess) write(eLogU,*) 'WARNING!!! bioalkalinity is switched off, ISWTALK forced to be 1'
         iswtalk=1
     end if
   end if

   ITRXO2o=ITRNO2o
   ITRXO3c=ITRNO3c
   ITRXN1p=ITRNN1p
   ITRXN3n=ITRNN3n
   ITRXN4n=ITRNN4n
   ITRXN5s=ITRNN5s
   ITRXR6c=ITRNR6c
   ITRXR6p=ITRNR6p
   ITRXR6n=ITRNR6n
   ITRXR6s=ITRNR6s
   ITRXR1c=ITRNR1c
   ITRXR1p=ITRNR1p
   ITRXR1n=ITRNR1n
   ITRXR2c=ITRNR2c
   ITRXB1c=ITRNB1c
   ITRXB1p=ITRNB1p
   ITRXB1n=ITRNB1n
   ITRXP1c=ITRNP1c
   ITRXP1p=ITRNP1p
   ITRXP1n=ITRNP1n
   ITRXP1s=ITRNP1s
   ITRXP2c=ITRNP2c
   ITRXP2p=ITRNP2p
   ITRXP2n=ITRNP2n
   ITRXP3c=ITRNP3c
   ITRXP3p=ITRNP3p
   ITRXP3n=ITRNP3n
   ITRXP4c=ITRNP4c
   ITRXP4p=ITRNP4p
   ITRXP4n=ITRNP4n
   ITRXZ4c=ITRNZ4c
   ITRXZ5c=ITRNZ5c
   ITRXZ5p=ITRNZ5p
   ITRXZ5n=ITRNZ5n
   ITRXZ6c=ITRNZ6c
   ITRXZ6p=ITRNZ6p
   ITRXZ6n=ITRNZ6n
   ITRXR4c=ITRNr4c
   ITRXR4p=ITRNr4p
   ITRXR4n=ITRNr4n
   ITRXR8c=ITRNr8c
   ITRXR8p=ITRNr8p
   ITRXR8n=ITRNr8n
   ITRXR8s=ITRNr8s
   ITRXChl1=ITRNchl1
   ITRXChl2=ITRNchl2
   ITRXChl3=ITRNchl3
   ITRXChl4=ITRNchl4
   ITRXbioalk=ITRNbioalk

   ITRXL2c=ITRNL2c


   ITRXN7f=itrNN7f
   ITRXR4f=itrNR4f
   ITRXR6f=itrNR6f
   ITRXP1f=itrNP1f
   ITRXP2f=itrNP2f
   ITRXP3f=itrNP3f
   ITRXP4f=itrNP4f


   ITRXADY=itrNady

   close(99)

   ! Perform allocations according to switches
   lb = min(1,N_COMP)

   ! Partial pressure of CO2, replaceable by scalar, used in gas_dynamics
   if (iswCO2X.eq.1) then
      call allocateErsemDouble(pco2w,'pco2w',lb,N_Comp)
      call allocateErsemDouble(TOTA,'TOTA',lb,N_Comp)
      call allocateErsemDouble(CCo2,'CCo2',lb,N_Comp)
      call allocateErsemDouble(CarbA,'CarbA',lb,N_Comp)
      call allocateErsemDouble(Bicarb,'Bicarb',lb,N_Comp)
      call allocateErsemDouble(Carb,'Carb',lb,N_Comp)
      call allocateErsemDouble(fairmg,'fairmg',lb,n_upperX)
   end if

   if ( ISWZeroControl.NE.0 ) then
      allocate (cccP(lb:N_Comp,i_state),stat=ialloc)
      call allocerr ('cccP',N_Comp-lb+1,i_state,ialloc)
      if ( ialloc.ne.0 ) stop
      cccP=0._fp8
   else
      cccP => ccc
   end if

   O2oP => cccP(:, iO2o )
   O3cP => cccP(:, iO3c )
   N1pP => cccP(:, iN1p )
   N3nP => cccP(:, iN3n )
   N4nP => cccP(:, iN4n )
   N5sP => cccP(:, iN5s )
   R6cP => cccP(:, iR6c )
   R6nP => cccP(:, iR6n )
   R6pP => cccP(:, iR6p )
   R6sP => cccP(:, iR6s )
   R1cP => cccP(:, iR1c )
   R1nP => cccP(:, iR1n )
   R1pP => cccP(:, iR1p )
   P1cP => cccP(:, iP1c )
   P1nP => cccP(:, iP1n )
   P1pP => cccP(:, iP1p )
   P1sP => cccP(:, iP1s )
   P2cP => cccP(:, iP2c )
   P2nP => cccP(:, iP2n )
   P2pP => cccP(:, iP2p )
   P3cP => cccP(:, iP3c )
   P3nP => cccP(:, iP3n )
   P3pP => cccP(:, iP3p )
   P4cP => cccP(:, iP4c )
   P4nP => cccP(:, iP4n )
   P4pP => cccP(:, iP4p )
   Z4cP => cccP(:, iZ4c )
   Z5cP => cccP(:, iZ5c )
   Z5nP => cccP(:, iZ5n )
   Z5pP => cccP(:, iZ5p )
   Z6cP => cccP(:, iZ6c )
   Z6nP => cccP(:, iZ6n )
   Z6pP => cccP(:, iZ6p )
   B1cP => cccP(:, iB1c )
   B1nP => cccP(:, iB1n )
   B1pP => cccP(:, iB1p )
   R4cP => cccP(:, iR4c )
   R4nP => cccP(:, iR4n )
   R4pP => cccP(:, iR4p )
   R8cP => cccP(:, iR8c )
   R8nP => cccP(:, iR8n )
   R8pP => cccP(:, iR8p )
   R8sP => cccP(:, iR8s )
   R2cP => cccP(:, iR2c )
   Chl1P => cccP(:, iChl1 )
   Chl2P => cccP(:, iChl2 )
   Chl3P => cccP(:, iChl3 )
   Chl4P => cccP(:, iChl4 )
   bioalkP => cccP(:,ibioalk)

   L2cP => cccP(:, iL2c )


   N7fP => cccP(:, iN7f )
   P1fP => cccP(:, iP1f )
   P2fP => cccP(:, iP2f )
   P3fP => cccP(:, iP3f )
   P4fP => cccP(:, iP4f )
   R6fP => cccP(:, iR6f )
   R4fP => cccP(:, iR4f )





   ADYP => cccp(:, iADY )


   R3cP => cccP(:, iR3c )


   end subroutine set_pelagic_switches
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Assign initial conditions
!
! !DESCRIPTION:
! Initialise pelagic state variables.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_initial_pelagic_conditions
!
! !USES:
   use ersem_constants, only: CMass
   use ersem_variables, only: IOProcess, eLogU, eDbgU, iswZeroControl
   use pelagic_parameters
   use primary_producers
   use bacteria
!
   implicit none
!
! !LOCAL VARIABLES:
   real(fp8) :: O2oR,O3cR,N1pR,N3nR,N4nR,N5sR
   real(fp8) :: R6cR,R6nR,R6pR,R6sR,R1cR,R1nR,R1pR
   real(fp8) :: P1cR,P1nR,P1pR,P1sR,P2cR,P2nR,P2pR
   real(fp8) :: P3cR,P3nR,P3pR,P4cR,P4nR,P4pR
   real(fp8) :: Z4cR,Z5cR,Z5nR,Z5pR,Z6cR,Z6nR,Z6pR
   real(fp8) :: r4cR,r4nR,r4pR,r8cR,r8nR,r8pR,r8sR
   real(fp8) :: chl1R, chl2R, chl3R, chl4R
   real(fp8) :: B1cR,B1nR,B1pR,X1xR
   real(fp8) :: bioalkR
   real(fp8),parameter :: qsRPIcX=15._fp8/106._fp8/CMass
   real(fp8),parameter :: lowMass=1.E-2_fp8
   real(fp8) :: L2cR,R2cR,R3cR

   real(fp8) :: N7fR,P1fR,P2fR,P3fR,P4fR,R4fR,R6fR,adyR
   namelist/initial_pelagic_sv_conditions/O2oR,O3cR,N1pR,&
      N3nR,N4nR,N5sR,R6cR,&
      R6nR,R6pR,R6sR,R1cR,R1nR,R1pR,R2cR,P1cR,P1nR,P1pR,&
      P1sR,P2cR,P2nR,P2pR,P3cR,P3nR,P3pR,P4cR,P4nR,P4pR,Z4cR,Z5cR,&
      Z5nR,Z5pR,Z6cR,Z6nR,Z6pR,r4cR,r4nR,r4pR,r8cR,r8nR,r8pR,r8sR,&
      chl1R,chl2R,chl3R,chl4R,B1cR,B1nR,B1pR,X1xR, &
      ESSXR, &
      N7fR,P1fR,P2fR,P3fR,P4fR,R4fR,R6fR,adyR, &
      L2cR, &
      bioalkR,R3cR

   integer :: i
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading ersem pelagic initial conditions'
   open(99,file="include/initial_pelagic_sv_conditions.nml",status='old')
   read(99,nml=initial_pelagic_sv_conditions)
   close(99)

   do i = 1,n_comp

      O2o(i)= O2oR
      O3c(i)= O3cR
      N1p(i)= N1pR
      N3n(i)= N3nR
      N4n(i)= N4nR
      N5s(i)= N5sR

      R6c(i)= R6cR
      R6n(i)= R6nR
      R6p(i)= R6pR
      R6s(i)= R6sR

      R1c(i)= R1cR
      R1n(i)= R1nR
      R1p(i)= R1pR

      R2c(i)= R2cR

      P1c(i)= P1cR
      P1n(i)= P1nR
      P1p(i)= P1pR
      P1s(i)= P1sR

      P2c(i)= P2cR
      P2n(i)= P2nR
      P2p(i)= P2pR

      P3c(i)= P3cR
      P3n(i)= P3nR
      P3p(i)= P3pR

      P4c(i)= P4cR
      P4n(i)= P4nR
      P4p(i)= P4pR


      L2c(i)= L2cR

      Chl1(I) = Chl1R
      Chl2(I) = Chl2R
      Chl3(I) = Chl3R
      Chl4(I) = Chl4R

      Z4c(i)= Z4cR
      Z4n(i) = Z4c(I) * qnZIcX

      Z5c(i)= Z5cR
      Z5n(i)= Z5nR
      Z5p(i)= Z5pR

      Z6c(i)= Z6cR
      Z6n(i)= Z6nR
      Z6p(i)= Z6pR

      r4c(i)= r4cR
      r4n(i)= r4nR
      r4p(i)= r4pR

      r8c(i)= r8cR
      r8n(i)= r8nR
      r8p(i)= r8pR
      r8s(i)= r8sR

      B1c(i)= B1cR
      B1n(i)= B1nR
      B1p(i)= B1pR

      bioalk(i) = bioalkR

      N7f(I) = N7fR
      P1f(I) = P1fR
      P2f(I) = P2fR
      P3f(I) = P3fR
      P4f(I) = P4fR
      R4f(I) = R4fR
      R6f(I) = R6fR





      ADY(I) = adyR

      X1x(i)= X1xR

   end do

   if (ISWZeroControl.NE.0) then
       P1c0=.01*lowMass
       P1n0=P1c0*qnRPIcX
       P1p0=P1c0*qpRPIcX
       P1s0=qsP1cX*P1c0
       P2c0=.1*lowMass
       P2n0=P2c0*qnRPIcX
       P2p0=P2c0*qpRPIcX

       P3c0=.88*lowMass
       P3n0=P3c0*qnRPIcX
       P3p0=P3c0*qpRPIcX

       P4c0=.01*lowMass
       P4n0=P4c0*qnRPIcX
       P4p0=P4c0*qpRPIcX


       L2c0=.1_fp8*lowMass

       Chl10=P1c0*phimP1X
       Chl20=P2c0*phimP2X
       Chl30=P3c0*phimP3X
       Chl40=P4c0*phimP4X

       O2o0=.001_fp8*chdB1oX ! unused
       O3c0=O2o0
       N1p0=.1_fp8*lowMass*qpRPIcX
       N3n0=.1_fp8*lowMass*qnRPIcX
       N4n0=.1_fp8*lowMass*qnRPIcX
       N5s0=.001_fp8*chP1sX

       R6c0=.33_fp8*lowMass
       R6n0=qnRPIcX*R6c0
       R6p0=qpRPIcX*R6c0
       R6s0=qsRPIcX*R6c0


       R1c0=.34_fp8*lowMass
       R2c0=.33_fp8*lowMass
       R3c0=.33_fp8*lowMass





       R1n0=qnRPIcX*R1c0
       R1p0=qpRPIcX*R1c0

       Z4c0=.33_fp8*lowMass

       Z5c0=.33_fp8*lowMass
       Z5n0=qnRPIcX*Z5c0
       Z5p0=qpRPIcX*Z5c0

       Z6c0=.33_fp8*lowMass
       Z6n0=qnRPIcX*Z6c0
       Z6p0=qpRPIcX*Z6c0

       R4c0=.34_fp8*lowMass
       R4n0=R4c0*qnRPIcX
       R4p0=R4c0*qpRPIcX

       R8c0=.33_fp8*lowMass
       R8n0=R8c0*qnRPIcX
       R8p0=R8c0*qpRPIcX
       R8s0=qsRPIcX*R6c0

       B1c0=lowMass
       B1n0=qnB1cX*B1c0
       B1p0=qpB1cX*B1c0

       bioalk0=1.e-15_fp8 ! unused


       N7f0=.1_fp8*lowMass*qfRP1cX
       P1f0=P1c0*qfRP1cX
       P2f0=P2c0*qfRP2cX
       P3f0=P3c0*qfRP3cX
       P4f0=P4c0*qfRP4cX
       R4f0=R4c0*qfRP1cX
       R6f0=R6c0*qfRP1cX





       ADY0=1.e-15_fp8

   end if

   end subroutine set_initial_pelagic_conditions
!
!EOC
!-----------------------------------------------------------------------

   end module pelagic_initialisations

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
