

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Light extinction
!
! !DESCRIPTION:
! Holds routines and parameters for the calculation of light
! extinction.\\[\baselineskip]
!
! !INTERFACE:
   module light_extinction
!
! !USES:
   use ersem_constants, only: fp8
!
   implicit none
!
! Default all is public.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_light_extinction_parameters, calculate_extinction

   public calczenith

!
! !LOCAL VARIABLES:

! Specific adsorption coefficient of diatoms [m^2/mg Chl]
   real(fp8), public :: aphP1=.007_fp8

! Specific adsorption coefficient of nanophytoplankton [m^2/mg Chl]
   real(fp8), public :: aphP2=.031_fp8

! Specific adsorption coefficient of picophytoplankton [m^2/mg Chl]
   real(fp8), public :: aphP3=.03_fp8

! Specific adsorption coefficient of microphytoplankton [m^2/mg Chl]
   real(fp8), public :: aphP4=.01_fp8

! Specific backscattering coefficient of diatoms [m^2/mg Chl]
   real(fp8), public :: bphP1=.00049_fp8

! Specific backscattering coefficient of nanophytoplankton [m^2/mg Chl]
   real(fp8), public :: bphP2=.0032_fp8

! Specific backscattering coefficient of picophytoplankton [m^2/mg Chl]
   real(fp8), public :: bphP3=.0032_fp8

! Specific backscattering coefficient of microphytoplankton [m^2/mg Chl]
   real(fp8), public :: bphP4=.00049_fp8

! Specific adsorption coefficient of small POC [m^2/mg C]
   real(fp8), public ::adR4=1.9e-4_fp8

! Specific adsorption coefficient of medium POC [m^2/mg C]
   real(fp8), public :: adR6=1.9e-4_fp8

! Specific adsorption coefficient of large POC [m^2/mg C]
   real(fp8), public :: adR8=1.9e-4_fp8

! Specific backscattering coefficient of small POC [m^2/mg C]
   real(fp8), public :: bbR4=1.63e-5_fp8

! Specific backscattering coefficient of medium POC [m^2/mg C]
   real(fp8), public :: bbR6=1.63e-5_fp8

! Specific backscattering coefficient of large POC [m^2/mg C]
   real(fp8), public :: bbR8=1.63e-5_fp8

! Adsorption coefficient of clear water[1/m]
   real(fp8), public :: a0w=.15_fp8

! Backscattering coefficient of clear water[1/m]
   real(fp8), public :: b0w=.0013_fp8
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
   contains
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise parameters.
!
! !DESCRIPTION:
! Initialises light extinction parameters from namelist
! \texttt{iopParameters} if the iop light model is used
! (pre-processing option \texttt{1}) or from
! \texttt{d3morfParametsers} for the default light model.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine set_light_extinction_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU
   use pelagic_variables, only: n_comp, eps0x
!
! !LOCAL VARIABLES:
   real(fp8) :: EPS0Xr
   integer :: i
   namelist/iopParameters/aphP1,aphP2,aphP3,aphP4, &
      bphP1,bphP2,bphP3,bphP4, &
      adR4,adR6,adR8, &
      bbR4,bbR6,bbR8, &
      a0w,b0w
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading iop parameters'
   open(99,file='BioParams/iop.nml',status='old')
   read(99,nml=iopParameters)
   close(99)
   return
   end subroutine set_light_extinction_parameters
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Calculate extinction
!
! !DESCRIPTION:
! Calculation of vertical light extinction.\\[\baselineskip]
!
! !INTERFACE:
   subroutine calculate_extinction
!
! !USES:
   use ersem_constants, only: fp8
   use pelagic_variables, only: n_comp, xeps
   use pelagic_variables, only: Chl1,Chl2,Chl3,Chl4,R4c,R6c,R8c,ESS, &
      zenithA,iopADS,iopBBS
   use pelagic_variables, only : ADY
!
! !LOCAL VARIABLES:
   real(fp8), parameter :: bpk=.00022_fp8
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
! adsorption coefficients (chlorophyll specific for P):
! note that if 1 is not used, ESS should hold the local
! background adsorption of non-modelled substances
   iopADS=aphP1*Chl1+aphP2*Chl2+aphP3*Chl3+aphP4*Chl4+ady+a0w
   iopBBS=bphP1*Chl1+bphP2*Chl2+bphP3*Chl3+bphP4*Chl4+ &
      bpk+b0w
   ! Attenuation (Lee et al.2005):
   xEPS = (1.+.005*zenithA)*iopADS &
          + 4.18*(1.-.52*exp(-10.8*iopADS))*iopBBS
   return
   end subroutine calculate_extinction
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Calculate zenith angle
!
! !DESCRIPTION:
! Calculates zenith angle from current time.
! Used in iop light model.\\[\baselineskip]
!
! !INTERFACE:
   subroutine calczenith(yd,secs)
!
! !USES:
   use pelagic_variables, only:zenithA,elon,elat
!
! !INPUT PARAMETERS
   integer,intent(in) :: yd ! day in the year
   real,intent(in) :: secs ! second of the day passed
!
! !LOCAL VARIABLES:
   real(fp8) :: h,th0,th02,th03,coszen,sundec,thsun
   real(fp8), parameter :: pi=acos(-1._fp8)
   real(fp8), parameter :: deg2rad=pi/180._fp8
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
   h=secs/3600._fp8
   ! Leap year not considered:
   th0 = pi*yd/182.5_fp8
   th02 = 2._fp8*th0
   th03 = 3._fp8*th0
   ! Sun declination :
   sundec = 0.006918_fp8 - 0.399912_fp8*cos(th0) + 0.070257_fp8*sin(th0) &
        - 0.006758_fp8*cos(th02) + 0.000907_fp8*sin(th02) &
        - 0.002697_fp8*cos(th03) + 0.001480_fp8*sin(th03)
   ! Sun hour angle :
   zenithA = (h-12._fp8)*15._fp8*deg2rad + elon
   ! Cosine of the solar zenith angle :
   zenithA =sin(elat)*sin(sundec)+cos(elat)*cos(sundec)*cos(zenithA)
   zenithA =acos(max(0._fp8,zenithA))/deg2rad
   end subroutine calczenith
!EOC
!-----------------------------------------------------------------------
   end module light_extinction
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
