

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Microzooplankton
!
! !DESCRIPTION:
! This module contains the parametrisation for microzooplankton and
! the routines to initialse the respective parameters from namelist
! and launch the microzooplankton dynamics.\\[\baselineskip]
!
! !INTERFACE:
   module microzooplankton
!
! !USES:
   use ersem_constants, only: fp8
   use pelagic_variables
   use pelagic_parameters
   use gas_dynamics, only: ub1c_o2x,urB1_O2X

   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_microzooplankton_parameters, microzooplankton_dynamics
!
! !PUBLIC DATA MEMBERS:
! Michaelis-Menten constant for food uptake by microzooplankton [mg C/m^3]
   real(fp8), public :: chuz5cX

! Maximal specific uptake of microzooplankton at reference temperature [1/d]
   real(fp8), public :: sumz5X

! Relative microplankton assimilation efficiency
   real(fp8), public :: puz5X

! DOM-Fraction of microzooplankton excretion
   real(fp8), public :: pe_r1z5X

! Regulating temperature factor Q10 for microzooplankton
   real(fp8), public :: q10z5X

! Specific rest respiration of microzooplankton at
! reference temperature [1/d]
   real(fp8), public :: srsz5X

! Michaelis-Menten constant for oxygen limitation of mesozooplankton
   real(fp8), public :: chrz5oX

! Excreted fraction of POM uptake by microzooplankton
   real(fp8), public :: pu_eaz5X

! Specific mortality of microzooplantkon due to oxygen
! limitation [1/d]
   real(fp8), public :: sdz5oX

! Specific basal mortality of microzooplankton [1/d]
   real(fp8), public :: sdz5X

! Maximal nitrogen to carbon ratio of microzooplankton
! [mmol N/mg C]
   real(fp8), public :: qnz5cX

! Maximal phospherus to carbon ratio of microzooplankton
! [mmol P/mg C]
   real(fp8), public :: qpz5cX

! Michaelis-Menten constant to perceive food for microzooplankton
! [mg C/m^2]
   real(fp8), public :: minfoodz5X

! Specific ammonium excretion rate of microzooplankton [1/d]
   real(fp8), public :: stempz5nX

! Specific phosphate excretion rate of microzooplankton [1/d]
   real(fp8), public :: stempz5pX
!
! !LOCAL VARIABLES:
   real(fp8) :: fZ5R6n,fZ5R6p,fZ5Rdc,fZ5Rdn
   real(fp8) :: fZ5Rdp,fZ5RIp,fZ5RIn

   real(fp8) ,public ::retZ5

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise parameters \label{sec:microzoodynamics}
!
! !DESCRIPTION:
! Initialises microzooplankton parameters from namelist \texttt{microzooplankton\_parameters}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_microzooplankton_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU

   implicit none

! !LOCAL VARIABLES:
   namelist/microzooplankton_parameters/chuZ5cX,sumZ5X,puZ5X,pe_R1Z5X,&
     q10Z5X,srsZ5X,chrZ5oX,pu_eaZ5X,sdZ5oX,sdZ5X,qnZ5cX,qpZ5cX,&
     minfoodZ5X,stempZ5nX,stempZ5pX
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading microzooplankton parameters'
   open(99,file='BioParams/microzooplankton.nml',status='old')
   read(99,nml=microzooplankton_parameters)

   close(99)

   return

   end subroutine set_microzooplankton_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Microzooplankton dynamics \label{sec:microzoodynamics}
!
! !DESCRIPTION:
! The predation of microzooplankton on other states of the modelled
! ecosystem is based on the prey available in the current environment
! according to:
! \begin{eqnarray*}
! \overset{\textit{c,p,n}}{\textit{Pr}}_{\textit{MICRO}} & = & \left.f_{\textit{pr}}\right|_{B}^{Z_{\textit{MICRO}}}\frac{\overset{c}{B'}}{\overset{c}{B'}+f_{\textit{mMICRO}}}\overset{\textit{c,p,n}}{B'}+\left.f_{\textit{pr}}\right|_{P_{\textit{dia}}}^{Z_{\textit{MICRO}}}\frac{\overset{c}{P'}_{\textit{dia}}}{\overset{c}{P'}_{\textit{dia}}+f_{\textit{mMICRO}}}\overset{\textit{c,p,n}}{P'}_{\textit{dia}}\! & & \left.f_{\textit{pr}}\right|_{P_{\textit{MICRO}}}^{Z_{\textit{MICRO}}}\frac{\overset{c}{P'}_{\textit{MICRO}}}{\overset{c}{P'}_{\textit{MICRO}}+f_{\textit{mMICRO}}}\overset{\textit{c,p,n}}{P'}_{\textit{MICRO}}+\left.f_{\textit{pr}}\right|_{P_{\textit{nano}}}^{Z_{\textit{MICRO}}}\frac{\overset{c}{P'}_{\textit{nano}}}{\overset{c}{P'}_{\textit{nano}}+f_{\textit{mMICRO}}}\overset{\textit{c,p,n}}{P'}_{\textit{nano}}\! & & +\left.f_{\textit{pr}}\right|_{P_{\textit{pico}}}^{Z_{\textit{MICRO}}}\frac{\overset{c}{P'}_{\textit{pico}}}{\overset{c}{P'}_{\textit{pico}}+f_{\textit{mMICRO}}}\overset{\textit{c,p,n}}{P'}_{\textit{pico}}+\left.f_{\textit{pr}}\right|_{Z_{\textit{het}}}^{Z_{\textit{MICRO}}}\frac{\overset{c}{Z'}_{\textit{het}}}{\overset{c}{Z'}_{\textit{het}}+f_{\textit{mMICRO}}}\overset{\textit{c,p,n}}{Z'}_{\textit{het}}\! & & +\left.f_{\textit{pr}}\right|_{Z_{\textit{MICRO}}}^{Z_{\textit{MICRO}}}\frac{\overset{c}{Z'}_{\textit{MICRO}}}{\overset{c}{Z'}_{\textit{MICRO}}+f_{\textit{mMICRO}}}\overset{\textit{c,p,n}}{Z'}_{\textit{MICRO}}\,,



! \end{eqnarray*}
! where $\left.f_{\textit{pr}}\right|_{X}^{Z_{\textit{MICRO}}}$ are
! the food preferences of microzooplankton on state $X$ and $f_{\textit{mMICRO}}$
! is the minimum food constant limiting the uptake efficiency at low
! concentrations of a specific prey.

! Specific food uptake by microzooplankton
! is a temperature regulated function of its own biomass
! and prey availability, additionally limited towards low concentrations
! of total prey:
! \begin{equation}
! \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}=\overset{\textit{MICRO}}{g}_{\textit{max}}\overset{\textit{MICRO}}{l}_{T}\frac{\overset{c}{Z}_{\textit{MICRO}}}{\overset{\textit{c}}{\textit{Pr}}_{\textit{MICRO}}+\overset{\textit{MICRO}}{h}_{\textit{up}}}\,,
! \end{equation}
! leading to a total carbon uptake of:
! \begin{equation}
! \left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{growth}}=\mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{c}}{\textit{Pr}}_{\textit{MICRO}}\,,
! \end{equation}
! where $\overset{\textit{MICRO}}{h}_{\textit{up}}$ is the Michaelis-Menten
! constant for food uptake by microzooplankton. The temperature response
! factor $\overset{\textit{MICRO}}{l}_{T}$ is given by the equation:
! \begin{equation}
! \overset{\textit{MICRO}}{l}_{T}=\left.\overset{\textit{MICRO}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-10}{10}}-\left.\overset{\textit{MICRO}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-32}{3}}.
! \end{equation}
!
! The gross uptake is subject to losses to organic matter and activity
! respiration. The losses to organic matter like excretion or sloppy
! feeding are computed as:
! \begin{equation}
! \left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{excr}}=\left(1-\overset{\textit{MICRO}}{p}_{\textit{eff}}\right)\overset{\textit{MICRO}}{p}_{\textit{excr}}\left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{growth}}
! \end{equation}
! with the parameter$\overset{\textit{MICRO}}{p}_{\textit{eff}}$ estimating
! the fraction of uptake being effectively synthesised and $\overset{\textit{MICRO}}{p}_{\textit{excr}}$
! giving the fraction of uptake loss directed to organic matter.
!
! Microzooplankton respiration is computed according to:
! \begin{equation}
! \left.\frac{\partial\overset{c}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{resp}}=\left(1-\overset{\textit{MICRO}}{p}_{\textit{eff}}\right)\left(1-\overset{\textit{MICRO}}{p}_{\textit{excr}}\right)\left.\frac{\partial\overset{c}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{growth}}+\overset{\textit{MICRO}}{r}_{\textit{resp}}\overset{\textit{MICRO}}{l}_{T}\overset{c}{Z'}_{\textit{MICRO}}\,,
! \end{equation}
! where $\overset{\textit{MICRO}}{r}_{\textit{resp}}$ is the specific
! basal respiration rate at rest.

! Mortality is given as:
! \begin{equation}
! \left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{mort}}=\left(\left(1-\overset{\textit{MICRO}}{l}_{O_{2}}\right)\overset{\textit{MICRO}}{p}_{\textit{mort}O2}+\overset{\textit{MICRO}}{p}_{\textit{mort}}\right)\overset{\textit{c,p,n}}{Z'}_{\textit{MICRO}}
! \end{equation}
! with $\overset{\textit{MICRO}}{p}_{\textit{mort}}$ being the specific
! mortality rate of bacteria, $\overset{\textit{MICRO}}{p}_{\textit{mort}O2}$
! is the maximum specific rate of mortality due to oxygen limitation
! and the oxygen limitation state $\overset{\textit{MICRO}}{l}_{O_{2}}$
! is given by:
! \begin{equation}
! \overset{\textit{MICRO}}{l}_{O_{2}}=\frac{s_{\textit{relO}_{2}+}s_{\textit{relO}_{2}}\overset{\textit{MICRO}}{h}_{O_{2}}}{s_{\textit{relO}_{2}}+\overset{\textit{MICRO}}{h}_{O_{2}}}\,.
! \end{equation}
!
! The fluxes of prey to microzooplankton are then given by the equations:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{B}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{B'}\!  \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{P}_{\textit{dia}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{P'}_{\textit{dia}}\! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{P}_{\textit{MICRO}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{P'}_{\textit{MICRO}}\!  \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{P}_{\textit{nano}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{P'}_{\textit{nano}}\! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{P}_{\textit{pico}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{P'}_{\textit{pico}}\!  \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{Z}_{\textit{het}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{Z'}_{\textit{het}}\! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MICRO}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{Z'}_{\textit{MICRO}}\,.






! \end{eqnarray*}
!
! The loss to organic matter is split between dissolved and particulate
! matter as:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{MICRO}}}^{\overset{c}{M}_{\textit{\textit{med}}}} & = & \left(1-\overset{\textit{MICRO}}{p}_{\textit{dis}}\right)\left(\left.\frac{\partial\overset{c}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{c}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{mort}}\right)\! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{MICRO}}}^{\overset{c}{M}_{\textit{\textit{dis}}}} & =\overset{\textit{MICRO}}{p}_{\textit{dis}} & \overset{\textit{MICRO}}{p}_{\textit{lab}}\left(\left.\frac{\partial\overset{c}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{c}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{mort}}\right)\! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{MICRO}}}^{\overset{c}{M}_{\textit{\textit{slab}}}} & = & \overset{\textit{MICRO}}{p}_{\textit{dis}}\left(1-\overset{\textit{MICRO}}{p}_{\textit{lab}}\right)\left(\left.\frac{\partial\overset{c}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{c}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{mort}}\right)\,,


! \end{eqnarray*}
! where $\overset{\textit{MICRO}}{p}_{\textit{dis}}$ is the fraction
! of uptake loss directed to dissolved organic matter and $\overset{\textit{MICRO}}{p}_{\textit{lab}}$
! is the fraction of this in labile state.

! The phosphate and nitrogen fluxes to organic matter are given by:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{p,n}{Z}_{\textit{MICRO}}}^{\overset{p,n}{M}_{\textit{\textit{dis}}}} & = & \overset{\textit{MICRO}}{p}_{\textit{dis}}p_{\textit{DPcyto}}\left(\left.\frac{\partial\overset{p,n}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{p,n}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{mort}}\right)\! \left.\mathcal{F}\right|_{\overset{p,n}{Z}_{\textit{MICRO}}}^{\overset{p,n}{M}_{\textit{\textit{med}}}} & = & \left(1-\overset{\textit{MICRO}}{p}_{\textit{dis}}p_{\textit{DPcyto}}\right)\left(\left.\frac{\partial\overset{p,n}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{p,n}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{mort}}\right)\,,

! \end{eqnarray*}
! with $p_{\textit{DNcyto}}$ being the phosphate/nitrogen fraction
! in cytoplasm of DOM vs. structural components.
!
! The internal stochiometric relation of phosporus and nitrogen is then
! balanced according to:
! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{MICRO}}}^{N_{\textit{phos,amm}}}=\textit{min}\left(0,\overset{\textit{p,n}}{Z'}_{\textit{MICRO}}-\overset{\textit{MICRO}}{q}_{\textit{p,n:c}}\overset{c}{Z'}_{\textit{MICRO}}\right)\overset{\textit{p,n}}{r}_{\textit{MICROex}}\,,
! \end{equation}
! where $\overset{p,n}{r}_{MICROex}$ is the reference specific phosphate
! excretion rate of microzooplankton.
!
! The respiration fluxes towards inorganic gasses are given as:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{MICRO}}}^{G_{\textit{\textit{CO}}_{2}}} & = & \frac{1}{\mathcal{\mathbb{C}}}\left.\frac{\partial\overset{c}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{resp}}\! \left.\mathcal{F}\right|_{G_{O_{2}}}^{\textit{sink}} & = & \overset{B}{p}_{O_{2}}\left.\frac{\partial\overset{c}{Z}_{\textit{MICRO}}}{\partial t}\right|_{\textit{resp}}\,.

! \end{eqnarray*}
!
! The silicate contained in the prey fields is directly cycled to organic
! matter:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{s}{P}_{\textit{dia}}}^{\overset{\textit{s}}{M}_{\textit{med}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{s}}{P'}_{\textit{dia}}\,.
! \end{eqnarray*}
!
! Correspondingly, if iron is used (pre-preocessing option \texttt{1}),
! the iron contained in the prey fields is directly cycled to organic
! matter:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{dia}}}^{\overset{f}{M}_{\textit{med}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{f}}{P'}_{\textit{dia}}\!  \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{MICRO}}}^{\overset{f}{M}_{\textit{med}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{f}}{P'}_{\textit{MICRO}}\! \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{nano}}}^{\overset{f}{M}_{\textit{small}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{f}}{P'}_{\textit{nano}}\!  \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{pico}}}^{\overset{f}{M}_{\textit{small}}} & = & \mathcal{\overset{\textit{MICRO}}{S}}_{\textit{growth}}\overset{\textit{f}}{P'}_{\textit{pico}}



! \end{eqnarray*}
!
! If calcification is considered (activated through the preprocessing
! option \texttt{1}), absorption of DIC into calcite is estimated
! as:
! \begin{equation}
! \left.\mathcal{F}\right|_{G_{\textit{CO}_{2}}}^{\overset{c}{M}_{\textit{calc}}}=q_{\textit{rain}}p_{\textit{gutd}}\left(1-\overset{\textit{MICRO}}{p}_{\textit{eff}}\right)\overset{\textit{MICRO}}{p}_{\textit{excr}}\left.\mathcal{F}\right|_{\overset{\textit{c}}{P}_{\textit{nano}}}^{\overset{\textit{c}}{Z}_{\textit{MICRO}}}\,.
! \end{equation}
!
! !INTERFACE:
   subroutine microzooplankton_dynamics(I)
!
! !USES:
   use ersem_constants, only: CMass

   use calcification, only: gutdiss


   implicit none
!
! !INPUT PARAMETERS:
   integer, intent(in) :: i
!
! !LOCAL VARIABLES:
   real(fp8) :: etZ5,CORROX,eO2Z5, rumZ5, put_uZ5
   real(fp8) :: rrsZ5, rraZ5, rugZ5, rdZ5,sdZ5



   real(fp8) :: ruP1Z5c, ruP2Z5c, ruP3Z5c
   real(fp8) :: ruP4Z5c
   real(fp8) :: ruZ6Z5c, ruZ5Z5c
   real(fp8) :: ruB1Z5c
   real(fp8) :: sP1Z5,sP2Z5,sP3Z5,sP4Z5,sB1Z5,sZ6Z5,ineffZ5
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Temperature effect :
   etZ5 = q10Z5X**((ETW(I)-10._fp8)/10._fp8) - q10Z5X**((ETW(I)-32._fp8)/3._fp8)

   ! Oxygen limitation :
   CORROX = 1._fp8 + chrZ5oX
   eO2Z5 = MIN(1._fp8,CORROX*(eO2mO2/(chrZ5oX + eO2mO2)))

   ! Available food :
   sB1Z5 = suB1_Z5X*B1cP(I)/(B1cP(I)+minfoodZ5X)
   sP1Z5 = suP1_Z5X*P1cP(I)/(P1cP(I)+minfoodZ5X)
   sP2Z5 = suP2_Z5X*P2cP(I)/(P2cP(I)+minfoodZ5X)
   sP3Z5 = suP3_Z5X*P3cP(I)/(P3cP(I)+minfoodZ5X)
   sP4Z5 = suP4_Z5X*P4cP(I)/(P4cP(I)+minfoodZ5X)
   sZ6Z5 = suZ6_Z5X*Z6cP(I)/(Z6cP(I)+minfoodZ5X)
   ruZ5Z5c = suZ5_Z5X*Z5cP(I)/(Z5cP(I)+minfoodZ5X)*Z5cP(I)
   ruB1Z5c = sB1Z5*B1cP(I)
   ruP1Z5c = sP1Z5*P1cP(I)
   ruP2Z5c = sP2Z5*P2cP(I)
   ruP3Z5c = sP3Z5*P3cP(I)
   ruP4Z5c = sP4Z5*P4cP(I)
   ruZ6Z5c = sZ6Z5*Z6cP(I)

   rumZ5 = ruP1Z5c + ruP2Z5c + ruP3Z5c + ruP4Z5c &
        + ruB1Z5c + ruZ5Z5c + ruZ6Z5c

   ! Uptake :
   put_uZ5 = sumZ5X/(rumZ5 + chuZ5cX)*etZ5*Z5c(I)
   rugZ5 = put_uZ5*rumZ5

   ! Fluxes into microplankton :
   fB1Z5c(I) = put_uZ5*ruB1Z5c
   fP1Z5c(I) = put_uZ5*ruP1Z5c
   fP2Z5c(I) = put_uZ5*ruP2Z5c
   fP3Z5c(I) = put_uZ5*ruP3Z5c
   fP4Z5c(I) = put_uZ5*ruP4Z5c
   fZ5Z5c(I) = put_uZ5*ruZ5Z5c
   fZ6Z5c(I) = put_uZ5*ruZ6Z5c
   sB1Z5 = put_uZ5*sB1Z5
   sP1Z5 = put_uZ5*sP1Z5
   sP2Z5 = put_uZ5*sP2Z5
   sP3Z5 = put_uZ5*sP3Z5
   sP4Z5 = put_uZ5*sP4Z5
   sZ6Z5 = put_uZ5*sZ6Z5

   ! Zooplakton Grazing
! Z5herb(I) = fP1Z5c(I) + fP2Z5c(I) + fP3Z5c(I) + fP4Z5c(I)
! Z5carn(I) = fB1Z5c(I) + fZ6Z5c(I)

   ! Mortality
   sdZ5 = ((1._fp8 - eO2Z5)*sdZ5oX + sdZ5X)
   rdZ5 = sdZ5*Z5cP(I)

   ! Assimilation inefficiency:
   ineffZ5 = (1._fp8 - puZ5X)

   ! Excretion
   retZ5 = ineffZ5 * rugZ5 * pu_eaZ5X
   fZ5RDc = (retZ5 + rdZ5)*pe_R1Z5X
   fZ5R6c(I) = (retZ5 + rdZ5)*(1._fp8 - pe_R1Z5X)

   fZXRDc(I) = fZXRDc(I)+fZ5RDc


   ! Rest respiration, corrected for prevailing temperature
   rrsZ5 = srsZ5X*etZ5*Z5cP(I)

   ! Activity respiration
   rraZ5 = ineffZ5 * rugZ5 -retZ5

   ! Total respiration
   fZ5O3c(I) = rrsZ5 + rraZ5


   fO3L2c(I) = fO3L2c(I) + &
      RainR(I) * gutdiss * (1._fp8 - puZ5X)* pu_eaZ5X * fP2Z5c(I)

   ! Source equation
   SZ5c(I) = SZ5c(I) + rugZ5 &
             - fZ5R6c(I) - fZ5RDc - fZ5Z5c(I) - fZ5O3c(I)

   ! Flows from and to detritus
   SR1c(I) = SR1c(I) + (fZ5RDc * R1R2X)
   SR2c(I) = SR2c(I) + (fZ5RDc * (1._fp8-R1R2X))
   SR6c(I) = SR6c(I) + fZ5R6c(I)

   ! Grazing and predation
   SP1c(I) = SP1c(I) - fP1Z5c(I)
   SP2c(I) = SP2c(I) - fP2Z5c(I)
   SP3c(I) = SP3c(I) - fP3Z5c(I)
   SP4c(I) = SP4c(I) - fP4Z5c(I)
   SZ6c(I) = SZ6c(I) - fZ6Z5c(I)
   SB1c(I) = SB1c(I) - fB1Z5c(I)
   SChl1(I) = SChl1(I) - sP1Z5*chl1P(I)
   SChl2(I) = SChl2(I) - sP2Z5*chl2P(I)
   SChl3(I) = SChl3(I) - sP3Z5*chl3P(I)
   SChl4(I) = SChl4(I) - sP4Z5*chl4P(I)

   ! Respiration
   SO3c(I) = SO3c(I) + fZ5O3c(I)/CMass
   SO2o(I) = SO2o(I) - fZ5O3c(I)*urB1_O2X

   ! Nutrient dynamics in microzooplankton, derived from carbon flows

   ! Phosphorus dynamics
   fZ5RIp = retZ5*qpZ5c(I)+sdZ5*Z5pP(I)
   fZ5RDp = fZ5RIp*min(1._fp8,pe_R1Z5X*xR1pX)
   fZ5R6p = (fZ5RIp - fZ5RDp)
   fZ5N1p(I) = MAX( 0._fp8, Z5pP(I) - qpZ5cX*Z5cP(I))*stempZ5pX

   ! Source equations
   SZ5p(I) = SZ5p(I) + sP3Z5*P3pP(I) &
                     + sP2Z5*P2pP(I) &
                     + sP1Z5*P1pP(I) &
                     + sP4Z5*P4pP(I) &
                     + sZ6Z5*Z6pP(I) &
                     + sB1Z5*B1pP(I) &
                     - fZ5R6p &
                     - fZ5RDp - fZ5N1p(I)


   ! iron dynamics
   ! following Vichi et al., 2007 it is assumed that the iron fraction of the ingested phytoplankton
   ! is egested as particulate detritus (Luca)

   SR6f(I)=SR6f(I)+sP1Z5*P1fP(I)+sP4Z5*P4fP(I)
   SR4f(I)=SR4f(I)+sP3Z5*P3fP(I)+sP2Z5*P2fP(I)

   SP1f(I)=SP1f(I)-sP1Z5*P1fP(I)
   SP4f(I)=SP4f(I)-sP4Z5*P4fP(I)
   SP3f(I)=SP3f(I)-sP3Z5*P3fP(I)
   SP2f(I)=SP2f(I)-sP2Z5*P2fP(I)


   ! P-flow to pool
   SN1p(I) = SN1p(I) + fZ5N1p(I)

   ! Phosphorus flux from/to detritus
   SR6p(I) = SR6p(I) + fZ5R6p
   SR1p(I) = SR1p(I) + fZ5RDp

   ! Phosphorus flux from prey
   SP1p(I) = SP1p(I) - sP1Z5*P1pP(I)
   SP2p(I) = SP2p(I) - sP2Z5*P2pP(I)
   SP3p(I) = SP3p(I) - sP3Z5*P3pP(I)
   SP4p(I) = SP4p(I) - sP4Z5*P4pP(I)
   SZ6p(I) = SZ6p(I) - sZ6Z5*Z6pP(I)
   SB1p(I) = SB1p(I) - sB1Z5*B1pP(I)

   ! Nitrogen dynamics
   fZ5RIn = retZ5*qnZ5c(I)+sdZ5*Z5nP(I)
   fZ5RDn = fZ5RIn*min(1._fp8,pe_R1Z5X*xR1nX)
   fZ5R6n = (fZ5RIn - fZ5RDn )

   fZ5NIn(I) = MAX( 0._fp8, Z5nP(I) - qnZ5cX*Z5cP(I))*stempZ5nX
   SN4n(I) = SN4n(I) + fZ5NIn(I)
   SZ5n(I) = SZ5n(I) + sP3Z5*P3nP(I) &
                     + sP2Z5*P2nP(I) &
                     + sP1Z5*P1nP(I) &
                     + sP4Z5*P4nP(I) &
                     + sZ6Z5*Z6nP(I) &
                     + sB1Z5*B1nP(I) &
                     - fZ5R6n &
                     - fZ5RDn - fZ5NIn(I)

   ! Nitrogen flux from/to detritus
   SR6n(I) = SR6n(I) + fZ5R6n
   SR1n(I) = SR1n(I) + fZ5RDn

   ! Nitrogen flux from prey
   SP1n(I) = SP1n(I) - sP1Z5*P1nP(I)
   SP2n(I) = SP2n(I) - sP2Z5*P2nP(I)
   SP3n(I) = SP3n(I) - sP3Z5*P3nP(I)
   SP4n(I) = SP4n(I) - sP4Z5*P4nP(I)
   SZ6n(I) = SZ6n(I) - sZ6Z5*Z6nP(I)
   SB1n(I) = SB1n(I) - sB1Z5*B1nP(I)

   ! Silica-flux from diatoms due to microzooplankton grazing
   SP1s(I) = SP1s(I) - sP1Z5 * P1sP(I)
   SR6s(I) = SR6s(I) + sP1Z5 * P1sP(I)

   return

   end subroutine microzooplankton_dynamics
!
!EOC
!-----------------------------------------------------------------------

   end module microzooplankton

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
