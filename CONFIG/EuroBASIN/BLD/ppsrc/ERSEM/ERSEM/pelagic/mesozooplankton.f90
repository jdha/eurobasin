

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Mesozooplankton
!
! !DESCRIPTION:
! This module contains the parametrisation for mesozooplankton and the
! routines to initialse the respective parameters from namelist. It
! also contains routines to detect the vertically integrated prey biomass
! of mesozooplankton in order to switch into hibernation in its absense
! and the routines to launch the full mesozooplankton dynamics or hibernation
! dynamics.\\[\baselineskip]
!
! !INTERFACE:
   module mesozooplankton
!
! !USES:
   use ersem_constants, only: fp8
   use pelagic_variables
   use pelagic_parameters
   use general, only: Adjust_Fixed_Nutrients
   use gas_dynamics, only: ub1c_o2x,urB1_O2X

   implicit none

! Default all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_mesozooplankton_parameters, mesozooplankton_prey_integration, &
      mesozooplankton_over_wintering_dynamics, mesozooplankton_dynamics
!
! !PUBLIC DATA MEMBERS:
! Michaelis-Menten constant for food uptake by mesozooplankton
! [mg C/m^3]
   real(fp8), public :: chuz4cX

! Maximal specific uptake of mesozooplankton at reference
! temperature [1/d]
   real(fp8), public :: sumz4X

! Relative mesoplankton assimilation efficiency
   real(fp8), public :: puz4X

! Regulating temperature factor Q10 for mesozooplankton
   real(fp8), public :: q10z4X

! Specific rest respiration of mesozooplankton at reference
! temperature [1/d]
   real(fp8), public :: srsz4X

! Excreted fraction of uptake by mesozooplantkon
   real(fp8), public :: pu_eaz4X

! Excreted fraction of POM uptake by mesozooplankton
   real(fp8), public :: pu_earz4X

! Michaelis-Menten constant to perceive food for
! mesozooplankton [mg C/m^2]
   real(fp8), public :: minfoodz4X

! Specific mortality of mesozooplantkon due to oxygen limitation [1/d]
   real(fp8), public :: sdz4oX

! Specific basal mortality of mesozooplankton [1/d]
   real(fp8), public :: sdz4X

! DOM-fraction of mesozooplankton excretion
   real(fp8), public :: pe_r1z4X

! Michaelis Menten constant for oxygen limitation
   real(fp8), public :: chrz4oX

! Specific overwintering respiration of mesozooplankton [1/d]
   real(fp8), public :: Z4repwX

! Specific overwintering mortality of mesozooplankton [1/d]
   real(fp8), public :: Z4mortX
!
! !LOCAL VARIABLES:
   real(fp8) :: fZ4R8n,fZ4R8p,fZ4Rdc,fZ4Rdn,fZ4Rdp,ineffZ4
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise parameters
!
! !DESCRIPTION:
! Initialises general mesozooplankton parameters from namelist \texttt{mesozooplankton\_parameters}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_mesozooplankton_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU

   implicit none
!
! !LOCAL VARIABLES:
   namelist/mesozooParameters/chuZ4cX,sumZ4X,puZ4X,q10Z4X,srsZ4X,&
     pu_eaZ4X,pu_eaRZ4X,minfoodZ4X,sdZ4oX,sdZ4X,pe_R1Z4X,chrZ4oX,&
     Z4repwX,Z4mortX
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading mesozooplankton parameters'
   open(99,file='BioParams/mesozoo.nml',status='old')
   read(99,nml=mesozooParameters)

   close(99)

   return

   end subroutine set_mesozooplankton_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Prey integration \label{sec:preyintegration}
!
! !DESCRIPTION:
! The vertically integrated prey availability to mesozooplankton is
! computed according to:
! \begin{eqnarray*}
! \overset{\textit{ow}}{\textit{Pr}}_{\textit{av}} & = & \intop_{\textit{seafloor}}^{0}\left(\left.f_{\textit{pr}}\right|_{P_{\textit{dia}}}^{Z_{\textit{MESO}}}\overset{c}{P'}_{\textit{dia}}+\left.f_{\textit{pr}}\right|_{P_{\textit{nano}}}^{Z_{\textit{MESO}}}\overset{c}{P'}_{\textit{nano}}\right.\! & & +\left.f_{\textit{pr}}\right|_{P_{\textit{pico}}}^{Z_{\textit{MESO}}}\overset{c}{P'}_{\textit{pico}}+\left.f_{\textit{pr}}\right|_{P_{micro}}^{Z_{\textit{MESO}}}\overset{c}{P'}_{\textit{micro}}\! & & \left.+\left.f_{\textit{pr}}\right|_{Z_{het}}^{Z_{\textit{MESO}}}\overset{c}{Z'}_{\textit{het}}+\left.f_{\textit{pr}}\right|_{Z_{micro}}^{Z_{\textit{MESO}}}\overset{c}{Z'}_{\textit{micro}}+\left.f_{\textit{pr}}\right|_{Z_{\textit{MESO}}}^{Z_{\textit{MESO}}}\overset{c}{Z'}_{\textit{MESO}}\right)\mathrm{d}z


! \end{eqnarray*}
! where $\left.f_{\textit{pr}}\right|_{P}^{Z}$ is the food preference
! of zooplankton type $Z$ on prey $P$.
!
! The prey availability $\overset{\textit{ow}}{\textit{Pr}}_{\textit{av}}$
! is used in \texttt{pelagic\_zone\_dynamics} to determine the activity
! level of mesozooplankton.\\[\baselineskip]
!
! !INTERFACE:
   subroutine mesozooplankton_prey_integration
!
! !USES:



   use pelagic_parameters
!
! !LOCAL VARIABLES:
   integer :: j,i_s
   real(fp8) :: preyINT_2d
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   preyINT = 0._fp8

   do i_s=1,n_upperX
       preyINT_2d=0._fp8

       j=i_s
       do while (j.ne.0)
          preyINT_2d= preyINT_2d + &
                     ( suP1_Z4X * p1cP(j) + &
                       suP2_Z4X * p2cP(j) + &
                       suP3_Z4X * p3cP(j) + &
                       suP4_Z4X * p4cP(j) + &
                       suZ4_Z4X * z4cP(j) + &
                       suZ5_Z4X * z5cP(j) + &
                       suZ6_Z4X * z6cP(j) + &
                       suB1_Z4X * b1cP(j) + &
                       suR6_Z4X * r6cP(j) ) * pdepth(j)
          j=lowerX(j) ! Go down the water column






       end do

       j=i_s
       do while (j.ne.0)
          preyint(j)=preyINT_2d
          j=lowerX(j) ! Go down the water column
       end do

   end do

   return

   end subroutine mesozooplankton_prey_integration
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Mesozooplankton overwintering dynamics \label{sec:mesozooow}
!
! !DESCRIPTION:
! In hibernation (overwintering) stage the only active processes for
! mesozooplankton are respiration and mortality. The basal rates of
! respiration and mortality ($r_{\textit{owresp}}$ and $r_{\textit{owmort}})$
! are modified with respect to the awake state:
! and $r_{\textit{zmort}})$:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{mes}}}^{G_{CO_{2}}} & = & r_{\textit{owresp}}\overset{c}{Z'}_{\textit{mes}}\!  \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{mes}}}^{\overset{c}{M}_{\textit{large}}} & = & r_{\textit{owmort}}\overset{c}{Z'}_{\textit{mes}}

! \end{eqnarray*}
!
! !INTERFACE:
   subroutine mesozooplankton_over_wintering_dynamics( I )
!
! !USES:
   use ersem_constants, only: CMass



   use pelagic_parameters, only: qnZIcX, qpZIcX
   use general, only: Adjust_fixed_nutrients
!
! !INPUT PARAMETERS:
   integer, intent(in) :: I
!
! !LOCAL VARIABLES:
   real(fp8) :: SZ4n, SZ4p
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Respiration
   fZ4O3c(i) = Z4cP(i) * Z4repwX

   ! Mortality
   fZ4R8c(i) = Z4cP(i) * Z4mortX
   ! Source terms
   SZ4n = SZ4c(I) * qnZIcX
   SZ4p = SZ4c(I) * qpZIcX
   SZ4c(i) = SZ4c(i) - fZ4R8c(i) - fZ4O3c(i)
   SO3c(i) = SO3c(i) + fZ4O3c(i)/CMass
   SZ4n = SZ4n - fZ4R8c(i) * qnZIcX
   SZ4p = SZ4p - fZ4R8c(i) * qpZIcX
   SR8c(i) = SR8c(i) + fZ4R8c(i)
   SR8n(i) = SR8n(i) + fZ4R8c(i) * qnZIcX
   SR8p(i) = SR8p(i) + fZ4R8c(i) * qpZIcX
   call Adjust_fixed_nutrients ( SZ4c(I), SZ4n, SZ4p, qnZIcX, &
      & qpZIcX, SN4n(I), SN1p(I), SR8c(I) )
   return
   end subroutine mesozooplankton_over_wintering_dynamics
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Mesozooplankton dynamics \label{sec:mesozoodynamics}
!
! !DESCRIPTION:
! In contrast to the other zooplankton groups, for mesozooplankton a
! constant stochiometric ratio between carbon, nitrate and phosphate
! is imposed by applying a flux correction in the final step of the
! mesozooplankton dynamics.
!
! The predation of mesozooplankton on other
! states of the modelled ecosystem is based on the prey available in
! the current environment according to:
! \begin{eqnarray*}
! \overset{\textit{c,p,n}}{\textit{Pr}}_{\textit{MESO}} & = & \left.f_{\textit{pr}}\right|_{B}^{Z_{\textit{MESO}}}\frac{\overset{c}{B'}}{\overset{c}{B'}+f_{\textit{mMESO}}}\overset{\textit{c,p,n}}{B'}+\left.f_{\textit{pr}}\right|_{P_{\textit{dia}}}^{Z_{\textit{MESO}}}\frac{\overset{c}{P'}_{\textit{dia}}}{\overset{c}{P'}_{\textit{dia}}+f_{\textit{mMESO}}}\overset{\textit{c,p,n}}{P'}_{\textit{dia}}\! & & \left.f_{\textit{pr}}\right|_{P_{\textit{micro}}}^{Z_{\textit{MESO}}}\frac{\overset{c}{P'}_{\textit{micro}}}{\overset{c}{P'}_{\textit{micro}}+f_{\textit{mMESO}}}\overset{\textit{c,p,n}}{P'}_{\textit{micro}}+\left.f_{\textit{pr}}\right|_{P_{\textit{nano}}}^{Z_{\textit{MESO}}}\frac{\overset{c}{P'}_{\textit{nano}}}{\overset{c}{P'}_{\textit{nano}}+f_{\textit{mMESO}}}\overset{\textit{c,p,n}}{P'}_{\textit{nano}}\! & & +\left.f_{\textit{pr}}\right|_{P_{\textit{pico}}}^{Z_{\textit{MESO}}}\frac{\overset{c}{P'}_{\textit{pico}}}{\overset{c}{P'}_{\textit{pico}}+f_{\textit{mMESO}}}\overset{\textit{c,p,n}}{P'}_{\textit{pico}}+\left.f_{\textit{pr}}\right|_{Z_{\textit{het}}}^{Z_{\textit{MESO}}}\frac{\overset{c}{Z'}_{\textit{het}}}{\overset{c}{Z'}_{\textit{het}}+f_{\textit{mMESO}}}\overset{\textit{c,p,n}}{Z'}_{\textit{het}}\! & & +\left.f_{\textit{pr}}\right|_{M_{\textit{med}}}^{Z_{\textit{MESO}}}\frac{\overset{c}{M'}_{\textit{med}}}{\overset{c}{M'}_{\textit{med}}+f_{\textit{mMESO}}}\overset{\textit{c,p,n}}{M'}_{\textit{med}}+\left.f_{\textit{pr}}\right|_{Z_{\textit{micro}}}^{Z_{\textit{MESO}}}\frac{\overset{c}{Z'}_{\textit{micro}}}{\overset{c}{Z'}_{\textit{micro}}+f_{\textit{mMESO}}}\overset{\textit{c,p,n}}{Z'}_{\textit{micro}}\! & & +\left.f_{\textit{pr}}\right|_{Z_{\textit{MESO}}}^{Z_{\textit{MESO}}}\frac{\overset{c}{Z'}_{\textit{MESO}}}{\overset{c}{Z'}_{\textit{MESO}}+f_{\textit{mMESO}}}\overset{\textit{c,p,n}}{Z'}_{\textit{MESO}}\,,
! \end{eqnarray*}
! where $\left.f_{\textit{pr}}\right|_{X}^{Z_{\textit{MESO}}}$ are
! the food preferences of mesozooplankton on state $X$ and $f_{\textit{mMESO}}$
! is the minimum food constant limiting the uptake efficiency at low
! concentrations of a specific prey.
!
! Specific food uptake by microzooplankton
! is a temperature regulated function of its own biomass
! and prey availability, additionally limited towards low concentrations
! of total prey:
! \begin{equation}
! \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}=\overset{\textit{MESO}}{g}_{\textit{max}}\overset{\textit{MESO}}{l}_{T}\frac{\overset{c}{Z}_{\textit{MESO}}}{\overset{\textit{c}}{\textit{Pr}}_{\textit{MESO}}+\overset{\textit{MESO}}{h}_{\textit{up}}}\,,
! \end{equation}
! leading to a total carbon uptake of:
! \begin{equation}
! \left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{growth}}=\mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{c}}{\textit{Pr}}_{\textit{MESO}}\,,
! \end{equation}
! where $\overset{\textit{MESO}}{h}_{\textit{up}}$ is the Michaelis-Menten
! constant for food uptake by mesozooplankton. The temperature response
! factor $\overset{MESO}{l}_{T}$ is given by the equation:
! \begin{equation}
! \overset{\textit{MESO}}{l}_{T}=\left.\overset{\textit{MESO}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-10}{10}}-\left.\overset{\textit{MESO}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-32}{3}}.
! \end{equation}
!
! The gross uptake is subject to losses to organic matter and activity
! respiration. The losses to organic matter like excretion or sloppy
! feeding are computed as:
! \begin{eqnarray*}
! \left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{excr}} & = & \left(1-\overset{\textit{MESO}}{p}_{\textit{eff}}\right)\overset{\textit{MESO}}{p}_{\textit{excr}}\left(\left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{micro}}}{\partial t}\right|_{\textit{growth}}-\mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{\textit{c,n,p}}}{\textit{M'}}_{\textit{med}}\right)\!   &  & +\left(1-\overset{\textit{MESO}}{p}_{\textit{eff}}\right)\overset{\textit{MESO}}{p}_{\textit{Mexcr}}\mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{\textit{c,n,p}}}{\textit{M'}}_{\textit{med}}
! \end{eqnarray*}
! with the parameter$\overset{\textit{MESO}}{p}_{\textit{eff}}$ estimating
! the fraction of uptake being effectively synthesised and $\overset{\textit{MESO}}{p}_{\textit{excr}}$
! giving the fraction of uptake loss directed to organic matter with
! the exception of particulate matter output, whose uptake loss fraction
! is set in the parameter $\overset{\textit{MESO}}{p}_{\textit{Mexcr}}$.
!
! Mesozooplankton respiration is computed according to:
! \begin{equation}
! \left.\frac{\partial\overset{c}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{resp}}=\left(1-\overset{\textit{MESO}}{p}_{\textit{eff}}\right)\left(1-\overset{\textit{MESO}}{p}_{\textit{excr}}\right)\left.\frac{\partial\overset{c}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{growth}}+\overset{\textit{MESO}}{r}_{\textit{resp}}\overset{\textit{MESO}}{l}_{T}\overset{c}{Z'}_{\textit{MESO}}\,,
! \end{equation}
! where $\overset{\textit{MESO}}{r}_{\textit{resp}}$ is the specific
! basal respiration rate at rest.
! Mortality is given as:
! \begin{equation}
! \left.\frac{\partial\overset{\textit{c,p,n}}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{mort}}=\left(\left(1-\overset{\textit{MESO}}{l}_{O_{2}}\right)\overset{\textit{MESO}}{p}_{\textit{mort}O2}+\overset{\textit{MESO}}{p}_{\textit{mort}}\right)\overset{\textit{c,p,n}}{Z'}_{\textit{MESO}}
! \end{equation}
! with $\overset{\textit{MESO}}{p}_{\textit{mort}}$ being the specific
! mortality rate of bacteria, $\overset{\textit{MESO}}{p}_{\textit{mort}O2}$
! is the maximum specific rate of mortality due to oxygen limitation
! and the oxygen limitation state $\overset{\textit{MESO}}{l}_{O_{2}}$
! is given by:
! \begin{equation}
! \overset{\textit{MESO}}{l}_{O_{2}}=\frac{s_{\textit{relO}_{2}+}s_{\textit{relO}_{2}}\overset{\textit{MESO}}{h}_{O_{2}}}{s_{\textit{relO}_{2}}+\overset{\textit{MESO}}{h}_{O_{2}}}\,.
! \end{equation}
!
! The fluxes of prey to mesozooplankton are then given by the equations:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{B}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MESO}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{B'}\!  \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{P}_{\textit{dia}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MESO}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{P'}_{\textit{dia}}\! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{P}_{\textit{micro}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MESO}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{P'}_{\textit{micro}}\!  \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{P}_{\textit{nano}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MESO}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{P'}_{\textit{nano}}\! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{P}_{\textit{pico}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MESO}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{P'}_{\textit{pico}}\!  \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{Z}_{\textit{het}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MESO}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{Z'}_{\textit{het}}\! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{Z}_{\textit{micro}}}^{\overset{\textit{c,p,n}}{Z}_{\textit{MESO}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{c,p,n}}{Z'}_{\textit{micro}}\,.
! \end{eqnarray*}
!
! The loss to organic matter is split between dissolved and particulate
! matter as:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{micro}}}^{\overset{c}{M}_{\textit{\textit{large}}}} & = & \left(1-\overset{\textit{MESO}}{p}_{\textit{dis}}\right)\left(\left.\frac{\partial\overset{c}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{c}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{mort}}\right)\! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{micro}}}^{\overset{c}{M}_{\textit{\textit{dis}}}} & =\overset{\textit{MESO}}{p}_{\textit{dis}} & \overset{\textit{MESO}}{p}_{\textit{lab}}\left(\left.\frac{\partial\overset{c}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{c}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{mort}}\right)\! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{micro}}}^{\overset{c}{M}_{\textit{\textit{slab}}}} & = & \overset{\textit{MESO}}{p}_{\textit{dis}}\left(1-\overset{\textit{MESO}}{p}_{\textit{lab}}\right)\left(\left.\frac{\partial\overset{c}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{c}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{mort}}\right)\,,
! \end{eqnarray*}
! where $\overset{\textit{MESO}}{p}_{\textit{dis}}$ is the fraction
! of uptake loss directed to dissolved organic matter and $\overset{\textit{MESO}}{p}_{\textit{lab}}$
! is the fraction of this in labile state.
! The phosphate and nitrogen fluxes to organic matter are given by:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{p,n}{Z}_{\textit{MESO}}}^{\overset{p,n}{M}_{\textit{\textit{dis}}}} & = & \overset{\textit{MESO}}{p}_{\textit{dis}}p_{\textit{DPcyto}}\left(\left.\frac{\partial\overset{p,n}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{p,n}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{mort}}\right)\! \left.\mathcal{F}\right|_{\overset{p,n}{Z}_{\textit{MESO}}}^{\overset{p,n}{M}_{\textit{\textit{med}}}} & = & \left(1-\overset{\textit{MESO}}{p}_{\textit{dis}}p_{\textit{DPcyto}}\right)\left(\left.\frac{\partial\overset{p,n}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{excr}}+\left.\frac{\partial\overset{p,n}{Z}_{\textit{MESO}}}{\partial t}\right|_{\textit{mort}}\right)\,,
! \end{eqnarray*}
! with $p_{\textit{DNcyto}}$ being the phosphate/nitrogen fraction
! in cytoplasm of DOM vs. structural components.
! The respiration fluxes towards inorganic gasses are given as:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{Z}_{\textit{micro}}}^{G_{\textit{\textit{CO}}_{2}}} & = & \frac{1}{\mathcal{\mathbb{C}}}\left.\frac{\partial\overset{c}{Z}_{\textit{micro}}}{\partial t}\right|_{\textit{resp}}\! \left.\mathcal{F}\right|_{G_{O_{2}}}^{\textit{sink}} & = & \overset{B}{p}_{O_{2}}\left.\frac{\partial\overset{c}{Z}_{\textit{micro}}}{\partial t}\right|_{\textit{resp}}\,.
! \end{eqnarray*}
!
! These fluxes are counter-balanced in a final step in order to maintain
! the fixed internal stochiometric relation of carbon, nitrogen and
! phosphorus by releasing any constituent assimilated in excess to large
! particulate organic carbon ($\overset{c}{M}_{\textit{large}}$), disolved
! inorganic phosphate ($N_{phos})$ or ammonium ($N_{amm})$.
!
! The silicate contained in the prey fields is directly cycled to organic
! matter:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{s}{P}_{\textit{dia}}}^{\overset{\textit{s}}{M}_{\textit{large}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{s}}{P'}_{\textit{dia}}\,.
! \end{eqnarray*}
!
! Correspondingly, if iron is used (pre-preocessing option \texttt{1}),
! the iron contained in the prey fields is directly cycled to organic
! matter:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{dia}}}^{\overset{f}{M}_{\textit{med}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{f}}{P'}_{\textit{dia}}\!  \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{micro}}}^{\overset{f}{M}_{\textit{med}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{f}}{P'}_{\textit{micro}}\! \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{nano}}}^{\overset{f}{M}_{\textit{small}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{f}}{P'}_{\textit{nano}}\!  \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{pico}}}^{\overset{f}{M}_{\textit{small}}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{f}}{P'}_{\textit{pico}}\! \left.\mathcal{F}\right|_{\overset{f}{M}_{\textit{med}}}^{N_{fe}} & = & \mathcal{\overset{\textit{MESO}}{S}}_{\textit{growth}}\overset{\textit{f}}{M'}_{\textit{med}}\,.
! \end{eqnarray*}
!
! If calcification is considered (activated through the preprocessing
! option \texttt{1}), absorption of DIC into calcite is estimated
! as:
! \begin{equation}
! \left.\mathcal{F}\right|_{G_{\textit{CO}_{2}}}^{\overset{c}{M}_{\textit{calc}}}=q_{\textit{rain}}p_{\textit{gutd}}\left(1-\overset{\textit{MESO}}{p}_{\textit{eff}}\right)\overset{\textit{MESO}}{p}_{\textit{excr}}\left.\mathcal{F}\right|_{\overset{\textit{c}}{P}_{\textit{nano}}}^{\overset{\textit{c}}{Z}_{\textit{MESO}}}\,.
! \end{equation}
!
! !INTERFACE:
   subroutine mesozooplankton_dynamics(I)
!
! !USES:
   use ersem_constants, only: CMass
   use calcification, only: gutdiss
!
! !INPUT PARAMETERS:
   integer :: i
!
! !LOCAL VARIABLES:
   real(fp8) :: SZ4n, SZ4p, etZ4
   real(fp8) :: rumZ4, put_uZ4, rrsZ4, rraZ4, rugZ4, rdZ4, sdZ4
   real(fp8) :: sP1Z4,sP2Z4,sP3Z4,sP4Z4,sB1Z4,sR6Z4,sZ5Z4,sZ6Z4
   real(fp8) :: corrox, eO2Z4, fZ4RIp, fZ4RIn
   real(fp8) :: retZ4
   real(fp8) :: ruP1Z4c, ruP2Z4c, ruP3Z4c, ruP4Z4c
   real(fp8) :: ruZ6Z4c, ruB1Z4c, ruZ5Z4c, ruR6Z4c, ruZ4Z4c
   real(fp8) :: temp_n,temp_p
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Set up dummy source terms for nutrient components....................
   SZ4n = SZ4c(I) * qnZIcX
   SZ4p = SZ4c(I) * qpZIcX
   ! Temperature effect :
   etZ4 = q10Z4X**((ETW(I)-10._fp8)/10._fp8) - q10Z4X**((ETW(I)-32._fp8)/3._fp8)
   ! Oxygen limitation :
   CORROX = 1._fp8 + chrZ4oX
   eO2Z4 = MIN(1._fp8,CORROX*(eO2mO2/(chrZ4oX + eO2mO2)))
   ! Available food :
   sB1Z4 = suB1_Z4X*B1cP(I)/(B1cP(I)+minfoodZ4X)
   sP1Z4 = suP1_Z4X*P1cP(I)/(P1cP(I)+minfoodZ4X)
   sP2Z4 = suP2_Z4X*P2cP(I)/(P2cP(I)+minfoodZ4X)
   sP3Z4 = suP3_Z4X*P3cP(I)/(P3cP(I)+minfoodZ4X)
   sP4Z4 = suP4_Z4X*P4cP(I)/(P4cP(I)+minfoodZ4X)
   sZ5Z4 = suZ5_Z4X*Z5cP(I)/(Z5cP(I)+minfoodZ4X)
   sZ6Z4 = suZ6_Z4X*Z6cP(I)/(Z6cP(I)+minfoodZ4X)
   sR6Z4 = suR6_Z4X*R6cP(I)/(R6cP(I)+minfoodZ4X)
   ruZ4Z4c = suZ4_Z4X*Z4cP(I)*Z4cP(I)/(Z4cP(I)+minfoodZ4X)
   ruB1Z4c = sB1Z4*B1cP(I)
   ruP1Z4c = sP1Z4*P1cP(I)
   ruP2Z4c = sP2Z4*P2cP(I)
   ruP3Z4c = sP3Z4*P3cP(I)
   ruP4Z4c = sP4Z4*P4cP(I)
   ruZ5Z4c = sZ5Z4*Z5cP(I)
   ruZ6Z4c = sZ6Z4*Z6cP(I)
   ruR6Z4c = sR6Z4*R6cP(I)
   rumZ4 = ruP1Z4c + ruP2Z4c + ruP3Z4c + ruP4Z4c + ruZ4Z4c &
      & + ruB1Z4c + ruZ5Z4c + ruZ6Z4c + ruR6Z4c
   ! Uptake :
   put_uZ4 = sumZ4X/(rumZ4 + chuZ4cX)*etZ4*Z4c(I)
   rugZ4 = put_uZ4*rumZ4
   ! Fluxes into mesoplankton :
   fB1Z4c(I) = put_uZ4*ruB1Z4c
   fP1Z4c(I) = put_uZ4*ruP1Z4c
   fP2Z4c(I) = put_uZ4*ruP2Z4c
   fP3Z4c(I) = put_uZ4*ruP3Z4c
   fP4Z4c(I) = put_uZ4*ruP4Z4c
   fZ5Z4c(I) = put_uZ4*ruZ5Z4c
   fZ6Z4c(I) = put_uZ4*ruZ6Z4c
   fR6Z4c(I) = put_uZ4*ruR6Z4c
   fZ4Z4c(I) = put_uZ4*ruZ4Z4c
   sB1Z4 = put_uZ4*sB1Z4
   sP1Z4 = put_uZ4*sP1Z4
   sP2Z4 = put_uZ4*sP2Z4
   sP3Z4 = put_uZ4*sP3Z4
   sP4Z4 = put_uZ4*sP4Z4
   sZ5Z4 = put_uZ4*sZ5Z4
   sZ6Z4 = put_uZ4*sZ6Z4
   sR6Z4 = put_uZ4*sR6Z4
   ! Zooplankton Grazing
   ! Z4herb(i) = fP1Z4c(i) + fP2Z4c(i) + fP3Z4c(i) +fP4Z4c(i)
   ! Z4carn(i) = fB1Z4c(i) + fZ5Z4c(i) + fZ6Z4c(i) + fR6Z4c(i)
   ! Mortality
   sdZ4 = ((1._fp8 - eO2Z4)*sdZ4oX + sdZ4X)
   rdZ4 = sdZ4*Z4cP(I)
   ! Assimilation inefficiency:
   ineffZ4 = (1._fp8 - puZ4X)
   ! Excretion
   retZ4 = ineffZ4 * ( (rugZ4-fR6Z4c(I)) * pu_eaZ4X &
         + fR6Z4c(I) * pu_eaRZ4X )
   fZ4RDc = (retZ4 + rdZ4)*pe_R1Z4X
   fZ4R8c(I) = (retZ4 + rdZ4)*(1._fp8 - pe_R1Z4X)
   fZXRDc(I) = fZXRDc(I)+fZ4RDc
   ! Rest respiration, corrected for prevailing temperature
   rrsZ4 = srsZ4X*etZ4*Z4cP(I)
   ! Activity respiration
   rraZ4 = ineffZ4 * rugZ4 - retZ4
   ! Total respiration
   fZ4O3c(I) = rrsZ4 + rraZ4
   fO3L2c(I) = fO3L2c(I) + &
   RainR(I) * gutdiss * (1._fp8 - puZ4X)* pu_eaZ4X * fP2Z4c(I)
   rraZ4 = rugZ4*(1._fp8 - puZ4X)-retZ4
   ! Source equations
   SZ4c(I) = SZ4c(I) + rugZ4 &
                         - fZ4RDc - fZ4R8c(I) - fZ4Z4c(I) - fZ4O3c(I)
   ! Flows from and to detritus
   SR1c(I) = SR1c(I) + (fZ4RDc * R1R2X)
   SR2c(I) = SR2c(I) + (fZ4RDc * (1._fp8-R1R2X))
   SR8c(I) = SR8c(I) + fZ4R8c(I)
   SR6c(I) = SR6c(I) - fR6Z4c(I)
   ! Grazing and predation
   SP1c(I) = SP1c(I) - fP1Z4c(I)
   SP2c(I) = SP2c(I) - fP2Z4c(I)
   SP3c(I) = SP3c(I) - fP3Z4c(I)
   SP4c(I) = SP4c(I) - fP4Z4c(I)
   SZ5c(I) = SZ5c(I) - fZ5Z4c(I)
   SZ6c(I) = SZ6c(I) - fZ6Z4c(I)
   SB1c(I) = SB1c(I) - fB1Z4c(I)
   SChl1(I) = SChl1(I) - sP1Z4*chl1P(I)
   SChl2(I) = SChl2(I) - sP2Z4*chl2P(I)
   SChl3(I) = SChl3(I) - sP3Z4*chl3P(I)
   SChl4(I) = SChl4(I) - sP4Z4*chl4P(I)
   ! Respiration
   SO3c(I) = SO3c(I) + fZ4O3c(I)/CMass
   SO2o(I) = SO2o(I) - fZ4O3c(I)*urB1_O2X
   ! Phosphorus dynamics in mesozooplankton, derived from carbon flows....
   fZ4RIp = (fZ4RDc + fZ4R8c(I)) * qpZIcX
   fZ4RDp = min(fZ4RIp, fZ4RDc * qpZIcX * xR1pX)
   fZ4R8p = fZ4RIp - fZ4RDp
   ! Source equations
   SZ4p = SZ4p + sP1Z4*P1pP(I) &
               + sP2Z4*P2pP(I) &
               + sP3Z4*P3pP(I) &
               + sP4Z4*P4pP(I) &
               + sZ5Z4*Z5pP(I) &
               + sZ6Z4*Z6pP(I) &
               + sB1Z4*B1pP(I) &
               + sR6Z4*R6pP(I) &
               - fZ4R8p - fZ4RDp
   ! Iron dynamics
   ! following Vichi et al., 2007 it is assumed that the iron fraction of the ingested phytoplankton
   ! is egested as particulate detritus (Luca)
   SR6f(I)=SR6f(I) + sP1Z4*P1fP(I)+sP4Z4*P4fP(I)-sR6Z4*R6fP(I)
   SR4f(I)=SR4f(I) + sP2Z4*P2fP(I)+sP3Z4*P3fP(I)
   SN7f(I)=SN7f(I)+sR6Z4*R6fP(I)
   SP1f(I)=SP1f(I)- sP1Z4*P1fP(I)
   SP4f(I)=SP4f(I)- sP4Z4*P4fP(I)
   SP2f(I)=SP2f(I)- sP2Z4*P2fP(I)
   SP3f(I)=SP3f(I)- sP3Z4*P3fP(I)
   ! Phosphorus flux from/to detritus
   SR1p(I) = SR1p(I) + fZ4RDp
   SR6p(I) = SR6p(I) - sR6Z4*R6pP(I)
   SR8p(I) = SR8p(I) + fZ4R8p
   ! Phosphorus flux from prey
   SP1p(I) = SP1p(I) - sP1Z4*P1pP(I)
   SP2p(I) = SP2p(I) - sP2Z4*P2pP(I)
   SP3p(I) = SP3p(I) - sP3Z4*P3pP(I)
   SP4p(I) = SP4p(I) - sP4Z4*P4pP(I)
   SZ5p(I) = SZ5p(I) - sZ5Z4*Z5pP(I)
   SZ6p(I) = SZ6p(I) - sZ6Z4*Z6pP(I)
   SB1p(I) = SB1p(I) - sB1Z4*B1pP(I)
   ! Nitrogen dynamics in mesozooplankton, derived from carbon flows......
   fZ4RIn = (fZ4RDc + fZ4R8c(I)) * qnZIcX
   fZ4RDn = min(fZ4RIn, fZ4RDc * qnZIcX * xR1nX)
   fZ4R8n = fZ4RIn - fZ4RDn
   ! Source equations
   SZ4n = SZ4n + sP1Z4*P1nP(I) &
               + sP2Z4*P2nP(I) &
               + sP3Z4*P3nP(I) &
               + sP4Z4*P4nP(I) &
               + sZ5Z4*Z5nP(I) &
               + sZ6Z4*Z6nP(I) &
               + sB1Z4*B1nP(I) &
               + sR6Z4*R6nP(I) &
               - fZ4R8n - fZ4RDn
   ! Nitrogen flux from/to detritus
   SR1n(I) = SR1n(I) + fZ4RDn
   SR6n(I) = SR6n(I) - sR6Z4*R6nP(I)
   SR8n(I) = SR8n(I) + fZ4R8n
   ! Nitrogen flux from prey
   SP1n(I) = SP1n(I) - sP1Z4*P1nP(I)
   SP2n(I) = SP2n(I) - sP2Z4*P2nP(I)
   SP3n(I) = SP3n(I) - sP3Z4*P3nP(I)
   SP4n(I) = SP4n(I) - sP4Z4*P4nP(I)
   SZ5n(I) = SZ5n(I) - sZ5Z4*Z5nP(I)
   SZ6n(I) = SZ6n(I) - sZ6Z4*Z6nP(I)
   SB1n(I) = SB1n(I) - sB1Z4*B1nP(I)
   ! Silica-flux from diatoms due to mesozooplankton grazing
   SP1s(I) = SP1s(I) - sP1Z4 * P1sP(I)
   SR8s(I) = SR8s(I) + sP1Z4 * P1sP(I)
   ! Re-establish the fixed nutrient ratio in zooplankton.................
   temp_p = SN1p(I)
   temp_n = SN4n(I)
   call Adjust_fixed_nutrients ( SZ4c(I), SZ4n, SZ4p, qnZIcX, &
                               qpZIcX, SN4n(I), SN1p(I), SR8c(I) )
   fZ4N1p(I) = SN1p(I)-temp_p
   fZ4NIn(I) = SN4n(I)-temp_n
   return
   end subroutine mesozooplankton_dynamics
!
!EOC
!-----------------------------------------------------------------------
   end module mesozooplankton
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
