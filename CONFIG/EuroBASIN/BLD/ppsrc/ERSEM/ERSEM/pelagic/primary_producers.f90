

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Primary producers
!
! !DESCRIPTION:
! This module contains the process descriptions and parameters
! related to primary production and phytoplankton physiology. The subroutines
! defined in this class are a routine to set the phytoplankton parameters
! from namelist, a routine for the computation of the nutrient limitation
! state of each phytoplankton class and four routines for the dynamics
! of phytoplankton growth, respiration, excretion and lysis of each
! phytoplankton type.\\[\baselineskip]
!
! !INTERFACE:
   module primary_producers
!
! !USES:
   use ersem_constants, only: fp8, zeroX
   use pelagic_variables
   use pelagic_parameters, only: pco2a3, r1r2x
   use gas_dynamics, only: ub1c_o2x,urB1_O2X
!
   implicit none
!
! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_primary_producer_parameters, nutrient_limitation, &
      diatom_dynamics, nanophytoplankton_dynamics, &
      picophytoplankton_dynamics, microphytoplankton_dynamics
!
! !PUBLIC DATA MEMBERS:
! Inverse Redfield ratio of Phosphorus to carbon [mmol P/mg C]
   real(fp8), public :: qnrpicX

! Inverse Redfield ratio of nitrogen to carbon [mmol N/mg C]
   real(fp8), public :: qprpicX

! Specific maximal productivity of diatoms at reference temperature [1/d]
   real(fp8), public :: sump1X

! Regulating temperature factor Q10 for diatoms
   real(fp8), public :: q10p1X

! Specific rest respiration of diatoms at reference temperature [1/d]
   real(fp8), public :: srsp1X

! Excreted fraction of primary production by diatoms
   real(fp8), public :: pu_eap1X

! Respired fraction of primary production by diatoms
! (activity respiration)
   real(fp8), public :: pu_rap1X

! Michaelis-Menten constant for Silicate limitation [mmol Si/m^3]
   real(fp8), public :: chp1sX

! Minimal nitrogen to carbon ratio for diatoms [mmol N/mg C]
   real(fp8), public :: qnlp1cX

! Minimal phosphorus to carbon ratio for diatoms [mmol P/mg C]
   real(fp8), public :: qplp1cX

! Factor of qnRPIcX giving the treshold for nitrogen limitation
! of diatoms
   real(fp8), public :: xqcp1pX

! Factor of qpRPIcX giving the treshold for phospherus limitation
! of diatoms
   real(fp8), public :: xqcp1nX

! Factor of qnRPIcX giving the maximal nitrogen to carbon ratio
! for diatoms
   real(fp8), public :: xqnp1X

! Factor of qpRPIcX giving the maximal phosporus to carbon ratio
! for diatoms
   real(fp8), public :: xqpp1X

! Nitrate affinity of diatoms [m^3/mg C/d]
   real(fp8), public :: qup1n3X

! Ammonium affinity of diatoms [m^3/mg C/d]
   real(fp8), public :: qup1n4X

! Phosphate affinity of diatoms [m^3/mg C/d]
   real(fp8), public :: qurp1pX

! Maximal silicon to carbon ratio of diatoms [mmol Si/mg C]
   real(fp8), public :: qsp1cX

! Level of nutrient limitation below which diatoms subside
   real(fp8), public :: esnip1X

! Maximal subsiding velocity of diatoms [m/d]
   real(fp8), public :: resp1mX

! 1.1 of minimal specific lysis rate of diatoms [1/d]
   real(fp8), public :: sdop1X

! Specific maximal productivity of nanophytoplankton at
! reference temperature [1/d]
   real(fp8), public :: sump2X

! Regulating temperature factor Q10 for nanophytoplankton
   real(fp8), public :: q10p2X

! Specific rest respiration of nanophytoplankton at
! reference temperature [1/d]
   real(fp8), public :: srsp2X

! Excreted fraction of primary production by nanophytoplankton
   real(fp8), public :: pu_eap2X

! Respired fraction of primary production by nanophytoplankton
! (activity respiration)
   real(fp8), public :: pu_rap2X

! Minimal nitrogen to carbon ratio for nanophytoplankton
! [mmol N/mg C]
   real(fp8), public :: qnlp2cX

! Minimal phosphorus to carbon ratio for nanophytoplankton
! [mmol P/mg C]
   real(fp8), public :: qplp2cX

! Factor of qnRPIcX giving the treshold for nitrogen limitation
! of nanophytoplankton
   real(fp8), public :: xqcp2pX

! Factor of qpRPIcX giving the treshold for phospherus limitation
! of nanophytoplankton
   real(fp8), public :: xqcp2nX

! Factor of qnRPIcX giving the maximal nitrogen to carbon ratio
! for nanophytoplankton
   real(fp8), public :: xqnp2X

! Factor of qpRPIcX giving the maximal phosporus to carbon ratio
! for nanophytoplankton
   real(fp8), public :: xqpp2X

! Nitrate affinity of nanophytoplankton [m^3/mg C/d]
   real(fp8), public :: qup2n3X

! Ammonium affinity of nanophytoplankton [m^3/mg C/d]
   real(fp8), public :: qup2n4X

! Phosphate affinity of nanophytoplankton [m^3/mg C/d]
   real(fp8), public :: qurp2pX

! Level of nutrient limitation below which nanophytoplankton subsides
   real(fp8), public :: esnip2X

! Maximal subsiding velocity of nanophytoplankton [m/d]
   real(fp8), public :: resp2mX

! 1.1 of minimal specific lysis rate of nanophytoplankton [1/d]
   real(fp8), public :: sdop2X

! Specific maximal productivity of picophytoplankton at
! reference temperature [1/d]
   real(fp8), public :: sump3X

! Regulating temperature factor Q10 for picophytoplankton
   real(fp8), public :: q10p3X

! Specific rest respiration of picophytoplankton at
! reference temperature [1/d]
   real(fp8), public :: srsp3X

! Excreted fraction of primary production by picophytoplankton
   real(fp8), public :: pu_eap3X

! Respired fraction of primary production by picophytoplankton
! (activity respiration)
   real(fp8), public :: pu_rap3X

! Minimal nitrogen to carbon ratio for picophytoplankton
! [mmol N/mg C]
   real(fp8), public :: qnlp3cX

! Minimal phosphorus to carbon ratio for picophytoplankton
! [mmol P/mg C]
   real(fp8), public :: qplp3cX

! Factor of qnRPIcX giving the treshold for nitrogen limitation
! of picophytoplankton
   real(fp8), public :: xqcp3pX

! Factor of qpRPIcX giving the treshold for phospherus limitation
! of picophytoplankton
   real(fp8), public :: xqcp3nX

! Factor of qnRPIcX giving the maximal nitrogen to carbon ratio
! for picophytoplankton
   real(fp8), public :: xqnp3X

! Factor of qpRPIcX giving the maximal phosporus to carbon ratio
! for picophytoplankton
   real(fp8), public :: xqpp3X

! Nitrate affinity of picophytoplankton [m^3/mg C/d]
   real(fp8), public :: qup3n3X

! Ammonium affinity of picophytoplankton [m^3/mg C/d]
   real(fp8), public :: qup3n4X

! Phosphate affinity of picophytoplankton [m^3/mg C/d]
   real(fp8), public :: qurp3pX

! Level of nutrient limitation below which picophytoplankton subsides
   real(fp8), public :: esnip3X

! Maximal subsiding velocity of picophytoplankton [m/d]
   real(fp8), public :: resp3mX

! 1.1 of minimal specific lysis rate of picophytoplankton [1/d]
   real(fp8), public :: sdop3X

! Specific maximal productivity of microphytoplankton at
! reference temperature [1/d]
   real(fp8), public :: sump4X

! Regulating temperature factor Q10 for microphytoplankton
   real(fp8), public :: q10p4X

! Specific rest respiration of microphytoplankton at
! reference temperature [1/d]
   real(fp8), public :: srsp4X

! Excreted fraction of primary production by microphytoplankton
   real(fp8), public :: pu_eap4X

! Respired fraction of primary production by microphytoplankton
! (activity respiration)
   real(fp8), public :: pu_rap4X

! Minimal nitrogen to carbon ratio for microphytoplankton
! [mmol N/mg C]
   real(fp8), public :: qnlp4cX

! Minimal phosphorus to carbon ratio for microphytoplankton
! [mmol P/mg C]
   real(fp8), public :: qplp4cX

! Factor of qnRPIcX giving the treshold for nitrogen limitation
! of microphytoplankton
   real(fp8), public :: xqcp4pX

! Factor of qpRPIcX giving the treshold for phospherus limitation
! of microphytoplankton
   real(fp8), public :: xqcp4nX

! Factor of qnRPIcX giving the maximal nitrogen to carbon ratio
! for microphytoplankton
   real(fp8), public :: xqnp4X

! Factor of qpRPIcX giving the maximal phosporus to carbon ratio
! for microphytoplankton
   real(fp8), public :: xqpp4X

! Nitrate affinity of microphytoplankton [m^3/mg C/d]
   real(fp8), public :: qup4n3X

! Ammonium affinity of microphytoplankton [m^3/mg C/d]
   real(fp8), public :: qup4n4X

! Phosphate affinity of microphytoplankton [m^3/mg C/d]
   real(fp8), public :: qurp4pX

! Level of nutrient limitation below which microphytoplankton subsides
   real(fp8), public :: esnip4X

! Maximal subsiding velocity of microphytoplankton [m/d]
   real(fp8), public :: resp4mX

! Extra mortality rate for large biomasses for microphytoplankton,
! currently not active (1/day)
   real(fp8), public :: seop4X

! 1.1 of minimal specific lysis rate of microphytoplankton [1/d]
   real(fp8), public :: sdop4X

! Additional activity respiration as fraction of specific production,
! currnently not active
   real(fp8), public :: P4resX

! Initial slope of PI-curve for diatoms[mg C m^2 /mg Chl/W/d]
   real(fp8), public :: alphaP1X

! Initial slope of PI-curve for nanophytoplankton [mg C m^2 /mg Chl/W/d]
   real(fp8), public :: alphaP2X

! Initial slope of PI-curve for picophytoplankton [mg C m^2 /mg Chl/W/d]
   real(fp8), public :: alphaP3X

! Initial slope of PI-curve for microphytoplankton [mg C m^2 /mg Chl/W/d]
   real(fp8), public :: alphaP4X

! Photo-inhibition paramter for diatoms [mg C m^2 /mg Chl/W/d]
   real(fp8), public :: betaP1X

! Photo-inhibition paramter for nanophytoplankton [mg C m^2 /mg Chl/W/d]
   real(fp8), public :: betaP2X

! Photo-inhibition paramter for picophytoplankton [mg C m^2 /mg Chl/W/d]
   real(fp8), public :: betaP3X

! Photo-inhibition paramter for microphytoplankton [mg C m^2 /mg Chl/W/d]
   real(fp8), public :: betaP4X

! Maximal effective chlorophyll to carbon photosynthesis ratio of
! diatoms [mg Chl/mg C]
   real(fp8), public :: phimP1X

! Maximal effective chlorophyll to carbon photosynthesis ratio of
! nanophytoplankton [mg Chl/mg C]
   real(fp8), public :: phimP2X

! Maximal effective chlorophyll to carbon photosynthesis ratio of
! picophytoplankton [mg Chl/mg C]
   real(fp8), public :: phimP3X

! Maximal effective chlorophyll to carbon photosynthesis ratio of
! microphytoplankton [mg Chl/mg C]
   real(fp8), public :: phimP4X

! Minimal effective chlorophyll to carbon photosynthesis ratio of
! Diatoms [mg Chl/mg C]
   real(fp8), public :: phiP1HX

! Minimal effective chlorophyll to carbon photosynthesis ratio of
! Nanophytoplankton [mg Chl/mg C]
   real(fp8), public :: phiP2HX

! Minimal effective chlorophyll to carbon photosynthesis ratio of
! Picophytoplankton [mg Chl/mg C]
   real(fp8), public :: phiP3HX

! Minimal effective chlorophyll to carbon photosynthesis ratio of
! Microphytoplankton [mg Chl/mg C]
   real(fp8), public :: phiP4HX

! Minimal iron to carbon ratio of diatoms [umol Fe/mg C]
   real(fp8), public :: qflP1cX

! Minimal iron to carbon ratio of nanophytoplankton [umol Fe/mg C]
   real(fp8), public :: qflP2cX

! Minimal iron to carbon ratio of picophytoplankton [umol Fe/mg C]
   real(fp8), public :: qflP3cX

! Minimal iron to carbon ratio of microphytoplankton [umol Fe/mg C]
   real(fp8), public :: qflP4cX

! Maximal/optimal iron to carbon ratio of diatoms [umol Fe/mg C]
   real(fp8), public :: qfRP1cX

! Maximal/optimal iron to carbon ratio of nanophytoplankton [umol Fe/mg C]
   real(fp8), public :: qfRP2cX

! Maximal/optimal iron to carbon ratio of picophytoplankton [umol Fe/mg C]
   real(fp8), public :: qfRP3cX

! Maximal/optimal iron to carbon ratio of microphytoplankton [umol Fe/mg C]
   real(fp8), public :: qfRP4cX

! Specific affinity constant of diatoms for iron [m^3/mg C/d]
   real(fp8), public :: qurp1fX

! Specific affinity constant of nanophytoplankton for iron [m^3/mg C/d]
   real(fp8), public :: qurp2fX

! Specific affinity constant of picophytoplankton for iron [m^3/mg C/d]
   real(fp8), public :: qurp3fX

! Specific affinity constant of microphytoplankton for iron [m^3/mg C/d]
   real(fp8), public :: qurp4fX

! Min CHl:C ratio
   real(fp8), public, parameter :: ChlCmin=0.002

! photosynthetically available fraction of irradiation
   real(fp8), public :: pEIR_eowX

! TODO
   real(fp8), public :: parEIR

! TODO
   integer, public :: limnutX





! Flux diagnostics
   real(fp8) :: fP1R6p,fP2R4p,fP3R4p,fP4R6p
   real(fp8) :: fP1Rdp,fP2Rdp,fP3Rdp,fP4Rdp
   real(fp8) :: fP1R6n,fP2R4n,fP3R4n,fP4R6n
   real(fp8) :: fP1Rdn,fP2Rdn,fP3Rdn,fP4Rdn
   real(fp8) :: fP1R6s
   real(fp8) :: fN5P1s,fNIP1n,fNIP2n,fNIP3n,fNIP4n

   ! TODO
   real(fp8) :: iniP1,iniP2,iniP3,iniP4,inP1s


! Iron flux diagnostics
   real(fp8) :: fP1R6f,fP2R4f,fP3R4f,fP4R6f

! TODO
   real(fp8) :: iNP1f,iNP2f,iNP3f,iNP4f

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise parameters
!
! !DESCRIPTION:
! Initialises general phytoplankton parameters from namelist
! \texttt{primary\_producer\_parameters}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_primary_producer_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU
!
! !LOCAL VARIABLES:
   namelist/primary_producer_parameters/peir_eowX,qnRPIcX,qpRPIcX,&
      alphaP1X,alphaP2X,alphaP3X,alphaP4X,betaP1X,betaP2X,betaP3X,&
      betaP4X,phimP1X,phimP2X,phimP3X,phimP4X,phiP1HX,phiP2HX,phiP3HX, &
      phiP4HX,sumP1X,q10P1X,srsP1X,pu_eaP1X,pu_raP1X,&
      chP1sX,qnlP1cX,qplP1cX,xqcP1pX,xqcP1nX,xqnP1X,xqpP1X,quP1n3X,&
      quP1n4X,qurP1pX,qsP1cX,esNIP1X,resP1mX,sdoP1X,sumP2X,q10P2X,&
      srsP2X,pu_eaP2X,pu_raP2X,qnlP2cX,qplP2cX,xqcP2pX,xqcP2nX,&
      xqnP2X,xqpP2X,quP2n3X,quP2n4X,qurP2pX,esNIP2X,resP2mX,sdoP2X,&
      sumP3X,q10P3X,srsP3X,pu_eaP3X,pu_raP3X,qnlP3cX,qplP3cX,&
      xqcP3pX,xqcP3nX,xqnP3X,xqpP3X,quP3n3X,quP3n4X,qurP3pX,&
      esNIP3X,resP3mX,sdoP3X,sumP4X,q10P4X,srsP4X,pu_eaP4X,&
      pu_raP4X,qnlP4cX,qplP4cX,xqcP4pX,xqcP4nX,xqnP4X,xqpP4X,&
      quP4n3X,quP4n4X,qurP4pX,esNIP4X,resP4mX,seoP4X,sdoP4X,P4resX,&
      qflP1cX,qflP2cX,qflP3cX,qflP4cX,&
      qfRP1cX,qfRP2cX,qfRP3cX,qfRP4cX,qurP1fX,qurP2fX,qurP3fX,qurP4fX
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading primary producer parameters'
   open(99,file='BioParams/primary_producers.nml',status='old')
   read(99,nml=primary_producer_parameters)

   close(99)

   return

   end subroutine set_primary_producer_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Nutrient limitation \label{sec:nutrientlimitation}
!
! !DESCRIPTION:
! Nutrient limitation factors for each of the four phytoplankton types
! are computed. The limitation for nitrate and phosphate is computed
! as:
!
! \begin{eqnarray*}
! \overset{x}{l}_{\textit{phos}} & = & \textit{min}\left(1,\textit{max}\left(0,\frac{\overset{x}{q}_{\textit{p:c}}-\overset{x}{q}_{\textit{pmin}}}{\overset{x}{q}_{\textit{pref}}-\overset{x}{q}_{\textit{pmin}}}\right)\right)\! \overset{x}{l}_{\textit{nitr}} & = & \textit{min}\left(1,\textit{max}\left(0,\frac{\overset{x}{q}_{\textit{n:c}}-\overset{x}{q}_{\textit{nmin}}}{\overset{x}{q}_{\textit{nref}}-\overset{x}{q}_{\textit{nmin}}}\right)\right)\,,

! \end{eqnarray*}
!
! where $x$ represents any phytoplankton type (dia, micro, nano, pico), $\overset{x}{q}_{\textit{nref}}$
! is its reference internal quota and $\overset{x}{q}_{\textit{nmin}}$ is
! its minimal internal quoata.
!
! Four types of combination are availalbe for the nitrogen and phosphorus
! co-limitation $\overset{x}{l}_{np}$, switchable through the namelist
! switch \texttt{LimnutX}:
!
! \begin{description}
! \item [{\texttt{LimnutX}$=0$:}] $\overset{x}{l}_{\textit{np}}$ is the
! geometric mean of $\overset{x}{l}_{\textit{phos}}$ and $\overset{x}{l}_{\textit{nitr}}$,
! \item [{\texttt{LimnutX}$=2$:}] $\overset{x}{l}_{\textit{np}}$ is the
! harmonic mean of $\overset{x}{l}_{\textit{phos}}$ and $\overset{x}{l}_{\textit{nitr}}$.
! \item [{\texttt{LimnutX}$=1$:}] $\overset{x}{l}_{\textit{np}}$ is the
! minimum of $\overset{x}{l}_{\textit{phos}}$ and $\overset{x}{l}_{\textit{nitr}}$
! \end{description}
!
! The silicate limitation factor is computed from the external availability
! of dissolved silicate:
!
! \begin{equation}
! \overset{x}{l}_{\textit{sil}}=\textit{min}\left(1,\frac{N_{\textit{sil}}}{N_{\textit{sil}}-\overset{s}{h}_{\textit{x}}}\right)
! \end{equation}
!
! For runs with the iron cycle incorporated (preprocessing option \texttt{1}),
! the limitation factor is computed in the same way as for nitrate and
! phosphate:
!
! \begin{equation}
! \overset{x}{l}_{\textit{fe}}=\textit{min}\left(1,\textit{max}\left(0,\frac{\overset{x}{q}_{\textit{f:c}}-\overset{x}{q}_{\textit{fmin}}}{\overset{x}{q}_{\textit{fref}}-\overset{x}{q}_{\textit{fmin}}}\right)\right)
! \end{equation}
!
! If calcification is activated (preprocessing option \texttt{1}),
! the rain ration is modified according to temperature, phosphate and
! nitrate limitation state of nanophytoplankton:
!
! \begin{equation}
! q_{\textit{rain}}=\textit{min}\left(0.005,q_{\textit{rain0}}*\textit{min}\left(\frac{1-\overset{\textit{np}}{l}_{\textit{phos}}}{\overset{\textit{np}}{l}_{\textit{nitr}}}\right)*\frac{\textit{max}\left(1,T\right)}{2+\textit{max}\left(1,T\right)}\right)
! \end{equation}
!
! !INTERFACE:
   subroutine nutrient_limitation(I)
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i
!
! !LOCAL VARIABLES:
   real(fp8) :: iNP1p,iNP3p,iNP4p, iNP1n,iNP3n,iNP4n
   real(fp8) :: iNP2p,iNP2n,t
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Phosphorus internal limitation
   iNP1p = MIN(1._fp8, &
           MAX(0._fp8, (qpP1c(I)-qplP1cX) / (xqcP1pX*qpRPIcX-qplP1cX) ))
   iNP2p = MIN(1._fp8, &
           MAX(0._fp8, (qpP2c(I)-qplP2cX) / (xqcP2pX*qpRPIcX-qplP2cX) ))
   iNP3p = MIN(1._fp8, &
           MAX(0._fp8, (qpP3c(I)-qplP3cX) / (xqcP3pX*qpRPIcX-qplP3cX) ))
   iNP4p = MIN(1._fp8, &
           MAX(0._fp8, (qpP4c(I)-qplP4cX) / (xqcP4pX*qpRPIcX-qplP4cX) ))

   ! Nitrogen internal limitation
   iNP1n = MIN(1._fp8, &
           MAX(0._fp8, (qnP1c(I)-qnlP1cX) / (xqcP1nX*qnRPIcX-qnlP1cX) ))
   iNP2n = MIN(1._fp8, &
           MAX(0._fp8, (qnP2c(I)-qnlP2cX) / (xqcP2nX*qnRPIcX-qnlP2cX) ))
   iNP3n = MIN(1._fp8, &
           MAX(0._fp8, (qnP3c(I)-qnlP3cX) / (xqcP3nX*qnRPIcX-qnlP3cX) ))
   iNP4n = MIN(1._fp8, &
           MAX(0._fp8, (qnP4c(I)-qnlP4cX) / (xqcP4nX*qnRPIcX-qnlP4cX) ))


   ! Iron internal limitation (Luca)
   iNP1f = MIN(1._fp8, &
      & MAX(ZeroX, (qfP1c(I)-qflP1cX) / (qfRP1cX-qflP1cX) ))
   iNP2f = MIN(1._fp8, &
      & MAX(ZeroX, (qfP2c(I)-qflP2cX) / (qfRP2cX-qflP2cX) ))
   iNP3f = MIN(1._fp8, &
      & MAX(ZeroX, (qfP3c(I)-qflP3cX) / (qfRP3cX-qflP3cX) ))
   iNP4f = MIN(1._fp8, &
      & MAX(ZeroX, (qfP4c(I)-qflP4cX) / (qfRP4cX-qflP4cX) ))



   ! Silicate external limitation
   iNP1s = MIN(1._fp8, N5s(I)/(N5s(I)+chP1sX))

! Combination of nutrient limitations:
! LimnutX = 0 multiplicative limitation is used
! LimnutX = 1 liebig ( most limiting ) is used
! LimnutX = 2
   if ( LimnutX .eq. 0 ) then
      iNIP4 = (iNP4p * iNP4n)**.5_fp8
      iNIP3 = (iNP3p * iNP3n)**.5d0
      iNIP2 = (iNP2p * iNP2n)**.5d0
      iNIP1 = (iNP1p * iNP1n)**.5d0
   else if ( LimnutX .eq. 1 ) then
      iNIP4 = MIN(iNP4p, iNP4n)
      iNIP3 = MIN(iNP3p, iNP3n)
      iNIP2 = MIN(iNP2p, iNP2n)
      iNIP1 = MIN(iNP1p, iNP1n)
   else if ( LimnutX .eq. 2 ) then
      iNIP4 = 2.0d0 / (1._fp8/iNP4p + 1._fp8/iNP4n)
      iNIP3 = 2.0d0 / (1._fp8/iNP3p + 1._fp8/iNP3n)
      iNIP2 = 2.0d0 / (1._fp8/iNP2p + 1._fp8/iNP2n)
      iNIP1 = 2.0d0 / (1._fp8/iNP1p + 1._fp8/iNP1n)
   end if


   ! Calculate nutrient limitation impact on rain ratio:
   t=max(0._fp8,ETW(I)) ! this is to avoid funny values of rain ratio when ETW ~ -2 degrees
   RainR(I) = RainR(I) * min((1._fp8-inP2p),inP2n) * (t/(2._fp8+t)) !* max(1.,P2c(I)/2.) removd as P2 is a broad class not just calicifiers
   RainR(i)= max(RainR(I),0.005_fp8)


   return

   end subroutine nutrient_limitation
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Diatoms dynamics \label{sec:diatomdynamics}
!
! !DESCRIPTION:
! Specific gross primary production is computed from specific maximum
! growth $\overset{\textit{dia}}{g}_{\textit{max}}$ regulated by
! temperature and limited by the nutritional state and the light
! conditions:
!
! \begin{equation}
! \mathcal{\overset{\textit{dia}}{S}}_{\textit{gpp}}=\overset{\textit{dia}}{g}_{\textit{max}}\overset{\textit{dia}}{l}_{T}\overset{\textit{dia}}{l}_{\textit{sil}}\left(1-\textit{exp}\left(-\frac{\overset{\textit{dia}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{dia}}{q}_{\textit{chl:c}}}{\overset{\textit{dia}}{g}_{\textit{max}}\overset{\textit{dia}}{l}_{T}\overset{\textit{dia}}{l}_{\textit{sil}}}\right)\right)\textit{exp}\left(-\frac{\overset{\textit{dia}}{\beta}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{dia}}{q}_{\textit{chl:c}}}{\overset{\textit{dia}}{g}_{\textit{max}}\overset{\textit{dia}}{l}_{T}\overset{\textit{dia}}{l}_{\textit{sil}}}\right)\,,
! \end{equation}
!
! where the temperature response factor $\overset{\textit{dia}}{l}_{T}$
! is given by the equation
!
! \begin{equation}
! \overset{\textit{dia}}{l}_{T}=\left.\overset{\textit{dia}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-10}{10}}-\left.\overset{\textit{dia}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-32}{3}}.
! \end{equation}
!
! The light acclimation model used here regulates the internal
! chlorophyll-a to carbon ratio in relation to the given light
! condition. It is based on the formulation by
! \citet{Geider1997} modified for photoinhibition according to
! \citet{Blackford2004}.
! The photosynthetically available radiation $I_{\textit{PAR}}$ is
! estimated as a fixed fraction $p_{\textit{PAR}}$ of short-wave radiation
! $I_{\textit{SWR}}$:
!
! \begin{equation}
! I_{\textit{PAR}}=p_{\textit{PAR}}I_{\textit{SWR}}\,.
! \end{equation}
!
! Chlorophyll-a is synthesised at a ratio
!
! \begin{equation}
! \rho_{\textit{dia}}=\left(\textit{\ensuremath{\phi}}_{\textit{dia}}-q_{\textit{chlcmin}}\right)\frac{\mathcal{\overset{\textit{dia}}{S}}_{\textit{gpp}}}{\overset{\textit{dia}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{dia}}{q}_{\textit{chl:c}}}+q_{\textit{chlcmin}}\,,
! \end{equation}
!
!
! where
!
! \begin{equation}
! \phi_{\textit{dia}}=\overset{\textit{dia}}{q}_{\phi\textit{min}}+\frac{\overset{\textit{dia}}{q}_{\textit{chl:c}}}{\overset{\textit{dia}}{q}_{\phi\textit{max}}}\left(\overset{\textit{dia}}{q}_{\phi\textit{max}}-\overset{\textit{dia}}{q}_{\phi\textit{min}}\right)\,.
! \end{equation}
!
! Gross primary production in carbon and chlorophyll-a is then given
! by:
!
! \begin{eqnarray*}
! \left.\frac{\partial\overset{\textit{c}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{gpp}} & = & \mathcal{\overset{\textit{dia}}{S}}_{\textit{gpp}}\overset{\textit{c}}{P}_{\textit{dia}}\! \left.\frac{\partial\overset{\mathcal{\mathscr{C}}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{gpp}} & = & \rho_{\textit{dia}}\overset{\textit{c}}{P}_{\textit{dia}}

! \end{eqnarray*}
!
! Photosynthesis is subject to losses by excretion separated in a fixed
! part and a variable part depending on the nutrient limitation status:
!
! \begin{equation}
! \mathcal{\overset{\textit{dia}}{S}}_{\textit{growth}}=\mathcal{\overset{\textit{dia}}{S}}_{\textit{gpp}}-\left(\left(1-\overset{\textit{dia}}{l}_{\textit{np}}\right)\left(1-\overset{\textit{dia}}{p}_{\textit{excr}}\right)+\overset{\textit{dia}}{p}_{\textit{excr}}\right)\,,
! \end{equation}
!
! \begin{equation}
! \left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{excr}}=\left(\left(1-\overset{\textit{dia}}{l}_{\textit{np}}\right)\left(1-\overset{\textit{dia}}{p}_{\textit{excr}}\right)+\overset{\textit{dia}}{p}_{\textit{excr}}\right)\left.\frac{\partial\overset{\textit{c,\ensuremath{\mathscr{C}}}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{gpp}}\,.
! \end{equation}


! Diatom respiration is computed according to

! \begin{equation}
! \left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{resp}}=\overset{\textit{dia}}{r}_{\textit{aresp}}\left(\left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{growth}}-\left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{excr}}\right)+\overset{\textit{dia}}{r}_{\textit{resp}}\overset{\textit{dia}}{l}_{T}\overset{\textit{\textit{c,}}\mathscr{C}}{P'}_{\textit{dia}}\,,
! \end{equation}
!
! where $\overset{\textit{dia}}{r}_{\textit{resp}}$ is the specific
! basal respiration rate at rest and $\overset{\textit{dia}}{r}_{\textit{aresp}}$
! is the fraction of non-excreted gross primary production respired
! in activity.
!
! Lysis of diatoms is determined as a fixed fraction of the current
! biomass
!
! \begin{eqnarray*}
! \mathcal{\overset{\textit{dia}}{S}}_{\textit{lys}} & = & \frac{1}{\textit{min}\left(\overset{\textit{dia}}{l}_{\textit{np}},\overset{\textit{dia}}{l}_{\textit{sil}}\right)}\overset{\textit{dia}}{r}_{\textit{lys}}\,.\! \left.\frac{\partial\overset{\textit{c,s,}\mathscr{C}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{lys}} & = & \mathcal{\overset{\textit{dia}}{S}}_{\textit{lys}}\overset{\textit{c,s,}\mathscr{C}}{P'}_{\textit{dia}}\,.

! \end{eqnarray*}
!
! The carbon fluxes from phytoplankton to organic matter are then divided
! as:
!
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{dia}}}^{\overset{c}{M}_{\textit{med}}} & = & \overset{\textit{dia}}{p}_{\textit{part}}\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{lys}}\,,\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{dia}}}^{\overset{c}{M}_{\textit{dis}}} & = & \overset{\textit{dia}}{p}_{\textit{lab}}\left(\left(1-\overset{\textit{dia}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{lys}}+\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{excr}}\right)\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{dia}}}^{\overset{c}{M}_{\textit{slab}}} & = & \left(1-\overset{\textit{dia}}{p}_{\textit{lab}}\right)\left(\left(1-\overset{\textit{dia}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{lys}}+\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{excr}}\right)


! \end{eqnarray*}


! while the nitrogen and phosphate fluxes to organic
! matter is given by the lysis term directed at minimum internal quota
! to particulate matter and the remainder excreted in dissolved form:

! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{p}{P}_{\textit{dia}}}^{\overset{p}{M}_{\textit{med}}} & = & \mathcal{\overset{\textit{dia}}{S}}_{\textit{lys}}\textit{min}\left(\overset{\textit{p}}{P'}_{\textit{dia}},\overset{\textit{dia}}{q}_{\textit{pmin}}\overset{c}{P'}_{\textit{dia}},\right)\,,\! \left.\mathcal{F}\right|_{\overset{c}{p}_{\textit{dia}}}^{\overset{p}{M}_{\textit{dis}}} & = & \mathcal{\overset{\textit{dia}}{S}}_{\textit{lys}}\overset{\textit{p}}{P'}_{\textit{dia}},-\left.\mathcal{F}\right|_{\overset{p}{P}_{\textit{dia}}}^{\overset{p}{M}_{\textit{med}}}\,.\!  \left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{dia}}}^{\overset{n}{M}_{\textit{med}}} & = & \mathcal{\overset{\textit{dia}}{S}}_{\textit{lys}}\textit{min}\left(\overset{\textit{n}}{P'}_{\textit{dia}},\overset{\textit{dia}}{q}_{\textit{nmin}}\overset{c}{P'}_{\textit{dia}},\right)\,,\!  \left.\mathcal{F}\right|_{\overset{n}{p}_{\textit{dia}}}^{\overset{n}{M}_{\textit{dis}}} & = & \mathcal{\overset{\textit{dia}}{S}}_{\textit{lys}}\overset{\textit{n}}{P'}_{\textit{dia}},-\left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{dia}}}^{\overset{n}{M}_{\textit{med}}}\,.



! \end{eqnarray*}

! Silicate lysis entirely results in particulate matter:

! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{s}{P}_{\textit{dia}}}^{\overset{s}{M}_{\textit{med}}}=\left.\frac{\partial\overset{\textit{s}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{lys}}\,,
! \end{equation}
! while its internal regulation releases silicate directly in dissolved
! inorganic form, that combines with the uptake to

! \begin{equation}
! \left.\mathcal{F}\right|_{N_{\textit{sil}}}^{\overset{s}{P}_{\textit{dia}}}=\textit{max}\left(0,\overset{\textit{dia}}{q}_{sref}\mathcal{\overset{\textit{dia}}{S}}_{\textit{growth}}\right)-\textit{max}\left(0,\overset{\textit{s}}{P'}_{\textit{dia}}-\overset{\textit{dia}}{q}_{sref}\overset{\textit{c}}{P'}_{\textit{dia}}\right)\,.
! \end{equation}
!
! Nitrate and phosphate can be assimilated in surplus of the optimum
! quota for growth to build up a limited internal storage.
! Consequently, uptake of nitrate and phosphate is subject to
! availibility and capped such that the maximum internal quota can not
! be exceeded:
!
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{N_{\textit{phos}}}^{\overset{p}{P}_{\textit{dia}}} & = & \textit{min}\left(\overset{\textit{dia}}{r}_{\textit{affp}}N_{\textit{phos}}\overset{c}{P}_{\textit{dia}},\right.\! & & \overset{p}{q}_{\textit{pmax}}\overset{c}{P'}_{\textit{dia}}-\overset{p}{P'}_{\textit{dia}}\! & & \left.+\mathcal{\overset{\textit{dia}}{S}}_{\textit{growth}}\overset{p}{q}_{\textit{pmax}}\overset{c}{P}_{\textit{dia}}-\overset{\textit{dia}}{r}_{\textit{resp}}\overset{p}{P'}_{\textit{dia}}\right)\,,\!  \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{dia}}} & = & \textit{min}\left(\left(\overset{\textit{dia}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{dia}}{r}_{\textit{affa}}N_{\textit{amm}}\right)\overset{c}{P}_{\textit{dia}},\right.\!   &  & \overset{n}{q}_{\textit{pmax}}\overset{c}{P'}_{\textit{dia}}-\overset{n}{P'}_{\textit{dia}}\!   &  & \left.+\mathcal{\overset{\textit{dia}}{S}}_{\textit{growth}}\overset{n}{q}_{\textit{pmax}}\overset{c}{P}_{\textit{dia}}-\overset{\textit{dia}}{r}_{\textit{resp}}\overset{n}{P'}_{\textit{dia}}\right)\,,\! \left.\mathcal{F}\right|_{N_{\textit{nitr}}}^{\overset{n}{P}_{\textit{dia}}} & = & \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{dia}}}\frac{\overset{\textit{dia}}{r}_{\textit{affn}}N_{\textit{nitr}}}{\overset{\textit{dia}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{dia}}{r}_{\textit{affa}}N_{\textit{amm}}}\,,\! \left.\mathcal{F}\right|_{N_{\textit{amm}}}^{\overset{n}{P}_{\textit{dia}}} & = & \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{dia}}}\frac{\overset{\textit{dia}}{r}_{\textit{affa}}N_{\textit{amm}}}{\overset{\textit{dia}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{dia}}{r}_{\textit{affa}}N_{\textit{amm}}}\,.







! \end{eqnarray*}
!
! The respiration fluxes towards inorganic gasses are given as
!
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{dia}}}^{G_{\textit{\textit{CO}}_{2}}} & = & \frac{1}{\mathcal{\mathbb{C}}}\left(\left.\frac{\partial\overset{c}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{resp}}-\left.\frac{\partial\overset{\textit{c}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{gpp}}\right)\! \left.\mathcal{F}\right|_{G_{O_{2}}}^{\textit{sink}} & = & \overset{B}{p}_{O_{2}}\left.\frac{\partial\overset{c}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{resp}}\,-\overset{B}{p}_{phO_{2}}\left.\frac{\partial\overset{c}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{gpp}}.

! \end{eqnarray*}


! If the dynamic bacteria model is active (activated through the preprocessing
! option \texttt{1}), excretion is entirely directed to semi-labile
! organic matter and the dissolved part of lysis to the labile dissolved
! organic matter:

! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{dia}}}^{\overset{c}{M}_{\textit{dis}}} & = & \left(1-\overset{\textit{dia}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{lys}}\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{dia}}}^{\overset{c}{M}_{\textit{slab}}} & = & \left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{dia}}}{\partial t}\right|_{\textit{excr}}

! \end{eqnarray*}


! If iron is used (activated through the preprocessing option \texttt{1}),
! gross primary production is additionally limited by the iron limitation
! factor $\overset{\textit{dia}}{l}_{\textit{fe}}$:

! \begin{equation}
! \mathcal{\overset{\textit{dia}}{S}}_{\textit{gpp}}=\overset{\textit{dia}}{g}_{\textit{max}}\overset{\textit{dia}}{l}_{T}\overset{\textit{dia}}{l}_{\textit{sil}}\overset{\textit{dia}}{l}_{\textit{fe}}\left(1-\textit{exp}\left(-\frac{\overset{\textit{dia}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{dia}}{q}_{\textit{chl:c}}}{\overset{\textit{dia}}{g}_{\textit{max}}\overset{\textit{dia}}{l}_{T}\overset{\textit{dia}}{l}_{\textit{sil}}\overset{\textit{dia}}{l}_{\textit{fe}}}\right)\right)\textit{exp}\left(-\frac{\overset{\textit{dia}}{\beta}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{dia}}{q}_{\textit{chl:c}}}{\overset{\textit{dia}}{g}_{\textit{max}}\overset{\textit{dia}}{l}_{T}\overset{\textit{dia}}{l}_{\textit{sil}}\overset{\textit{dia}}{l}_{\textit{fe}}}\right)\,.
! \end{equation}
!
! Iron uptake is regulated analog to phosphate and nitrate uptake, but
! without luxuary buffer
!
! \begin{equation}
! \left.\mathcal{F}\right|_{N_{\textit{fe}}}^{\overset{f}{P}_{\textit{dia}}}=\textit{min}\left(\overset{\textit{dia}}{r}_{\textit{affp}}N_{\textit{fe}}\overset{f}{P}_{\textit{dia}},\overset{\textit{dia}}{q}_{\textit{fmax}}\overset{c}{P'}_{\textit{dia}}-\overset{f}{P'}_{\textit{dia}}+\mathcal{\overset{\textit{dia}}{S}}_{\textit{growth}}\overset{\textit{dia}}{q}_{\textit{fmax}}\overset{c}{P}_{\textit{dia}}-\overset{\textit{dia}}{r}_{\textit{resp}}\overset{f}{P'}_{\textit{dia}}\right)\,,
! \end{equation}
!
! while lysis is entirely directed to particuale iron:
!
! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{dia}}}^{\overset{f}{M}_{\textit{med}}}=\mathcal{\overset{\textit{dia}}{S}}_{\textit{lys}}\overset{\textit{f}}{P'}_{\textit{dia}}\,.
! \end{equation}
!
! If the \texttt{CENH} preprocessing option is used in order to test
! the impact of acidification on primary production, gross and net primary
! production of carbon are enhanced by a factor $p_{\textit{cenh}}$
! depending on atmospheric partial pressure of carbon dioxide $\mathscr{\textrm{p}}_{\textit{CO}_{2}}$
! defined as:
!
! \begin{equation}
! p_{\textit{cenh}}=1.+\mathscr{\textrm{p}}_{\textit{CO}_{2}}-379.48*0.0005\,.
! \end{equation}\\[\baselineskip]
!
! !INTERFACE:
   subroutine diatom_dynamics(I)
!
! !USES:
   use ersem_constants, only: CMass
!
! !INPUT PARAMETERS:
   integer, intent(in) :: i
!
! !LOCAL VARIABLES:
   real(fp8) :: sdoP1,sumP1,sugP1,seoP1,seaP1
   real(fp8) :: sraP1,sunP1,runP1,rugP1,sP1R6
   real(fp8) :: runP1p,misP1p,rumP1p
   real(fp8) :: runP1n,misP1n,rumP1n,rumP1n3,rumP1n4
   real(fp8) :: etP1,SDP1,pe_R6P1
   real(fp8) :: rho,Chl_inc,Chl_loss
   real(fp8) :: phi,ChlCpp

   real(fp8) :: runP1f,rumP1f,misP1f


   real(fp8) :: fP1R1c,fP1R2c

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Regulation factors...................................................

   ! Temperature response
   etP1 = q10P1X**((ETW(I)-10._fp8)/10._fp8) - q10P1X**((ETW(I)-32._fp8)/3._fp8)

   ! Production...........................................................

   ! Calculate chl to C ratio.............................................

   !ChlCpp = max(min(phimP1X,chl1(I)/(p1c(I)-chl1(I)+zeroX)),ChlCmin)
   ChlCpp = chl1(I)/p1c(I)

   ! Gross photosynthetic activity :

   sumP1 = sumP1X*etP1*iNP1s*iNP1f




   phi = phiP1HX + (ChlCpp/phimP1X)*(phimP1X-phiP1HX)

   parEIR = pEIR_eowX*EIR(I)

   if (parEIR.gt.zeroX) then
      sumP1 = sumP1 * (1._fp8-exp(-alphaP1X*parEIR*ChlCpp/sumP1)) * EXP(-betaP1X*parEIR*ChlCpp/sumP1)
      rho = (phi - ChlCmin) * (sumP1/(alphaP1X*parEIR*ChlCpp)) + ChlCmin
   else
      sumP1 = 0._fp8
      rho = ChlCmin
   end if





   ! Nutrient-stress lysis rate :
   sdoP1 = (1._fp8/(MIN( iNP1s, iNIP1 )+0.1_fp8))*sdoP1X

   ! Excretion rate, as regulated by nutrient-stress
   seoP1 = sumP1*(1._fp8-iNIP1)*(1._fp8-pu_eaP1X)

   ! Activity-dependent excretion :
   seaP1 = sumP1*pu_eaP1X
   sugP1 = sumP1-seoP1-seaP1

   ! Apportioning of LOC- and DET- fraction of excretion/lysis fluxes:
   pe_R6P1 = MIN(qplP1cX/(qpP1c(I)+ZeroX),qnlP1cX/(qnP1c(I)+ZeroX))

   sP1R6 = pe_R6P1*sdoP1
   fP1R6c(I) = sP1R6*P1cP(I)


   fP1R1c= (1._fp8-pe_R6P1)*sdoP1*P1cP(I)
   fP1R2c= (seoP1 + seaP1)*P1c(I)
   fP1RDc(I) = fP1R1c+fP1R2c




   ! Respiration..........................................................

   ! Rest respiration rate :
   srsP1 = etP1*srsP1X

   ! Activity respiration rate :
   sraP1 = sugP1*pu_raP1X

   ! Total respiration flux :
   fP1O3c(I) = srsP1*P1cP(I)+sraP1*P1c(I)

   ! Gross production as flux from inorganic CO2 to P1 :
   fO3P1c(I) = sumP1*P1c(I)




   rugP1 = fO3P1c(I)

   ! Production and productivity
   sunP1 = sumP1-(seoP1+seaP1+sraP1) ! net productivity
   runP1 = sunP1*P1c(I)-srsP1*P1cP(I) ! net production

   ! To save net production

   netP1(I) = runP1




   ! Carbon Source equations

   rho = MIN(rho,0.1_fp8)

   ! Chl changes (note that Chl is a component of PXc and not involved
   ! in mass balance)

   Chl_inc = min(iNP1f,iNIP1)*rho*(sumP1-sraP1)*P1c(I)



   Chl_loss = (sdoP1+srsP1)*Chl1P(I) + (seoP1+seaP1)*Chl1(I)

   SP1c(I) = SP1c(I)+fO3P1c(I)-fP1O3c(I)-fP1R6c(I)-fP1RDc(I)

   SR1c(I) = SR1c(I)+fP1R1c
   SR2c(I) = SR2c(I)+fP1R2c




   SR6c(I) = SR6c(I)+fP1R6c(I)
   SChl1(I) = Schl1(I) + Chl_inc - Chl_loss

   SO3c(I) = SO3c(I) + (fP1O3c(I) - fO3P1c(I))/CMass
   SO2o(I) = SO2o(I) + fO3P1c(I)*uB1c_O2X - fP1O3c(I)*urB1_O2X

   ! Phosphorus flux through P1...........................................

   ! Lysis loss of phosphorus
   fP1R6p = sP1R6 * min(qplP1cX*P1cP(I),P1pP(I))
   fP1RDp = sdoP1 * P1pP(I) - fP1R6p

   ! Net phosphorus uptake
   rumP1p = qurP1pX * N1pP(I) * P1c(I)
   misP1p = xqpP1X * qpRPIcX*P1cP(I) - P1pP(I)
   runP1p = sunP1*P1c(I) * qpRPIcX*xqpP1X - srsP1*P1pP(I)
   fN1P1p(I) = MIN(rumP1p, runP1p+misP1p)

   ! Source equations
   SP1p(I) = SP1p(I)+fN1P1p(I)-fP1RDp-fP1R6p
   SN1p(I) = SN1p(I)-fN1P1p(I)
   SR6p(I) = SR6p(I)+fP1R6p
   SR1p(I) = SR1p(I)+fP1RDp

   ! Nitrogen flux through P1.............................................

   ! Nitrogen loss by lysis
   fP1R6n = sP1R6 * min(qnlP1cX*P1cP(I),P1nP(I))
   fP1RDn = sdoP1 * P1nP(I) - fP1R6n

   ! Net nitrogen uptake
   rumP1n3 = quP1n3X * N3nP(I) * P1c(I)
   rumP1n4 = quP1n4X * N4nP(I) * P1c(I)
   rumP1n = rumP1n3 + rumP1n4

   misP1n = xqnP1X * qnRPIcX*P1cP(I) - P1nP(I)
   runP1n = sunP1*P1c(I) * qnRPIcX*xqnP1X - srsP1*P1nP(I)
   fNIP1n = MIN(rumP1n, runP1n + misP1n)

   ! Partitioning over NH4 and NO3 uptake
   if (fNIP1n .gt. 0._fp8) then
      fN3P1n(I) = fNIP1n * rumP1n3 / rumP1n
      fN4P1n(I) = fNIP1n * rumP1n4 / rumP1n
   else
      fN3P1n(I) = 0._fp8
      fN4P1n(I) = fNIP1n
   end if

   ! Source equations
   SP1n(I) = SP1n(I)+fN4P1n(I)+fN3P1n(I)-fP1RDn-fP1R6n
   SN3n(I) = SN3n(I)-fN3P1n(I)
   SN4n(I) = SN4n(I)-fN4P1n(I)
   SR6n(I) = SR6n(I)+fP1R6n
   SR1n(I) = SR1n(I)+fP1RDn

   ! Silicate flux through P1.............................................

   ! Excretion loss of silicate
   fP1R6s = sdoP1 * P1sP(I)

   ! Loss of excess silicate (qsP1c > qsP1cX)
   fP1N5s(I) = MAX ( 0._fp8, P1sP(I)-qsP1cX * P1cP(I))

   ! Net silicate uptake
   fN5P1s = MAX ( 0._fp8, qsP1cX*runP1) - fP1N5s(I)

   fN5PXs(I) = fN5P1s


   ! Source equations
   SP1s(I) = SP1s(I) + fN5P1s - fP1R6s
   SN5s(I) = SN5s(I) - fN5P1s
   SR6s(I) = SR6s(I) + fP1R6s


   ! Iron flux through P1................................................

   ! Iron loss by lysis
   ! Because its high affinity with particles all the iron lost from phytoplankton by lysis is supposed to be
   ! associated to organic particulate detritus. (luca)

   fP1R6f = sdoP1 * P1fP(I)

   ! Net iron uptake
   rumP1f = qurP1fX * N7fP(I) * P1c(I)
   misP1f = qfRP1cX*P1cP(I) - P1fP(I)
   runP1f = sunP1*P1c(I) * qfRP1cX - srsP1*P1fP(I)
   fN7P1f(I) = MIN(rumP1f, runP1f+misP1f)

   ! Source equations
   SP1f(I) = SP1f(I)+fN7P1f(I)-fP1R6f
   SN7f(I) = SN7f(I)-fN7P1f(I)
   SR6f(I) = SR6f(I)+fP1R6f


   ! Sedimentation and resting stages.....................................
   SDP1 = resP1mX * MAX(0._fp8, (esNIP1X - iNIP1) )
   SDP1c(I) = SDP1c(I) + SDP1
   SDP1p(I) = SDP1p(I) + SDP1
   SDP1n(I) = SDP1n(I) + SDP1
   SDP1s(I) = SDP1s(I) + SDP1
   SDChl1(I) = SDChl1(I) + SDP1

   SDP1f(I) = SDP1f(I) + SDP1


   ! add dia
! if ((P1c(I) .LT. 0.2).AND. (parEIR .gt. 0.5)) then
! SP1c(I) = SP1c(I) + (0.2 - P1c(I))
! SP1n(I) = SP1n(I) + (0.2 - P1c(I))*16.0/106.0
! SP1p(I) = SP1p(I) + (0.2 - P1c(I))/106.0
! SP1s(I) = SP1s(I) + (0.2 - P1c(I))*16.0/106.0
! SChl1(I) = SChl1(I) + (0.2 - P1c(I))*ChlCpp
! end if

   return

   end subroutine diatom_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Nanophytoplankton dynamics \label{sec:nanophytdynamics}
!
! !DESCRIPTION:
! Specific gross primary production is computed from specific maximum
! growth $\overset{\textit{nano}}{g}_{\textit{max}}$ regulated by
! temperature and limited by the nutritional state and the light
! conditions:
!
! \begin{equation}
! \mathcal{\overset{\textit{nano}}{S}}_{\textit{gpp}}=\overset{\textit{nano}}{g}_{\textit{max}}\overset{\textit{nano}}{l}_{T}\left(1-\textit{exp}\left(-\frac{\overset{\textit{nano}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{nano}}{q}_{\textit{chl:c}}}{\overset{\textit{nano}}{g}_{\textit{max}}\overset{\textit{nano}}{l}_{T}}\right)\right)\textit{exp}\left(-\frac{\overset{\textit{nano}}{\beta}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{nano}}{q}_{\textit{chl:c}}}{\overset{\textit{nano}}{g}_{\textit{max}}\overset{\textit{nano}}{l}_{T}}\right)\,,
! \end{equation}
! where the temperature response factor $\overset{\textit{nano}}{l}_{T}$
! is given by the equation:
! \begin{equation}
! \overset{\textit{nano}}{l}_{T}=\left.\overset{\textit{nano}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-10}{10}}-\left.\overset{\textit{nano}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-32}{3}}.
! \end{equation}
!
! The light acclimation model used here regulates the internal
! chlorophyll-a to carbon ratio in relation to the give light
! condition. It is based on the formulation by
! \citet{Geider1997} modified for photoinhibition according to
! \citet{Blackford2004}.
! The photosynthetically available radiation $I_{\textit{PAR}}$ is
! estimated as a fixed fraction $p_{\textit{PAR}}$ of short-wave radiation
! $I_{\textit{SWR}}$:

! \begin{equation}
! I_{\textit{PAR}}=p_{\textit{PAR}}I_{\textit{SWR}}\,.
! \end{equation}


! Chlorophyll-a is synthesised at a ratio

! \begin{equation}
! \rho_{\textit{nano}}=\left(\textit{\ensuremath{\phi}}_{\textit{nano}}-q_{\textit{chlcmin}}\right)\frac{\mathcal{\overset{\textit{nano}}{S}}_{\textit{gpp}}}{\overset{\textit{nano}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{nano}}{q}_{\textit{chl:c}}}+q_{\textit{chlcmin}}\,,
! \end{equation}
!
!
! where
!
! \begin{equation}
! \phi_{\textit{nano}}=\overset{\textit{nano}}{q}_{\phi\textit{min}}+\frac{\overset{\textit{nano}}{q}_{\textit{chl:c}}}{\overset{\textit{nano}}{q}_{\phi\textit{max}}}\left(\overset{\textit{nano}}{q}_{\phi\textit{max}}-\overset{\textit{nano}}{q}_{\phi\textit{min}}\right)\,.
! \end{equation}
! Gross primary production in carbon and chlorophyll-a is then given
! by

! \begin{eqnarray*}
! \left.\frac{\partial\overset{\textit{c}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{gpp}} & = & \mathcal{\overset{\textit{nano}}{S}}_{\textit{gpp}}\overset{\textit{c}}{P}_{\textit{nano}}\! \left.\frac{\partial\overset{\mathcal{\mathscr{C}}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{gpp}} & = & \rho_{\textit{nano}}\overset{\textit{c}}{P}_{\textit{nano}}

! \end{eqnarray*}


! Photosynthesis is subject to losses by excretion separated in a fixed
! part and a variable part depending on the nutrient limitation status:

! \begin{equation}
! \mathcal{\overset{\textit{nano}}{S}}_{\textit{growth}}=\mathcal{\overset{\textit{nano}}{S}}_{\textit{gpp}}-\left(\left(1-\overset{\textit{nano}}{l}_{\textit{np}}\right)\left(1-\overset{\textit{nano}}{p}_{\textit{excr}}\right)+\overset{\textit{nano}}{p}_{\textit{excr}}\right)\,,
! \end{equation}


! \begin{equation}
! \left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{excr}}=\left(\left(1-\overset{\textit{nano}}{l}_{\textit{np}}\right)\left(1-\overset{\textit{nano}}{p}_{\textit{excr}}\right)+\overset{\textit{nano}}{p}_{\textit{excr}}\right)\left.\frac{\partial\overset{\textit{c,\ensuremath{\mathscr{C}}}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{gpp}}\,.
! \end{equation}


! Nanophytoplankton respiration is computed according to

! \begin{equation}
! \left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{resp}}=\overset{\textit{nano}}{r}_{\textit{aresp}}\left(\left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{growth}}-\left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{excr}}\right)+\overset{\textit{nano}}{r}_{\textit{resp}}\overset{\textit{nano}}{l}_{T}\overset{\textit{\textit{c,}}\mathscr{C}}{P'}_{\textit{nano}}\,,
! \end{equation}


! where $\overset{\textit{nano}}{:}_{\textit{resp}}$ is the specific
! basal respiration rate at rest and $\overset{\textit{nano}}{r}_{\textit{aresp}}$
! is the fraction of non-excreted gross primary production respired
! in activity.

! Lysis of nanophytoplankton is determined as a fixed fraction of the
! current biomass
!
! \begin{eqnarray*}
! \mathcal{\overset{\textit{nano}}{S}}_{\textit{lysis}} & = & \frac{1}{\overset{\textit{nano}}{l}_{\textit{np}}}\overset{\textit{nano}}{r}_{\textit{lysis}}\,.\! \left.\frac{\partial\overset{\textit{c,}\mathscr{C}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{lysis}} & = & \mathcal{\overset{\textit{nano}}{S}}_{\textit{lysis}}\overset{\textit{c,}\mathscr{C}}{P'}_{\textit{nano}}\,.

! \end{eqnarray*}


! The carbon fluxes from nanophytoplankton to organic matter are then
! divided as:

! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{nano}}}^{\overset{c}{M}_{\textit{small}}} & = & \overset{\textit{nano}}{p}_{\textit{part}}\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{lysis}}\,,\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{nano}}}^{\overset{c}{M}_{\textit{dis}}} & = & \overset{\textit{nano}}{p}_{\textit{lab}}\left(\left(1-\overset{\textit{nano}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{lysis}}+\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{excr}}\right)\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{nano}}}^{\overset{c}{M}_{\textit{slab}}} & = & \left(1-\overset{\textit{nano}}{p}_{\textit{lab}}\right)\left(\left(1-\overset{\textit{nano}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{lysis}}+\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{excr}}\right)


! \end{eqnarray*}


! while the nitrogen and phosphate fluxes to organic
! matter is given by the lysis term directed at minimum internal quota
! to particulate matter and the remainder excreted in dissolved form:

! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{p}{P}_{\textit{nano}}}^{\overset{p}{M}_{\textit{small}}} & = & \mathcal{\overset{\textit{nano}}{S}}_{\textit{lysis}}\textit{min}\left(\overset{\textit{p}}{P'}_{\textit{nano}},\overset{\textit{nano}}{q}_{\textit{pmin}}\overset{c}{P'}_{\textit{nano}}\right)\,,\! \left.\mathcal{F}\right|_{\overset{c}{p}_{\textit{nano}}}^{\overset{p}{M}_{\textit{dis}}} & = & \mathcal{\overset{\textit{nano}}{S}}_{\textit{lysis}}\overset{\textit{p}}{P'}_{\textit{nano}}-\left.\mathcal{F}\right|_{\overset{p}{P}_{\textit{nano}}}^{\overset{p}{M}_{\textit{med}}}\,.\!  \left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{nano}}}^{\overset{n}{M}_{\textit{small}}} & = & \mathcal{\overset{\textit{nano}}{S}}_{\textit{lysis}}\textit{min}\left(\overset{\textit{n}}{P'}_{\textit{nano}},\overset{\textit{nano}}{q}_{\textit{nmin}}\overset{c}{P'}_{\textit{nano}}\right)\,,\!  \left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{nano}}}^{\overset{n}{M}_{\textit{dis}}} & = & \mathcal{\overset{\textit{nano}}{S}}_{\textit{lysis}}\overset{\textit{n}}{P'}_{\textit{nano}}-\left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{nano}}}^{\overset{n}{M}_{\textit{med}}}\,.



! \end{eqnarray*}


! Nitrate and phosphate can be assimilated in surplus of the optimum
! quota for growth to build up a limited internal storage.
! Consequently, uptake of nitrate and phosphate is subject to
! availibility and capped such that the maximum internal quota can not
! be exceeded:

! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{N_{\textit{phos}}}^{\overset{p}{P}_{\textit{nano}}} & = & \textit{min}\left(\overset{\textit{nano}}{r}_{\textit{affp}}N_{\textit{phos}}\overset{c}{P}_{\textit{nano}},\right.\! & & \overset{p}{q}_{\textit{pmax}}\overset{c}{P'}_{\textit{nano}}-\overset{p}{P'}_{\textit{nano}}\! & & \left.+\mathcal{\overset{\textit{nano}}{S}}_{\textit{growth}}\overset{p}{q}_{\textit{pmax}}\overset{c}{P}_{\textit{nano}}-\overset{\textit{nano}}{r}_{\textit{resp}}\overset{p}{P'}_{\textit{nano}}\right)\,,\!  \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{nano}}} & = & \textit{min}\left(\left(\overset{\textit{nano}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{nano}}{r}_{\textit{affa}}N_{\textit{amm}}\right)\overset{c}{P}_{\textit{nano}},\right.\!   &  & \overset{n}{q}_{\textit{pmax}}\overset{c}{P'}_{\textit{nano}}-\overset{n}{P'}_{\textit{nano}}\!   &  & \left.+\mathcal{\overset{\textit{nano}}{S}}_{\textit{growth}}\overset{n}{q}_{\textit{pmax}}\overset{c}{P}_{\textit{nano}}-\overset{\textit{nano}}{r}_{\textit{resp}}\overset{n}{P'}_{\textit{nano}}\right)\,,\! \left.\mathcal{F}\right|_{N_{\textit{nitr}}}^{\overset{n}{P}_{\textit{nano}}} & = & \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{nano}}}\frac{\overset{\textit{nano}}{r}_{\textit{affn}}N_{\textit{nitr}}}{\overset{\textit{nano}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{nano}}{r}_{\textit{affa}}N_{\textit{amm}}}\,,\! \left.\mathcal{F}\right|_{N_{\textit{amm}}}^{\overset{n}{P}_{\textit{nano}}} & = & \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{nano}}}\frac{\overset{\textit{nano}}{r}_{\textit{affa}}N_{\textit{amm}}}{\overset{\textit{nano}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{nano}}{r}_{\textit{affa}}N_{\textit{amm}}}\,.







! \end{eqnarray*}


! The respiration fluxes towards inorganic gasses are given as

! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{nano}}}^{G_{\textit{\textit{CO}}_{2}}} & = & \frac{1}{\mathcal{\mathbb{C}}}\left(\left.\frac{\partial\overset{c}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{resp}}-\left.\frac{\partial\overset{\textit{c}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{gpp}}\right)\! \left.\mathcal{F}\right|_{G_{O_{2}}}^{\textit{sink}} & = & \overset{B}{p}_{O_{2}}\left.\frac{\partial\overset{c}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{resp}}\,-\overset{B}{p}_{phO_{2}}\left.\frac{\partial\overset{c}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{gpp}}.

! \end{eqnarray*}


! If the dynamic bacteria model is active (activated through the preprocessing
! option \texttt{1}), excretion is entirely directed to semi-labile
! organic matter and the dissolved part of lysis to the labile dissolved
! organic matter:

! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{nano}}}^{\overset{c}{M}_{\textit{dis}}} & = & \left(1-\overset{\textit{nano}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{lysis}}\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{nano}}}^{\overset{c}{M}_{\textit{slab}}} & = & \left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{nano}}}{\partial t}\right|_{\textit{excr}}

! \end{eqnarray*}


! If iron is used (activated through the preprocessing option \texttt{1}),
! gross primary production is additionally limited by the iron limitation
! factor $\overset{\textit{nano}}{l}_{\textit{fe}}$:

! \begin{equation}
! \mathcal{\overset{\textit{nano}}{S}}_{\textit{gpp}}=\overset{\textit{nano}}{g}_{\textit{max}}\overset{\textit{nano}}{l}_{T}\overset{\textit{nano}}{l}_{\textit{fe}}\left(1-\textit{exp}\left(-\frac{\overset{\textit{nano}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{nano}}{q}_{\textit{chl:c}}}{\overset{\textit{nano}}{g}_{\textit{max}}\overset{\textit{nano}}{l}_{T}\overset{\textit{nano}}{l}_{\textit{fe}}}\right)\right)\textit{exp}\left(-\frac{\overset{\textit{nano}}{\beta}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{nano}}{q}_{\textit{chl:c}}}{\overset{\textit{nano}}{g}_{\textit{max}}\overset{\textit{nano}}{l}_{T}\overset{\textit{nano}}{l}_{\textit{fe}}}\right)\,.
! \end{equation}


! Iron uptake is regulated analog to phosphate and nitrate uptake, but
! without luxuary buffer

! \begin{equation}
! \left.\mathcal{F}\right|_{N_{\textit{fe}}}^{\overset{f}{P}_{\textit{nano}}}=\textit{min}\left(\overset{\textit{nano}}{r}_{\textit{affp}}N_{\textit{fe}}\overset{f}{P}_{\textit{nano}},\overset{\textit{nano}}{q}_{\textit{fmax}}\overset{c}{P'}_{\textit{nano}}-\overset{f}{P'}_{\textit{nano}}+\mathcal{\overset{\textit{nano}}{S}}_{\textit{growth}}\overset{\textit{nano}}{q}_{\textit{fmax}}\overset{c}{P}_{\textit{nano}}-\overset{\textit{nano}}{r}_{\textit{resp}}\overset{f}{P'}_{\textit{nano}}\right)\,,
! \end{equation}


! while lysis is entirely directed to particuale iron:

! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{nano}}}^{\overset{f}{M}_{\textit{med}}}=\mathcal{\overset{\textit{nano}}{S}}_{\textit{lysis}}\overset{\textit{f}}{P'}_{\textit{nano}}\,.
! \end{equation}


! If the \texttt{CENH} preprocessing option is used in order to test
! the impact of acidification on primary production, gross and net primary
! production of carbon are enhanced by a factor $p_{\textit{cenh}}$
! depending on atmospheric partial pressure of carbon dioxide $\mathscr{\textrm{p}}_{\textit{CO}_{2}}$
! defined as

! \begin{equation}
! p_{\textit{cenh}}=1.+\mathscr{\textrm{p}}_{\textit{CO}_{2}}-379.48*0.0005\,.
! \end{equation}
!
! !INTERFACE:
   subroutine nanophytoplankton_dynamics(I)
!
! !USES:
   use ersem_constants, only: CMass
!
! !INPUT PARAMETERS:
   integer :: i
!
! !LOCAL VARIABLES:
   real(fp8) :: sdoP2,sumP2,sugP2,seoP2,seaP2
   real(fp8) :: sraP2,sunP2,runP2,rugP2,sP2R4
   real(fp8) :: runP2p,misP2p,rumP2p
   real(fp8) :: runP2n,misP2n,rumP2n,rumP2n3,rumP2n4
   real(fp8) :: etP2,SDP2,pe_R6P2
   real(fp8) :: rho,Chl_inc,Chl_loss
   real(fp8) :: phi,ChlCpp

   real(fp8) :: runP2f,rumP2f,misP2f


   real(fp8) :: fP2R1c,fP2R2c

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Regulation factors...................................................

   ! Temperature response
   etP2 = q10P2X**((ETW(I)-10._fp8)/10._fp8) - q10P2X**((ETW(I)-32._fp8)/3._fp8)

   ! Production...........................................................

   ! calculate chl to C ratio.............................................

   !ChlCpp = max(min(phimP2X,chl2(I)/(p2c(I)-chl2(I)+zeroX)),ChlCmin)
   ChlCpp = chl2(I)/p2c(I)

   ! Gross photosynthetic activity :

   sumP2 = sumP2X*etP2*iNP2f




   phi = phiP2HX + (ChlCpp/phimP2X)*(phimP2X-phiP2HX)

   parEIR = pEIR_eowX*EIR(I)
   if (parEIR .gt. zeroX) then
      sumP2 = sumP2 * (1._fp8-exp(-alphaP2X*parEIR*ChlCpp/sump2)) * EXP(-betaP2X*parEIR*ChlCpp/sumP2)
      rho = (phi - ChlCmin) * (sumP2/(alphaP2X*parEIR*ChlCpp)) + ChlCmin
   else
      sumP2 = 0._fp8
      rho = ChlCmin
   end if






   ! Nutrient-stress lysis rate :
   sdoP2 = (1._fp8/(iNIP2+0.1_fp8))*sdoP2X

   ! Excretion rate, as regulated by nutrient-stress
   seoP2 = sumP2*(1.d0-iNIP2)*(1._fp8-pu_eaP2X)

   ! Activity-dependent excretion :
   seaP2 = sumP2*pu_eaP2X
   sugP2 = sumP2-seoP2-seaP2

   ! Apportioning of LOC- and DET- fraction of excretion/lysis fluxes:
   pe_R6P2 = MIN(qplP2cX/(qpP2c(I)+ZeroX),qnlP2cX/(qnP2c(I)+ZeroX))
   sP2R4 = pe_R6P2*sdoP2
   fP2R4c(I) = sP2R4*P2cP(I)


   fP2R1c= (1._fp8-pe_R6P2)*sdoP2*P2cP(I)
   fP2R2c= (seoP2 + seaP2)*P2c(I)
   fP2RDc(I) = fP2R1c+fP2R2c





! Calcified matter:
   fO3L2c(I) = fO3L2c(I) + fP2R4c(I)*RainR(I)


   ! Respiration..........................................................

   ! Rest respiration rate :
   srsP2 = etP2*srsP2X

   ! Activity respiration rate :
   sraP2 = sugP2*pu_raP2X

   ! Total respiration flux :
   fP2O3c(I) = srsP2*P2cP(I)+sraP2*P2c(I)

   ! Gross production as flux from inorganic CO2 to P2 :
   fO3P2c(I) = sumP2*P2c(I)





   rugP2 = fO3P2c(I)

   ! Production and productivity
   sunP2 = sumP2-(seoP2+seaP2+sraP2) ! net productivity
   runP2 = sunP2*P2c(I)-srsP2*P2cP(I) ! net production

   ! To save net production

   netP2(I) = runP2




   ! Carbon Source equations

   rho = MIN(rho,0.1_fp8)
   ! Chl changes (note that Chl is a component of PXc and not involved
   ! in mass balance)

   Chl_inc = min(iNP2f,iNIP2)*rho*(sumP2-sraP2)*P2c(I)



   Chl_loss = (sdoP2+srsP2)*Chl2P(I) + (seoP2+seaP2)*Chl2(I)

   SP2c(I) = SP2c(I)+fO3P2c(I)-fP2O3c(I)-fP2R4c(I)-fP2RDc(I)

   SR1c(I) = SR1c(I)+fP2R1c
   SR2c(I) = SR2c(I)+fP2R2c




   SR4c(I) = SR4c(I)+fP2R4c(I)
   SChl2(I) = SChl2(I) + Chl_inc - Chl_loss

   SO3c(I) = SO3c(I) + (fP2O3c(I) - fO3P2c(I))/CMass
   SO2o(I) = SO2o(I) + fO3P2c(I)*uB1c_O2X - fP2O3c(I)*urB1_O2X

   ! Phosphorus flux through P2...........................................

   ! Lysis loss of phosphorus
   fP2R4p = sP2R4 * min(qplP2cX*P2cP(I),P2pP(I))
   fP2RDp = sdoP2 * P2pP(I) - fP2R4p

   ! Net phosphorus uptake
   rumP2p = qurP2pX * N1pP(I) * P2c(I)
   misP2p = xqpP2X * qpRPIcX*P2cP(I) - P2pP(I)
   runP2p = sunP2*P2c(I) * qpRPIcX*xqpP2X - srsP2*P2pP(I)
   fN1P2p(I) = MIN(rumP2p, runP2p+misP2p)

   ! Source equations
   SP2p(I) = SP2p(I)+fN1P2p(I)-fP2RDp-fP2R4p
   SN1p(I) = SN1p(I)-fN1P2p(I)
   SR4p(I) = SR4p(I)+fP2R4p
   SR1p(I) = SR1p(I)+fP2RDp

   ! Nitrogen flux through P2.............................................

   ! Nitrogen loss by lysis
   fP2R4n = sP2R4 * min(qnlP2cX*P2cP(I),P2nP(I))
   fP2RDn = sdoP2 * P2nP(I) - fP2R4n

   ! Net nitrogen uptake
   rumP2n3 = quP2n3X * N3nP(I) * P2c(I)
   rumP2n4 = quP2n4X * N4nP(I) * P2c(I)

   rumP2n = rumP2n3 + rumP2n4
   misP2n = xqnP2X * qnRPIcX*P2cP(I) - P2nP(I)
   runP2n = sunP2*P2c(I) * qnRPIcX*xqnP2X - srsP2*P2nP(I)
   fNIP2n = MIN(rumP2n, runP2n + misP2n)

   ! Partitioning over NH4 and NO3 uptake
   if (fNIP2n .gt. 0._fp8) then
      fN3P2n(I) = fNIP2n * rumP2n3 / rumP2n
      fN4P2n(I) = fNIP2n * rumP2n4 / rumP2n
   else
      fN3P2n(I) = 0._fp8
      fN4P2n(I) = fNIP2n
   end if

   ! Source equations
   SP2n(I) = SP2n(I)+fN4P2n(I)+fN3P2n(I)-fP2RDn-fP2R4n
   SN3n(I) = SN3n(I)-fN3P2n(I)
   SN4n(I) = SN4n(I)-fN4P2n(I)
   SR4n(I) = SR4n(I)+fP2R4n
   SR1n(I) = SR1n(I)+fP2RDn


   ! Iron flux through P2................................................

   ! Iron loss by lysis
   ! Because its high affinity with particles all the iron lost from phytoplankton by lysis is supposed to be
   ! associated to organic particulate detritus. (luca)

   fP2R4f = sdoP2 * P2fP(I)

   ! net iron uptake
   rumP2f = qurP2fX * N7fP(I) * P2c(I)
   misP2f = qfRP2cX*P2cP(I) - P2fP(I)
   runP2f = sunP2*P2c(I) * qfRP2cX - srsP2*P2fP(I)
   fN7P2f(I) = MIN(rumP2f, runP2f+misP2f)

   ! Source equations
   SP2f(I) = SP2f(I)+fN7P2f(I)-fP2R4f
   SN7f(I) = SN7f(I)-fN7P2f(I)
   SR4f(I) = SR4f(I)+fP2R4f


   ! sedimentation and resting stages.....................................

   SDP2 = resP2mX * MAX(0._fp8, (esNIP2X - iNIP2) )
   SDP2c(I) = SDP2c(I) + SDP2
   SDP2p(I) = SDP2p(I) + SDP2
   SDP2n(I) = SDP2n(I) + SDP2
   SDChl2(I) = SDChl2(I) + SDP2

   SDP2f(I) = SDP2f(I) + SDP2


   return

   end subroutine nanophytoplankton_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Picophytoplankton dynamics \label{sec:picophytdynamics}
!
! !DESCRIPTION:
! Specific gross primary production is computed from specific maximum
! growth $\overset{\textit{pico}}{g}_{\textit{max}}$ regulated by
! temperature and limited by the nutritional state and the light
! conditions:
!
! \begin{equation}
! \mathcal{\overset{\textit{pico}}{S}}_{\textit{gpp}}=\overset{\textit{pico}}{g}_{\textit{max}}\overset{\textit{pico}}{l}_{T}\left(1-\textit{exp}\left(-\frac{\overset{\textit{pico}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{pico}}{q}_{\textit{chl:c}}}{\overset{\textit{pico}}{g}_{\textit{max}}\overset{\textit{pico}}{l}_{T}}\right)\right)\textit{exp}\left(-\frac{\overset{\textit{pico}}{\beta}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{pico}}{q}_{\textit{chl:c}}}{\overset{\textit{pico}}{g}_{\textit{max}}\overset{\textit{pico}}{l}_{T}}\right)\,,
! \end{equation}
! where the temperature response factor $\overset{\textit{pico}}{l}_{T}$
! is given by the equation
! \begin{equation}
! \overset{\textit{pico}}{l}_{T}=\left.\overset{\textit{pico}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-10}{10}}-\left.\overset{\textit{pico}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-32}{3}}.
! \end{equation}
!
! The light acclimation model used here regulates the internal
! chlorophyll-a to carbon ratio in relation to the give light
! condition. It is based on the formulation by
! \citet{Geider1997} modified for photoinhibition according to
! \citet{Blackford2004}.
! The photosynthetically available radiation $I_{\textit{PAR}}$ is
! estimated as a fixed fraction $p_{\textit{PAR}}$ of short-wave radiation
! $I_{\textit{SWR}}$:
! \begin{equation}
! I_{\textit{PAR}}=p_{\textit{PAR}}I_{\textit{SWR}}\,.
! \end{equation}
!
! Chlorophyll-a is synthesised at a ratio:
! \begin{equation}
! \rho_{\textit{pico}}=\left(\textit{\ensuremath{\phi}}_{\textit{pico}}-q_{\textit{chlcmin}}\right)\frac{\mathcal{\overset{\textit{pico}}{S}}_{\textit{gpp}}}{\overset{\textit{pico}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{pico}}{q}_{\textit{chl:c}}}+q_{\textit{chlcmin}}\,,
! \end{equation}
!
!
! where
!
! \begin{equation}
! \phi_{\textit{pico}}=\overset{\textit{pico}}{q}_{\phi\textit{min}}+\frac{\overset{\textit{pico}}{q}_{\textit{chl:c}}}{\overset{\textit{pico}}{q}_{\phi\textit{max}}}\left(\overset{\textit{pico}}{q}_{\phi\textit{max}}-\overset{\textit{pico}}{q}_{\phi\textit{min}}\right)\,.
! \end{equation}
!
! Gross primary production in carbon and chlorophyll-a is then given
! by:
! \begin{eqnarray*}
! \left.\frac{\partial\overset{\textit{c}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{gpp}} & = & \mathcal{\overset{\textit{pico}}{S}}_{\textit{gpp}}\overset{\textit{c}}{P}_{\textit{pico}}\! \left.\frac{\partial\overset{\mathcal{\mathscr{C}}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{gpp}} & = & \rho_{\textit{pico}}\overset{\textit{c}}{P}_{\textit{pico}}

! \end{eqnarray*}
!
! Photosynthesis is subject to losses by excretion separated in a fixed
! part and a variable part depending on the nutrient limitation status:
! \begin{equation}
! \mathcal{\overset{\textit{pico}}{S}}_{\textit{growth}}=\mathcal{\overset{\textit{pico}}{S}}_{\textit{gpp}}-\left(\left(1-\overset{\textit{pico}}{l}_{\textit{np}}\right)\left(1-\overset{\textit{pico}}{p}_{\textit{excr}}\right)+\overset{\textit{pico}}{p}_{\textit{excr}}\right)\,,
! \end{equation}
! \begin{equation}
! \left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{excr}}=\left(\left(1-\overset{\textit{pico}}{l}_{\textit{np}}\right)\left(1-\overset{\textit{pico}}{p}_{\textit{excr}}\right)+\overset{\textit{pico}}{p}_{\textit{excr}}\right)\left.\frac{\partial\overset{\textit{c,\ensuremath{\mathscr{C}}}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{gpp}}\,.
! \end{equation}
!
! Picophytoplankton respiration is computed according to
! \begin{equation}
! \left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{resp}}=\overset{\textit{pico}}{r}_{\textit{aresp}}\left(\left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{growth}}-\left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{excr}}\right)+\overset{\textit{pico}}{r}_{\textit{resp}}\overset{\textit{pico}}{l}_{T}\overset{\textit{\textit{c,}}\mathscr{C}}{P'}_{\textit{pico}}\,,
! \end{equation}
! where $\overset{\textit{pico}}{r}_{\textit{resp}}$ is the specific
! basal respiration rate at rest and $\overset{\textit{pico}}{r}_{\textit{aresp}}$
! is the fraction of non-excreted gross primary production respired
! in activity.
!
! Lysis of picophytoplankton is determined as a fixed fraction of the
! current biomass

! \begin{eqnarray*}
! \mathcal{\overset{\textit{pico}}{S}}_{\textit{lysis}} & = & \frac{1}{\overset{\textit{pico}}{l}_{\textit{np}}}\overset{\textit{pico}}{r}_{\textit{lysis}}\,.\! \left.\frac{\partial\overset{\textit{c,}\mathscr{C}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{lysis}} & = & \mathcal{\overset{\textit{pico}}{S}}_{\textit{lysis}}\overset{\textit{c,}\mathscr{C}}{P'}_{\textit{pico}}\,.

! \end{eqnarray*}
!
! The carbon fluxes from phytoplankton to organic matter are then divided
! as:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{pico}}}^{\overset{c}{M}_{\textit{small}}} & = & \overset{\textit{pico}}{p}_{\textit{part}}\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{lysis}}\,,\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{pico}}}^{\overset{c}{M}_{\textit{dis}}} & = & \overset{\textit{pico}}{p}_{\textit{lab}}\left(\left(1-\overset{\textit{pico}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{lysis}}+\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{excr}}\right)\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{pico}}}^{\overset{c}{M}_{\textit{slab}}} & = & \left(1-\overset{\textit{pico}}{p}_{\textit{lab}}\right)\left(\left(1-\overset{\textit{pico}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{lysis}}+\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{excr}}\right)


! \end{eqnarray*}

! while the nitrogen and phosphate fluxes to organic
! matter is given by the lysis term directed at minimum internal quota
! to particulate matter and the remainder excreted in dissolved form:

! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{p}{P}_{\textit{pico}}}^{\overset{p}{M}_{\textit{small}}} & = & \mathcal{\overset{\textit{pico}}{S}}_{\textit{lysis}}\textit{min}\left(\overset{\textit{p}}{P'}_{\textit{pico}},\overset{\textit{pico}}{q}_{\textit{pmin}}\overset{c}{P'}_{\textit{pico}}\right)\,,\! \left.\mathcal{F}\right|_{\overset{c}{p}_{\textit{pico}}}^{\overset{p}{M}_{\textit{dis}}} & = & \mathcal{\overset{\textit{pico}}{S}}_{\textit{lysis}}\overset{\textit{p}}{P'}_{\textit{pico}},-\left.\mathcal{F}\right|_{\overset{p}{P}_{\textit{pico}}}^{\overset{p}{M}_{\textit{med}}}\,.\!  \left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{pico}}}^{\overset{n}{M}_{\textit{small}}} & = & \mathcal{\overset{\textit{pico}}{S}}_{\textit{lysis}}\textit{min}\left(\overset{\textit{n}}{P'}_{\textit{pico}},\overset{\textit{pico}}{q}_{\textit{nmin}}\overset{c}{P'}_{\textit{pico}}\right)\,,\!  \left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{pico}}}^{\overset{n}{M}_{\textit{dis}}} & = & \mathcal{\overset{\textit{pico}}{S}}_{\textit{lysis}}\overset{\textit{n}}{P'}_{\textit{pico}}-\left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{pico}}}^{\overset{n}{M}_{\textit{med}}}\,.



! \end{eqnarray*}
!
! Nitrate and phosphate can be assimilated in surplus of the optimum
! quota for growth to build up a limited internal storage.
! Consequently, uptake of nitrate and phosphate is subject to
! availibility and capped such that the maximum internal quota can not
! be exceeded:

! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{N_{\textit{phos}}}^{\overset{p}{P}_{\textit{pico}}} & = & \textit{min}\left(\overset{\textit{pico}}{r}_{\textit{affp}}N_{\textit{phos}}\overset{c}{P}_{\textit{pico}},\right.\! & & \overset{p}{q}_{\textit{pmax}}\overset{c}{P'}_{\textit{pico}}-\overset{p}{P'}_{\textit{pico}}\! & & \left.+\mathcal{\overset{\textit{pico}}{S}}_{\textit{growth}}\overset{p}{q}_{\textit{pmax}}\overset{c}{P}_{\textit{pico}}-\overset{\textit{pico}}{r}_{\textit{resp}}\overset{p}{P'}_{\textit{pico}}\right)\,,\!  \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{pico}}} & = & \textit{min}\left(\left(\overset{\textit{pico}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{pico}}{r}_{\textit{affa}}N_{\textit{amm}}\right)\overset{c}{P}_{\textit{pico}},\right.\!   &  & \overset{n}{q}_{\textit{pmax}}\overset{c}{P'}_{\textit{pico}}-\overset{n}{P'}_{\textit{pico}}\!   &  & \left.+\mathcal{\overset{\textit{pico}}{S}}_{\textit{growth}}\overset{n}{q}_{\textit{pmax}}\overset{c}{P}_{\textit{pico}}-\overset{\textit{pico}}{r}_{\textit{resp}}\overset{n}{P'}_{\textit{pico}}\right)\,,\! \left.\mathcal{F}\right|_{N_{\textit{nitr}}}^{\overset{n}{P}_{\textit{pico}}} & = & \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{pico}}}\frac{\overset{\textit{pico}}{r}_{\textit{affn}}N_{\textit{nitr}}}{\overset{\textit{pico}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{pico}}{r}_{\textit{affa}}N_{\textit{amm}}}\,,\! \left.\mathcal{F}\right|_{N_{\textit{amm}}}^{\overset{n}{P}_{\textit{pico}}} & = & \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{pico}}}\frac{\overset{\textit{pico}}{r}_{\textit{affa}}N_{\textit{amm}}}{\overset{\textit{pico}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{pico}}{r}_{\textit{affa}}N_{\textit{amm}}}\,.







! \end{eqnarray*}
!
! The respiration fluxes towards inorganic gasses are given as:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{pico}}}^{G_{\textit{\textit{CO}}_{2}}} & = & \frac{1}{\mathcal{\mathbb{C}}}\left(\left.\frac{\partial\overset{c}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{resp}}-\left.\frac{\partial\overset{\textit{c}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{gpp}}\right)\! \left.\mathcal{F}\right|_{G_{O_{2}}}^{\textit{sink}} & = & \overset{B}{p}_{O_{2}}\left.\frac{\partial\overset{c}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{resp}}\,-\overset{B}{p}_{phO_{2}}\left.\frac{\partial\overset{c}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{gpp}}.

! \end{eqnarray*}
!
! If the dynamic bacteria model is active (activated through the preprocessing
! option \texttt{1}), excretion is entirely directed to semi-labile
! organic matter and the dissolved part of lysis to the labile dissolved
! organic matter:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{pico}}}^{\overset{c}{M}_{\textit{dis}}} & = & \left(1-\overset{\textit{pico}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{lysis}}\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{pico}}}^{\overset{c}{M}_{\textit{slab}}} & = & \left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{pico}}}{\partial t}\right|_{\textit{excr}}

! \end{eqnarray*}
!
! If iron is used (activated through the preprocessing option \texttt{1}),
! gross primary production is additionally limited by the iron limitation
! factor $\overset{\textit{pico}}{l}_{\textit{fe}}$:
! \begin{equation}
! \mathcal{\overset{\textit{pico}}{S}}_{\textit{gpp}}=\overset{\textit{pico}}{g}_{\textit{max}}\overset{\textit{pico}}{l}_{T}\overset{\textit{pico}}{l}_{\textit{fe}}\left(1-\textit{exp}\left(-\frac{\overset{\textit{pico}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{pico}}{q}_{\textit{chl:c}}}{\overset{\textit{pico}}{g}_{\textit{max}}\overset{\textit{pico}}{l}_{T}\overset{\textit{pico}}{l}_{\textit{fe}}}\right)\right)\textit{exp}\left(-\frac{\overset{\textit{pico}}{\beta}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{pico}}{q}_{\textit{chl:c}}}{\overset{\textit{pico}}{g}_{\textit{max}}\overset{\textit{pico}}{l}_{T}\overset{\textit{pico}}{l}_{\textit{fe}}}\right)\,.
! \end{equation}
!
! Iron uptake is regulated analog to phosphate and nitrate uptake, but
! without luxuary buffer

! \begin{equation}
! \left.\mathcal{F}\right|_{N_{\textit{fe}}}^{\overset{f}{P}_{\textit{pico}}}=\textit{min}\left(\overset{\textit{pico}}{r}_{\textit{affp}}N_{\textit{fe}}\overset{f}{P}_{\textit{pico}},\overset{\textit{pico}}{q}_{\textit{fmax}}\overset{c}{P'}_{\textit{pico}}-\overset{f}{P'}_{\textit{pico}}+\mathcal{\overset{\textit{pico}}{S}}_{\textit{growth}}\overset{\textit{pico}}{q}_{\textit{fmax}}\overset{c}{P}_{\textit{pico}}-\overset{\textit{pico}}{r}_{\textit{resp}}\overset{f}{P'}_{\textit{pico}}\right)\,,
! \end{equation}
! while lysis is entirely directed to particuale iron:
! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{pico}}}^{\overset{f}{M}_{\textit{med}}}=\mathcal{\overset{\textit{pico}}{S}}_{\textit{lysis}}\overset{\textit{f}}{P'}_{\textit{pico}}\,.
! \end{equation}
!
! If the \texttt{CENH} preprocessing option is used in order to test
! the impact of acidification on primary production, gross and net primary
! production of carbon are enhanced by a factor $p_{\textit{cenh}}$
! depending on atmospheric partial pressure of carbon dioxide $\mathscr{\textrm{p}}_{\textit{CO}_{2}}$
! defined as:
! \begin{equation}
! p_{\textit{cenh}}=1.+\mathscr{\textrm{p}}_{\textit{CO}_{2}}-379.48*0.0005\,.
! \end{equation}
!
! !INTERFACE:
   subroutine picophytoplankton_dynamics(I)
!
! !USES:
   use ersem_constants, only: CMass
!
! !INPUT PARAMETERS:
   integer :: i
!
! !LOCAL VARIABLES:
   real(fp8) :: sdoP3,sumP3,sugP3,seoP3,seaP3
   real(fp8) :: sraP3,sunP3,runP3,rugP3,sP3R4
   real(fp8) :: runP3p,misP3p,rumP3p
   real(fp8) :: runP3n,misP3n,rumP3n,rumP3n3,rumP3n4
   real(fp8) :: etP3,SDP3,pe_R6P3
   real(fp8) :: rho,Chl_inc,Chl_loss
   real(fp8) :: phi,ChlCpp

   real(fp8) :: runP3f,rumP3f,misP3f


   real(fp8) :: fP3R1c,fP3R2c

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Regulation factors...................................................

   ! Temperature response
   etP3 = q10P3X**((ETW(I)-10._fp8)/10._fp8) - q10P3X**((ETW(I)-32._fp8)/3._fp8)

   ! Production...........................................................

   ! calculate chl to C ratio.............................................

   !ChlCpp = max(min(phimP3X,chl3(I)/(p3c(I)-chl3(I)+zeroX)),ChlCmin)
   ChlCpp = chl3(I)/p3c(I)

   ! Gross photosynthetic activity :

   sumP3 = sumP3X*etP3*iNP3f




   phi = phiP3HX + (ChlCpp/phimP3X)*(phimP3X-phiP3HX)

   parEIR = pEIR_eowX*EIR(I)
   if (parEIR.gt.zeroX) then
      sumP3 = sumP3 * (1.-exp(-alphaP3X*parEIR*ChlCpp/sumP3)) * EXP(-betaP3X*parEIR*ChlCpp/sumP3)
      rho = (phi - ChlCmin) * (sumP3/(alphaP3X*parEIR*ChlCpp)) + ChlCmin
   else
      sumP3 = 0._fp8
      rho = ChlCmin
   end if




   ! Nutrient-stress lysis rate :
   sdoP3 = (1._fp8/(iNIP3+0.1_fp8))*sdoP3X

   ! Excretion rate, as regulated by nutrient-stress
   seoP3 = sumP3*(1._fp8-iNIP3)*(1._fp8-pu_eaP3X)

   ! activity-dependent excretion :
   seaP3 = sumP3*pu_eaP3X
   sugP3 = sumP3-seoP3-seaP3

   ! Apportioning of LOC- and DET- fraction of excretion/lysis fluxes:
   pe_R6P3 = MIN(qplP3cX/(qpP3c(I)+ZeroX),qnlP3cX/(qnP3c(I)+ZeroX))
   sP3R4 = pe_R6P3*sdoP3
   fP3R4c(I) = sP3R4*P3cP(I)


   fP3R1c= (1._fp8-pe_R6P3)*sdoP3*P3cP(I)
   fP3R2c= (seoP3 + seaP3)*P3c(I)
   fP3RDc(I) = fP3R1c+fP3R2c




   ! Respiration..........................................................

   ! Rest respiration rate :
   srsP3 = etP3*srsP3X

   ! Activity respiration rate :
   sraP3 = sugP3*pu_raP3X

   ! Total respiration flux :
   fP3O3c(I) = srsP3*P3cP(I)+sraP3*P3c(I)

   ! Gross production as flux from inorganic CO2 to P3 :
   fO3P3c(I) = sumP3*P3c(I)




   rugP3 = fO3P3c(I)

   ! Production and productivity
   sunP3 = sumP3-(seoP3+seaP3+sraP3) ! net productivity
   runP3 = sunP3*P3c(I)-srsP3*P3cP(I) ! net production

   ! To save net production

   netP3(I) = runP3




   ! Carbon Source equations

   rho = MIN(rho,0.1_fp8)
   ! Chl changes (note that Chl is a component of PXc and not involved
   ! in mass balance)

   Chl_inc = min(iNP3f,iNIP3)*rho*(sumP3-sraP3)*P3c(I)



   Chl_loss = (sdoP3+srsP3)*Chl3P(I) + (seoP3+seaP3)*Chl3(I)

   SP3c(I) = SP3c(I)+fO3P3c(I)-fP3O3c(I)-fP3R4c(I)-fP3RDc(I)

   SR1c(I) = SR1c(I)+fP3R1c
   SR2c(I) = SR2c(I)+fP3R2c




   SR4c(I) = SR4c(I)+fP3R4c(I)
   SChl3(I) = Schl3(I) + Chl_inc - Chl_loss

   SO3c(I) = SO3c(I) + (fP3O3c(I) - fO3P3c(I))/CMass
   SO2o(I) = SO2o(I) + fO3P3c(I)*uB1c_O2X - fP3O3c(I)*urB1_O2X

   ! Phosphorus flux through P3...........................................

   ! Lysis loss of phosphorus
   fP3R4p = sP3R4*min(qplP3cX*P3cP(I),P3pP(I))
   fP3RDp = sdoP3*P3pP(I) - fP3R4p

   ! Net phosphorus uptake
   rumP3p = qurP3pX * N1pP(I) * P3c(I)
   misP3p = xqpP3X * qpRPIcX*P3cP(I) - P3pP(I)
   runP3p = sunP3*P3c(I) * qpRPIcX*xqpP3X - srsP3*P3pP(I)
   fN1P3p(I) = MIN(rumP3p, runP3p+misP3p)

   ! Source equations
   SP3p(I) = SP3p(I)+fN1P3p(I)-fP3RDp-fP3R4p
   SN1p(I) = SN1p(I)-fN1P3p(I)
   SR4p(I) = SR4p(I)+fP3R4p
   SR1p(I) = SR1p(I)+fP3RDp

   ! Nitrogen flux through P3.............................................

   ! Nitrogen loss by lysis
   fP3R4n = sP3R4 * min(qnlP3cX*P3cP(I),P3nP(I))
   fP3RDn = sdoP3 * P3nP(I) - fP3R4n

   ! Net nitrogen uptake
   rumP3n3 = quP3n3X * N3nP(I) * P3c(I)
   rumP3n4 = quP3n4X * N4nP(I) * P3c(I)
   rumP3n = rumP3n3 + rumP3n4

   misP3n = xqnP3X * qnRPIcX*P3cP(I) - P3nP(I)
   runP3n = sunP3 *P3c(I) * qnRPIcX*xqnP3X - srsP3*P3nP(I)
   fNIP3n = MIN(rumP3n, runP3n + misP3n)

   ! Partitioning over NH4 and NO3 uptake
   if (fNIP3n .gt. 0._fp8) then
      fN3P3n(I) = fNIP3n * rumP3n3 / rumP3n
      fN4P3n(I) = fNIP3n * rumP3n4 / rumP3n
   else
      fN3P3n(I) = 0._fp8
      fN4P3n(I) = fNIP3n
   end if

   ! Source equations
   SP3n(I) = SP3n(I)+fN4P3n(I)+fN3P3n(I)-fP3RDn-fP3R4n
   SN3n(I) = SN3n(I)-fN3P3n(I)
   SN4n(I) = SN4n(I)-fN4P3n(I)
   SR4n(I) = SR4n(I)+fP3R4n
   SR1n(I) = SR1n(I)+fP3RDn


   ! Iron flux through P3................................................

   ! Iron loss by lysis
! Because its high affinity with particles all the iron lost from phytoplankton by lysis is supposed to be
! associated to organic particulate detritus. (luca)

   fP3R4f = sdoP3 * P3fP(I)

   ! net iron uptake
   rumP3f = qurP3fX * N7fP(I) * P3c(I)
   misP3f = qfRP3cX*P3cP(I) - P3fP(I)
   runP3f = sunP3*P3c(I) * qfRP3cX - srsP3*P3fP(I)
   fN7P3f(I) = MIN(rumP3f, runP3f+misP3f)

   ! Source equations
   SP3f(I) = SP3f(I)+fN7P3f(I)-fP3R4f
   SN7f(I) = SN7f(I)-fN7P3f(I)
   SR4f(I) = SR4f(I)+fP3R4f


   ! sedimentation and resting stages.....................................

   SDP3 = resP3mX * MAX(0._fp8, (esNIP3X - iNIP3) )
   SDP3c(I) = SDP3c(I) + SDP3
   SDP3p(I) = SDP3p(I) + SDP3
   SDP3n(I) = SDP3n(I) + SDP3
   SDChl3(I) = SDChl3(I) + SDP3

   SDP3f(I) = SDP3f(I) + SDP3


   return

   end subroutine picophytoplankton_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Microphytoplankton dynamics \label{sec:microphytdynamics}
!
! !DESCRIPTION:
! Specific gross primary production is computed from specific maximum
! growth $\overset{\textit{micro}}{g}_{\textit{max}}$ regulated by
! temperature and limited by the nutritional state and the light
! conditions:
! \begin{equation}
! \mathcal{\overset{\textit{micro}}{S}}_{\textit{gpp}}=\overset{\textit{micro}}{g}_{\textit{max}}\overset{\textit{micro}}{l}_{T}\left(1-\textit{exp}\left(-\frac{\overset{\textit{micro}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{micro}}{q}_{\textit{chl:c}}}{\overset{\textit{micro}}{g}_{\textit{max}}\overset{\textit{micro}}{l}_{T}}\right)\right)\textit{exp}\left(-\frac{\overset{\textit{micro}}{\beta}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{micro}}{q}_{\textit{chl:c}}}{\overset{\textit{micro}}{g}_{\textit{max}}\overset{\textit{micro}}{l}_{T}}\right)\,,
! \end{equation}
! where the temperature response factor $\overset{\textit{micro}}{l}_{T}$
! is given by the equation:
! \begin{equation}
! \overset{\textit{micro}}{l}_{T}=\left.\overset{\textit{micro}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-10}{10}}-\left.\overset{\textit{micro}}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-32}{3}}.
! \end{equation}
!
! The light acclimation model used here regulates the internal
! chlorophyll-a to carbon ratio in relation to the give light
! condition. It is based on the formulation by
! \citet{Geider1997} modified for photoinhibition according to
! \citet{Blackford2004}.
! The photosynthetically available radiation $I_{\textit{PAR}}$ is
! estimated as a fixed fraction $p_{\textit{PAR}}$ of short-wave radiation
! $I_{\textit{SWR}}$:
! \begin{equation}
! I_{\textit{PAR}}=p_{\textit{PAR}}I_{\textit{SWR}}\,.
! \end{equation}
!
! Chlorophyll-a is synthesised at a ratio:
! \begin{equation}
! \rho_{\textit{micro}}=\left(\textit{\ensuremath{\phi}}_{\textit{micro}}-q_{\textit{chlcmin}}\right)\frac{\mathcal{\overset{\textit{micro}}{S}}_{\textit{gpp}}}{\overset{\textit{micro}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{micro}}{q}_{\textit{chl:c}}}+q_{\textit{chlcmin}}\,,
! \end{equation}
!
!
! where
!
! \begin{equation}
! \phi_{\textit{micro}}=\overset{\textit{micro}}{q}_{\phi\textit{min}}+\frac{\overset{\textit{micro}}{q}_{\textit{chl:c}}}{\overset{\textit{micro}}{q}_{\phi\textit{max}}}\left(\overset{\textit{micro}}{q}_{\phi\textit{max}}-\overset{\textit{micro}}{q}_{\phi\textit{min}}\right)\,.
! \end{equation}
!
! Gross primary production in carbon and chlorophyll-a is then given
! by:
! \begin{eqnarray*}
! \left.\frac{\partial\overset{\textit{c}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{gpp}} & = & \mathcal{\overset{\textit{micro}}{S}}_{\textit{gpp}}\overset{\textit{c}}{P}_{\textit{micro}}\! \left.\frac{\partial\overset{\mathcal{\mathscr{C}}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{gpp}} & = & \rho_{\textit{micro}}\overset{\textit{c}}{P}_{\textit{micro}}

! \end{eqnarray*}
!
! Photosynthesis is subject to losses by excretion separated in a fixed
! part and a variable part depending on the nutrient limitation status:
! \begin{equation}
! \mathcal{\overset{\textit{micro}}{S}}_{\textit{growth}}=\mathcal{\overset{\textit{micro}}{S}}_{\textit{gpp}}-\left(\left(1-\overset{\textit{micro}}{l}_{\textit{np}}\right)\left(1-\overset{\textit{micro}}{p}_{\textit{excr}}\right)+\overset{\textit{micro}}{p}_{\textit{excr}}\right)\,,
! \end{equation}
! \begin{equation}
! \left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{excr}}=\left(\left(1-\overset{\textit{micro}}{l}_{\textit{np}}\right)\left(1-\overset{\textit{micro}}{p}_{\textit{excr}}\right)+\overset{\textit{micro}}{p}_{\textit{excr}}\right)\left.\frac{\partial\overset{\textit{c,\ensuremath{\mathscr{C}}}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{gpp}}\,.
! \end{equation}
!
! Microphytoplankton respiration is computed according to:
! \begin{equation}
! \left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{resp}}=\overset{\textit{micro}}{r}_{\textit{aresp}}\left(\left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{growth}}-\left.\frac{\partial\overset{\textit{\textit{c,}}\mathscr{C}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{excr}}\right)+\overset{\textit{micro}}{r}_{\textit{resp}}\overset{\textit{micro}}{l}_{T}\overset{\textit{\textit{c,}}\mathscr{C}}{P'}_{\textit{micro}}\,,
! \end{equation}
! where $\overset{\textit{micro}}{r}_{\textit{resp}}$ is the specific
! basal respiration rate at rest and $\overset{\textit{micro}}{r}_{\textit{aresp}}$
! is the fraction of non-excreted gross primary production respired
! in activity.
!
! Lysis of microphytoplankton is determined as a fixed fraction of the
! current biomass
! \begin{eqnarray*}
! \mathcal{\overset{\textit{micro}}{S}}_{\textit{lysis}} & = & \frac{1}{\overset{\textit{micro}}{l}_{\textit{np}}}\overset{\textit{micro}}{r}_{\textit{lysis}}\,.\! \left.\frac{\partial\overset{\textit{c,}\mathscr{C}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{lysis}} & = & \mathcal{\overset{\textit{micro}}{S}}_{\textit{lysis}}\overset{\textit{c,}\mathscr{C}}{P'}_{\textit{micro}}\,.

! \end{eqnarray*}
!
! The carbon fluxes from microphytoplankton to organic matter are then
! divided as:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{micro}}}^{\overset{c}{M}_{\textit{med}}} & = & \overset{\textit{micro}}{p}_{\textit{part}}\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{lysis}}\,,\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{micro}}}^{\overset{c}{M}_{\textit{dis}}} & = & \overset{\textit{micro}}{p}_{\textit{lab}}\left(\left(1-\overset{\textit{micro}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{lysis}}+\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{excr}}\right)\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{micro}}}^{\overset{c}{M}_{\textit{slab}}} & = & \left(1-\overset{\textit{micro}}{p}_{\textit{lab}}\right)\left(\left(1-\overset{\textit{micro}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{lysis}}+\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{excr}}\right)


! \end{eqnarray*}
! while the nitrogen and phosphate fluxes to organic
! matter is given by the lysis term directed at minimum internal quota
! to particulate matter and the remainder excreted in dissolved form:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{p}{P}_{\textit{micro}}}^{\overset{p}{M}_{\textit{med}}} & = & \mathcal{\overset{\textit{micro}}{S}}_{\textit{lysis}}\textit{min}\left(\overset{\textit{p}}{P'}_{\textit{micro}},\overset{\textit{micro}}{q}_{\textit{pmin}}\overset{c}{P'}_{\textit{micro}},\right)\,,\! \left.\mathcal{F}\right|_{\overset{c}{p}_{\textit{micro}}}^{\overset{p}{M}_{\textit{dis}}} & = & \mathcal{\overset{\textit{micro}}{S}}_{\textit{lysis}}\overset{\textit{p}}{P'}_{\textit{micro}},-\left.\mathcal{F}\right|_{\overset{p}{P}_{\textit{micro}}}^{\overset{p}{M}_{\textit{med}}}\,.\!  \left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{micro}}}^{\overset{n}{M}_{\textit{med}}} & = & \mathcal{\overset{\textit{micro}}{S}}_{\textit{lysis}}\textit{min}\left(\overset{\textit{n}}{P'}_{\textit{micro}},\overset{\textit{micro}}{q}_{\textit{nmin}}\overset{c}{P'}_{\textit{micro}},\right)\,,\!  \left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{micro}}}^{\overset{n}{M}_{\textit{dis}}} & = & \mathcal{\overset{\textit{micro}}{S}}_{\textit{lysis}}\overset{\textit{n}}{P'}_{\textit{micro}},-\left.\mathcal{F}\right|_{\overset{n}{P}_{\textit{micro}}}^{\overset{n}{M}_{\textit{med}}}\,.



! \end{eqnarray*}
!
! Nitrate and phosphate can be assimilated in surplus of the optimum
! quota for growth to build up a limited internal storage.
! Consequently, uptake of nitrate and phosphate is subject to
! availibility and capped such that the maximum internal quota can not
! be exceeded:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{N_{\textit{phos}}}^{\overset{p}{P}_{\textit{micro}}} & = & \textit{min}\left(\overset{\textit{micro}}{r}_{\textit{affp}}N_{\textit{phos}}\overset{c}{P}_{\textit{micro}},\right.\! & & \overset{p}{q}_{\textit{pmax}}\overset{c}{P'}_{\textit{micro}}-\overset{p}{P'}_{\textit{micro}}\! & & \left.+\mathcal{\overset{\textit{micro}}{S}}_{\textit{growth}}\overset{p}{q}_{\textit{pmax}}\overset{c}{P}_{\textit{micro}}-\overset{\textit{micro}}{r}_{\textit{resp}}\overset{p}{P'}_{\textit{micro}}\right)\,,\!  \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{micro}}} & = & \textit{min}\left(\left(\overset{\textit{micro}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{micro}}{r}_{\textit{affa}}N_{\textit{amm}}\right)\overset{c}{P}_{\textit{micro}},\right.\!   &  & \overset{n}{q}_{\textit{pmax}}\overset{c}{P'}_{\textit{micro}}-\overset{n}{P'}_{\textit{micro}}\!   &  & \left.+\mathcal{\overset{\textit{micro}}{S}}_{\textit{growth}}\overset{n}{q}_{\textit{pmax}}\overset{c}{P}_{\textit{micro}}-\overset{\textit{micro}}{r}_{\textit{resp}}\overset{n}{P'}_{\textit{micro}}\right)\,,\! \left.\mathcal{F}\right|_{N_{\textit{nitr}}}^{\overset{n}{P}_{\textit{micro}}} & = & \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{micro}}}\frac{\overset{\textit{micro}}{r}_{\textit{affn}}N_{\textit{nitr}}}{\overset{\textit{micro}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{micro}}{r}_{\textit{affa}}N_{\textit{amm}}}\,,\! \left.\mathcal{F}\right|_{N_{\textit{amm}}}^{\overset{n}{P}_{\textit{micro}}} & = & \left.\mathcal{F}\right|_{n}^{\overset{n}{P}_{\textit{micro}}}\frac{\overset{\textit{micro}}{r}_{\textit{affa}}N_{\textit{amm}}}{\overset{\textit{micro}}{r}_{\textit{affn}}N_{\textit{nitr}}+\overset{\textit{micro}}{r}_{\textit{affa}}N_{\textit{amm}}}\,.







! \end{eqnarray*}
!
! The respiration fluxes towards inorganic gasses are given as:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{micro}}}^{G_{\textit{\textit{CO}}_{2}}} & = & \frac{1}{\mathcal{\mathbb{C}}}\left(\left.\frac{\partial\overset{c}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{resp}}-\left.\frac{\partial\overset{\textit{c}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{gpp}}\right)\! \left.\mathcal{F}\right|_{G_{O_{2}}}^{\textit{sink}} & = & \overset{B}{p}_{O_{2}}\left.\frac{\partial\overset{c}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{resp}}\,-\overset{B}{p}_{phO_{2}}\left.\frac{\partial\overset{c}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{gpp}}.

! \end{eqnarray*}
!
! If the dynamic bacteria model is active (activated through the preprocessing
! option \texttt{1}), excretion is entirely directed to semi-labile
! organic matter and the dissolved part of lysis to the labile dissolved
! organic matter:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{micro}}}^{\overset{c}{M}_{\textit{dis}}} & = & \left(1-\overset{\textit{micro}}{p}_{\textit{part}}\right)\left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{lysis}}\! \left.\mathcal{F}\right|_{\overset{c}{P}_{\textit{micro}}}^{\overset{c}{M}_{\textit{slab}}} & = & \left.\frac{\partial\overset{\textit{\textit{c}}}{P}_{\textit{micro}}}{\partial t}\right|_{\textit{excr}}

! \end{eqnarray*}
!
! If iron is used (activated through the preprocessing option \texttt{1}),
! gross primary production is additionally limited by the iron limitation
! factor $\overset{\textit{micro}}{l}_{\textit{fe}}$:
! \begin{equation}
! \mathcal{\overset{\textit{micro}}{S}}_{\textit{gpp}}=\overset{\textit{micro}}{g}_{\textit{max}}\overset{\textit{micro}}{l}_{T}\overset{\textit{micro}}{l}_{\textit{fe}}\left(1-\textit{exp}\left(-\frac{\overset{\textit{micro}}{\alpha}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{micro}}{q}_{\textit{chl:c}}}{\overset{\textit{micro}}{g}_{\textit{max}}\overset{\textit{micro}}{l}_{T}\overset{\textit{micro}}{l}_{\textit{fe}}}\right)\right)\textit{exp}\left(-\frac{\overset{\textit{micro}}{\beta}_{\textit{PI}}I_{\textit{PAR}}\overset{\textit{micro}}{q}_{\textit{chl:c}}}{\overset{\textit{micro}}{g}_{\textit{max}}\overset{\textit{micro}}{l}_{T}\overset{\textit{micro}}{l}_{\textit{fe}}}\right)\,.
! \end{equation}
!
! Iron uptake is regulated analog to phosphate and nitrate uptake, but
! without luxuary buffer

! \begin{equation}
! \left.\mathcal{F}\right|_{N_{\textit{fe}}}^{\overset{f}{P}_{\textit{micro}}}=\textit{min}\left(\overset{\textit{micro}}{r}_{\textit{affp}}N_{\textit{fe}}\overset{f}{P}_{\textit{micro}},\overset{\textit{np}}{q}_{\textit{fmax}}\overset{c}{P'}_{\textit{micro}}-\overset{f}{P'}_{\textit{micro}}+\mathcal{\overset{\textit{micro}}{S}}_{\textit{growth}}\overset{\textit{np}}{q}_{\textit{fmax}}\overset{c}{P}_{\textit{micro}}-\overset{\textit{micro}}{r}_{\textit{resp}}\overset{f}{P'}_{\textit{micro}}\right)\,,
! \end{equation}
! while lysis is entirely directed to particuale iron:
! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{f}{P}_{\textit{micro}}}^{\overset{f}{M}_{\textit{med}}}=\mathcal{\overset{\textit{micro}}{S}}_{\textit{lysis}}\overset{\textit{f}}{P'}_{\textit{micro}}\,.
! \end{equation}
!
! If the \texttt{CENH} preprocessing option is used in order to test
! the impact of acidification on primary production, gross and net primary
! production of carbon are enhanced by a factor $p_{\textit{cenh}}$
! depending on atmospheric partial pressure of carbon dioxide $\mathscr{\textrm{p}}_{\textit{CO}_{2}}$
! defined as:
! \begin{equation}
! p_{\textit{cenh}}=1.+\mathscr{\textrm{p}}_{\textit{CO}_{2}}-379.48*0.0005\,.
! \end{equation}
!
! !INTERFACE:
   subroutine microphytoplankton_dynamics(I)
!
! !USES:
   use ersem_constants, only: CMass
!
! !INPUT PARAMETERS:
   integer :: i
!
! !LOCAL VARIABLES:
   real(fp8) :: sdoP4,sumP4,sugP4,seoP4,seaP4
   real(fp8) :: sraP4,sunP4,runP4,rugP4,sP4R6
   real(fp8) :: runP4p,misP4p,rumP4p
   real(fp8) :: runP4n,misP4n,rumP4n,rumP4n3,rumP4n4
   real(fp8) :: etP4,SDP4,pe_R6P4
   real(fp8) :: rho,Chl_inc,Chl_loss
   real(fp8) :: phi,ChlCpp

   real(fp8) :: runP4f,rumP4f,misP4f


   real(fp8) :: fP4R1c,fP4R2c

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Regulation factors...................................................

   ! Temperature response
   etP4 = q10P4X**((ETW(I)-10._fp8)/10._fp8) - q10P4X**((ETW(I)-32._fp8)/3._fp8)

   ! Production...........................................................

   ! calculate chl to C ratio.............................................

   !ChlCpp = max(min(phimP4X,chl4(I)/(p4c(I)-chl4(I)+zeroX)),ChlCmin)
   ChlCpp = chl4(I)/p4c(I)

   ! Gross photosynthetic activity :

   sumP4 = sumP4X*etP4*iNP4f




   phi = phiP4HX + (ChlCpp/phimP4X)*(phimP4X-phiP4HX)

   parEIR = pEIR_eowX*EIR(I)

   if (parEIR .gt. zeroX) then
      sumP4 = sumP4 * (1.-exp(-alphaP4X*parEIR*ChlCpp/sumP4)) * EXP(-betaP4X*parEIR*ChlCpp/sumP4)
      rho = (phi - ChlCmin) * (sumP4/(alphaP4X*parEIR*ChlCpp)) + ChlCmin
   else
      sump4 = 0._fp8
      rho = ChlCmin
   end if






   ! Nutrient-stress lysis rate :
   sdoP4 = (1._fp8/(iNIP4+0.1_fp8))*sdoP4X

   ! extra lysis rate, quadratic mortality (active only in P4):
! sdoP4 = sdoP4 + seoP4X * P4c(I) * (P4c(I)/100.0)

   ! Excretion rate, as regulated by nutrient-stress
   seoP4 = sumP4*(1._fp8-iNIP4)*(1._fp8-pu_eaP4X)

   ! activity-dependent excretion :
   seaP4 = sumP4*pu_eaP4X
   sugP4 = sumP4-seoP4-seaP4

   ! Apportioning of LOC- and DET- fraction of excretion/lysis fluxes:
   pe_R6P4 = MIN(qplP4cX/(qpP4c(I)+ZeroX),qnlP4cX/(qnP4c(I)+ZeroX))
   sP4R6 = pe_R6P4*sdoP4
   fP4R6c(I) = sP4R6*P4cP(I)


   fP4R1c= (1._fp8-pe_R6P4)*sdoP4*P4cP(I)
   fP4R2c= (seoP4 + seaP4)*P4c(I)
   fP4RDc(I) = fP4R1c+fP4R2c




   ! Respiration..........................................................

   ! Rest respiration rate :
   srsP4 = etP4*srsP4X

   ! Activity respiration rate :
   sraP4 = sugP4*pu_raP4X

! if (resP4mX .gt. 10.0) sraP4 = sraP4 + (P4resX*sugP4*pu_raP4X)

   ! Total respiration flux :
   fP4O3c(I) = srsP4*P4cP(I)+sraP4*P4c(I)

   ! Gross production as flux from inorganic CO2 to P4 :
   fO3P4c(I) = sumP4*P4c(I)




   rugP4 = fO3P4c(I)

   ! Production and productivity
   sunP4 = sumP4-(seoP4+seaP4+sraP4) ! net productivity
   runP4 = sunP4*P4c(I)-srsP4*P4cP(I) ! net production

   ! To save net production

   netP4(I) = runP4




   ! Carbon Source equations

   rho = MIN(rho, 0.1_fp8)
   ! Chl changes (note that Chl is a component of PXc and not involved
   ! in mass balance)

   Chl_inc = min(iNP4f,iNIP4)*rho*(sumP4-sraP4)*P4c(I)



   Chl_loss = (sdoP4+srsP4)*Chl4P(I) + (seoP4+seaP4)*Chl4(I)

   SP4c(I) = SP4c(I)+fO3P4c(I)-fP4O3c(I)-fP4R6c(I)-fP4RDc(I)

   SR1c(I) = SR1c(I)+fP4R1c
   SR2c(I) = SR2c(I)+fP4R2c




   SR6c(I) = SR6c(I)+fP4R6c(I)
   SChl4(I) = Schl4(I) + Chl_inc - Chl_loss

   SO3c(I) = SO3c(I) + (fP4O3c(I) - fO3P4c(I))/CMass
   SO2o(I) = SO2o(I) + fO3P4c(I)*uB1c_O2X - fP4O3c(I)*urB1_O2X

   ! Phosphorus flux through P4...........................................

   ! Excretion loss of phosphorus
   fP4R6p = sP4R6 * min(qplP4cX*P4cP(I),P4pP(I))
   fP4RDp = sdoP4 * P4pP(I) - fP4R6p

   ! Net phosphorus uptake
   rumP4p = qurP4pX * N1pP(I) * P4c(I)
   misP4p = xqpP4X * qpRPIcX*P4cP(I) - P4pP(I)
   runP4p = sunP4*P4c(I) * qpRPIcX*xqpP4X - srsP4*P4pP(I)
   fN1P4p(I) = MIN(rumP4p, runP4p+misP4p)

   ! Source equations
   SP4p(I) = SP4p(I)+fN1P4p(I)-fP4RDp-fP4R6p
   SN1p(I) = SN1p(I)-fN1P4p(I)
   SR6p(I) = SR6p(I)+fP4R6p
   SR1p(I) = SR1p(I)+fP4RDp

   ! Nitrogen flux through P4.............................................

   ! Nitrogen loss by excretion
   fP4R6n = sP4R6 * min(qnlP4cX*P4cP(I),P4nP(I))
   fP4RDn = sdoP4 * P4nP(I) - fP4R6n

   ! Net nitrogen uptake
   rumP4n3 = quP4n3X * N3nP(I) * P4c(I)
   rumP4n4 = quP4n4X * N4nP(I) * P4c(I)
   rumP4n = rumP4n3 + rumP4n4
   misP4n = xqnP4X * qnRPIcX*P4cP(I) - P4nP(I)
   runP4n = sunP4*P4c(I) * qnRPIcX*xqnP4X - srsP4*P4nP(I)
   fNIP4n = MIN(rumP4n, runP4n + misP4n)

   ! Partitioning over NH4 and NO3 uptake
   if (fNIP4n .gt. 0._fp8) then
      fN3P4n(I) = fNIP4n * rumP4n3 / rumP4n
      fN4P4n(I) = fNIP4n * rumP4n4 / rumP4n
   else
      fN3P4n(I) = 0._fp8
      fN4P4n(I) = fNIP4n
   end if

   ! Source equations
   SP4n(I) = SP4n(I)+fN4P4n(I)+fN3P4n(I)-fP4RDn-fP4R6n
   SN3n(I) = SN3n(I)-fN3P4n(I)
   SN4n(I) = SN4n(I)-fN4P4n(I)
   SR6n(I) = SR6n(I)+fP4R6n
   SR1n(I) = SR1n(I)+fP4RDn

   ! Iron flux through P3................................................


   ! Iron loss by lysis
   ! Because its high affinity with particles all the iron lost from phytoplankton by lysis is supposed to be
   ! associated to organic particulate detritus. (luca)

   fP4R6f = sdoP4 * P4fP(I)

   ! net iron uptake
   rumP4f = qurP4fX * N7fP(I) * P4c(I)
   misP4f = qfRP4cX*P4cP(I) - P4fP(I)
   runP4f = sunP4*P4c(I) * qfRP4cX - srsP4*P4fP(I)
   fN7P4f(I) = MIN(rumP4f, runP4f+misP4f)

   ! Source equations
   SP4f(I) = SP4f(I)+fN7P4f(I)-fP4R6f
   SN7f(I) = SN7f(I)-fN7P4f(I)
   SR6f(I) = SR6f(I)+fP4R6f


   ! sedimentation or vertical migration.....................................

! SWP4(I) = 0.0
   SDP4 = resP4mX * MAX(0._fp8, (esNIP4X - iNIP4) )
   SDP4c(I) = SDP4c(I) + SDP4
   SDP4p(I) = SDP4p(I) + SDP4
   SDP4n(I) = SDP4n(I) + SDP4
   SDChl4(I) = SDChl4(I) + SDP4

   SDP4f(I) = SDP4f(I) + SDP4


   return

   end subroutine microphytoplankton_dynamics
!
!EOC
!-----------------------------------------------------------------------

   end module primary_producers

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
