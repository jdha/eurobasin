

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Bacteria
!
! !DESCRIPTION:
! This modules contains the bacteria parametrisation and the routines
! to initialse the parameters from namelist and launch the bacteria
! driven processes, as well as a routine for the parametrised remineralisation
! of organic matter.\\[\baselineskip]
!
! !INTERFACE:
   module bacteria
!
! !USES:
   use ersem_constants, only: fp8
   use ersem_variables, only: IOProcess, eLogU
   use gas_dynamics, only: ub1c_o2x,urB1_O2X

   implicit none

! !Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_bacteria_parameters, bacteria_dynamics, &
      do_remineralisation
!
! !PUBLIC DATA MEMBERS:
! Max. specific uptake of bacteria at reference temperature [1/m^3/d]
   real(fp8), public :: sumB1X

! Michaelis-Menten constant for oxygen limitation of bacteria
   real(fp8), public :: chdB1oX

! Fraction of small, medium, & large size POM available for bacteria
   real(fp8), public :: pur4_B1X, pur6_B1X, pur8_B1X

! Bacterial efficiency at high oxygen levels
   real(fp8), public :: puB1X

! Bacterial efficiency at high oxygen levels
   real(fp8), public :: puB1oX

! Regulating temperature factor Q10 for bacterial metabolic processes
   real(fp8), public :: q10B1X

! Specific rest respiration at reference temperature [1/m^3/d]
   real(fp8), public :: srsB1X

! Specific mortality of bacteria at reference temperature [1/m^3/d]
   real(fp8), public :: sdB1X

! Max cell-quotum Nitrogen/Carbon of bacteria [mmol N/mg C]
   real(fp8), public :: qnB1cX

! Max cell-quotum Phosphate/Carbon of bacteria [mmol P/mg C]
   real(fp8), public :: qpB1cX

! Michaelis-Menten constant for Phosphate limitation [mmol P/m^3]
   real(fp8), public :: chB1pX

! Michaelis-Menten constant for Nitrate limitation [mmol N/m^3]
   real(fp8), public :: chB1nX

! Specific dissolution of labile DOM to Inorganic nutrients [1/d]
   real(fp8), public :: sr1n4X

! Specific dissolution of labile DOM to Inorganic nutrients [1/d]
   real(fp8), public :: sr1n1X

! Specific nitrification rate at temperature and silt
! concentration [1/d/m^3]
   real(fp8), public :: sn4n3X

! Silt conceentration at which relative rate of nitrification is 1%
   real(fp8), public :: cessX

! Carbon/Nitrogen ratio (Redfield)
   real(fp8), public :: redfieldX

! Specific rate for breakdown of semi-labile to labile DOM [1/d]
   real(fp8), public :: rR2R1X

! Remineralisation of small POM to DOM [1/d]
   real(fp8), public :: sr4r1

! Remineralisation of medium size POM to DOM [1/d]
   real(fp8), public :: sr6r1

! Remineralisation of large POM to DOM [1/d]
   real(fp8), public :: sr8r1

! Fraction of semi-labile DOC available to Bacteria
   real(fp8), public :: rR2B1X

! Fraction of semi-refractory DOC available to Bacteria
   real(fp8), public :: rR3B1X

! Fraction in proportion to activity respiration of bacterial
! uptake converted to semi-refractory DOC
   real(fp8), public :: frB1R3

! Cubic Michaelis-Menten constant for oxygenic control of
! nitrogen transformation puR4_B1X: Fraction of small size
! POM available for bacteria [mmol O2/m^3]
   real(fp8), public :: chN3oX

! Switch for bacteria nutrient limitation (0=minimum of
! inorganic and organic availability,1=additive availabiility)
   integer, public :: iswblimX

! Specific scavenging rate for iron [1/d]
   real(fp8), public :: fsinkX

! Activity respiration
   real(fp8), public :: rraB1
!
! !LOCAL VARIABLES:
   real(fp8) :: etB1,eO2B1,rutB1,rumB1,rugB1,ruRPRD
   real(fp8) :: Nlim, Plim
   real(fp8), parameter :: onedayX = 1._fp8
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise parameters
!
! !DESCRIPTION:
! Initialises general pelagic parameters from namelist
! \texttt{bacteriaParameters}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_bacteria_parameters
!
! !LOCAL VARIABLES:
   namelist/bacteriaParameters/sumB1X,chdB1oX,puR4_B1X,puR6_B1X,&
      puR8_B1X,puB1X,puB1oX,q10B1X,srsB1X,sdB1X,qnB1cX,qpB1cX,&
      chB1pX,chB1nX,redfieldX,sR1N4X,sR1N1X,sN4N3X,cessX,rR2R1X, &
      fsinkX,sr4r1,sr6r1,sr8r1,frB1R3,rR3B1X,rR2B1X,chN3oX
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading bacterial parameters'
   open(99,file='BioParams/bacteria.nml',status='old')
   read(99,nml=bacteriaParameters)

   close (99)

   return

   end subroutine set_bacteria_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Bacteria dynamics \label{sec:bacteriadynamics}
!
! !DESCRIPTION:
! This routine computes the bacteria growth dynamics.
! For the default bacteria sub-model bacterial growth is given by the
! minimum of potential uptake, regulated by temperature and limited by nutrient and oxygen conditions, and available substrate:
! \begin{equation}
! \mathcal{S}_{\textit{growth}}=\textit{min}\left(\overset{B}{g}_{\textit{max}}\overset{B}{l}_{T}\overset{B}{l}_{O_{2}}\textit{min}\left(\overset{B}{l}_{p},\overset{B}{l}_{n}\right)\frac{\overset{c}{B}}{\overset{c}{M}_{\textit{dis}}},1\right),
! \end{equation}
! \begin{equation}
! \left.\frac{\partial\overset{c}{B}}{\partial t}\right|_{\textit{growth}}=\mathcal{S}_{\textit{growth}}\overset{c}{M'}_{\textit{diss}},
! \end{equation}
! where $g_{max}$ is the maximum growth parameter. Growth is limitied
! by the following limitation factors:
!
! The temperature response factor $\overset{B}{l}_{T}$ is given by
! the equation:
! \begin{equation}
! \overset{B}{l}_{T}=\left.\overset{B}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-10}{10}}-\left.\overset{B}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-32}{3}},
! \end{equation}
!
! The oxygen limitation factor $\overset{B}{l}_{O_{2}}$ is given by:
! \begin{equation}
! \overset{B}{l}_{O_{2}}=\frac{s_{\textit{relO}_{2}}}{s_{\textit{relO}_{2}}+\overset{B}{h}_{O_{2}}},
! \end{equation}
! while phosphorus and nitrogen limitation $\overset{B}{l}_{p}$ , $\overset{B}{l}_{n}$
! are given by the equations:
! \begin{equation}
! \overset{B}{l}_{p}=\begin{cases}
! \textit{min}\left(\frac{N_{p}}{N_{p}+\overset{B}{h}_{p}},\frac{\overset{p}{M}_{dis}}{\overset{p}{M}_{dis}+\overset{B}{h}_{p}}\right) & \textrm{if }\mathtt{ISWBlimX=1}\! \frac{N_{amm}+\overset{p}{M}_{dis}}{N_{amm}+\overset{p}{M}_{dis}+\overset{B}{h}_{p}} & \textrm{if }\mathtt{ISWBlimX=2}

! \end{cases}
! \end{equation}
! and analog:
! \begin{equation}
! \overset{B}{l}_{n}=\begin{cases}
! \textit{min}\left(\frac{N_{amm}}{N_{amm}+\overset{B}{h}_{n}},\frac{\overset{n}{M}_{dis}}{\overset{n}{M}_{dis}+\overset{B}{h}_{n}}\right) & \textrm{if }\mathtt{ISWBlimX=1}\! \frac{N_{amm}+\overset{n}{M}_{dis}}{N_{amm}+\overset{n}{M}_{dis}+\overset{B}{h}_{n}} & \textrm{if }\mathtt{ISWBlimX=2}

! \end{cases},
! \end{equation}
! where $\overset{B}{h}_{p,n}$ are the Michaelis-Menten constants for
! phosphorus and nitrogen limitation.
!
! Bacteria respiration is computed according to activity respiration as an
! investment of activity in growth depentent on the oxygen state and
! a basal part:
! \begin{equation}
! \left.\frac{\partial\overset{c}{B}}{\partial t}\right|_{\textit{resp}}=\left(1-\overset{B}{p}_{\textit{highO}_{2}}\overset{B}{l}_{O_{2}}-\overset{B}{p}_{\textit{lowO}_{2}}(1-\overset{B}{l}_{O_{2}})\right)\left.\frac{\partial\overset{c}{B}}{\partial t}\right|_{\textit{growth}}+\overset{B}{r}_{\textit{resp}}\overset{B}{l}_{T}\overset{c}{B'}\,,
! \end{equation}
! where $\overset{B}{r}_{\textit{resp}}$ is the specific basal respiration
! rate at rest and $\overset{B}{p}_{\textit{highO}_{2},\textit{lowO}_{2}}$
! are the bacterial efficiencies at high and low oxygen levels.
!
! Mortality is given as a constant fraction of bacteria biomass:
! \begin{equation}
! \left.\frac{\partial\overset{c}{B}}{\partial t}\right|_{\textit{mort}}=\overset{B}{p}_{\textit{mort}}\overset{c}{B}
! \end{equation}
! with $\overset{B}{p}_{\textit{mort}}$ being the specific mortality
! rate of bacteria.

! The resulting carbon fluxes are then computed according to:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{B}}^{\overset{\textit{\textit{c,p,n}}}{M}_{\textit{dis}}} & = & -\left.\frac{\partial\overset{\textit{\textit{c,p,n}}}{B}}{\partial t}\right|_{\textit{growth}}+\overset{B}{p}_{\textit{lab}}\left.\frac{\partial\overset{\textit{\textit{c,p,n}}}{B}}{\partial t}\right|_{\textit{mort}}\! \left.\mathcal{F}\right|_{\overset{c}{B}}^{\overset{c}{M}_{\textit{slab}}} & = & (1-\overset{B}{p}_{\textit{lab}})\left.\frac{\partial\overset{c}{B}}{\partial t}\right|_{\textit{mort}}\! \left.\mathcal{F}\right|_{\overset{c}{B}}^{G_{\textit{\textit{CO}}_{2}}} & = & \frac{1}{\mathcal{\mathbb{C}}}\left.\frac{\partial\overset{c}{B}}{\partial t}\right|_{\textit{resp}}\! \left.\mathcal{F}\right|_{G_{O_{2}}}^{\textit{sink}} & = & \overset{B}{p}_{O_{2}}\left.\frac{\partial\overset{c}{B}}{\partial t}\right|_{\textit{resp}}



! \end{eqnarray*}
! where $\overset{B}{p}_{\textit{dis}}$ is the fraction of fraction
! of labile dissolved organic matter production of bacteria and $\overset{B}{p}_{O_{2}}$
! is the parameter for converting respired carbon into oxygen consumption.
!
! The internal stochiometric relation of phosporus is then balanced
! according to:
! \begin{equation}
! \left.\mathcal{F}\right|_{N_{\textit{phos}}}^{\overset{p}{B}}=\begin{cases}
! \left(\overset{B}{q}_{\textit{p:c}}-\overset{B}{q}_{\textit{pcopt}}\right)\overset{c}{B}' & \text{\textrm{if} }\overset{B}{q}_{\textit{p:c}}\geqslant\overset{B}{q}_{\textit{pcopt}}\!  \left(\overset{B}{q}_{\textit{p:c}}-\overset{B}{q}_{\textit{pcopt}}\right)\overset{c}{B}\frac{N'_{\textit{phos}}}{N'_{\textit{phos}}+\overset{B}{h}_{p}} & \text{\textrm{if} }\overset{B}{q}_{\textit{p:c}}<\overset{B}{q}_{\textit{pcopt}}

! \end{cases}
! \end{equation}
! with $q_{\textit{ncopt}}$ being the optimal phosphorus to carbon
! quota of bacteria.
!
! For nitrgogen the internal stochiometric balance is computed according
! to:
! \begin{equation}
! \left.\mathcal{F}\right|_{N_{\textit{amm}}}^{\overset{p}{B}}=\begin{cases}
! \left(\overset{B}{q}_{\textit{n:c}}-\overset{B}{q}_{\textit{ncopt}}\right)\overset{c}{B}' & \text{\textrm{if} }\overset{B}{q}_{\textit{n:c}}\geqslant\overset{B}{q}_{\textit{ncopt}}\!  \left(\overset{B}{q}_{\textit{n:c}}-\overset{B}{q}_{\textit{ncopt}}\right)\overset{c}{B}\frac{N'_{\textit{amm}}}{N'_{\textit{amm}}+\overset{B}{h}_{n}} & \text{\textrm{if} }\overset{B}{q}_{\textit{n:c}}<\overset{B}{q}_{\textit{ncmopt}}

! \end{cases}
! \end{equation}
!
! A fully dynamic model of bacteria following the formulation in
! \citet{Polimene2006} is available as alternative to the
! default bacteria model. If it is applied (activated by
! the preprocessing option \texttt{1}) bacteria is feeding on
! particulate and dissolved organic matter:
! \begin{equation}
! \overset{\textit{c,p,n}}{\widetilde{M}}=\overset{\textit{\textit{c,p,n}}}{M'}_{\textit{dis}}+p_{\textit{Mslab}}\overset{\textit{c,p,n}}{M'}_{\textit{slab}}+p_{\textit{Msrefr}}\overset{\textit{c,p,n}}{M'}_{\textit{srefr}}+p_{\textit{Msmall}}\overset{\textit{c,p,n}}{M'}_{\textit{small}}+p_{\textit{Mmed}}\overset{\textit{c,p,n}}{M'}_{\textit{med}}+p_{\textit{Mlarge}}\overset{\textit{c,p,n}}{M'}_{\textit{large}}\,,
! \end{equation}
! where the parameters $p_{\textit{Mx}}$ give fractions of the respective
! substrate available to bacteria, so that the equation for specific
! bacteria growth becomes:
! \begin{equation}
! \mathcal{S}_{\textit{growth}}=\textit{min}\left(g_{\textit{max}}\overset{B}{l}_{T}\overset{B}{l}_{O_{2}}\frac{\overset{c}{B}}{\overset{c}{\widetilde{M}}},1\right).
! \end{equation}
!
! In this case the growth is not nutrient limited as the internal stochiometric
! quota of bacteria is balanced directly through the regulating fluxes
! described in the following. The internal regulation of carbon in excess
! is achieved by replacing the fluxes from bacteria to dissolved and
! semilabile matter with:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{B}}^{\overset{\textit{c,p,n}}{M}_{\textit{dis}}} & = & -\mathcal{S}_{\textit{growth}}\overset{\textit{c,p,n}}{M'}_{\textit{dis}}+\left.\frac{\partial\overset{\textit{c,p,n}}{B}}{\partial t}\right|_{\textit{mort}}\!  \left.\mathcal{F}\right|_{\overset{c}{B}}^{\overset{c}{M}_{\textit{slab}}} & = & -\mathcal{S}_{\textit{growth}}p_{\textit{Mslab}}\overset{c}{M'}_{\textit{slab}}+\textit{max}(0,\textit{max}(1-\frac{q_{\textit{p:c}}}{q_{\textit{pcopt}}},1-\frac{q_{\textit{n:c}}}{q_{\textit{ncopt}}})\overset{c}{B'}

! \end{eqnarray*}
!
! In addition to these fluxes semi-refractory matter is produced corresponding
! to a fraction $p_{\textit{srefr}}$ of the activity respiration:
! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{c}{B}}^{\overset{c}{M}_{\textit{srefr}}}=-\mathcal{S}_{\textit{growth}}p_{\textit{Msrefr}}\overset{c}{M}_{\textit{srefr}}+p_{\textit{srefr}}\left(1-\overset{B}{p}_{\textit{highO}_{2}}\overset{B}{l}_{O_{2}}-\overset{B}{p}_{\textit{lowO}_{2}}(1-\overset{B}{l}_{O_{2}})\right)\left.\frac{\partial\overset{c}{B}}{\partial t}\right|_{\textit{growth}}.
! \end{equation}
!
! The fluxes of particulate matter to bacteria are then computed according
! to:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{B}}^{\overset{\textit{c,p,n}}{M}_{\textit{small}}} & = & -\mathcal{S}_{\textit{growth}}p_{\textit{Msmall}}\overset{\textit{c,p,n}}{M'}_{\textit{small}}\!  \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{B}}^{\overset{\textit{c,p,n}}{M}_{\textit{med}}} & = & -\mathcal{S}_{\textit{growth}}p_{\textit{Mmed}}\overset{\textit{c,p,n}}{M'}_{\textit{med}}\quad.\! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{B}}^{\overset{\textit{c,p,n}}{M}_{\textit{large}}} & = & -\mathcal{S}_{\textit{growth}}p_{\textit{Mlarge}}\overset{\textit{c,p,n}}{M'}_{\textit{large}}


! \end{eqnarray*}
!
! !INTERFACE:
   subroutine bacteria_dynamics(I)
!
! !USES:
   use ersem_constants, only: CMass



   use pelagic_variables
   use pelagic_parameters, only: r1r2x
!
! !INPUT PARAMETERS:
   integer :: i ! TODO
!
! !LOCAL VARIABLES:
   real(fp8) :: fR1B1n,fR1B1p,fB1Rdp,fB1rdn
   real(fp8) :: sB1RD,sutB1,sugB1

   real(fp8) :: fB1R2c,sB1R2,fB1R1c,fR4B1n,fR6B1n
   real(fp8) :: fR8B1n,fR4B1p,fR6B1p,fR8B1p,fB1R3c

!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Temperature effect on pelagic bacteria:
   etB1 = q10B1X**((ETW(I)-10._fp8)/10._fp8) - q10B1X**((ETW(I)-32._fp8)/3._fp8)

   ! Prevailing Oxygen limitation for bacteria:
   eO2B1 = eO2mO2/( eO2mO2 + chdB1oX )

!..Potential nutrient limitation on bacterial growth after Anderson
!..bacterial mortality
   sB1RD=sdB1X*etB1
   fB1R1c = sB1RD*B1cP(I)
!..Total amount of substrate available
   sutB1 = 1. ! DOM-specific uptake rate
   ! rutB1 = sutB1*R1cP(I)
!..Potential uptake :
   rumB1 = sumB1X*etB1*eO2B1*B1c(I)
!..Actual uptake
   ! rugB1 = MIN(rumB1,rutB1)
   ! specific in substrate concentration:
   sugB1 = rumB1/max(rumB1/sutB1,R1cP(I)+R2cP(I)*rR2B1X+R3cP(I)*rR3B1X+R4cP(I)*sR4R1+R6cP(I)*sR6R1+R8cP(I)*sR8R1)
       ! = MIN(rumB1,rutB1)=MIN(rumB1/(R1cP+R2cP*rR2B1X,sutB1) avoid pot. div. by 0
   rugB1 = sugB1*(R1cP(I)+R2cP(I)*rR2B1X+R3cP(I)*rR3B1X+R4cP(I)*sR4R1+R6cP(I)*sR6R1+R8cP(I)*sR8R1)
   fRDBXc(I)= sugB1*(R1cP(I)+R2cP(I)*rR2B1X+R3cP(I)*rR3B1X)
   fRPBXc(I)= sugB1*(R4cP(I)*sR4R1+R6cP(I)*sR6R1+R8cP(I)*sR8R1)
   ! Respiration :
   ! Activity respiration
   rraB1 = rugB1 * ( 1._fp8 - puB1X*eO2mO2 - puB1oX*( 1._fp8 - eO2mO2 ) )
   ! Total respiration
   fB1O3c(I) = rraB1 + srsB1X * B1cP(I) * etB1
! fB1O3c(I) = ( 1._fp8 - puB1X*eO2mO2 - puB1oX*( 1._fp8 - eO2mO2 ) )&
! * rugB1 + srsB1X * B1cP(I) * etB1
! specific release of semilabile DOC
! fudge factor 1. as used in Polimene et al. AME 2006
   sB1R2=max(0._fp8,max(1._fp8-(qpB1c(I)/qpB1cX),1._fp8-(qnB1c(I)/qnB1cX)))*1._fp8
   fB1R2c=sB1R2*B1cP(I)
   fB1R3c=frB1R3*rraB1
   fB1RDc(I) = fB1R1c + fB1R2c + fB1R3c
   ! Net bacterial production
   netb1(I) = rugB1 - fB1o3c(I) - fB1RDc(I)
   if (netB1(I).gt.0._fp8) then
      BGE(I)=netB1(I)/rugB1
   else
      BGE(I)=0._fp8
   end if
   ! Source equations
   SB1c(I) = SB1c(I) + netb1(I)
   SR1c(I) = SR1c(I) + fB1R1c - sugB1*R1cP(I)
   SR2c(I) = SR2c(I) + fB1R2c - sugB1*R2cP(I)*rR2B1X
   SR3c(I) = SR3c(I) + fB1R3c - sugB1*R3cP(I)*rR3B1X
   SR4c(I) = SR4c(I) - sugB1*R4cP(I)*sR4R1
   SR6c(I) = SR6c(I) - sugB1*R6cP(I)*sR6R1
   SR8c(I) = SR8c(I) - sugB1*R8cP(I)*sR8R1
   SO3c(I) = SO3c(I) + fB1O3c(I)/CMass
   SO2o(I) = SO2o(I) - fB1O3c(I)*urB1_O2X
   ! Phosporous dynamics in bacteria
   if ((qpB1c(I) - qpB1cX).gt.0._fp8) then
      fB1N1p(I) = (qpB1c(I) - qpB1cX ) * B1cP(I) /onedayX ! release
   else
      fB1N1p(I) = (qpB1c(I) - qpB1cX ) * B1c(I) *&
         N1pP(I)/(N1pP(I)+chB1pX) /onedayX ! uptake
   end if
   ! Uptake of DOP
   fR1B1p = sugB1*R1pP(I)
   ! Flux of DOP from B1
   fB1RDp = sB1RD*B1pP(I)
   ! Source equations
   fR4B1p = sugB1*R4pP(I)*sR4R1
   fR6B1p = sugB1*R6pP(I)*sR6R1
   fR8B1p = sugB1*R8pP(I)*sR8R1
   SB1p(I) = SB1p(I) + fR4B1p + fR6B1p + fR8B1p
   SR4p(I) = SR4p(I) - fR4B1p
   SR6p(I) = SR6p(I) - fR6B1p
   SR8p(I) = SR8p(I) - fR8B1p
   SB1p(I) = SB1p(I) + fR1B1p - fB1N1p(I) - fB1RDp
   SN1p(I) = SN1p(I) + fB1N1p(I)
   SR1p(I) = SR1p(I) + fB1RDp - fR1B1p
   ! Nitrogen dynamics in bacteria........................................
   if ((qnB1c(I) - qnB1cX).gt.0._fp8) then
      fB1NIn(I) = (qnB1c(I) - qnB1cX ) * B1cP(I) /onedayX ! release
   else
      fB1NIn(I) = (qnB1c(I) - qnB1cX ) * B1c(I) * &
         N4nP(I)/(N4nP(I)+chB1nX) /onedayX ! uptake
   end if
   ! Uptake of DON
   fR1B1n = sugB1*R1nP(I)
   ! Flux of DON from B1
   fB1RDn = sB1RD*B1nP(I)
!..Source equations
   fR4B1n = sugB1*R4nP(I)*sR4R1
   fR6B1n = sugB1*R6nP(I)*sR6R1
   fR8B1n = sugB1*R8nP(I)*sR8R1
   SB1n(I) = SB1n(I) + fR4B1n + fR6B1n + fR8B1n
   SR4n(I) = SR4n(I) - fR4B1n
   SR6n(I) = SR6n(I) - fR6B1n
   SR8n(I) = SR8n(I) - fR8B1n
   SN4n(I) = SN4n(I) + fB1NIn(I)
   SB1n(I) = SB1n(I) + fR1B1n - fB1NIn(I) - fB1RDn
   SR1n(I) = sR1n(I) + fB1RDn - fR1B1n
   return
   end subroutine bacteria_dynamics
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Remineralisation dynamics \label{sec:remineralisation}
!
! !DESCRIPTION:
! In case of the default bacteria sub-model, the remineralisation of
! particulate matter into dissolved matter is parametrised as:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{M}_{\textit{small}}}^{\overset{\textit{c,p,n}}{M}_{\textit{dis}}} & = & q_{\textit{RFn}}\mathbb{C}q_{\textit{n:c}}r_{\textit{rsmall}}\overset{\textit{c,p,n}}{M'}_{\textit{small}}\!  \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{M}_{\textit{med}}}^{\overset{\textit{c,p,n}}{M}_{\textit{dis}}} & = & q_{\textit{RFn}}\mathbb{C}q_{\textit{n:c}}r_{\textit{rmed}}\overset{\textit{c,p,n}}{M'}_{\textit{med}\quad,}\! \left.\mathcal{F}\right|_{\overset{\textit{c,p,n}}{M}_{\textit{large}}}^{\overset{\textit{c,p,n}}{M}_{\textit{dis}}} & = & q_{\textit{RFn}}\mathbb{C}q_{\textit{n:c}}r_{\textit{rlarge}}\overset{\textit{c,p,n}}{M'}_{\textit{large}}
! \end{eqnarray*}
! where $r_{\textit{rx}}$ are the refernce rates for remineralistion
! of particulate organic mattter and $q_{\textit{RFn}}$ is the carbon
! to nitrogen Redfield ratio.
!
! Dissolved organic nutrients are remineralised according to:
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{p}{M}_{\textit{dis}}}^{N_{\textit{phos}}} & = & r_{\textit{prem}}\overset{p}{M'}_{\textit{dis}}\!  \left.\mathcal{F}\right|_{\overset{p}{M}_{\textit{dis}}}^{N_{\textit{amm}}} & = & r_{\textit{nrem}}\overset{n}{M'}_{\textit{dis}}
! \end{eqnarray*}
! where $r_{\textit{xrem}}$ are the specific remineralisation rates
! for nitrogen and phosphorus.
!
! Nitrification is a linear function of ammonium concentration regulated
! by temperaure, oxygen, and ph (if activated by \texttt{ISWPHX})
! according to the equation:
! \begin{equation}
! \left.\mathcal{F}\right|_{N_{\textit{amm}}}^{N_{\textit{nitr}}}=r_{\textit{nitr}}\overset{B}{l}_{T}l_{\textit{O2nitr}}l_{\textit{ph}}N'_{amm}
! \end{equation}
! where $\overset{B}{l}_{T}$ is the temperature response factor:
! \begin{equation}
! \overset{B}{l}_{T}=\left.\overset{B}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-10}{10}}-\left.\overset{B}{p}_{\mathrm{\mathit{q10}}}\right.^{\frac{T-32}{3}},
! \end{equation}
!
! $l_{\textit{O2nitr}}$ is the oxygen limitation factor for nitrification:
! \begin{equation}
! l_{\textit{O2nitr}}=\frac{G_{O_{2}}^{3}}{G_{o_{2}}^{3}+h_{n3}}
! \end{equation}
! with $h_{n3}$ being the cubic half-saturation constant for oxygen
! limitation of nitrification and $l_{\textit{ph}}$ is the pH-limitation
! factor for nitrification:
! \begin{equation}
! l_{\textit{ph}}=\textit{min}(2,\textit{max}(0,0.6111\textit{pH}-3.8889)).
! \end{equation}
!
! Breakdown of semi-labile to dissolved organic matter is parametrised
! according to:
! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{c}{M}_{\textit{slab}}}^{\overset{c}{M}_{\textit{dis}}}=r_{\textit{slab}}\overset{c}{M'}_{\textit{slab}}.
! \end{equation}
!
! In case of the fully dynamic bacteria sub-model, particulate and semi-labile matter is recycled
! explicitly through bacteria and consequently the fluxes $\left.\mathcal{F}\right|_{\overset{c,p,n}{M}_{\textit{small,med,large,slab}}}^{\overset{c,p,n}{M}_{\textit{dis}}}$
! do not exist.
!
! In the case of the sub-model with fully parameterised bacteria dynamics,
! the \texttt{bacteria\_dynamics} routine is not called.
! To emulate the temperature dependence of the bacteria metabolics,
! the response factor $\overset{B}{l}_{T}$ is multiplied on the remineralisation
! fluxes above. In addition bacteria respiration is parametrised according
! to:
! \begin{equation}
! \left.\mathcal{F}\right|_{\overset{c}{M}_{\textit{dis}}}^{G_{\textit{CO}_{2}}}=r_{\textit{prem}}\overset{c}{M'}_{\textit{dis}}.
! \end{equation}
!
! If iron is used (pre-preocessing option \texttt{1}), particulate
! iron is recycled to dissolved inorganic iron as
!
! \begin{eqnarray*}
! \left.\mathcal{F}\right|_{\overset{c}{M}_{\textit{med}}}^{N_{\textit{fe}}} & = & r_{\textit{fmed}}\overset{f}{M'}_{\textit{med}}\!  \left.\mathcal{F}\right|_{\overset{c}{M}_{\textit{small}}}^{N_{\textit{fe}}} & = & r_{\textit{fsmall}}\overset{f}{M'}_{\textit{small}}.
! \end{eqnarray*}
!
! In addition, scavenging of iron is accounted for by removing dissolved
! inorganic iron according to the formula:
! \begin{equation}
! \left.\mathcal{F}\right|_{N_{\textit{fe}}}^{\textit{sink}}=r_{\textit{fsink}}\textit{max}\left(0,N'_{\textit{fe}}-0.6\right).
! \end{equation}
!
! !INTERFACE:
   subroutine do_remineralisation(I)
! USES:
   use ersem_constants, only: CMass
   use ersem_variables, only: iswPHx
   use pelagic_variables
!
! !INPUT PARAMETERS:
   integer :: I
!
! !LOCAL VARIABLES:e
   real(fp8) :: ruRPRD, Fph
   real(fp8) :: fr4r1,fr6r1,fr8r1,fn4n3n
   real(fp8) :: etRemin
   real(fp8) :: o2state
   real(fp8) :: fR4N7f,fR6N7f,n7fsink
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Fluxes from particulate to dissolved
   etRemin = q10B1X**((ETW(I)-10._fp8)/10._fp8) - q10B1X**((ETW(I)-32._fp8)/3._fp8)
   ! Mineralisation of DOP to PO4
   fR1N1p(I) = sR1N1X * R1pP(I)
   ! Mineralisation of DON to Nh4
   fR1NIn(I) = sR1N4X * R1nP(I)
   ! Remineralization of particulate iron to Fe
   fr6N7f=sr6r1*R6fP(I)
   fr4N7f=sr4r1*R4fP(I)
   ! Sink of Fe
   ! This term takes into account the scavenging due to hydroxide
   ! precipitation and it is supposed to be regulateted by a threshold
   ! concentration (0.6 nM). See Aumont et al., 2003 (GBC) and Vichi
   ! et al., 2007 (JMS) for references. (Luca, 12/08)
   n7fsink=fsinkX*max(0._fp8,N7fP(I)-0.6_fp8)
    !..Source equations
   SR1p(I) = SR1p(I) - fR1N1p(I)
   SR1n(I) = SR1n(I) - fR1NIn(I)
   SN1p(I) = SN1p(I) + fR1N1p(I)
   SN4n(I) = SN4n(I) + fR1NIn(I)
   SR6f(I)=SR6f(I)-fr6n7f
   SR4f(I)=SR4f(I)-fr4n7f
   SN7f(I)=SN7f(I)+fr6n7f-n7fsink+fr4n7f
   ! Oxygen state for nitrogen species transformation
   o2state= O2o(I)**3/(O2o(I)**3 + chN3oX) ! half saturation
   ! Nitrification..
   ! Ph influence on nitrification - empirical equation
   ! use(1) or not(2)
   fN4N3n = sN4N3X * N4nP(I) * etRemin * o2state
   if (ISWphx.eq.1) then
      Fph = MIN(2._fp8,MAX(0._fp8,0.6111_fp8*phx(I)-3.8889_fp8))
      fN4N3n = fN4N3n * Fph
   end if
   nitrN(I)=fN4N3n
   SN3N(I) = SN3n(I) + fN4N3n
   SN4N(I) = SN4n(I) - fN4N3n
   ! Breakdown of semi labile to labile DOM
   return
   end subroutine do_remineralisation
!
!EOC
!-----------------------------------------------------------------------
   end module bacteria
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
