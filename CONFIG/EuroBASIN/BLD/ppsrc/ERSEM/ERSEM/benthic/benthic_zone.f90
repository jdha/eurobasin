

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: The benthic system
!
! !DESCRIPTION:
! This module contains procedures which coordinate the update of the
! benthic system.\\[\baselineskip]
!
! !INTERFACE:
   module benthic_zone
!
! !USES:
   use ersem_constants, only: fp8
   use benthic_variables

   implicit none

! Default all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_benthic_zone_parameters, do_benthic_zone_allocations, &
      benthic_zone_dynamics
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise parameters
!
! !DESCRIPTION:
! Initialise benthic zone parameters from namelist \texttt{benthosParameters}.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine set_benthic_zone_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU
   use pelagic_variables, only: eturx
   use ben_gen
   use benthic_parameters
!
! !LOCAL VARIABLES:
   namelist/benthosParameters/q10h1x,suQ6fH1X,suQ6sH1X,suQ7H1X,&
      suQ1H1X,purH1X,srH1X,ddH1X,sdH1X,puincH1X,&
      pdH1Q1X,pue6H1Q1X,pue7H1Q1X,q10H2X,suQ6fH2X,suQ6sH2X,suQ7H2X,&
      purH2X,srH2X,ddH2X,sdH2X,puincH2X,&
      q10Y2X,suY2X,huY2X,luY2X,pueY2X,purY2X,pudilY2X,srY2X,sdY2X,&
      dQ6Y2X,pueQ6Y2X,rlO2Y2X,hO2Y2X,sdmO2Y2X,sdcY2X,xdcY2X,xclY2X,&
      xcsY2X,xchY2X,q10Y3X,dwatY3X,suY3X,huY3X,luY3X,pueY3X,purY3X,&
      pudilY3X,srY3X,sdY3X,dQ6Y3X,pueQ6Y3X,rlO2Y3X,hO2Y3X,xcly3X, &
      xcsy3X,xchy3X,sdmO2Y3X,sdcY3X,xdcY3X,q10Y4X,suY4X,huY4X,luY4X,&
      pueY4X,purY4X,pudilY4X,srY4X,sdY4X,dQ6Y4X,pueQ6Y4X,rlO2Y4X,&
      hO2Y4X,sdmO2Y4X,sdcY4X,xdcY4X,hturX,pturY2X,dturX,mturX,&
      hirrX,pirrY2X,pirrY4X,mirrX,irr_minX,puH1Y2X,puH2Y2X,puY4Y2X,&
      puQ6Y2X,puP1Y3X,puP2Y3X,puP3Y3X,puR6Y3X,puQ6Y3X,puH1Y3X,puH1Y4X,&
      puH2Y4X,puY4Y4X,puQ6Y4X,reminQ1X,reminQ6X,reminQ7X,pQIN3X,&
      pe_R1P1X,pe_R1P2X,pe_R1P3X,pe_R1P4X,pe_R7P1X,pe_R7P2X,pe_R7R6X,&
      d_totX,qPWX,EturX,qnYIcX,qpYIcX,qnHIcX,qpHIcX
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading general benthic parameters'
   open(99,file='BioParams/benthos.nml',status='old')
   read(99,nml=benthosParameters)

   close(99)

   return

   end subroutine set_benthic_zone_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Allocate memory
!
! !DESCRIPTION:
! Allocates benthic non-state arrays.\\[\baselineskip]
!
! !INTERFACE:
   subroutine do_benthic_zone_allocations
!
! !USES:
   use allocation_helpers, only: allocerr, allocateErsemDouble
   use benthic_variables, only: n_compben
   use ben_gen, only: f_Q1,f_Q6,f_Q7,fatkg,fatkh,&
      fbtkg,fh1q6,fh1q7,fh2q6, &
      fh2q7,fq17q7,fy2q6,fy3q6,fy4q6,gq6h2,gq7h2,jmn, &
      pm1x,porox
!
! !LOCAL VARIABLES:
   integer :: ialloc
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Ensure, for the case that N_Comp is real, that the lower bound
   ! is also 0d0, otherwise array bound checking generates errors.
   lbben = min(1,N_Compben)

   call allocateErsemDouble(poroX,'poroX',lbben,N_Compben)

   call allocateErsemDouble(pM1X,'pM1X',lbben,N_Compben)

   call allocateErsemDouble(fY2Q6,'fY2Q6',1,4)

   call allocateErsemDouble(fY3Q6,'fY3Q6',1,4)

   call allocateErsemDouble(fY4Q6,'fY4Q6',1,4)

   call allocateErsemDouble(fH1Q6,'fH1Q6',1,4)

   call allocateErsemDouble(fH2Q6,'fH2Q6',1,4)

   call allocateErsemDouble(f_Q6,'f_Q6',1,4)

   call allocateErsemDouble(f_Q1,'f_Q1',1,4)

   call allocateErsemDouble(fQ17Q7,'fQ17Q7',1,4)

   call allocateErsemDouble(jMN,'jMN',1,7)

   call allocateErsemDouble(fATKG,'fATKG',1,7)

   call allocateErsemDouble(fBTKG,'fBTKG',1,7)

   call allocateErsemDouble(fH1Q7,'fH1Q7',1,4)

   call allocateErsemDouble(fH2Q7,'fH2Q7',1,4)

   call allocateErsemDouble(f_Q7,'f_Q7',1,4)

   call allocateErsemDouble(fATKH,'fATKH',1,7)

   call allocateErsemDouble(gQ6H2,'gQ6H2',1,4)

   call allocateErsemDouble(gQ7H2,'gQ7H2',1,4)

   return

   end subroutine do_benthic_zone_allocations
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthic zone dynamics
!
! !DESCRIPTION:
! The main subroutine governing the benthic components of the model.
! Calls routines dealing with: benthic biology, settling,
! bioturbation and irrigation, and benthic nutrients.\\[\baselineskip]
!
! !INTERFACE:
   subroutine benthic_zone_dynamics
!
! !USES:
   use pelagic_variables, only: n_comp, N1p, N4n, irr_enh, ibenx, &
      benthicIndex
   use ben_nut_oldb
   use ben_phys, only: settling,resuspension,benthic_morfology, &
      Bioturbation,Burying,Refractory_Distribution, &
      Reset_f_QX
   use ben_oldb, only: BenthicDistribution,diff1,diff2,diff3
   use benthic_zoa, only: meiobenthos_dynamics, &
      suspension_feeder_dynamics, deposit_feeder_dynamics
   use benthic_bacteria, only: aerobic_bacteria_dynamics, &
      anaerobic_bacteria_dynamics
   use ben_gen, only: fATKG,pQIN3X,reminQ1X,reminQ6X,&
      reminQ7X,pqin3x,f_q1,f_q6,f_q7,fatkg,fbtkg,fh1q6,&
      fh1q7,fh2q6,fh2q7,fq17q7,fy2q6,fy3q6,fy4q6,jmn
   use ben_gen, only: calc_nutrient_fluxes,init_nutrient_fluxes
   use ben_phys, only: detritus_distribution
!
! !LOCAL VARIABLES:
! real(fp8) :: f_Q7(4),f_Q6(4),f_Q1(4)
! real(fp8) :: fQ17Q7(4), fH1Q7(4), fH2Q7(4)
! real(fp8) :: fY2Q6(4), fY3Q6(4), fY4Q6(4)
! real(fp8) :: fH1Q6(4), fH2Q6(4)
! real(fp8) :: fBTKG(7),fATKG(7)
   real(fp8) :: vM1,vM4,vM11, poro
   real(fp8) :: fluxn, fluxp
   integer :: i,k
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Loop over each compartment...........................................
   do I = 1,N_COMP

      if ( ibenX(I) .gt. 0 ) then

         ! Benthic counter K
          K=benthicIndex(I)

         ! Calculate overview variables.......................................
         call Calc_overview

         ! settling (stored in f_QX):
         call Reset_f_QX ( K )

         ! Call either benthic returns module or main benthic modules...........
         ! benthic returns (ibenX = 1)
         ! Oldenburgh Benthic Model (ibenX = 2)
         ! no benthos, remineralisation in lowest pelagic box (ibenX = 3)
         if ( ibenX(I) .eq. 1 ) then

            call settling( I,K )

            call IronDynamics(I,K)


            call bl2cDynamics(I,K)

            call Benthic_returns(I,K)

            ! Resuspension
            call Resuspension( I,K )

         else if ( ibenX(i).eq.3) then

            call FloorRemineralisation(I)

         else if ( ibenX(i) .eq. 2) then

             call settling( I,K )

             ! Simple iron return dynamics.........................................
             call IronDynamics(I,K)


             call bl2cDynamics(I,K)


             ! Compute pseudo nutrient profiles for ammonium and phosphate:
             ! layer contents are scalars
             ! -> process whole benthos within k-loop!
             diff1 = EDZ_1X *Irr_enh(I)
             diff2 = EDZ_2X *Irr_enh(I)
             diff3 = EDZ_3X *Irr_enh(I)
             poro=benthic_morfology(1,K)
             vM1=poro*M1adsX
             vM11 = poro*M11adsX
             call BenthicDistribution(K,N1p(I),K1p(K), &
                vM1,vM1,vM11,k1,k11,k21)
             vM4=poro*M4adsX
             call BenthicDistribution(K,N4n(I),K4n(K), &
                vM4,vM4,vM4,k4,k14,k24)

             ! Compute fluxes......................................................
             ! Oxic layer biology.................................................
             call Init_nutrient_fluxes ( fBTKG, SK4n(K), SK1p(K), &
                       SK5s(K), SG2o(K), SG3c(K), SK3n(K), SG4n(K) )

             fluxn = SK14n
             fluxp = SK11p

             if (iswY2X .eq. 1 ) call deposit_feeder_dynamics(I,K)
             if (iswY3X .eq. 1 ) call suspension_feeder_dynamics(I,K)
             if (iswY4X .eq. 1 ) call meiobenthos_dynamics(I,K)
             if (iswH1X .eq. 1 ) call aerobic_bacteria_dynamics(I,K)

             call Calc_nutrient_fluxes ( fBTKG, SK4n(K), SK1p(K), &
                SK5s(K), SG2o(K), SG3c(K), SK3n(K), SG4n(K) )

             fluxn = SK14n - fluxn
             fluxp = SK11p - fluxp

             ! Anoxic layer biology...............................................

             call Init_nutrient_fluxes ( fATKG, SK14n, SK11p, &
                       SK5s(K), SK6e, SG3c(K), SK3n(K), SG4n(K) )
! call Init_nutrient_fluxes ( fATKH, SK14n, SK11p, &
! SK5s(K), SK6e, SG3c(K), SK3n(K), SG4n(K) )
             if (iswH2X .eq. 1 ) call anaerobic_bacteria_dynamics(I,K)
! call Calc_nutrient_fluxes_2 ( fATKG, gQ6H2, gQ7H2, &
! SK14n, SK11p, &
! SK5s(K), SK6e, SG3c(K), SK3n(K), SG4n(K) )
! call Calc_nutrient_fluxes_2 ( fATKH, gQ7H2, gQ6H2, &
! SK14n, SK11p, &
! SK5s(K), SK6e, SG3c(K), SK3n(K), SG4n(K) )
             call Calc_nutrient_fluxes ( fATKG, &
                     SK14n, SK11p, &
                     SK5s(K), SK6e, SG3c(K), SK3n(K), SG4n(K) )

             fATKG(1) = fATKG(1) + fluxn
             fATKG(2) = fATKG(2) + fluxp

             ! Bioirrigation, turbation and layers................................

             ! Diffusion enhancement and detritus penetration depth
             call Bioturbation (I,K)

             ! Burying of material beyond d_totX:
             call Burying (K)

             ! Changes to penetration depths Q7:
             call Refractory_Distribution ( K, fQ17Q7, fH1Q7, fH2Q7, f_Q7 )

             ! Benthic nutrients..................................................

             !fATKG(1) = fATKG(1)+fATKH(1)
             !fATKG(2) = fATKG(2)+fATKH(2)
             !fATKG(3) = fATKG(3)+fATKH(3)
             !fATKG(4) = fATKG(4)+fATKH(4)
             !fATKG(5) = fATKG(5)+fATKH(5)
             !fATKG(6) = fATKG(6)+fATKH(6)
             !fATKG(7) = fATKG(7)+fATKH(7)

             call Nutrient_DynamicsOL(I,K, fBTKG, fATKG, jMN, &
                                      Irr_enh(I))

             ! Aggregation of layers:
             SG2o(K) = SG2o(K) + SK6e
             SK6e = 0._fp8
             SK4n(K) = SK4n(K) + SK14n
             SK14n = 0._fp8
             SK1p(K) = SK1p(K) + SK11p
             SK11p = 0._fp8

             ! Resuspension
             call Resuspension( I,K )

             ! Changes to penetration depths Q6:
             call Detritus_distribution ( K, fY2Q6, fY3Q6, fY4Q6, &
                                          fH1Q6, fH2Q6, f_Q6 )

          end if

       end if

    end do

    ! End of main benthic program.......................................

    return

    end subroutine benthic_zone_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Calculate overview variables
!
! !DESCRIPTION:
! Calculates overview variables.\\[\baselineskip]
!
! !INTERFACE:
   subroutine calc_overview
!
! !LOCAL VARIABLES:
   integer :: K
!
! !TO DO:
! Presently all code commented
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
! YIc(K) = Y2c(K) + Y3c(K) + Y4c(K)
! YMc(K) = Y2c(K) + Y3c(K)

! HIc(K) = H1c(K) + H2c(K)

! Aero(K) = 0.0d0 - D1m(K)
! An(K) = 0.0d0 - D2m(K)

   return

   end subroutine calc_overview
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthic returns
!
! !DESCRIPTION:
! This subroutine models the returns into pelagic nutrients from
! benthic detritus as a simple benthic closure without full benthic
! dynamics. It replaces calls to the benthic biological and
! benthic nutrient routines and is activated by setting the switch
! \texttt{ibenXin} to $1$.\\[\baselineskip]
!
! !INTERFACE:
   subroutine benthic_returns(I,K)
!
! !USES:
   use ersem_constants, only: CMass
   use ersem_variables, only: eDbgU
   use pelagic_variables, only: wsiN1p, wsiN3n, wsiN4n,&
      wsiN5s, wsiO3c, pdepth
   use ben_gen, only: pQIN3X,reminq1x,reminq6x,reminq7x



!
! !INPUT PARAMETERS:
   integer,intent(in) :: i,k
!
! !LOCAL VARIABLES:
   real(fp8) :: b2p,p2b,flux
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   p2b = pdepth(I)
   b2p = 1._fp8/p2b

   flux = reminQ6X * Q6cP(K)
   wSOQ6c(K) = wSOQ6c(K) + flux
   wSIO3c(I) = wSIO3c(I) + flux/CMass*b2p

   flux = reminQ1X * Q1cP(K)
   wSOQ1c(K) = wSOQ1c(K) + flux
   wSIO3c(I) = wSIO3c(I) + flux/CMass*b2p

   flux = reminQ7X * Q7cP(K)
   wSOQ7c(K) = wSOQ7c(K) + flux
   wSIO3c(I) = wSIO3c(I) + flux/CMass*b2p

   flux = reminQ6X * Q6pP(K)
   wSOQ6p(K) = wSOQ6p(K) + flux
   wSIN1p(I) = wSIN1p(I) + flux*b2p

   flux = reminQ1X * Q1pP(K)
   wSOQ1p(K) = wSOQ1p(K) + flux
   wSIN1p(I) = wSIN1p(I) + flux*b2p

   flux = reminQ7X * Q7pP(K)
   wSOQ7p(K) = wSOQ7p(K) + flux
   wSIN1p(I) = wSIN1p(I) + flux*b2p

   flux = reminQ6X * Q6nP(K)
   wSOQ6n(K) = wSOQ6n(K) + flux
   wSIN3n(I) = wSIN3n(I) + flux*pQIN3X*b2p
   wSIN4n(I) = wSIN4n(I) + flux*(1._fp8-pQIN3X)*b2p

   flux = reminQ1X * Q1nP(K)
   wSOQ1n(K) = wSOQ1n(K) + flux
   wSIN3n(I) = wSIN3n(I) + flux*pQIN3X*b2p
   wSIN4n(I) = wSIN4n(I) + flux*(1._fp8-pQIN3X)*b2p

   flux = reminQ7X * Q7nP(K)
   wSOQ7n(K) = wSOQ7n(K) + flux
   wSIN3n(I) = wSIN3n(I) + flux*pQIN3X*b2p
   wSIN4n(I) = wSIN4n(I) + flux*(1._fp8-pQIN3X)*b2p

   flux = reminQ6X * Q6sP(K)
   wSOQ6s(K) = wSOQ6s(K) + flux
   wSIN5s(I) = wSIN5s(I) + flux*b2p
   return
   end subroutine benthic_returns
!
!EOC
!------------------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthic calcite dynamics
!
! !DESCRIPTION:
! This subroutine models the return of benthic detrital alkalinity
! into pelagic inorganic alkalinity. Dissolution of liths release
! 2 moles of alkalinity and 1 of DIC.\\[\baselineskip]
!
! !INTERFACE:
   subroutine bL2cDynamics(I,K)
!
! !USES:
   use calcification, only: bendiss
   use pelagic_variables, only: pvol, parea, benthicIndex, &
      wsibioalk, wsio3c
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i,k
!
! !LOCAL VARIABLES:
   real(fp8) :: b2p,p2b,flux
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   b2p = parea(I)/pvol(I)
   p2b = 1._fp8/b2p
   flux = bendiss * bl2c(K)
   WSObl2c(K)=WSObl2c(K)+flux
   wSIbioalk(I)= wSIbioalk(I) + 2._fp8*flux*b2p/12._fp8
   wSIO3c(I)=wSIO3c(I) + flux*b2p/12._fp8
   return
   end subroutine bL2cDynamics
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Iron dynamics
!
! !DESCRIPTION:
! This subroutine models the return of benthic detrital iron into
! pelagic inorganic iron. Iron is supposed to be remineralized at the
! same rates as C,N,P,Si in the benthic\_return model.\\[\baselineskip]
!
! !INTERFACE:
   subroutine IronDynamics(I,K)
!
! !USES:
   use pelagic_variables, only: wsiN7f, parea, pvol
   use ben_gen, only: pQIN3X,reminq1x,reminq6x,reminq7x
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i,k
!
! !LOCAL VARIABLES:
   real(fp8) :: b2p,p2b,flux
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   b2p = parea(I)/pvol(I)
   p2b = 1._fp8/b2p
   flux = reminQ6X * Q6fP(K)
   wSOQ6f(k)= wSOQ6f(k) + flux
   wSIN7f(I)= wSIN7f(I) + flux*b2p
   return
   end subroutine IronDynamics
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Floor remineralisation
!
! !DESCRIPTION:
! Routine for recycling of nutrients from organic matter in the lowest
! pelagic cell for usage without benthic sub-model (activated by
! setting the switch \texttt{ibenXin} to $3$).\\[\baselineskip]
!
! !INTERFACE:
   subroutine FloorRemineralisation(I)
!
! !USES:
   use ersem_constants, only: CMass
   use pelagic_variables, only: R1cP, R1nP, R1pP, &
      R4cP, R4nP, R4pP, &
      R6cP, R6nP, R6pP, R6sP, &
      R8cP, R8nP, R8pP, R8sP, &
      wsiN1p, wsiN3n, wsiN4n, wsiN5s, &
      wsiO3c, wsiR1c, &
      wsoR1c, wsoR1n, wsoR1p, &
      wsoR4c, wsoR4n, wsoR4p, &
      wsoR6c, wsoR6n, wsoR6p, wsoR6s, &
      wsoR8c, wsoR8n, wsoR8p, wsoR8s
   use pelagic_variables, only: R6fP, wsiN7f, wsoR6f
   use ben_gen, only: pQIN3X,reminq1x,reminq6x,reminq7x
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i
!
! !LOCAL VARIABLES:
   real(fp8) :: flux
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   flux = reminQ1X * R1cP(I)
   wSOR1c(I) = wSOR1c(I) + flux
   wSIO3c(I) = wSIO3c(I) + flux/CMass
   flux = reminQ1X * R1nP(I)
   wSOR1n(I) = wSOR1n(I) + flux
   wSIN3n(I) = wSIN3n(I) + flux*pQIN3X
   wSIN4n(I) = wSIN4n(I) + flux*(1._fp8-pQIN3X)
   flux = reminQ1X * R1pP(I)
   wSOR1p(I) = wSOR1p(I) + flux
   wSIN1p(I) = wSIN1p(I) + flux
   flux = reminQ6X * R4cP(I)
   wSOR4c(I) = wSOR4c(I) + flux
   wSIO3c(I) = wSIO3c(I) + flux/CMass
   flux = reminQ6X * R4nP(I)
   wSOR4n(I) = wSOR4n(I) + flux
   wSIN3n(I) = wSIN3n(I) + flux*pQIN3X
   wSIN4n(I) = wSIN4n(I) + flux*(1._fp8-pQIN3X)
   flux = reminQ6X * R4pP(I)
   wSOR4p(I) = wSOR4p(I) + flux
   wSIN1p(I) = wSIN1p(I) + flux
   flux = reminQ6X * R6cP(I)
   wSOR6c(I) = wSOR6c(I) + flux
   wSIO3c(I) = wSIO3c(I) + flux/CMass
   flux = reminQ6X * R6nP(I)
   wSOR6n(I) = wSOR6n(I) + flux
   wSIN3n(I) = wSIN3n(I) + flux*pQIN3X
   wSIN4n(I) = wSIN4n(I) + flux*(1._fp8-pQIN3X)
   flux = reminQ6X * R6pP(I)
   wSOR6p(I) = wSOR6p(I) + flux
   wSIN1p(I) = wSIN1p(I) + flux
   flux = reminQ6X * R6sP(I)
   wSOR6s(I) = wSOR6s(I) + flux
   wSIN5s(I) = wSIN5s(I) + flux
   flux = reminQ7X * R8cP(I)
   wSOR8c(I) = wSOR8c(I) + flux
   wSIO3c(I) = wSIO3c(I) + flux/CMass
   flux = reminQ7X * R8nP(I)
   wSOR8n(I) = wSOR8n(I) + flux
   wSIN3n(I) = wSIN3n(I) + flux*pQIN3X
   wSIN4n(I) = wSIN4n(I) + flux*(1._fp8-pQIN3X)
   flux = reminQ7X * R8pP(I)
   wSOR8p(I) = wSOR8p(I) + flux
   wSIN1p(I) = wSIN1p(I) + flux
   flux = reminQ7X * R8sP(I)
   wSOR8s(I) = wSOR8s(I) + flux
   wSIN5s(I) = wSIN5s(I) + flux
   flux = reminQ6X * R6fP(I)
   wSOR6f(I) = wSOR6f(I) + flux
   wSIN7f(I) = wSIN7f(I) + flux
   return
   end subroutine FloorRemineralisation
!
!EOC
!-----------------------------------------------------------------------
   end module benthic_zone
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
