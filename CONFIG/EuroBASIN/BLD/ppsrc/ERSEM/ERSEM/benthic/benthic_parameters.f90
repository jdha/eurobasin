

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Benthic parameters
!
! !DESCRIPTION:
! Module containing basic benthic parameters.\\[\baselineskip]
!
! !INTERFACE:
   module benthic_parameters
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is public
   public
!
! !PUBLIC DATA MEMBERS:
! Total depth of benthic layer system [m]
   real(fp8) :: d_totX

! Fraction of pore water in the sediment
   real(fp8) :: qpwX

! Maximal nitrogen to carbon ratio of benthic fauna [mmol N/mg C]
   real(fp8) :: qnyicX

! Maximal phospherus to carbon ratio of benthic fauna [mmol P/mg C]
   real(fp8) :: qpyicX

! Maximal nitrogen to carbon ratio of benthic bacteria [mmol N/mg C]
   real(fp8) :: qnhicX

! Maximal phospherus to carbon ratio of benthic bacteria [mmol P/mg C]
   real(fp8) :: qphicX

! Fraction of sedimented diatoms decomposing into benthic DOM
   real(fp8) :: pe_r1p1X

! Fraction of sedimented medium size phytoplankton decomposing
! into benthic DOM
   real(fp8) :: pe_r1p2X

! Fraction of sedimented small phytoplankton decomposing
! into benthic DOM
   real(fp8) :: pe_r1p3X

! Fraction of sedimented large phytoplankton decomposing
! into benthic DOM
   real(fp8) :: pe_r1p4X

! Fraction of sedimented diatoms decomposing into benthic
! refractory matter
   real(fp8) :: pe_r7p1X

! Fraction of sedimented medium size phytoplankton decomposing
! into benthic refractory matter
   real(fp8) :: pe_r7p2X

! Fraction of sedimented medium size POM turning into benthic POM
   real(fp8) :: pe_r7r6X
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team

! James Clark (PML) Jan 2014 - Created module benthic_parameters, and
! moved data members in from module global_declarations.
!
!EOP
!-----------------------------------------------------------------------

   end module benthic_parameters

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
