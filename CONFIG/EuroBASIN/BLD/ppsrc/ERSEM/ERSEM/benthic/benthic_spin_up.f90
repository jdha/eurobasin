

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Benthic spin up
!
! !DESCRIPTION:
! Module with infrastructure to spin-up the benthic sub-model to
! equilibrium state.\\[\baselineskip]
!
! !INTERFACE:
   module benSpinUp
!
! !USES:
   use ersem_constants, only: fp8
   use pelagic_variables
   use benthic_variables
   use sedimentation, only: set_sedimentation_velocities
   use ben_gen, only: porox,pm1x
   use benthic_zone, only: benthic_zone_dynamics
   use allocation_helpers

   use ncdfRestartErsem, only: ncdfRFile,ncdfWriteRestart,ccrst,cbrst


   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public benthicSpinUp

   type(ncdfRFile),public :: ncdfS

!
! !PRIVATE DATE MEMBERS:
   real(fp8),pointer,dimension(:,:) :: bbccb
   real(fp8) :: delt,mindelt,maxdelt
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Spin up the benthic system
!
! !DESCRIPTION:
! Computes benthic rates, computes the benthic-pelagic fluxes and
! integrates the benthic system keeping the pelagic state fixed.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine benthicSpinUp(nendBSU)
!
! !USES:
   use ersem_variables, only: strpidersem, eLogU, eDbgU, iswZeroControl
   use reset, only: reset_ersem

! !INPUT PARAMETERS:
   integer :: nendBSU
!
! !LOCAL VARIABLES:
   integer :: nitBSU,ialloc,m,i,k
   real(fp8) :: tres,mres,x
   integer, dimension(2) :: pres
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (any(Water)) then
      do I=1,n_comp
         ! For full benthos and analytic condition switch, reset benthic conditions.
         if (ibenX(I) .eq. 2 .AND. iswbenAnX .ne. 0) then
            k=benthicIndex(I)
            if (wdepth(K) .gt. 250._fp8) then
               x=250._fp8
            else
               X = wdepth(k)
            end if

            PoroX(k) = -3.E-09_fp8*x**3 + 1.E-06_fp8*x**2 + 0.0005_fp8*x + 0.3976_fp8
            PM1X(k) = 781.62_fp8*LOG(poroX(k)) + 792.5_fp8

            Y2c(k) = 8834.4_fp8*x**(-0.1651_fp8)
            Y3c(k) = -1300._fp8*LOG(X) + 8830.8_fp8
            Y4c(k) = 0.1689_fp8*x + 55.342_fp8
            K5s(k) = -0.7588_fp8*LOG(X) + 11.271_fp8
            K3n(k) = 0.0336_fp8*x**0.595_fp8
            K4n(k) = -11.65_fp8*LOG(X) + 84.126_fp8
            K1p(k) = 0.0021_fp8*x**2 - 0.1729_fp8*x + 20.124_fp8
            D9m(k) = 0.0495_fp8*x**0.0965_fp8
            D8m(k) = 8.E-06_fp8*x + 0.0155_fp8
            D7m(k) = 1E-05_fp8*x + 0.0155_fp8
            D6m(k) = 1E-05_fp8*x + 0.0232_fp8
            D5m(k) = 0.0484_fp8*x**0.1042_fp8
            H2c(k) = -156.98_fp8*LOG(X) + 1139.8_fp8
            H1c(k) = 0.61_fp8*X + 23.98_fp8
            Q6c(k) = -1069._fp8*LOG(X) + 10900._fp8
            Q6n(k) = -7.6368_fp8*LOG(X) + 78.564_fp8
            Q6p(k) = -0.545_fp8*LOG(X) + 6.0114_fp8
            Q6s(k) = -64.598_fp8*LOG(X) + 391.61_fp8
            Q1c(k) = 828.74_fp8*x**(-0.6246_fp8)
            Q1n(k) = 21.103_fp8*x**(-0.6882_fp8)
            Q1p(k) = 1.60278_fp8*x**(-0.6725_fp8)
            G2o(k) = 0.0294_fp8*x**0.5305_fp8
            D1m(k) = 0.0026_fp8*x**0.3286_fp8
            D2m(k) = 0.011_fp8*x**0.4018_fp8
            D3m(k) = 0.0486_fp8*x**0.103_fp8
            D4m(k) = 0.0484_fp8*x**0.104_fp8
            G3c(k) = 375._fp8
         end if
      end do

      allocate (bbccb(lbben:N_Compben,i_stateben),stat=ialloc)
      call allocerr ('bbccb',N_Compben-lbben+1,i_stateben,ialloc)

      nitBSU=0
      ! switch off depth intergation to maintain prescribed profile:
      itrXD1m=0
      itrXD2m=0
      itrXD3m=0
      itrXD4m=0
      itrXD5m=0
      itrXD6m=0
      itrXD7m=0
      itrXD8m=0
      itrXD9m=0

      if ( iswZeroControl .NE. 0 ) then
         do m=1,I_STATE
            do i=1,N_COMP
               cccP(i,m)=max(ccc(i,m)-ccc0(m),0._fp8)
            end do
         end do
      end if

      do while (nitBSU.le.nendBSU)

         if ( iswZeroControl .NE. 0 ) then
            do m=1,I_STATEBEN
               do i=1,N_COMPBEN
                  ccbP(i,m)=max(ccb(i,m)-ccb0(m),0._fp8)
               end do
            end do
         end if

         do I = 1, N_COMP
            call set_sedimentation_velocities(I)
         end do

         call benthic_zone_dynamics

         do m=1,i_stateben
            do i=1,n_compben
              sccb(i,m)=sccb(i,m)-wSOCCb(i,m)+wSICCb(i,m)
            end do
         end do

         call integrationEfw(nitBSU)
         call reset_ersem
         call residual(mres,tres,pres)

         if (mres.lt.1.e-15_fp8 .and. nitBSU .gt. 50) nendBSU=nitBSU

         if ((nitBSU.eq.nendBSU).OR.(nitBSU.eq.1)) then
            write(eDbgU,*) "Spin-up iteration ", nitBSU &

            , " on process ",strpidErsem &

            ,"- Total Residuum: ",tres, ", Max Residuum: ",mres &
            ," in state ",ccbstr(pres(2)),": ",ccb(pres(1),pres(2))
         end if
         nitBSU=nitBSU+1
      end do

      ! switch depth intergation back on:
      itrXD1m=2
      itrXD2m=2
      itrXD3m=2
      itrXD4m=2
      itrXD5m=2
      itrXD6m=2
      itrXD7m=2
      itrXD8m=2
      itrXD9m=2

      deallocate (bbccb,stat=ialloc)

   end if

   end subroutine benthicSpinUp
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Compute residual
!
! !DESCRIPTION:
! Conmputes total and maximum residual of benthic state with respect to
! previous state. Used as convergence criterium for benthic spin-up.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine residual(maxres,totres,maxpos)
!
! !INPUT PARAMETERS:
   real(fp8),intent(out) :: maxres,totres
   integer,dimension(2),intent(out) :: maxpos
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   maxres=maxval(abs(ccb-bbccb)/ccb)
   maxpos=maxloc(abs(ccb-bbccb)/ccb)
   totres=sum(abs(ccb-bbccb)/ccb)
!
   end subroutine residual
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Euler forward integration
!
! !DESCRIPTION:
! Euler-forward integration with time step adjustment.\\[\baselineskip]
!
! !INTERFACE:
   subroutine integrationEfw(nit)
!
! !USES:
   use ersem_variables, only: timestep
   use benthic_variables, only: i_stateben
   use reset, only: reset_ersem
!
! !INPUT PARAMETERS:
   integer,intent(in) :: nit
!
! !LOCAL VARIABLES:
   real(fp8),parameter :: eps=0._fp8
   real(fp8) :: min2D
   integer,parameter :: nmaxdelt=64
   integer :: j,nmin,nstep
   integer,dimension(2) :: blccc
!
! !REVISION HISTORY:
! Original author(s): Momme Butenschoen
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   nmin=0
   nstep=nmaxdelt
   bbccb=ccb
   delt=timestep

   TLOOP : do
      ! Integration benthos:
      do j=1,I_STATEBEN
         if (itrXccb(j) .gt. 0) then
            ccb(:,j) = ccb(:,j) + delt*sccb(:,j)
            where (ccb(:,j) .lt. ccb0(j))
               ccb(:,j)=ccb0(j)
            end where
         end if
      end do
      nmin=nmin+nstep

      ! Check for negative concentrations
      min2D=minval(ccb)
      if (min2D.lt.eps) then ! cut timestep
         write(6,*) ' ','==>Negative concentrations in benthic spin-up'
         blccc(:)=minloc(ccb)
         write(6,*) '  ', blccc
         write(6,*) '  ','Benthic Variable:',blccc(1),',',ccbstr(blccc(2))
         write(6,*) '  ','Old Value: ',bbccb(blccc(1),blccc(2)), &
                         ' Rate: ',sccb(blccc(1),blccc(2))
         write(6,*) '  ',' New Values: ',ccb(blccc(1),blccc(2))
         if (nstep.eq.1) then
            write(6,*) '  ', 'Necessary Time Step too small! Exiting...'
            write(6,*) '  ', 'EXIT at  iteration ',nit
            stop 'integration-efw'
         end if
         nstep=nstep/2
         nmin=0
         ccb=bbccb
         delt=nstep*mindelt
         write(6,*) '  ', 'Time Step cut! delt= ',delt,' nstep= ',nstep
      else
         if (nmin .eq. nmaxdelt) exit TLOOP
         call reset_ersem
         call benthic_zone_dynamics
      end if
   end do TLOOP

   end subroutine integrationEfw
!
!EOC
!-----------------------------------------------------------------------

   end module benSpinUp

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
