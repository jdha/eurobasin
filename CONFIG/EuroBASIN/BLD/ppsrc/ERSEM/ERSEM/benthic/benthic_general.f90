

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Benthic helper functions
!
! !DESCRIPTION:
! Module with general benthic parameters and benthic routines used
! across the benthic sub-module.\\[\baselineskip]
!
! !INTERFACE:
   module ben_gen
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_nut_morf_parameters,Respiration_fixed,partQ, &
      Init_Nutrient_Fluxes,Mortality, &
      Calc_Nutrient_Fluxes,AvQ7,AvQ6,Food_Partition
!
! !PUBLIC DATA MEMBERS:
! Sediment porosity
   real(fp8), public, pointer, dimension(:) :: poroX

! Sediment adsorption factor
   real(fp8), public, pointer, dimension(:) :: pm1X

! Regulating temperature factor Q10 for benthic aerobic bacteria
   real(fp8), public :: q10h1X

! Specific nutrient limited detritus uptake by benthic aerobic
! bacteria [1/d]
   real(fp8), public :: suq6fh1X

! Specific not nutrient limited detritus uptake by benthic
! aerobic bacteria [1/d]
   real(fp8), public :: suq6sh1X

! Specific not nutrient limited refractory matter uptake by benthic
! aerobic bacteria [1/d]
   real(fp8), public :: suq7h1X

! Specific DOC uptake by benthic aerobic bacteria [1/d]
   real(fp8), public :: suq1h1X

! Fraction of carbon uptake respired by benthic aerobic bacetria [1/d]
   real(fp8), public :: purh1X

! Specific rest respiration of benthic aerobic bacetria [1/d]
   real(fp8), public :: srh1X

! Michaelis-Menten constant for oxygen limitation through aerobic
! layer depth [1/m]
   real(fp8), public :: ddh1X

! Specific maximal mortality of benthic aerobic bacteria [1/d]
   real(fp8), public :: sdh1X

! Preference factor of nutrient content by benthic aerobic bacteria
   real(fp8), public :: puinch1X

! DOM-fraction of benthic aerobic bacteria mortality
   real(fp8), public :: pdh1q1X

! Excreted fraction of uptake of POM by benthic aerobic bacteria
   real(fp8), public :: pue6h1q1X

! Excreted fraction of uptake of refractory matter by benthic
! aerobic bacteria
   real(fp8), public :: pue7h1q1X

! Regulating temperature factor Q10 for benthic anaerobic bacteria
   real(fp8), public :: q10h2X

! Specific nutrient limited POM uptake by benthic anaerobic
! bacteria [1/d]
   real(fp8), public :: suq6fh2X

! Specific not nutrient limited POM uptake by benthic anaerobic
! bacteria [1/d]
   real(fp8), public :: suq6sh2X

! Specific not nutrient limited refractory matter uptake by benthic
! anaerobic bacteria [1/d]
   real(fp8), public :: suq7h2X

! Fraction of carbon uptake respired by benthic anaerobic
! bacetria [1/d]
   real(fp8), public :: purh2X

! Specific rest respiration of benthic anaerobic bacetria [1/d]
   real(fp8), public :: srh2X

! Michaelis-Menten constant for oxygen limitation through anaerobic
! layer depth [1/m]
   real(fp8), public :: ddh2X

! Specific maximal mortality of benthic anaerobic bacteria
   real(fp8), public :: sdh2X

! Preference factor of nutrient content by benthic anaerobic bacteria
   real(fp8), public :: puinch2X

! Regulating temperature factor Q10 for deposit feeders
   real(fp8), public :: q10y2X

! Specific maximal uptake by deposit feeders at reference
! temperature [1/d]
   real(fp8), public :: suy2X

! Michaelis-Menten constant for gross carbon uptake by deposit
! feeders [mg C/m^2]
   real(fp8), public :: huy2X

! Michaelis-Menten constant for food species uptake by deposit
! feeders [mg C/m^2]
   real(fp8), public :: luy2X

! Excreted fraction of fixed (carbon) uptake by deposit feeders
   real(fp8), public :: puey2X

! Respired fraction of uptake by deposit feeders
   real(fp8), public :: pury2X

! Relative nutrient content in the fecal pellets excreted by
! deposit feeders
   real(fp8), public :: pudily2X

! Specific rest respiration of deposit feeders at reference
! temperature [1/d]
   real(fp8), public :: sry2X

! Specific mortality of deposit feeders at reference
! temperature [1/d]
   real(fp8), public :: sdy2X

! Lower depth limit of POM available to deposit feeders [m]
   real(fp8), public :: dq6y2X

! Excreted fraction of (carbon) uptake of POM by deposit
! feeders
   real(fp8), public :: pueq6y2X

! Minimum treshold of oxygen required by deposit feeders [mmol/m^3]
   real(fp8), public :: rlo2y2X

! Michaelis-Menten constant for oxygen limitation of deposit
! feeders [mmol/m^3]
   real(fp8), public :: ho2y2X

! Specific maximal additional mortality of deposit feeders due to
! oxygen stress [1/d]
   real(fp8), public :: sdmo2y2X

! Specific maximal additional mortality of deposit feeders induced by
! cold temperature [1/d]
   real(fp8), public :: sdcy2X

! e-folding temperature factor of cold mortality response of deposit
! feeders [1/degree C]
   real(fp8), public :: xdcy2X

! Lower treshold for crowding effect on food uptake by deposit
! feeders [mg C/m^2]
   real(fp8), public :: xcly2X

! Michaelis-Menten constant for the impact of crowding on deposit
! feeders [mg C/m^2]
   real(fp8), public :: xcsy2X

! Concentration determining asymptotic treshold of crowding limitation
! (-> xchY2Xi/(1+xchY2Xi) for Y2c-> inf) [mg C/m^2]
   real(fp8), public :: xchy2X

! Regulating temperature factor Q10 for suspension feeders
   real(fp8), public :: q10y3X

! Water layer available to suspension feeders [1/m]
   real(fp8), public :: dwaty3X

! Specific maximal uptake by suspension feeders at reference
! temperature [1/d]
   real(fp8), public :: suy3X

! Michaelis-Menten constant for gross carbon uptake by suspension
! feeders [mg C/m^2]
   real(fp8), public :: huy3X

! Michaelis-Menten constant for food species uptake by suspension
! feeders [mg C/m^2]
   real(fp8), public :: luy3X

! Excreted fraction of fixed (carbon) uptake by suspension feeders
   real(fp8), public :: puey3X

! Respired fraction of uptake by suspension feeders
   real(fp8), public :: pury3X

! Relative nutrient content in the fecal pellets excreted by suspension
! feeders
   real(fp8), public :: pudily3X

! Specific rest respiration of suspension feeders at reference
! temperature [1/d]
   real(fp8), public :: sry3X

! Specific mortality of suspension feeders at reference temperature [1/d]
   real(fp8), public :: sdy3X

! Lower depth limit of benthic POM available to suspension feeders [m]
   real(fp8), public :: dq6y3X

! Excreted fraction of (carbon) uptake of benthic POM by suspension
! feeders
   real(fp8), public :: pueq6y3X

! Minimum treshold of oxygen required by suspension feeders [mmol/m^3]
   real(fp8), public :: rlo2y3X

! Michaelis-Menten constant for oxygen limitation of suspension
! feeders [mmol/m^3]
   real(fp8), public :: ho2y3X

! Lower treshold for shading effect on food uptake by suspension
! feeders [mg C/m^2]
   real(fp8), public :: xcly3X

! Michaelis-Menten constant for the impact of shading on suspension
! feeders [mg C/m^2]
   real(fp8), public :: xcsy3X

! Concentration determining asymptotic treshold of shading limitation
! (-> xchY3Xi/(1+xchY3Xi) for Y3c-> inf) [mg C/m^2]
   real(fp8), public :: xchy3X

! Specific maximal additional mortality of suspension feeders due to
! oxygen stress [1/d]
   real(fp8), public :: sdmo2y3X

! Specific maximal additional mortality of suspension feeders induced
! by cold temperature [1/d]
   real(fp8), public :: sdcy3X

! e-folding temperature factor of cold mortality response of suspension
! feeders [1/degree C]
   real(fp8), public :: xdcy3X

! Regulating temperature factor Q10 for meiobenthos
   real(fp8), public :: q10y4X

! Specific maximal uptake of meiobenthos at reference
! temperature [1/d]
   real(fp8), public :: suy4X

! Michaelis-Menten constant for gross carbon uptake by
! meiobenthos [mg C/m^2]
   real(fp8), public :: huy4X

! Michaelis-Menten constant for food species uptake by
! meiobenthos [mg C/m^2]
   real(fp8), public :: luy4X

! Excreted fraction of fixed (carbon) uptake by meiobenthos
   real(fp8), public :: puey4X

! Respired fraction of uptake by meiobenthos
   real(fp8), public :: pury4X

! Relative nutrient content in the fecal pellets excreted by
! suspension feeders
   real(fp8), public :: pudily4X

! Specific rest respiration of meiobenthos at reference
! temperature[1/d]
   real(fp8), public :: sry4X

! Specific mortality of meiobenthos at reference temperature [1/d]
   real(fp8), public :: sdy4X

! Lower depth limit of POM matter available to meiobenthos [m]
   real(fp8), public :: dq6y4X

! Excreted fraction of (carbon) uptake of POM matter by
! suspension feeders
   real(fp8), public :: pueq6y4X

! Minimum treshold of oxygen required by meiobenthos [mmol/m^3]
   real(fp8), public :: rlo2y4X

! Michaelis-Menten constant for oxygen limitation of meiobenthos
! [mmol/m^3]
   real(fp8), public :: ho2y4X

! Specific maximal additional mortality of meiobenthos due to
! oxygen stress [1/d]
   real(fp8), public :: sdmo2y4X

! Specific maximal additional mortality of meiobenthos induced by
! cold temperature [1/d]
   real(fp8), public :: sdcy4X

! e-folding temperature factor of cold mortality response of
! meiobenthos [1/degree C]
   real(fp8), public :: xdcy4X

! Michaelis-Menten constant for bioturbation [mg C/m^2/d]
   real(fp8), public :: hturX

! Relative contribution of deposit feeders to bioturbation
   real(fp8), public :: ptury2X

! Bioturbation depth [m]
   real(fp8), public :: dturX

! Maximal relative turbation enhancement
   real(fp8), public :: mturX

! Michaelis-Menten constant for bioirrigation [mg C/m^2/d]
   real(fp8), public :: hirrX

! Relative contribution of deposit feeders to bioirrigation
   real(fp8), public :: pirry2X

! Relative contribution of meiobenthos to Bioirrigation
   real(fp8), public :: pirry4X

! Maximal relative diffusion enhancement due to bioirrigation
   real(fp8), public :: mirrX

! Minimum diffusion enhancement through bio irrigation
   real(fp8), public :: irr_minX

! Food preference of deposit feeders for aerobic bacteria
   real(fp8), public :: puh1y2X

! Food preference of deposit feeders for anaerobic bacteria
   real(fp8), public :: puh2y2X

! Food preference of deposit feeders for meiobenthos
   real(fp8), public :: puy4y2X

! Food preference of deposit feeders for POM
   real(fp8), public :: puq6y2X

! Food preference of suspension feeders for diatoms
   real(fp8), public :: pup1y3X

! Food preference of suspension feeders for medium size phytoplnkton
   real(fp8), public :: pup2y3X

! Food preference of suspension feeders for small size phytoplankton
   real(fp8), public :: pup3y3X

! Food preference of suspension feeders for medium size POM
   real(fp8), public :: pur6y3X

! Food preference of suspension feeders for benthic POM
   real(fp8), public :: puq6y3X

! Food preference of suspension feeders for aerobic bacteria
   real(fp8), public :: puh1y3X

! Food preference of meiobenthos for aerobic bacteria
   real(fp8), public :: puh1y4X

! Food preference of meiobenthos for anaerobic bacteria
   real(fp8), public :: puh2y4X

! Food preference of meiobenthos for meiobenthos
   real(fp8), public :: puy4y4X

! Food preference of meiobenthos for POM
   real(fp8), public :: puq6y4X

! Remineralisation rate of benthic DOM (benthic return only) [1/d]
   real(fp8), public :: reminq1X

! Remineralisation rate of benthic POM (benthic organic matter
! benthic return only) [1/d]
   real(fp8), public :: reminq6X

! Remineralistion rate of benthic refractory matter (benthic
! return only) [1/d]
   real(fp8), public :: reminq7X

! Fraction of remineralisation of benthic matter going to
! nitrate (benthic return only)
   real(fp8), public :: pqin3X

   real(fp8), public, pointer, dimension(:) :: fY2Q6,fY3Q6
   real(fp8), public, pointer, dimension(:) :: fY4Q6
   real(fp8), public, pointer, dimension(:) :: fH1Q6, fH2Q6, f_Q6
   real(fp8), public, pointer, dimension(:) :: f_Q1,fQ17Q7
   real(fp8), public, pointer, dimension(:) :: jMN, fATKG, fBTKG
   real(fp8), public, pointer, dimension(:) :: fH1Q7, fH2Q7, f_Q7,fATKH
   real(fp8), public, pointer, dimension(:) :: gQ6H2, gQ7H2
   real(fp8), public :: sm4_m3nX,sq6s_m5sX,q10miX,q10m4X
   real(fp8), public :: q10m3X,q10m5X,qom3nX,qom4nX,qem6oX
   real(fp8), public :: xdiffg2X,xdiffm1X,xdiffm3X
   real(fp8), public :: xdiffm4X,xdiffm5X,xdiffm6X
   real(fp8), public :: cld12mX,clm3n_d2mX,chm5sX,cvm5sX
   real(fp8), public :: poro_mX,poro_hX,poro_plX,poro_phX
   real(fp8), public :: sm6m7eX,smkiX,plm1m11pX
   real(fp8), public :: cm3_g4oX,cxirri_mX,sm3m6X,cm3_m6eX
   real(fp8), public :: clm1pX,chm6eX,clm6eX,clm4nX
   real(fp8), public :: clq6sX
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialse parameters
!
! !DESCRIPTION:
! Initialise the arrays prorX and pM1x from nameslist \texttt{nut\_morfParameters}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_nut_morf_parameters
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU
   use benthic_variables, only: n_compben
!
! !LOCAL VARIABLES:
   real(fp8) :: poroXr,pM1Xr
   integer :: i

   namelist/nut_morfParameters/poroXr,pM1Xr
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
    if (IOProcess) write(eLogU,*) '       reading benthic nutrient parameters'
    open(99,file='BioParams/nut_morf.nml',status='old')
    read(99,nml=nut_morfParameters)

    do i = 1,n_compben
       poroX(i) = poroXr
       pM1X(i) = pM1Xr
    end do

    close(99)

    return

    end subroutine set_nut_morf_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Respiration: fixed
!
! !DESCRIPTION:
! Sets the source terms for the biology and gases due
! to respiration.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Respiration_fixed ( K, nfluxc, SYc )
!
! !USES:
   use ersem_constants, only: CMass
   use benthic_variables, only: SG2o,SG3c
!
! !INPUT PARAMETERS:
! Net flux into state
   real(fp8), intent(in) :: nfluxc

! Compartment identifier
   integer, intent(in) :: k
!
! !INPUT/OUTPUT PARAMETERS:
! Source term
   real(fp8) :: SYc
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Set source terms.....................................................
   SYc = SYc - nfluxc
   SG2o(K) = SG2o(K) - nfluxc/CMass
   SG3c(K) = SG3c(K) + nfluxc/CMass

   return

   end subroutine Respiration_fixed
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Mortality
!
! !DESCRIPTION:
! Sets the source terms due to the process of mortality acting
! on a fixed nutrient ratio bio-state. Dead material is converted to
! detritus and/or dissolved organics each component of which (cnp) is
! treated.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Mortality (K, sfluxQ6, sfluxQ1, SYc, SYn, SYp, &
      dc, dn, dp)
!
! !USES:
   use benthic_variables, only : SQ6c,SQ6n,SQ6p,SQ1c,SQ1n,SQ1p
!
! !INPUT PARAMETERS:
! Specific flux from bio-state to detritus
   real(fp8), intent(in) :: sfluxQ6

! Specific flux from bio-state to dissolved
   real(fp8), intent(in) :: sfluxQ1

! Bio-state in c, n and p
   real(fp8), intent(in) :: dc, dn, dp

! Compartment identifier
   integer, intent(in) :: k
!
! !INPUT/OUTPUT PARAMETERS:
! Source term of bio state (cnp)
   real(fp8) :: SYc, SYn, SYp
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Set source terms...................................................

   SQ6c(K) = SQ6c(K) + sfluxQ6 * dc
   SQ6n(K) = SQ6n(K) + sfluxQ6 * dn
   SQ6p(K) = SQ6p(K) + sfluxQ6 * dp

   SQ1c(K) = SQ1c(K) + sfluxQ1 * dc
   SQ1n(K) = SQ1n(K) + sfluxQ1 * dn
   SQ1p(K) = SQ1p(K) + sfluxQ1 * dp

   SYc = SYc - ( sfluxQ6 + sfluxQ1 ) * dc
   SYn = SYn - ( sfluxQ6 + sfluxQ1 ) * dn
   SYp = SYp - ( sfluxQ6 + sfluxQ1 ) * dp


   return

   end subroutine Mortality
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Food partition
!
! !DESCRIPTION:
! Distributes the uptake among a number of food
! components.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Food_Partition (rate, lfood, hfood, &
      avail1, food1, flux1,&
      avail2, food2, flux2,&
      avail3, food3, flux3,&
      avail4, food4, flux4,&
      avail5, food5, flux5,&
      avail6, food6, flux6,&
      avail7, food7, flux7,&
      avail8, food8, flux8,&
      avail9, food9, flux9)
!
! !USES:
   use general, only: eMM
!
! !INPUT PARAMETERS:
! Prey rate of uptake
   real(fp8), intent(in) :: rate

! Lower food threshold
   real(fp8), intent(in) :: lfood

! Half saturation MM constant uptake
   real(fp8), intent(in) :: hfood

! Fraction of food source available to predator
   real(fp8), intent(in) :: avail1,avail2,avail3,avail4,avail5, &
      avail6,avail7,avail8,avail9

! Concentration/density of available food source
   real(fp8), intent(in) :: food1,food2,food3,food4,food5,food6, &
      food7,food8,food9
!
! !OUTPUT PARAMETERS:
! Specific Flux from food i to predator
   real(fp8),intent(out) :: flux1,flux2,flux3,flux4,flux5,flux6, &
      flux7,flux8,flux9
!
! !LOCAL VARIABLES:
   real(fp8) :: foodsum, mm
   real(fp8) :: f1, f2, f3, f4, f5, f6, f7, f8, f9
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
!..food from each source and total food calculated......................

   f1 = avail1 * eMM ( avail1*food1, lfood )
   f2 = avail2 * eMM ( avail2*food2, lfood )
   f3 = avail3 * eMM ( avail3*food3, lfood )
   f4 = avail4 * eMM ( avail4*food4, lfood )
   f5 = avail5 * eMM ( avail5*food5, lfood )
   f6 = avail6 * eMM ( avail6*food6, lfood )
   f7 = avail7 * eMM ( avail7*food7, lfood )
   f8 = avail8 * eMM ( avail8*food8, lfood )
   f9 = avail9 * eMM ( avail9*food9, lfood )

   foodsum = food1*f1 + food2*f2 + food3*f3 + food4*f4 + &
             food5*f5 + food6*f6 + food7*f7 + food8*f8 + food9*f9

   mm = foodsum + hfood

   if (foodsum .gt. 0._fp8) then
      flux1 = rate * f1 / mm
      flux2 = rate * f2 / mm
      flux3 = rate * f3 / mm
      flux4 = rate * f4 / mm
      flux5 = rate * f5 / mm
      flux6 = rate * f6 / mm
      flux7 = rate * f7 / mm
      flux8 = rate * f8 / mm
      flux9 = rate * f9 / mm
   else
      flux1 = 0._fp8
      flux2 = 0._fp8
      flux3 = 0._fp8
      flux4 = 0._fp8
      flux5 = 0._fp8
      flux6 = 0._fp8
      flux7 = 0._fp8
      flux8 = 0._fp8
      flux9 = 0._fp8
   end if

   return

   end subroutine Food_Partition
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Compute available detritus (Q7)
!
! !DESCRIPTION:
! Calculates the detritus available in the layer between the depths
! d\_top and d\_bot.\\[\baselineskip]
!
! !INTERFACE:
   subroutine AvQ7( K, d_top, d_bot, AQ7c, AQ7n, AQ7p )
!
! !USES:
   use benthic_variables, only: Q7cP,Q7nP,Q7pP,d3m,d4m,d5m
   use benthic_parameters, only: d_totx
!
! !INPUT PARAMETERS:
! Compartment identifier
   integer,intent(in) :: k

! Top limit of detritus utilisation
   real(fp8),intent(in) :: d_top

! Bottom limit of detritus utilisation
   real(fp8),intent(in) :: d_bot
!
! !OUTPUT PARAMETERS:
! Available detrital carbon
   real(fp8),intent(out) :: AQ7c

! Available detrital nitrogen
   real(fp8),intent(out) :: AQ7n

! Available detrital phosphorous
   real(fp8),intent(out) :: AQ7p
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Code...................................................
   AQ7c = Q7cP(K) * partQ( D3m(K), d_top, d_bot, d_totX )
   AQ7n = Q7nP(K) * partQ( D4m(K), d_top, d_bot, d_totX )
   AQ7p = Q7pP(K) * partQ( D5m(K), d_top, d_bot, d_totX )

   return

   end subroutine AvQ7
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Calculate available detritus (Q6)
!
! !DESCRIPTION:
! Calculates the detritus available in the layer between the depths
! d\_top and d\_bot.\\[\baselineskip]
!
! !INTERFACE:
   subroutine AvQ6( K, d_top, d_bot, AQ6c, AQ6n, AQ6p )
!
! !USES:
   use benthic_variables, only: Q6cP,Q6nP,Q6pP,D6m,D7m,D8m
   use benthic_parameters, only: d_totx
!
! !INPUT PARAMETERS:
! Compartment identifier
   integer,intent(in) :: k

! Top limit of detritus utilisation
   real(fp8),intent(in) :: d_top

! Bottom limit of detritus utilisation
   real(fp8),intent(in) :: d_bot
!
! !OUTPUT PARAMETERS:
! Available detrital carbon
   real(fp8),intent(out) :: AQ6c

! Available detrital nitrogen
   real(fp8),intent(out) :: AQ6n

! Available detrital phosphorous
   real(fp8),intent(out) :: AQ6p
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   AQ6c = Q6cP(K) * partQ( D6m(K), d_top, d_bot, d_totX )
   AQ6n = Q6nP(K) * partQ( D7m(K), d_top, d_bot, d_totX )
   AQ6p = Q6pP(K) * partQ( D8m(K), d_top, d_bot, d_totX )

   end subroutine AvQ6
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Compute fraction of detritus between depth levels
!
! !DESCRIPTION:
! Computes the fraction of detritus between d\_top and d\_bot. NOTE 1:
! Does not treat silicate. NOTE 2: Factor 13.8. This mystic factor
! takes care that exp(-..) won't become too
! small so that the simulation crashes. -13.8 is the smallest
! number so that exp(-13.8) is evaluated correctly. It depends
! on the accuracy of the implemented FORTRAN.
! \\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function partQ( d_pen, d_top, d_bot, d_max )
!
! !INPUT PARAMETERS:
! Penetration depth of detrital component
   real(fp8), intent(in) :: d_pen

! Top of detrital layer
   real(fp8), intent(in) :: d_top

! Bottom of detrital layer
   real(fp8), intent(in) :: d_bot

! Maximum depth of detrital layer
   real(fp8), intent(in) :: d_max
!
! !LOCAL VARIABLES:
   real(fp8) :: norm

   real(fp8) :: d_top1

   real(fp8) :: d_bot1

   real(fp8) :: d_max1
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Code...................................................
   d_max1 = MIN( d_pen*13.8_fp8, d_max )
   d_bot1 = MIN( d_bot, d_max1 )
   d_top1 = MIN( d_top, d_bot1 )

   if ( d_max1 .gt. 0._fp8 ) then
      norm = 1._fp8 -EXP( -d_max1/d_pen )
      partQ = ( EXP( -d_top1/d_pen ) -EXP( -d_bot1/d_pen )) / norm
   else
      if ( d_top .eq. 0._fp8 ) then
         partQ = 1._fp8
      else
         partQ = 0._fp8
      end if
   end if

   return

   end function partQ
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise cnps flux
!
! !DESCRIPTION:
! This routine is used to record the starting values of the SX
! source terms. Used in conjuction with Calc\_cnps\_flux the net flux
! into SX between the two calls is saved in the RECORD flux.
! Call this routine before any source terms are calculated. OUT: Record initialised
! with the starting values of SX....flux.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Init_cnps_flux ( flux, SXc, SXn, SXp, SXs )
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: SXc,SXn,SXp,SXs
!
! !LOCAL VARIABLES:
   real(fp8),intent(out) :: flux(4)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   flux(1) = SXc
   flux(2) = SXn
   flux(3) = SXp
   flux(4) = SXs

   return

   end subroutine Init_cnps_flux
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Compute cnps flux
!
! !DESCRIPTION:
! This routine is used to determine the net flux into SX. Used in
! conjunction with the routine init\_cnps\_flux the net flux
! into SX between the two calls is saved in the RECORD flux.
! Call this routine after the relevant source terms are calculated. OUT:
! Record holding the net flux into SX...................flux. \\[\baselineskip]
!
! !INTERFACE:
   subroutine Calc_cnps_flux ( flux, SXc, SXn, SXp, SXs )
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: SXc,SXn,SXp,SXs
!
! !OUTPUT PARAMETERS:
   real(fp8) flux(4)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   flux(1) = SXc - flux(1)
   flux(2) = SXn - flux(2)
   flux(3) = SXp - flux(3)
   flux(4) = SXs - flux(4)

   return

   end subroutine Calc_cnps_flux
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise nutrient fluxes
!
! !DESCRIPTION:
! This routine is used to record the starting values of the nutrient
! source terms. Used in conjuction with Calc\_nutrient\_fluxes the net
! flux into the various nutrients between the two calls is saved in the
! RECORD flux. Call this routine before any source terms are
! calculated. OUT: Record initialised with the starting values of SQ6..flux. \\[\baselineskip]
!
! !INTERFACE:
   subroutine Init_nutrient_fluxes (flux, SXn, SXp, SXs, &
      SXo2, SXco2, SXno3, SXn2)
!
! !INPUT PARAMETERS:
   real(fp8),intent(in) :: SXn,SXp,SXs,SXo2,SXco2,SXno3,SXn2
!
! !OUTPUT PARAMETERS:
   real(fp8),intent(out) :: flux(7)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Code...................................................
   flux(1) = SXn
   flux(2) = SXp
   flux(3) = SXs
   flux(4) = SXo2
   flux(5) = SXco2
   flux(6) = SXno3
   flux(7) = SXn2

   return

   end subroutine Init_nutrient_fluxes
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Compute nutrient flux (1)
!
! !DESCRIPTION:
! This routine is used to determine the net flux into nutrients. Used
! in conjunction with the routine init\_nutrient\_fluxes the net flux
! into the various nutrients between the two calls is saved in the
! RECORD flux. Call this routine after the relevant source terms are
! calculated. OUT: Record holding the net flux into Q6.................flux. \\[\baselineskip]
!
! !INTERFACE:
   subroutine Calc_nutrient_fluxes ( flux, SXn, SXp, SXs, &
      SXo2, SXco2, SXno3, SXn2)
!
! !INPUT PARAMETERS:
   real(fp8),intent(in) :: SXn,SXp,SXs,SXo2,SXco2,SXno3,SXn2
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8) flux(7)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!

   ! Code...................................................
   flux(1) = SXn - flux(1)
   flux(2) = SXp - flux(2)
   flux(3) = SXs - flux(3)
   flux(4) = SXo2 - flux(4)
   flux(5) = SXco2 - flux(5)
   flux(6) = SXno3 - flux(6)
   flux(7) = SXn2 - flux(7)

   return

   end subroutine Calc_nutrient_fluxes
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Compute nutrient fluxes (2)
!
! !DESCRIPTION:
! This routine is used to determine the net flux into nutrients. Used
! in conjunction with the routine init\_nutrient\_fluxes the net flux
! into the various nutrients between the two calls is saved in the
! RECORD flux. Call this routine after the relevant source terms are
! calculated. OUT: Record holding the net flux into Q6..flux. \\[\baselineskip]
!
! !INTERFACE:
   subroutine Calc_nutrient_fluxes_2 (flux, f1,f2, SXn, SXp, SXs, &
      SXo2, SXco2, SXno3, SXn2)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: SXn,SXp,SXs,SXo2,SXco2,SXno3,SXn2, &
                            f1(4),f2(4)
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8) :: flux(7)
!
! !LOCAL VARIABLES:
   real(fp8) c,n,p,s,sum
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Code...................................................
   c=0._fp8
   n=0._fp8
   p=0._fp8
   s=0._fp8

   sum=f1(1)+f2(1)
   if (sum.ne.0._fp8) c=f1(1)/sum

   sum=f1(2)+f2(2)
   if (sum.ne.0._fp8) n=f1(2)/sum

   sum=f1(3)+f2(3)
   if (sum.ne.0._fp8) p=f1(3)/sum

   sum=f1(4)+f2(4)
   if (sum.ne.0._fp8) s=f1(4)/sum


   flux(1) = (SXn - flux(1) ) * n
   flux(2) = (SXp - flux(2)) * p
   flux(3) = (SXs - flux(3)) * s
   flux(4) = (SXo2 - flux(4)) * c
   flux(5) = (SXco2 - flux(5)) * c
   flux(6) = (SXno3 - flux(6)) * n
   flux(7) = (SXn2 - flux(7)) * n

   return

   end subroutine Calc_nutrient_fluxes_2
!
!EOC
!-----------------------------------------------------------------------

   end module ben_gen

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
