

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Benthic variables
!
! !DESCRIPTION:
! Repository for state and anscillary variables specific to the benthic
! component of {\em ERSEM}. It contains the definitions of the global benthic
! array \texttt{ccb} and the corresponding auxilliary arrays
! and provides pointers to the
! individual benthic states within the \texttt{ccb} array
! along with the respective auxilliary pointers.\\[\baselineskip]
!
! !INTERFACE:
   module benthic_variables
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is public
   public
!
! !PUBLIC MEMBER FUNCTIONS:
   public do_benthic_variable_allocations
!
! !PUBLIC DATA MEMBERS:
! Number of benthic grid locations
   integer :: n_compben=0

! Number of benthic state variables
   integer :: i_stateben=0

! Extra state variables dependent on preprocessor defs

   integer :: calc_stateben


   integer :: iron_stateben


! Lower bound used in array allocations
   integer :: lbben=0

! Integer identifiers for benthic state variables
   integer :: iH1c,iH2c,iQ1c
   integer :: iQ1n,iQ1p,iQ6c,iQ6n,iQ6p,iQ6s,iQ7c,iQ7n
   integer :: iQ7p,iQ17c,iQ17n,iQ17p,iG2o,iG3c,iG4n
   integer :: iK5s,iK4n
   integer :: iK1p,iK3n
   integer :: iD1m,iD2m,iD3m,iD4m,iD5m,id6m,iD7m
   integer :: iD8m,iD9m,iY2c,iY3c,iY4c

   integer :: iQ6f


   integer :: ibl2c


! Transport switches for benthic state variables
   integer, pointer :: itrXh1c,itrXh2c,itrXq1c
   integer, pointer :: itrXq1n,itrXq1p,itrXq6c,itrXq6n
   integer, pointer :: itrXq6p,itrXq6s,itrXq7c,itrXq7n
   integer, pointer :: itrXq7p,itrXq17c,itrXq17n,itrXq17p
   integer, pointer :: itrXg2o,itrXg3c,itrXg4n
   integer, pointer :: itrXk5s,itrXk4n
   integer, pointer :: itrXk1p,itrXk3n
   integer, pointer :: itrXd1m,itrXd2m,itrXd3m,itrXd4m
   integer, pointer :: itrXd5m,itrXd6m,itrXd7m
   integer, pointer :: itrXd8m,itrXd9m,itrXy2c,itrXy3c,itrXy4c

   integer, pointer :: itrXQ6f


   integer, pointer :: itrXbl2c


! Benthic state variables
   real(fp8), pointer, dimension(:) :: h1c
   real(fp8), pointer, dimension(:) :: h2c,q1c,q1n,q1p,q6c
   real(fp8), pointer, dimension(:) :: q6n,q6p,q6s,q7c,q7n
   real(fp8), pointer, dimension(:) :: q7p,q17c,q17n,q17p,g2o
   real(fp8), pointer, dimension(:) :: g3c,g4n,k5s
   real(fp8), pointer, dimension(:) :: k4n
   real(fp8), pointer, dimension(:) :: k1p,k3n
   real(fp8), pointer, dimension(:) :: d1m,d2m,d3m
   real(fp8), pointer, dimension(:) :: d4m,d5m,d6m,d7m,d8m
   real(fp8), pointer, dimension(:) :: d9m,y2c,y3c,y4c

   real(fp8), pointer, dimension(:) :: q6f


   real(fp8), pointer, dimension(:) :: bL2c


! Processable concentration
   real(fp8), pointer, dimension(:) :: h1cP
   real(fp8), pointer, dimension(:) :: h2cP,q1cP,q1nP,q1pP,q6cP
   real(fp8), pointer, dimension(:) :: q6nP,q6pP,q6sP,q7cP,q7nP
   real(fp8), pointer, dimension(:) :: q7pP,q17cP,q17nP,q17pP,g2oP
   real(fp8), pointer, dimension(:) :: g3cP,g4nP,k5sP
   real(fp8), pointer, dimension(:) :: k4nP
   real(fp8), pointer, dimension(:) :: k1pP,k3nP
   real(fp8), pointer, dimension(:) :: d1mP,d2mP,d3mP
   real(fp8), pointer, dimension(:) :: d4mP,d5mP,d6mP,d7mP,d8mP
   real(fp8), pointer, dimension(:) :: d9mP,y2cP,y3cP,y4cP

   real(fp8), pointer,dimension(:) :: Q6fP


   real(fp8), pointer,dimension(:) :: bL2cP


! Asymptopic minimal concentration
   real(fp8),pointer :: h1c0
   real(fp8),pointer :: h2c0,q1c0,q1n0,q1p0,q6c0
   real(fp8),pointer :: q6n0,q6p0,q6s0,q7c0,q7n0
   real(fp8),pointer :: q7p0,q17c0,q17n0,q17p0,g2o0
   real(fp8),pointer :: g3c0,g4n0,k5s0
   real(fp8),pointer :: k4n0
   real(fp8),pointer :: k1p0,k3n0
   real(fp8),pointer :: d1m0,d2m0,d3m0
   real(fp8),pointer :: d4m0,d5m0,d6m0,d7m0,d8m0
   real(fp8),pointer :: d9m0,y2c0,y3c0,y4c0

   real(fp8), pointer :: Q6f0


   real(fp8), pointer :: bL2c0


! Sink/source terms for benthic state variables
   real(fp8), pointer, dimension(:) :: sh1c
   real(fp8), pointer, dimension(:) :: sh2c,sq1c,sq1n,sq1p,sq6c
   real(fp8), pointer, dimension(:) :: sq6n,sq6p,sq6s,sq7c,sq7n
   real(fp8), pointer, dimension(:) :: sq7p,sq17c,sq17n,sq17p
   real(fp8), pointer, dimension(:) :: sg2o,sg3c,sg4n,sk5s
   real(fp8), pointer, dimension(:) :: sk4n
   real(fp8), pointer, dimension(:) :: sk1p
   real(fp8), pointer, dimension(:) :: sk3n
   real(fp8), pointer, dimension(:) :: sd1m,sd2m,sd3m,sd4m
   real(fp8), pointer, dimension(:) :: sd5m,sd6m,sd7m,sd8m,sd9m
   real(fp8), pointer, dimension(:) :: sy2c,sy3c,sy4c

   real(fp8), pointer,dimension(:) :: SQ6f


   real(fp8), pointer,dimension(:) :: sbL2c


! Vertical transport flux into each benthic state variable
   real(fp8), pointer, dimension(:) :: wsih1c,wsih2c,wsiq1c,wsiq1n
   real(fp8), pointer, dimension(:) :: wsiq1p,wsiq6c,wsiq6n,wsiq6p
   real(fp8), pointer, dimension(:) :: wsiq6s,wsiq7c,wsiq7n,wsiq7p
   real(fp8), pointer, dimension(:) :: wsiq17c,wsiq17n,wsiq17p,wsig2o
   real(fp8), pointer, dimension(:) :: wsig3c,wsig4n,wsik5s
   real(fp8), pointer, dimension(:) :: wsik4n
   real(fp8), pointer, dimension(:) :: wsik1p
   real(fp8), pointer, dimension(:) :: wsik3n
   real(fp8), pointer, dimension(:) :: wsid1m,wsid2m,wsid3m
   real(fp8), pointer, dimension(:) :: wsid4m,wsid5m,wsid6m,wsid7m
   real(fp8), pointer, dimension(:) :: wsid8m,wsid9m,wsiy2c,wsiy3c
   real(fp8), pointer, dimension(:) :: wsiy4c

   real(fp8), pointer, dimension(:) :: wsiQ6f


   real(fp8), pointer, dimension(:) :: wsibl2c


! Vertical transport flux out of each benthic state variable
   real(fp8), pointer, dimension(:) :: wsoh1c,wsoh2c,wsoq1c,wsoq1n
   real(fp8), pointer, dimension(:) :: wsoq1p,wsoq6c,wsoq6n,wsoq6p
   real(fp8), pointer, dimension(:) :: wsoq6s,wsoq7c,wsoq7n,wsoq7p
   real(fp8), pointer, dimension(:) :: wsoq17c,wsoq17n,wsoq17p,wsog2o
   real(fp8), pointer, dimension(:) :: wsog3c,wsog4n,wsok5s
   real(fp8), pointer, dimension(:) :: wsok4n
   real(fp8), pointer, dimension(:) :: wsok1p
   real(fp8), pointer, dimension(:) :: wsok3n
   real(fp8), pointer, dimension(:) :: wsod1m,wsod2m,wsod3m
   real(fp8), pointer, dimension(:) :: wsod4m,wsod5m,wsod6m,wsod7m
   real(fp8), pointer, dimension(:) :: wsod8m,wsod9m,wsoy2c,wsoy3c
   real(fp8), pointer, dimension(:) :: wsoy4c

   real(fp8), pointer, dimension(:) :: wsoQ6f


   real(fp8), pointer, dimension(:) :: wsobl2c


! Sedimentation fluxes
   real(fp8), pointer, dimension(:) :: sdh1c,sdh2c,sdq1c,sdq1n
   real(fp8), pointer, dimension(:) :: sdq1p,sdq6c,sdq6n,sdq6p
   real(fp8), pointer, dimension(:) :: sdq6s,sdq7c,sdq7n,sdq7p
   real(fp8), pointer, dimension(:) :: sdq17c,sdq17n,sdq17p,sdg2o
   real(fp8), pointer, dimension(:) :: sdg3c,sdg4n,sdk5s
   real(fp8), pointer, dimension(:) :: sdk4n
   real(fp8), pointer, dimension(:) :: sdk1p
   real(fp8), pointer, dimension(:) :: sdk3n
   real(fp8), pointer, dimension(:) :: sdd1m,sdd2m,sdd3m
   real(fp8), pointer, dimension(:) :: sdd4m,sdd5m,sdd6m,sdd7m
   real(fp8), pointer, dimension(:) :: sdd8m,sdd9m,sdy2c,sdy3c
   real(fp8), pointer, dimension(:) :: sdy4c

   real(fp8), pointer,dimension(:) :: sdQ6f


   real(fp8), pointer,dimension(:) :: sdbl2c


! Equivalence arrays
   real(fp8), allocatable, target, dimension(:,:) :: ccb,sccb
   real(fp8), pointer, dimension(:,:) :: ccbP
   real(fp8), allocatable, target, dimension(:,:) :: wsiccb,wsoccb
   real(fp8), allocatable, target, dimension(:,:) :: sdccb
   integer, allocatable, target, dimension(:) :: itrXccb
   real(fp8), allocatable, target, dimension(:) :: ccb0
   character(4),allocatable,target,dimension(:) :: ccbstr

! Diagnostic fluxes
   real(fp8) :: fbty4c,fbty3c,fbty2c
   real(fp8) :: jm3g4n

   real(fp8), pointer, dimension(:) :: fPXY3c,fPXY3n,fPXY3p,fPXY3s


! Update switches
   integer :: iswh1X,iswh2X,iswy2X,iswy3X,iswy4X
   integer :: iswbenX,ISWbenAnX=0

! Pseudo benthic states
   real(fp8) :: k4,k14,k24,k1,k11,k21

! Pseudo benthic rates
   real(fp8) :: SK11p,SK14n,SK6e

! Bed stress and undisturbed water depth
   real(fp8),pointer, dimension(:) :: bedstress,wdepth

! Location identifier for mapping between the benthic and pelagic
   integer,dimension(:),pointer :: pelagicIndex

! Budgeting arrays benthic for key variables Carbon, Nitrate
! Phosphate, Silicate
   real(fp8) :: ben_budget(1:9,1:4)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team

! James Clark (PML) Jan 2014 - Created module benthic_variables, and
! moved data members in from module global_declarations.
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthic variable array allocations
!
! !DESCRIPTION:
! Allocates benthic variable arrays and sets state pointers.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine do_benthic_variable_allocations
!
! !USES:
   use allocation_helpers, only: allocerr, allocateErsemDouble, &
      allocateErsemInteger
!
! !LOCAL VARIABLES:
   integer :: ialloc
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team

! James Clark (PML) Feb 2014: Moved benthic allocation statements from
! global_allocations.
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   allocate (itrXccb(i_stateben),stat=ialloc)
   call allocerr ('itrXccb',i_stateben,ialloc)
   if ( ialloc.ne.0 ) then

      !flush(eDbgU)
      !flush(eLogU)
      call MPI_abort(ialloc)



   end if

   ! Ensure, for the case that N_Compben is zero, that the lower bound
   ! is also zero, otherwise array bound checking generates errors.
   lbben = min(1,N_Compben)
   allocate (ccb(lbben:N_Compben,i_stateben),stat=ialloc)
   call allocerr ('ccb',N_Compben-lbben+1,i_stateben,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then

      call MPI_abort(ialloc)



   end if
!


   allocate (sccb(lbben:N_Compben,i_stateben),stat=ialloc)
   call allocerr ('sccb',N_Compben-lbben+1,i_stateben,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then

      call MPI_abort(ialloc)



   end if

   allocate (ccb0(i_stateben),stat=ialloc)
   call allocerr ('ccb0',i_stateben,0,0,0,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then

      call MPI_abort(ialloc)



   end if

   allocate (wsiccb(lbben:N_Compben,i_stateben),stat=ialloc)
   call allocerr ('wsiccb',N_Compben-lbben+1,i_stateben,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then

      call MPI_abort(ialloc)



   end if

   allocate (wsoccb(lbben:N_Compben,i_stateben),stat=ialloc)
   call allocerr ('wsoccb',N_Compben-lbben+1,i_stateben,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then

      call MPI_abort(ialloc)



   end if

   allocate (sdccb(lbben:N_Compben,i_stateben),stat=ialloc)
   call allocerr ('sdccb',N_Compben-lbben+1,i_stateben,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then

      call MPI_abort(ialloc)



   end if
!
   allocate (ccbstr(i_stateben),stat=ialloc)
   call allocerr ('ccbstr',i_stateben,ialloc)
   !flush(eDbgU)
   !flush(eLogU)
   if ( ialloc.ne.0 ) then

      call MPI_abort(ialloc)



   end if

   ! Assign position IDs in ccc/ccb arrays and identification strings

   ! Position ids - benthic
   iH1c = 1 ; ccbstr( iH1c ) = 'H1c'
   iH2c = 2 ; ccbstr( iH2c ) = 'H2c'
   iG2o = 3 ; ccbstr( iG2o ) = 'G2o'
   iG3c = 4 ; ccbstr( iG3c ) = 'G3c'
   iG4n = 5 ; ccbstr( iG4n ) = 'G4n'
   iK5s = 6 ; ccbstr( iK5s ) = 'K5s'
   iK4n = 7 ; ccbstr( iK4n ) = 'K4n'
   iK1p = 8 ; ccbstr( iK1p ) = 'K1p'
   iK3n = 9 ; ccbstr( iK3n ) = 'K3n'
   iD1m =10 ; ccbstr( iD1m ) = 'D1m'
   iD2m =11 ; ccbstr( iD2m ) = 'D2m'
   iD3m =12 ; ccbstr( iD3m ) = 'D3m'
   iD4m =13 ; ccbstr( iD4m ) = 'D4m'
   iD5m =14 ; ccbstr( iD5m ) = 'D5m'
   iD6m =15 ; ccbstr( iD6m ) = 'D6m'
   iD7m =16 ; ccbstr( iD7m ) = 'D7m'
   iD8m =17 ; ccbstr( iD8m ) = 'D8m'
   iD9m =18 ; ccbstr( iD9m ) = 'D9m'
   iY2c =19 ; ccbstr( iY2c ) = 'Y2c'
   iY3c =20 ; ccbstr( iY3c ) = 'Y3c'
   iY4c =21 ; ccbstr( iY4c ) = 'Y4c'
   iQ1c =22 ; ccbstr( iQ1c ) = 'Q1c'
   iQ1n =23 ; ccbstr( iQ1n ) = 'Q1n'
   iQ1p =24 ; ccbstr( iQ1p ) = 'Q1p'
   iQ6c =25 ; ccbstr( iQ6c ) = 'Q6c'
   iQ6n =26 ; ccbstr( iQ6n ) = 'Q6n'
   iQ6p =27 ; ccbstr( iQ6p ) = 'Q6p'
   iQ6s =28 ; ccbstr( iQ6s ) = 'Q6s'
   iQ7c =29 ; ccbstr( iQ7c ) = 'Q7c'
   iQ7n =30 ; ccbstr( iQ7n ) = 'Q7n'
   iQ7p =31 ; ccbstr( iQ7p ) = 'Q7p'
   iQ17c=32 ; ccbstr( iQ17c ) = 'Q17c'
   iQ17n=33 ; ccbstr( iQ17n ) = 'Q17n'
   iQ17p=34 ; ccbstr( iQ17p ) = 'Q17p'

   ibL2c= CALC_STATEBEN; ccbstr( ibL2c ) = 'bL2c'


   iQ6f = IRON_STATEBEN ; ccbstr( iQ6f ) = 'Q6f'


   ! Transport switches -- benthic
   itrXccb=0
   itrXH1c => itrXccb( iH1c )
   itrXH2c => itrXccb( iH2c )
   itrXG2o => itrXccb( iG2o )
   itrXG3c => itrXccb( iG3c )
   itrXG4n => itrXccb( iG4n )
   itrXK5s => itrXccb( iK5s )
   itrXK4n => itrXccb( iK4n )
   itrXK1p => itrXccb( iK1p )
   itrXK3n => itrXccb( iK3n )
   itrXD1m => itrXccb( iD1m )
   itrXD2m => itrXccb( iD2m )
   itrXD3m => itrXccb( iD3m )
   itrXD4m => itrXccb( iD4m )
   itrXD5m => itrXccb( iD5m )
   itrXD6m => itrXccb( iD6m )
   itrXD7m => itrXccb( iD7m )
   itrXD8m => itrXccb( iD8m )
   itrXD9m => itrXccb( iD9m )
   itrXY2c => itrXccb( iY2c )
   itrXY3c => itrXccb( iY3c )
   itrXY4c => itrXccb( iY4c )
   itrXQ1c => itrXccb( iQ1c )
   itrXQ1n => itrXccb( iQ1n )
   itrXQ1p => itrXccb( iQ1p )
   itrXQ6c => itrXccb( iQ6c )
   itrXQ6n => itrXccb( iQ6n )
   itrXQ6p => itrXccb( iQ6p )
   itrXQ6s => itrXccb( iQ6s )
   itrXQ7c => itrXccb( iQ7c )
   itrXQ7n => itrXccb( iQ7n )
   itrXQ7p => itrXccb( iQ7p )
   itrXQ17c=> itrXccb( iQ17c )
   itrXQ17n=> itrXccb( iQ17n )
   itrXQ17p=> itrXccb( iQ17p )

   itrXbl2c=> itrXccb( ibL2c )


   itrXQ6f=> itrXccb( iQ6f )


   ! For access to individual state vars -- pelagic
   H1c => ccb(:, iH1c ) ; H1c0 => ccb0( iH1c )
   H2c => ccb(:, iH2c ) ; H2c0 => ccb0( iH2c )
   G2o => ccb(:, iG2o ) ; G2o0 => ccb0( iG2o )
   G3c => ccb(:, iG3c ) ; G3c0 => ccb0( iG3c )
   G4n => ccb(:, iG4n ) ; G4n0 => ccb0( iG4n )
   K5s => ccb(:, iK5s ) ; K5s0 => ccb0( iK5s )
   K4n => ccb(:, iK4n ) ; K4n0 => ccb0( iK4n )
   K1p => ccb(:, iK1p ) ; K1p0 => ccb0( iK1p )
   K3n => ccb(:, iK3n ) ; K3n0 => ccb0( iK3n )
   D1m => ccb(:, iD1m ) ; D1m0 => ccb0( iD1m )
   D2m => ccb(:, iD2m ) ; D2m0 => ccb0( iD2m )
   D3m => ccb(:, iD3m ) ; D3m0 => ccb0( iD3m )
   D4m => ccb(:, iD4m ) ; D4m0 => ccb0( iD4m )
   D5m => ccb(:, iD5m ) ; D5m0 => ccb0( iD5m )
   D6m => ccb(:, iD6m ) ; D6m0 => ccb0( iD6m )
   D7m => ccb(:, iD7m ) ; D7m0 => ccb0( iD7m )
   D8m => ccb(:, iD8m ) ; D8m0 => ccb0( iD8m )
   D9m => ccb(:, iD9m ) ; D9m0 => ccb0( iD9m )
   Y2c => ccb(:, iY2c ) ; Y2c0 => ccb0( iY2c )
   Y3c => ccb(:, iY3c ) ; Y3c0 => ccb0( iY3c )
   Y4c => ccb(:, iY4c ) ; Y4c0 => ccb0( iY4c )
   Q1c => ccb(:, iQ1c ) ; Q1c0 => ccb0( iQ1c )
   Q1n => ccb(:, iQ1n ) ; Q1n0 => ccb0( iQ1n )
   Q1p => ccb(:, iQ1p ) ; Q1p0 => ccb0( iQ1p )
   Q6c => ccb(:, iQ6c ) ; Q6c0 => ccb0( iQ6c )
   Q6n => ccb(:, iQ6n ) ; Q6n0 => ccb0( iQ6n )
   Q6p => ccb(:, iQ6p ) ; Q6p0 => ccb0( iQ6p )
   Q6s => ccb(:, iQ6s ) ; Q6s0 => ccb0( iQ6s )
   Q7c => ccb(:, iQ7c ) ; Q7c0 => ccb0( iQ7c )
   Q7n => ccb(:, iQ7n ) ; Q7n0 => ccb0( iQ7n )
   Q7p => ccb(:, iQ7p ) ; Q7p0 => ccb0( iQ7p )
   Q17c=> ccb(:, iQ17c ) ; Q17c0 => ccb0( iQ17c )
   Q17n=> ccb(:, iQ17n ) ; Q17n0 => ccb0( iQ17n )
   Q17p=> ccb(:, iQ17p ) ; Q17p0 => ccb0( iQ17p )

   q6f=> ccb(:, iQ6f ) ; q6f0 => ccb0( iQ6f )


   bl2c=>ccb(:, ibL2c ) ; bL2c0=> ccb0( ibl2c)


   ! Initialised to 0
   ccb = 0._fp8
   ccb0 = 0._fp8

   ! Sink/source terms -- benthic
   sH1c => sccb(:, iH1c )
   sH2c => sccb(:, iH2c )
   sG2o => sccb(:, iG2o )
   sG3c => sccb(:, iG3c )
   sG4n => sccb(:, iG4n )
   sK5s => sccb(:, iK5s )
   sK4n => sccb(:, iK4n )
   sK1p => sccb(:, iK1p )
   sK3n => sccb(:, iK3n )
   sD1m => sccb(:, iD1m )
   sD2m => sccb(:, iD2m )
   sD3m => sccb(:, iD3m )
   sD4m => sccb(:, iD4m )
   sD5m => sccb(:, iD5m )
   sD6m => sccb(:, iD6m )
   sD7m => sccb(:, iD7m )
   sD8m => sccb(:, iD8m )
   sD9m => sccb(:, iD9m )
   sY2c => sccb(:, iY2c )
   sY3c => sccb(:, iY3c )
   sY4c => sccb(:, iY4c )
   sQ1c => sccb(:, iQ1c )
   sQ1n => sccb(:, iQ1n )
   sQ1p => sccb(:, iQ1p )
   sQ6c => sccb(:, iQ6c )
   sQ6n => sccb(:, iQ6n )
   sQ6p => sccb(:, iQ6p )
   sQ6s => sccb(:, iQ6s )
   sQ7c => sccb(:, iQ7c )
   sQ7n => sccb(:, iQ7n )
   sQ7p => sccb(:, iQ7p )
   sQ17c=> sccb(:, iQ17c )
   sQ17n=> sccb(:, iQ17n )
   sQ17p=> sccb(:, iQ17p )

   sQ6f=> sccb(:, iQ6f )


   sbl2c=> sccb(:, ibL2c)

   sccb=0._fp8

   ! Sedimentation rates -- benthic
   sdH1c => sdccb(:, iH1c )
   sdH2c => sdccb(:, iH2c )
   sdG2o => sdccb(:, iG2o )
   sdG3c => sdccb(:, iG3c )
   sdG4n => sdccb(:, iG4n )
   sdK5s => sdccb(:, iK5s )
   sdK4n => sdccb(:, iK4n )
   sdK1p => sdccb(:, iK1p )
   sdK3n => sdccb(:, iK3n )
   sdD1m => sdccb(:, iD1m )
   sdD2m => sdccb(:, iD2m )
   sdD3m => sdccb(:, iD3m )
   sdD4m => sdccb(:, iD4m )
   sdD5m => sdccb(:, iD5m )
   sdD6m => sdccb(:, iD6m )
   sdD7m => sdccb(:, iD7m )
   sdD8m => sdccb(:, iD8m )
   sdD9m => sdccb(:, iD9m )
   sdY2c => sdccb(:, iY2c )
   sdY3c => sdccb(:, iY3c )
   sdY4c => sdccb(:, iY4c )
   sdQ1c => sdccb(:, iQ1c )
   sdQ1n => sdccb(:, iQ1n )
   sdQ1p => sdccb(:, iQ1p )
   sdQ6c => sdccb(:, iQ6c )
   sdQ6n => sdccb(:, iQ6n )
   sdQ6p => sdccb(:, iQ6p )
   sdQ6s => sdccb(:, iQ6s )
   sdQ7c => sdccb(:, iQ7c )
   sdQ7n => sdccb(:, iQ7n )
   sdQ7p => sdccb(:, iQ7p )
   sdQ17c=> sdccb(:, iQ17c )
   sdQ17n=> sdccb(:, iQ17n )
   sdQ17p=> sdccb(:, iQ17p )

   sdQ6f=> sdccb(:, iQ6f )


   sdbl2c=> sccb(:, ibL2c)

   sdccb=0._fp8

   ! Into transport fluxes -- benthic
   wsiH1c => wsiccb(:, iH1c )
   wsiH2c => wsiccb(:, iH2c )
   wsiG2o => wsiccb(:, iG2o )
   wsiG3c => wsiccb(:, iG3c )
   wsiG4n => wsiccb(:, iG4n )
   wsiK5s => wsiccb(:, iK5s )
   wsiK4n => wsiccb(:, iK4n )
   wsiK1p => wsiccb(:, iK1p )
   wsiK3n => wsiccb(:, iK3n )
   wsiD1m => wsiccb(:, iD1m )
   wsiD2m => wsiccb(:, iD2m )
   wsiD3m => wsiccb(:, iD3m )
   wsiD4m => wsiccb(:, iD4m )
   wsiD5m => wsiccb(:, iD5m )
   wsiD6m => wsiccb(:, iD6m )
   wsiD7m => wsiccb(:, iD7m )
   wsiD8m => wsiccb(:, iD8m )
   wsiD9m => wsiccb(:, iD9m )
   wsiY2c => wsiccb(:, iY2c )
   wsiY3c => wsiccb(:, iY3c )
   wsiY4c => wsiccb(:, iY4c )
   wsiQ1c => wsiccb(:, iQ1c )
   wsiQ1n => wsiccb(:, iQ1n )
   wsiQ1p => wsiccb(:, iQ1p )
   wsiQ6c => wsiccb(:, iQ6c )
   wsiQ6n => wsiccb(:, iQ6n )
   wsiQ6p => wsiccb(:, iQ6p )
   wsiQ6s => wsiccb(:, iQ6s )
   wsiQ7c => wsiccb(:, iQ7c )
   wsiQ7n => wsiccb(:, iQ7n )
   wsiQ7p => wsiccb(:, iQ7p )
   wsiQ17c=> wsiccb(:, iQ17c )
   wsiQ17n=> wsiccb(:, iQ17n )
   wsiQ17p=> wsiccb(:, iQ17p )

   wsiQ6f=> wsiccb(:, iQ6f )


   wsibl2c => wsiccb(:,ibL2c )

   wsiccb = 0._fp8

   ! Out-of transport fluxes -- benthic
   wsoH1c => wsoccb(:, iH1c )
   wsoH2c => wsoccb(:, iH2c )
   wsoG2o => wsoccb(:, iG2o )
   wsoG3c => wsoccb(:, iG3c )
   wsoG4n => wsoccb(:, iG4n )
   wsoK5s => wsoccb(:, iK5s )
   wsoK4n => wsoccb(:, iK4n )
   wsoK1p => wsoccb(:, iK1p )
   wsoK3n => wsoccb(:, iK3n )
   wsoD1m => wsoccb(:, iD1m )
   wsoD2m => wsoccb(:, iD2m )
   wsoD3m => wsoccb(:, iD3m )
   wsoD4m => wsoccb(:, iD4m )
   wsoD5m => wsoccb(:, iD5m )
   wsoD6m => wsoccb(:, iD6m )
   wsoD7m => wsoccb(:, iD7m )
   wsoD8m => wsoccb(:, iD8m )
   wsoD9m => wsoccb(:, iD9m )
   wsoY2c => wsoccb(:, iY2c )
   wsoY3c => wsoccb(:, iY3c )
   wsoY4c => wsoccb(:, iY4c )
   wsoQ1c => wsoccb(:, iQ1c )
   wsoQ1n => wsoccb(:, iQ1n )
   wsoQ1p => wsoccb(:, iQ1p )
   wsoQ6c => wsoccb(:, iQ6c )
   wsoQ6n => wsoccb(:, iQ6n )
   wsoQ6p => wsoccb(:, iQ6p )
   wsoQ6s => wsoccb(:, iQ6s )
   wsoQ7c => wsoccb(:, iQ7c )
   wsoQ7n => wsoccb(:, iQ7n )
   wsoQ7p => wsoccb(:, iQ7p )
   wsoQ17c=> wsoccb(:, iQ17c )
   wsoQ17n=> wsoccb(:, iQ17n )
   wsoQ17p=> wsoccb(:, iQ17p )

   wsoQ6f=> wsoccb(:, iQ6f )


   wsobl2c => wsoccb(:,ibL2c )

   wsoccb=0._fp8

   ! Indices of seafloor boxes connected to benthos:
   call allocateErsemInteger(pelagicIndex,'pelagicIndex',lbben,N_Compben)
   ! Stress at seabed
   call allocateErsemDouble(bedstress,'bedstress',lbben,N_Compben)
   ! Undistrubed water depth
   call allocateErsemDouble(wdepth,'wdepth',lbben,N_Compben)

   ! Diagnostic fluxes

   call allocateErsemDouble(fPXY3c,'fPXY3c',lbben,N_Compben)
   call allocateErsemDouble(fPXY3n,'fPXY3n',lbben,N_Compben)
   call allocateErsemDouble(fPXY3p,'fPXY3p',lbben,N_Compben)
   call allocateErsemDouble(fPXY3s,'fPXY3s',lbben,N_Compben)


   end subroutine do_benthic_variable_allocations
!
!EOC
!-----------------------------------------------------------------------

   end module benthic_variables

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
