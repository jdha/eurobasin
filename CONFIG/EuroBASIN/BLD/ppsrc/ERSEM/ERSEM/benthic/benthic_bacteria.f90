

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Benthic bacteria
!
! !DESCRIPTION:
! Module with parametrisation and routines for benthic bacteria.
! \\[\baselineskip]
!
! !INTERFACE:
   module benthic_bacteria
!
! !USES:
   use ersem_constants, only: fp8
   use general, only: emm,eramp,Adjust_Fixed_Nutrients
   use ben_gen, only: fH1Q6,fH1Q7,fH2Q6,fH2Q7,gQ7H2,gQ6H2,ddH2X, &
      puincH2X,purh2x,q10h2x,sdh2x,srh2x,suq6fh2x, &
      suq6sh2x,suq7h2x,ddh1x,pdh1q1x,pue6h1q1x, &
      pue7h1q1x,puinch1x,purh1x,q10h1x,sdh1x,srh1x, &
      suq1h1x,suq6fh1x,suq7h1x,suQ6sH1X
   use ben_gen, only: AvQ6,AvQ7,Respiration_Fixed, &
      Mortality
   use ben_phys, only: Calculate_Adsorption

   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public aerobic_bacteria_dynamics, anaerobic_bacteria_dynamics
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Aerobic bacteria dynamics
!
! !DESCRIPTION:
! This routine treats the processes mediated by aerobic bacteria (H1).
! These can be categorised into: Uptake (and resulting excretion etc.),
! respiration, and mortality. Three subroutines set the source terms for
! these three processes taking the process fluxes as input. NB
! Detritus with high nutrient content preferred: Composition of
! decomposed detritus is "richer" than Q6. Slow degradation - independent
! of composition of Q6. Fast degradation - dependent on nutrient content
! of Q6. Fast degradation of Q1.\\[\baselineskip]
!
! !INTERFACE:
   subroutine aerobic_bacteria_dynamics(I,K)
!
! !USES:
   use pelagic_variables, only: etw
   use benthic_variables
   use benthic_parameters

   implicit none
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i, k
!
! !LOCAL VARIABLES:
! Limitation factor due to temperature (0d0-1.0)
   real(fp8) :: eT

! Limitation factor due to oxygen
   real(fp8) :: eOx

! Limitation factor due to nutrients
   real(fp8) :: eN

! Density dependant rate factor
   real(fp8) :: Limit

! Pseudo source terms for nutrient content (np)
   real(fp8) :: SH1n, SH1p

   real(fp8) :: AQ7n, AQ7p, AQ7c, fQ7H1c, sfQ7H1
   real(fp8) :: AQ6n, AQ6p, AQ6c, fQ6H1c, sfQ6H1, fQ1H1c, fQIH1c
   real(fp8) :: fQ7H1n, fQ7H1p
   real(fp8) :: fQ6H1n, fQ6H1p, fQ1H1n, fQ1H1p
   real(fp8) :: fK4H1n, fK1H1p, fH1G3c, sfH1QI, sfH1Q6, sfH1Q1
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Set up pseudo source terms for nutrient content......................
   SH1n = SH1c(K) * qnHIcX
   SH1p = SH1c(K) * qpHIcX

   ! Initialise net flux into Q6 calculation..............................
   fH1Q6(1) = SQ6c(K)
   fH1Q6(2) = SQ6n(K)
   fH1Q6(3) = SQ6p(K)
   fH1Q6(4) = SQ6s(K)
! call Init_cnps_flux ( fH1Q6, SQ6c(I), SQ6n(I), SQ6p(I), SQ6s(I) )
   fH1Q7(1) = SQ7c(K)
   fH1Q7(2) = SQ7n(K)
   fH1Q7(3) = SQ7p(K)
   fH1Q7(4) = 0._fp8
! call Init_cnps_flux ( fH1Q7, SQ7c(I), SQ7n(I), SQ7p(I), 0d0 )

   ! Calculate available detritus.........................................
   call AvQ6( K, 0._fp8, D1m(K), AQ6c, AQ6n, AQ6p )
   call AvQ7( K, 0._fp8, D1m(K), AQ7c, AQ7n, AQ7p )

   ! Calculate limitation factors.........................................
   et = q10H1X**((ETW(I)-10._fp8)/10._fp8) - q10H1X**((ETW(I)-32._fp8)/3._fp8)
   eOx = eMM(D1m(K), ddH1X)
   eN = eramp(AQ6n, qnHIcX*AQ6c) * eramp(AQ6p, qpHIcX*AQ6c)

   ! Calculate uptake fluxes..............................................
   Limit = eT * eOX * H1c(K)

   sfQ7H1 = ( suQ7H1X * Limit )
   sfQ6H1 = ( suQ6fH1X * Limit * eN ) &
          + ( suQ6sH1X * Limit )
   fQ7H1c = sfQ7H1 * AQ7c
   fQ6H1c = sfQ6H1 * AQ6c
   fQ1H1c = suQ1H1X * Limit * Q1cP(K)
   fQIH1c = fQ7h1c + fQ6H1c + fQ1H1c

   fQ7H1n = sfQ7H1 * AQ7n
   fQ7H1p = sfQ7H1 * AQ7p
   fQ6H1n = sfQ6H1 * AQ6n * puincH1X
   fQ6H1p = sfQ6H1 * AQ6p * puincH1X

   fQ1H1n = suQ1H1X * Limit * Q1nP(K)
   fQ1H1p = suQ1H1X * Limit * Q1pP(K)

   ! ... and nutrient uptake..................................................
   fK4H1n = fQIH1c * qnHIcX
   fK4H1n = fK4H1n &
      * eMM(K4/calculate_adsorption(4,K,0._fp8,D1m(K)),fK4H1n)
   fK1H1p = fQIH1c * qpHIcX
   fK1H1p = fK1H1p &
      * eMM(K1/calculate_adsorption(1,K,0._fp8,D1m(K)),fK1H1p)

   ! Calculate the source terms as a result of feeding activity...........
   call B_uptake( K,fQ7H1c, fQ7H1n, fQ7H1p, &
                 fQ6H1c, fQ6H1n, fQ6H1p, fQ1H1c, fQ1H1n, fQ1H1p,&
                 pue6H1Q1X, pue7H1Q1X,fK4H1n, fK1H1p, &
                 SH1c(K), SH1n, SH1p, SK4n(K), SK1p(K) )

   ! Calculate respiration as rest respiration plus activity respiration..
   fH1G3c = fQIH1c * purH1X + srH1X * H1cP(K) * eT
   call Respiration_fixed( K, fH1G3c, SH1c(K) )

   ! Calculate mortality..................................................
   sfH1QI = sdH1X * ( 1._fp8 - eOx )
   sfH1Q6 = sfH1QI * (1._fp8 - pdH1Q1X)
   sfH1Q1 = sfH1QI * pdH1Q1X

   call Mortality(K, sfH1Q6, sfH1Q1, &
      SH1c(K), SH1n, SH1p, &
      H1cP(K), H1cP(K)*qnHIcX, H1cP(K)*qpHIcX)

   ! Adjust cnp excretion to maintain fixed ratio.........................
   call Adjust_fixed_nutrients ( SH1c(K), SH1n, SH1p, qnHIcX, &
      qpHIcX, SK4n(K), SK1p(K), SQ6c(K) )

   ! Calculate net flux into Q6...........................................
   fH1Q6(1) = SQ6c(K) - fH1Q6(1)
   fH1Q6(2) = SQ6n(K) - fH1Q6(2)
   fH1Q6(3) = SQ6p(K) - fH1Q6(3)
   fH1Q6(4) = SQ6s(K) - fH1Q6(4)
! call Calc_cnps_flux ( fH1Q6, SQ6c(I), SQ6n(I), SQ6p(I), SQ6s(I) )
   fH1Q7(1) = SQ7c(K) - fH1Q7(1)
   fH1Q7(2) = SQ7n(K) - fH1Q7(2)
   fH1Q7(3) = SQ7p(K) - fH1Q7(3)
   fH1Q7(4) = 0._fp8 - fH1Q7(4)
! call Calc_cnps_flux ( fH1Q7, SQ7c(I), SQ7n(I), SQ7p(I), 0d0 )

   return

   end subroutine aerobic_bacteria_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Anaerobic bacteria dynamics
!
! !DESCRIPTION:
! This routine treats the processes mediated by Anaerobic bacteria (H2).
! These can be categorised into: Uptake (and resulting excretion etc.),
! respiration, and mortality. Three subroutines set the source terms
! for these three processes taking the process fluxes as input. NB
! Detritus with high nutrient content preferred: Composition of
! decomposed detritus is "richer" than Q6. Slow degradation -
! independent of composition of Q6. Fast degradation - dependent
! on nutrient content of Q6.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine anaerobic_bacteria_dynamics(I,K)
!
! !USES:
   use ersem_constants, only: CMass



   use pelagic_variables, only: etw
   use benthic_variables
   use benthic_parameters
!
! !INPUT PARAMETERS:
   integer :: i,k
!
! !LOCAL VARIABLES:
! Limitation factor due to temperature (0d0-1.0)
   real(fp8) :: eT

! Limitation factor due to oxygen
   real(fp8) :: eOx

! Limitation factor due to nutrients
   real(fp8) :: eN

! Density dependant rate factor
   real(fp8) :: Limit

! Pseudo source terms for nutrient content (np)
   real(fp8) :: SH2n, SH2p

   real(fp8) :: AQ7n, AQ7p, AQ7c, fQ7H2c
   real(fp8) :: AQ6n, AQ6p, AQ6c, fQ6H2c, fQ1H2c, fQIH2c
   real(fp8) :: fQ7H2n, fQ7H2p, fQ6H2n, fQ6H2p
   real(fp8) :: fQ1H2n, fQ1H2p, sfQ6H2, sfQ7H2
   real(fp8) :: fK14H2n, fK11H2p, fH2G3c, sfH2QI, sfH2Q6, sfH2Q1
   real(fp8) :: pueH2Q1X, pdH2Q1X, s
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Set up pseudo source terms for nutrient content......................
   SH2n = SH2c(K) * qnHIcX
   SH2p = SH2c(K) * qpHIcX

   ! Initialise net flux into Q6 calculation..............................
   gQ6H2(1) = 0._fp8
   gQ6H2(2) = 0._fp8
   gQ6H2(3) = 0._fp8
   gQ6H2(4) = 0._fp8
! call Init_cnps_flux ( gQ6H2, 0d0, 0d0, 0d0, 0d0 )
   gQ7H2(1) = 0._fp8
   gQ7H2(2) = 0._fp8
   gQ7H2(3) = 0._fp8
   gQ7H2(4) = 0._fp8
! call Init_cnps_flux ( gQ7H2,0d0, 0d0, 0d0, 0d0 )
   fH2Q6(1) = SQ6c(K)
   fH2Q6(2) = SQ6n(K)
   fH2Q6(3) = SQ6p(K)
   fH2Q6(4) = SQ6s(K)
! call Init_cnps_flux ( fH2Q6, SQ6c(I), SQ6n(I), SQ6p(I), SQ6s(I) )
   fH2Q7(1) = SQ7c(K)
   fH2Q7(2) = SQ7n(K)
   fH2Q7(3) = SQ7p(K)
   fH2Q7(4) = 0._fp8
! call Init_cnps_flux ( fH2Q7,SQ7c(I), SQ7n(I), SQ7p(I), 0d0 )

   ! Calculate available detritus.........................................
   call AvQ6( K, D1m(K), d_totX, AQ6c, AQ6n, AQ6p )
   call AvQ7( K, D1m(K), d_totX, AQ7c, AQ7n, AQ7p )

   ! Calculate limitation factors.........................................
   et = q10H2X**((ETW(I)-10._fp8)/10._fp8) - q10H2X**((ETW(I)-32._fp8)/3._fp8)
   eOx = eMM( D2m(K) - D1m(K), ddH2X )
   eN = eramp( AQ6n, qnHIcX * AQ6c) * eramp( AQ6p, qpHIcX * AQ6c)

   ! Calculate uptake fluxes..............................................
   Limit = eT * eOx * H2c(K)

   sfQ6H2 = ( suQ6fH2X * Limit * eN ) &
          + ( suQ6sH2X * Limit )
   sfQ7H2 = ( suQ7H2X * Limit )
   fQ6H2c = sfQ6H2 * AQ6c
   fQ7H2c = sfQ7H2 * AQ7c

   fQ1H2c = 0._fp8
   fQIH2c = fQ7H2c + fQ6H2c + fQ1H2c

   fQ7H2n = sfQ7H2 * AQ7n
   fQ7H2p = sfQ7H2 * AQ7p

   fQ6H2n = sfQ6H2 * AQ6n * puincH2X
   fQ6H2p = sfQ6H2 * AQ6p * puincH2X

   fQ1H2n = 0._fp8
   fQ1H2p = 0._fp8

   ! ...and nutrient uptake..................................................
   fK14H2n = fQIH2c * qnHIcX
   fK11H2p = fQIH2c * qpHIcX







   s = (K24+K14)/calculate_adsorption(4,K,D1m(K),d_totX)
   fK14H2n = fK14H2n* eMM(s,fK14H2n)
   s = (K21+K11)/calculate_adsorption(1,K,D1m(K),d_totX)
   fK11H2p = fK11H2p * eMM(s,fK11H2p)

   ! Calculate the source terms as a result of feeding activity...........
   pueH2Q1X = 0._fp8







   call B_uptake( K, fQ7H2c, fQ7H2n, fQ7H2p, &
                  fQ6H2c, fQ6H2n, fQ6H2p, fQ1H2c, fQ1H2n, fQ1H2p,&
                  pueH2Q1X, pueH2Q1X, fK14H2n, fK11H2p, &
                  SH2c(K), SH2n, SH2p, SK14n, SK11p )







   ! Calculate respiration as rest respiration plus activity respiration..

   fH2G3c = fQIH2c * purH2X + srH2X * H2cP(K) * eT

   SH2c(K) = SH2c(K) - fH2G3c
   SK6e = SK6e - fH2G3c / CMass
   SG3c(K) = SG3c(K) + fH2G3c / CMass

   ! Calculate mortality..................................................

   pdH2Q1X = 0._fp8
   sfH2QI = sdH2X * ( 1._fp8 - eOx )
   sfH2Q6 = sfH2QI * (1._fp8 - pdH2Q1X)
   sfH2Q1 = sfH2QI * pdH2Q1X

   call Mortality(K, sfH2Q6, sfH2Q1, SH2c(K), SH2n, SH2p, &
                                     H2cP(K), H2cP(K)*qnHIcX, H2cP(K)*qpHIcX)

   ! Adjust cnp excretion to maintain fixed ratio.........................

   call Adjust_fixed_nutrients ( SH2c(K), SH2n, SH2p, qnHIcX, qpHIcX, SK14n, SK11p, SQ6c(K) )







   ! Calculate net flux into Q6...........................................
   gQ6H2(1) = SQ6c(K) - gQ6H2(1)
   gQ6H2(2) = SQ6n(K) - gQ6H2(2)
   gQ6H2(3) = SQ6p(K) - gQ6H2(3)
   gQ6H2(4) = 0._fp8 - gQ6H2(4)
! call Calc_cnps_flux ( gQ6H2, fQ6H2c, fQ6H2n, fQ6H2p, 0d0 )
   gQ7H2(1) = SQ7c(K) - gQ7H2(1)
   gQ7H2(2) = SQ7n(K) - gQ7H2(2)
   gQ7H2(3) = SQ7p(K) - gQ7H2(3)
   gQ7H2(4) = 0._fp8 - gQ7H2(4)
! call Calc_cnps_flux ( gQ7H2, fQ7H2c, fQ7H2n, fQ7H2p, 0d0 )
   fH2Q6(1) = SQ6c(K) - fH2Q6(1)
   fH2Q6(2) = SQ6n(K) - fH2Q6(2)
   fH2Q6(3) = SQ6p(K) - fH2Q6(3)
   fH2Q6(4) = SQ6s(K) - fH2Q6(4)
! call Calc_cnps_flux ( fH2Q6, SQ6c(I), SQ6n(I), SQ6p(I), SQ6s(I) )
   fH2Q7(1) = SQ7c(K) - fH2Q7(1)
   fH2Q7(2) = SQ7n(K) - fH2Q7(2)
   fH2Q7(3) = SQ7p(K) - fH2Q7(3)
   fH2Q7(4) = 0._fp8 - fH2Q7(4)
! call Calc_cnps_flux ( fH2Q7, SQ7c(I), SQ7n(I), SQ7p(I), 0d0 )

   return

   end subroutine anaerobic_bacteria_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Bacteria uptake
!
! !DESCRIPTION:
! This subroutine handles uptake of a variable nutrient ratio benthic
! detritus and nutrients by fixed ratio bacteria with the resultant
! excretion. NB Nitrate uptake maybe either from K4 or K14 depending whether
! uptake is by aerobic or anaerobic bacteria. Hence the relevant
! source term is supplied as an arguement to this routine.\\[\baselineskip]
!
! !INTERFACE:
   subroutine b_uptake ( K, flux7c, flux7n, flux7p, &
      flux6c, flux6n, flux6p, &
      flux1c, flux1n, flux1p, p_ex6oX,p_ex7oX,&
      fluxKn, fluxKp, SHc, SHn, SHp, SKn, SKp)
!
! !USES:
   use benthic_variables
!
! !INPUT PARAMETERS:
! Compartment identifier
   integer :: k

! Flux from Q7 to bacteria (cnp)....................flux7x
   real(fp8) :: flux7c, flux7n, flux7p

! Flux from Q6 to bacteria (cnp)....................flux6x
   real(fp8) :: flux6c, flux6n, flux6p

! Flux from Q1 to bacteria (cnp)....................flux1x
   real(fp8) :: flux1c, flux1n, flux1p

! Flux from nutrient pool to bacteria (np)..........fluxKx
   real(fp8) :: fluxKn, fluxKp

   real(fp8) :: p_ex7oX, p_ex6oX
!
! !INPUT/OUTPUT PARAMETERS:
! Source terms for bacteria, nutrients and detritus updated.
   real(fp8) :: SHc, SHn, SHp, SKn, SKp
!
! !LOCAL VARIABLES:
! Excretion flux back to dissolved organics (cnp)
   real(fp8) :: excflux6c, excflux6n, excflux6p
   real(fp8) :: excflux7c, excflux7n, excflux7p

! Net flux into bacteria (cnp)
   real(fp8) :: netfluxc, netfluxn, netfluxp
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Determine net and exc fluxes into bacteria...........................

   excflux7c = p_ex7oX * flux7c
   excflux7n = p_ex7oX * flux7n
   excflux7p = p_ex7oX * flux7p

   excflux6c = p_ex6oX * flux6c
   excflux6n = p_ex6oX * flux6n
   excflux6p = p_ex6oX * flux6p

   netfluxc = flux7c +flux6c - excflux6c - excflux7c + flux1c
   netfluxn = flux7n +flux6n - excflux6n - excflux7n + flux1n + fluxKn
   netfluxp = flux7p +flux6p - excflux6p - excflux7p + flux1p + fluxKp

   ! Set source terms.....................................................

   SQ7c(K) = SQ7c(K) - flux7c
   SQ7n(K) = SQ7n(K) - flux7n
   SQ7p(K) = SQ7p(K) - flux7p

   SQ6c(K) = SQ6c(K) - flux6c
   SQ6n(K) = SQ6n(K) - flux6n
   SQ6p(K) = SQ6p(K) - flux6p

   SQ1c(K) = SQ1c(K) + excflux6c + excflux7c - flux1c
   SQ1n(K) = SQ1n(K) + excflux6n + excflux7n - flux1n
   SQ1p(K) = SQ1p(K) + excflux6p + excflux7p - flux1p

   SHc = SHc + netfluxc
   SHn = SHn + netfluxn
   SHp = SHp + netfluxp

   SKn = SKn - fluxKn
   SKp = SKp - fluxKp

   return

   end subroutine b_uptake
!
!EOC
!-----------------------------------------------------------------------

   end module benthic_bacteria

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
