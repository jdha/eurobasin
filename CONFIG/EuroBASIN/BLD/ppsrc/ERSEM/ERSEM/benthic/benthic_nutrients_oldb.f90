

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Oldenberg benthic nutrients
!
! !DESCRIPTION:
! Module for the benthic nutrient dynamics and parameters.\\[\baselineskip]
!
! !INTERFACE:
   module ben_nut_oldb
!
! !USES:
   use ersem_constants, only: fp8
   use ersem_variables, only: IOProcess, eLogU, eDbgU
   use ben_oldb, only: diff1,diff2,diff3,cmix,d1,d2,d3,EndProfile, &
                       EquProfile,NonEquFlux,Prof_Parameter

   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public set_benold_parameters, Nutrient_DynamicsOL
!
! !PUBLIC DATA MEMBERS:
! Oxygen diffusion time scale for benthic/pelagic interface [1/d]
   real(fp8), public :: relax_oX

! Carbondioxid diffusion time scale for benthic/pelagic interface [1/d]
   real(fp8), public :: relax_mX

! Diffusivity in 1st oxygenated layer [m2/day]
   real(fp8), public :: edz_1X

! Diffusivity in 2nd oxidised layer [m2/day]
   real(fp8), public :: edz_2X

! Diffusivity in 3rd reduced layer [m2/day]
   real(fp8), public :: edz_3X

! Equilibrium diffusive speed between sediment surface water [m/d]
   real(fp8), public :: edz_mixX

! Phospherus adsorption factor in oxygenated layer
   real(fp8), public :: m1adsX

! Phospherus adsorption factor in oxidisedlayer
   real(fp8), public :: m11adsX

! Ammonium adsorption factor
   real(fp8), public :: m4adsX

! Specific nitrification rate [1/d]
   real(fp8), public :: sm4m3X

! Michaelis-Menten constant for benthic nitrate limitiation
! of nitrification [mg/m^3]
   real(fp8), public :: hm4m3X

! Regulating temperature factor Q10 for nitrification
   real(fp8), public :: q10nitX

! Michaelis-Menten constant for nitrate limitation of
! dinitrification [mmol/m]
   real(fp8), public :: hm3g4X

! Fraction of H2-oxygen-consumption taken from NO3
   real(fp8), public :: pammonX

! Fraction of pammonX denitrified in N2. The remainder goes
! into NH4. Must be 0 in the closed system
   real(fp8), public :: pdenitX

! Specific silicate regeneration rate [1/d]
   real(fp8), public :: sq6m5X
!
! !LOCAL VARIABLES:
! Oxygen equivalents - the value for o2 is 1 by our definition:
   real(fp8), parameter :: xo2X = 1.0
   real(fp8), parameter :: xco2X = 1.0
   real(fp8), parameter :: xno3X = 2.0
   real(fp8), parameter :: xn2X = 0.75
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise parameters

! !DESCRIPTION:
! Initialise benthic nutrient parameters from namelist
! \texttt{benthicOldenburgParameters}. \\[\baselineskip]
!
! !INTERFACE:
   subroutine set_benold_parameters
!
! !LOCAL VARIABLES:
   namelist/benthicOldenburgParameters/relax_oX,relax_mX,EDZ_1X,&
      EDZ_2X,EDZ_3X,EDZ_mixX,M1adsX,M11adsX,M4adsX,sM4M3X,hM4M3X,&
      q10nitX,hM3G4X,pammonX,pdenitX,sQ6M5X
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading Oldenburg benthic model parameters'
   open(99,file='BioParams/benthicOldenburg.nml',status='old')
   read(99,nml=benthicOldenburgParameters)

   close (99)
   return

   end subroutine set_benold_parameters
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthic nutrient dynamics
!
! !DESCRIPTION:
! Computes the nutrient dynamics and then determines the
! inorganic fluxes at the benthic/pelagic interface and the benthic
! layer dynamics from the process rates in benthic layers.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Nutrient_DynamicsOL(I,K,jMU,jMI,jMN,irrenh)
!
! !USES:
   use ersem_variables, only: iswPHx



   use benthic_variables
   use pelagic_variables, only: n_comp, n_upperX, N4n, O2oP, &
      O3cP, N1pP, N3nP, N4nP, N5sP, phx, etw
   use benthic_parameters, only: qpwx, d_totx
   use general, only: eTq,eMM,NDiffusion
   use ben_gen, only: partQ
   use ben_phys, only: benthic_morfology
!
! !INPUT PARAMETERS:
! Diffusion enhancement due to irrigation
   real(fp8), intent(in) :: irrenh

! i: seafloor box, k: benthic box
   integer, intent(in) :: i,k
!
! !INPUT/OUTPUT PARAMETERS:
! Upper benthic layer inorganic fluxes NH4,PO4,SiO4,O2,CO2,NO3,N2
   real(fp8) :: jMU(7)

! Second benthic layer inorganic fluxes?
   real(fp8) :: JMI(7)

! Net flux at benthic/pelagic interface
   real(fp8) :: jMN(7)
!
! !LOCAL VARIABLES:
   real(fp8) :: Fph
   real(fp8) :: eT,eN
   real(fp8) :: jM4M3n,jM3M4n,jMIo2,jMIno3,MU_m
   real(fp8) :: poro,M1ads
   real(fp8) :: vG2,vM3,vM1,vM11,vM4,vM5,vG3
   real(fp8) :: dum
   real(fp8) :: rsil,jmun

! ProfileX: Working array with:
! 1: first layer production
! 2: second layer production
! 3: third layer production
! 4: concentration at seafloor (benthic/pelagic interface)
! 5: concentration at 1st/2nd layer interface
! 6: concentration at 2nd/3rd layer interface
! 7: concentraion at lower boundary of 3rd layer
! 8: 1st layer volume factor (porosity)
! 9: 2nd layer volume factor (porosity)
! 10: 3rd layer volume factor (porosity)
! 11: 1st layer content (per m^2)
! 12: 2nd layer content (per m^2)
! 13: 3rd layer content (per m^2)
! 14: computed difference in total benthic layer content
! 15: lower boundary of 3rd layer
   real(fp8) :: profO2(15),profNO3(15),profN(15),profP(15)
   real(fp8) :: profS(15),profCO2(15),profD(15)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   !--------------------------------------------
   ! Nitrification & Denitrification in mmol/day
   !--------------------------------------------

   !* Nitrification:
   ! Reaction: (NH4+,OH-) + 2*O2 -> (NO3-,H+) + 2*H2O
   ! treated as first order reaction for NH4 in the oxygenated layer
   ! Average nitrate density (mmol/m3): MU_m

   MU_m = K3n(K)/(D1m(K)+(D2m(K)-D1m(K))/3.0d0)

   eT = eTq(ETW(I),q10nitX)
   eN = eMM(hM4M3X,MU_m)

   ! Ph influence on nitrification - empirical equation
   if (ISWphx .eq. 1)then
      Fph = MIN(2._fp8,MAX(0._fp8,0.6111_fp8*phx(I)-3.8889_fp8))
      jM4M3n = Fph * sM4M3X * K4nP(K) * (D1m(K)/d_totX) * eT * eN
   else
      jM4M3n = sM4M3X*K4nP(K)*(D1m(K)/d_totX)*eT*eN
   end if

   SK4n(K) = SK4n(K) - jM4M3n
   SK3n(K) = SK3n(K) + jM4M3n
   SG2o(K) = SG2o(K) - xno3X*jM4M3n

   jMU(1) = jMU(1) - jM4M3n
   jMU(6) = jMU(6) + jM4M3n
   jMU(4) = jMU(4) - xno3X*jM4M3n

   !* Denitrification:
   ! The part pammonX (maximal) of anaerobic respiration reduces NO3.
   ! From this NO3 reduction the part pdenitX goes to N2-gas.

   MU_m = K3nP(K)/(D1m(K)+(D2m(K)-D1m(K))/3._fp8)
   eN = eMM(MU_m/3._fp8,hM3G4X)

   ! "borrowed" oxygen consumption in anaerobic layer:

   ! to avoid numerical problems, use the recalculated value from the record
   jMIo2=jMI(4)

   ! "expression in nitrate reduction (100%):
   jMIno3 = -jMIo2/(xno3X - xn2X*pdenitX)
   jM3M4n = jMIno3*eN*pammonX*(1._fp8-pdenitX)

   ! jjM3M4n(i) = jMIno3*eN*pammonX*(1.0d0-pdenitX)
   jM3G4n = jMIno3*eN*pammonX* pdenitX

   SK14n = SK14n + jM3M4n
   SK3n(K) = SK3n(K) - jM3M4n - jM3G4n
   SG4n(K) = SG4n(K) + jM3G4n
   SK6e = SK6e + xno3X*jM3M4n + (xno3X-xn2X)*jM3G4n

   jMI(1) = jMI(1) + jM3M4n
   jMI(6) = jMI(6) - jM3M4n - jM3G4n
   jMI(7) = jMI(7) + jM3G4n
   jMI(4) = jMI(4) + xno3X*jM3M4n + (xno3X-xn2X)*jM3G4n

   !--------------------------------------------
   ! Silicate regeneration - preliminary
   !--------------------------------------------

   ! aerobic layer: 0 .. D1m(K)
   rsil = sQ6M5X*partq(D9m(K),0._fp8,D1m(K),d_totX)

   SQ6s(K) = SQ6s(K) - rsil*Q6sP(K)
   SK5s(K) = SK5s(K) + rsil*Q6sP(K)

   jMU(3) = jMU(3) + rsil*Q6sP(K)

   SD9m(K) = SD9m(K) + (D1m(K)/2._fp8-D9m(K))*rsil






   !lower layer: D1m(K) .. d_totX
   rsil = sQ6M5X*partq(D9m(K),D1m(K),d_totX,d_totX)

   SQ6s(K) = SQ6s(K) - rsil*Q6sP(K)
   SK5s(K) = SK5s(K) + rsil*Q6sP(K)

   jMI(3) = jMI(3) + rsil*Q6sP(K)

   SD9m(K) = SD9m(K) + D1m(K)*rsil

   !------------------------------------------
   ! Nutrient profiles and surface gradients
   !------------------------------------------

   ! Preparing the profile:
   diff1 = EDZ_1X *irrenh
   diff2 = EDZ_2X *irrenh
   diff3 = EDZ_3X *irrenh
   cmix = 0._fp8
   d1 = D1m(K)
   d2 = D2m(K)
   d3 = d_totX

   ! Volume factors:
   poro=qPWX
   M1ads=M1adsX

   if (N_COMP-N_UPPERX.gt.0) then
      poro=benthic_morfology(1,K) ! porosity factor
      M1ads=benthic_morfology(2,K) ! adsorption factor
   end if

   vG2 = poro
   vM3 = poro
   vM4 = poro*M4adsX
   vM1 = poro*M1ads
   vM11 = poro*M11adsX
   vM5 = poro
   vG3 = poro

   ! Profile parameters:
   jmun = jmu(1)
   if (jmun .LT. 0._fp8) then
      jmu(1) = jmun*N4n(I)/(N4n(I)+0.5_fp8)
      jmi(1) = jmi(1) + (jmun - jmu(1))
   end if
   jmu(5) = SG3c(K)
   jmi(5) = 0._fp8

   call Prof_Parameter(profO2, jMU(4), jMI(4), 0._fp8, vG2,vG2,vG2)
   call Prof_Parameter(profNO3, jMU(6),jMI(6),0._fp8, vM3,vM3,vM3)
   call Prof_Parameter(profN, jMU(1), jMI(1), 0._fp8, vM4,vM4,vM4)
   call Prof_Parameter(profP, jMU(2), jMI(2), 0._fp8, vM1,vM1,vM11)
   call Prof_Parameter(profS, jMU(3), jMI(3), 0._fp8, vM5,vM5,vM5)
   call Prof_Parameter(profCO2, jMU(5),jMI(5),0._fp8, vG3,vG3,vG3)
   call Prof_Parameter(profD, d1,d2-d1,d3-d2, 1._fp8,1._fp8,1._fp8)

   call EquProfile(0._fp8,1._fp8,profD,dum,k)

   cmix = EDZ_mixX

   ! Oxygen
   call EndProfile(O2oP(I),G2oP(K),profO2,jMN(4))
   ! Non-equilibrium correction:
   jMN(4) = jMN(4) - profO2(14)/relax_oX

   ! Change of oxigen horizon:
   SD1m(K) = SD1m(K) + (max(D1m0,profO2(15))-D1m(K))/relax_oX

   ! Nitrate
   call EndProfile(N3nP(I),K3nP(K),profNO3,jMN(6))
   ! Non-equilibrium correction:
   jMN(6) = jMN(6) - profNO3(14)/relax_mX

   ! Change of nitrate horizon:
   SD2m(K) = SD2m(K) + (max(D2m0,profNO3(15))-D2m(K))/relax_mX

   ! Ammonium

   call EquProfile(N4nP(I),K4nP(K),profN,jMN(1),k)
   ! Non-equilibrium correction:
   call NonEquFlux(profN,profD,jMN(1))
   jMN(1) = profN(1) + profN(2) + profN(3)
   ! Phospate
   call EquProfile(N1pP(I),K1pP(K),profP,jMN(2),k)
   ! Non-equilibrium correction:
   call NonEquFlux(profP,profD,jMN(2))
   jMN(2) = profP(1) + profP(2) + profP(3)
   ! Silicate
   call EquProfile(N5sP(I),K5sP(K),profS,jMN(3),k)
   ! Non-equilibrium correction:
   call NonEquFlux(profS,profD,jMN(3))
   jMN(3) = profS(1) + profS(2) + profS(3)
   ! Carbon dioxide
   call EquProfile(O3cP(I),G3cP(K),profCO2,jMN(5),k)
   ! Non-equilibrium correction:
   call NonEquFlux(profCO2,profD,jMN(5))
   jMN(5) = profCO2(1) + profCO2(2) + profCO2(3)
   ! nitrate gas:
   jmn(7) = 0._fp8
   !-----------
   ! Diffusion
   !-----------
   call NDiffusion(I,K,jMN)
   return
   end subroutine Nutrient_DynamicsOL
!
!EOC
!-----------------------------------------------------------------------
   end module ben_nut_oldb
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
