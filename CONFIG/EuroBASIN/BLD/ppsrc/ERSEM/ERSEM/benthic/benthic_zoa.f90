

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Benthic zoa
!
! !DESCRIPTION:
! Module with benthic zoa dynamics and parameters.\\[\baselineskip]
!
! !INTERFACE:
   module benthic_zoa
!
! !USES:
   use ersem_constants, only: fp8
   use general, only: eMM3,eMM,adjust_fixed_nutrients
   use ben_gen, only: fY2Q6,fY3Q6,fY4Q6,dQ6Y4X,hO2Y4X,huy4x,luy4x, &
      pudily4x,puey4x,puh1y4x,puh2y4x,puq6y4x,pury4x, &
      puy4y4x,q10y4x,rlo2y4x,sdcY4X,sdmO2Y4X,sdY4X, &
      sry4x,suy4x,xdcy4x,dQ6Y3X,dwaty3x,dwaty3x, &
      ho2y3x,huy3x,luy3x,pudily3x,pueq6y3x,puey3x, &
      puh1y3x,pup1y3x,pup2y3x,pup3y3x,puq6y3x,pur6y3x, &
      pury3x,q10y3x,rlo2y3x,sdcy3x,sdmo2y3x,sdy3x, &
      sry3x,suy3x,xchy3x,xcly3x,dq6y2x,ho2y2x,huy2x, &
      luy2x,pudily2x,pueq6y2x,puey2x,puh1y2x,puh2y2x, &
      puq6y2x,pury2x,puy4y2x,q10y2x,rlo2y2x,sdcy2x, &
      sdmo2y2x,sdy2x,sry2x,suy2x,xchy2x,xcly2x,xcsy2x, &
      xdcy2x,xcsy3x,xdcy3x
   use ben_gen, only: AvQ6,Respiration_fixed,Mortality,Food_Partition

   implicit none

! Default all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public deposit_feeder_dynamics, suspension_feeder_dynamics, &
      meiobenthos_dynamics
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Deposit feeder dynamics
!
! !DESCRIPTION:
! This routine treats the processes mediated by deposit feeders Y2
! These can be categorised into: Uptake (and resulting excretion etc.),
! respiration, and mortality. Four subroutines set the source terms for
! these three processes taking the process fluxes as input.\\[\baselineskip]

! !INTERFACE:
   subroutine deposit_feeder_dynamics(I,K)
!
! !USES:
   use ersem_constants, only: zeroX
   use pelagic_variables, only: o2o, etw
   use benthic_variables
   use benthic_parameters
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i, k
!
! !LOCAL VARIABLES:
! Limitation factor due to temperature (0.0-1.0)
   real(fp8) :: eT

! Limitation factor due to oxygen
   real(fp8) :: eO

! Rate of uptake of Y2
   real(fp8) :: rate

! Pseudo source terms for nutrient content (np)
   real(fp8) :: SY2n, SY2p

   real(fp8) :: fH1Y2c, fH2Y2c, fY4Y2c, fQ6Y2c, fY2G3c, sfY2Q6, sfY2Q1
   real(fp8) :: sfH1Y2, sfH2Y2, sfY4Y2, sfQ6Y2
   real(fp8) :: nfH1Y2c, nfH2Y2c, nfY4Y2c, nfQ6Y2c, nfBTY2c, dummy
   real(fp8) :: AQ6c, AQ6c2, AQ6n, AQ6p, excp, excn, p_an
   real(fp8) :: mort_act, mort_ox, mort_cold
   real(fp8) :: eC, Y2, x
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Set up pseudo source terms for nutrient content......................

   SY2n = SY2c(K) * qnYIcX
   SY2p = SY2c(K) * qpYIcX

   ! Initialise net flux into Q6 calculation..............................

   fY2Q6(1) = SQ6c(K)
   fY2Q6(2) = SQ6n(K)
   fY2Q6(3) = SQ6p(K)
   fY2Q6(4) = SQ6s(K)
! call Init_cnps_flux ( fY2Q6, SQ6c(I), SQ6n(I), SQ6p(I), SQ6s(I) )

   ! Calculate limitation factors.........................................

   et = q10Y2X**((ETW(I)-10._fp8)/10._fp8) - q10Y2X**((ETW(I)-32._fp8)/3._fp8)

   eO = eMM3( O2o(I) - rlO2Y2X, hO2Y2X - rlO2Y2X )

   Y2 = Y2c(K) - xclY2X
   if ( Y2 .gt. 0._fp8 ) then
     x = Y2 * eMM( Y2, xcsY2X )
     eC = 1._fp8 - eMM( x, xchY2X )
   else
     eC = 1._fp8
   end if

   ! Calculate uptake rate................................................
   rate = suY2X * Y2c(K) * eT * eO * eC

   ! Calculate available detritus.........................................
   call AvQ6( K, dQ6Y3X, dQ6Y2X, AQ6c, AQ6n, AQ6p )
   call AvQ6( K, D1m(K), dQ6Y2X, AQ6c2, dummy, dummy )

   ! Determine partitioning between available food sources................
   call Food_Partition ( rate, luY2X, huY2X, &
                          puH1Y2X,H1cP(K), sfH1Y2, &
                          puH2Y2X,H2cP(K), sfH2Y2, &
                          puY4Y2X,Y4cP(K), sfY4Y2, &
                          puQ6Y2X,AQ6c, sfQ6Y2, &
                          0._fp8, 0._fp8, dummy, &
                          0._fp8, 0._fp8, dummy, &
                          0._fp8, 0._fp8, dummy, &
                          0._fp8, 0._fp8, dummy, &
                          0._fp8, 0._fp8, dummy )

   ! Calculate the source terms as a result of feeding activity...........
   call Y_uptake_fixed ( K, sfH1Y2, fH1Y2c, nfH1Y2c, pueY2X, pudilY2X, &
                      SY2c(K), SY2n, SY2p, SH1c(K), &
                      H1cP(K), H1cP(K)*qnHIcX, H1cP(K)*qpHIcX )
   call Y_uptake_fixed ( K, sfH2Y2, fH2Y2c, nfH2Y2c, pueY2X, pudilY2X, &
                      SY2c(K), SY2n, SY2p, SH2c(K), &
                      H2cP(K), H2cP(K)*qnHIcX, H2cP(K)*qpHIcX )
   call Y_uptake_fixed ( K, sfY4Y2, fY4Y2c, nfY4Y2c, pueY2X, pudilY2X, &
                      SY2c(K), SY2n, SY2p, SY4c(K), &
                      Y4cP(K), Y4cP(K)*qnYIcX, Y4cP(K)*qpYIcX )
   call Y_uptake_Q6 ( K, sfQ6Y2, fQ6Y2c, nfQ6Y2c, pueQ6Y2X, pudilY2X, &
                               AQ6c, AQ6n, AQ6p, SY2c(K), SY2n, SY2p )

   ! Calculate gross uptake flux (used in calculation of bioturbation)....
   fBTY2c = fH1Y2c + fH2Y2c + fY4Y2c + fQ6Y2c

   ! Calculate respiration as rest respiration plus activity respiration..
   nfBTY2c = nfH1Y2c +nfH2Y2c +nfY4Y2c +nfQ6Y2c

   fY2G3c = (srY2X * Y2cP(K) * eT) + (purY2X * nfBTY2c)
   call Respiration_fixed( K, fY2G3c, SY2c(K) )

   ! Calculate mortalities................................................
   mort_act = sdY2X * eT
   mort_ox = sdmO2Y2X * (1._fp8 - eO) * eT
   mort_cold = sdcY2X * EXP( ETW(I) * xdcY2X )

   sfY2Q6 = ( mort_act + mort_ox + mort_cold )

   sfY2Q1 = 0._fp8

   call Mortality( K, sfY2Q6, sfY2Q1, &
                      SY2c(K), SY2n, SY2p, &
                      Y2cP(K), Y2cP(K)*qnYIcX, Y2cP(K)*qpYIcX )

   ! Adjust cnp excretion to maintain fixed ratio.........................
   excn=0._fp8
   excp=0._fp8

   call Adjust_fixed_nutrients ( SY2c(K), SY2n, SY2p, qnYIcX, &
                                 qpYIcX, excn, excp, SQ6c(K) )

   p_an = (fH2Y2c+MIN(1._fp8,AQ6c2/(AQ6c+1e-15_fp8))*fQ6Y2c)/max(fBTY2c,zeroX)
   SK4n(K) =SK4n(K) + (1._fp8-p_an) * excn
   SK14n=SK14n + (p_an) * excn
   SK1p(K) =SK1p(K) + (1._fp8-p_an) * excp
   SK11p=SK11p + (p_an) * excp

   ! Calculate net flux into Q6...........................................
   fY2Q6(1) = SQ6c(K) - fY2Q6(1)
   fY2Q6(2) = SQ6n(K) - fY2Q6(2)
   fY2Q6(3) = SQ6p(K) - fY2Q6(3)
   fY2Q6(4) = SQ6s(K) - fY2Q6(4)
! call Calc_cnps_flux ( fY2Q6, SQ6c(I), SQ6n(I), SQ6p(I), SQ6s(I) )

   return

   end subroutine deposit_feeder_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Suspension feeder dynamics
!
! !DESCRIPTION:
! This routine treats the processes mediated by suspension feeders Y3.
! These can be categorised into: Uptake (and resulting excretion etc.),
! respiration, and mortality. Four subroutines set the source terms for
! these three processes taking the process fluxes as input.\\[\baselineskip]
!
! !INTERFACE:
   subroutine suspension_feeder_dynamics(I,K)
!
! !USES:
   use benthic_variables
   use pelagic_variables, only: etw, o2o, P1cP, P1nP, P1pP, P1sP, &
      P2cP, P2nP, P2pP, &
      P3cP, P3nP, P3pP, &
      P4nP, P4pP, &
      Chl1P, Chl2P, Chl3P, Chl4P, &
      R6cP, R6nP, R6pP, R6sP, &
      so2o, sP1c, sP1n, sP1p, sP1s, &
      sP2c, sP2n, sP2p, &
      sP3c, sP3n, sP3p, &
      sP4n, sP4p, &
      sChl1, sChl2, sChl3, sChl4, &
      sR6c, sR6n, sR6p, sR6s, &
      pdepth

   use pelagic_variables, only: P1fP, P2fP, P3fP, R6fP, &
      sP1f, sP2f, sP3f, sR6f, sN7f

   use benthic_parameters
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i, k
!
! !LOCAL VARIABLES:
! Limitation factor due to temperature (0.0-1.0)
   real(fp8) :: eT

! Rate of uptake of Y3
   real(fp8) :: rate

! Pseudo source terms for nutrient content (np).......SY3x
   real(fp8) :: SY3n, SY3p

! Net flux from AA to BB in x
   real(fp8) :: nfP1Y3c, nfP2Y3c, nfP3Y3c, nfR6Y3c, nfQ6Y3c, &
      nfH1Y3c

! Total net flux into Y3
   real(fp8) :: nfBTY3c

   real(fp8) :: eO, eC, x
   real(fp8) :: fP1Y3c, fP2Y3c, fP3Y3c, fR6Y3c, fH1Y3c, fQ6Y3c, &
      sfP1Y3, sfP2Y3, sfP3Y3, sfR6Y3, sfH1Y3, sfQ6Y3, &
      fY3G3c, sfY3Q6, sfY3Q1
   real(fp8) :: AQ6c, AQ6n, AQ6p, dummy, Y3, mort_act, &
      mort_ox, mort_cold
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Set up pseudo source terms for nutrient content......................
   SY3n = SY3c(K) * qnYIcX
   SY3p = SY3c(K) * qpYIcX

   ! Initialise net flux into Q6 calculation..............................
   fY3Q6(1) = SQ6c(K)
   fY3Q6(2) = SQ6n(K)
   fY3Q6(3) = SQ6p(K)
   fY3Q6(4) = SQ6s(K)
! call Init_cnps_flux ( fY3Q6, SQ6c(I), SQ6n(I), SQ6p(I), SQ6s(I) )

   ! Calculate limitation factors.........................................
   et = q10Y3X**((ETW(I)-10._fp8)/10._fp8) - q10Y3X**((ETW(I)-32._fp8)/3._fp8)

   eO = eMM3( O2o(I) - rlO2Y3X, hO2Y3X - rlO2Y3X )

   Y3 = Y3c(K) - xclY3X
   if ( Y3 .gt. 0._fp8 ) then
     x = Y3 * eMM( Y3, xcsY3X )
     eC = 1._fp8 - eMM( x, xchY3X )
   else
     eC = 1._fp8
   end if

   ! Calculate uptake rate................................................
   rate = suY3X * Y3c(K) * eT * eO * eC

   ! Calculate available detritus.........................................
   call AvQ6( K, 0._fp8, dQ6Y3X, AQ6c, AQ6n, AQ6p )

   ! Determine partitioning between available food sources................
   call Food_Partition ( rate, luY3X, huY3X, &
                       dwatY3X*puP1Y3X,P1cP(I), sfP1Y3, &
                       dwatY3X*puP2Y3X,P2cP(I), sfP2Y3, &
                       dwatY3X*puP3Y3X,P3cP(I), sfP3Y3, &
                       dwatY3X*puR6Y3X,R6cP(I), sfR6Y3, &
                               puQ6Y3X,AQ6c, sfQ6Y3, &
       MIN(1._fp8,dQ6Y3X/D1m(K))* puH1Y3X,H1cP(K), sfH1Y3, &
                                      0._fp8, 0._fp8, dummy, &
                                      0._fp8, 0._fp8, dummy, &
                                      0._fp8, 0._fp8, dummy )

   ! Calculate the source terms as a result of feeding activity...........
   call Y_uptake_pel ( I,K, sfP1Y3, fP1Y3c, nfP1Y3c, pueY3X, pudilY3X, &
            P1cP(I), P1nP(I), P1pP(I), P1sP(I), &
            SY3c(K), SY3n, SY3p, SP1c(I), SP1n(I), SP1p(I), SP1s(I) )
   call Y_uptake_pel ( I,K, sfP2Y3, fP2Y3c, nfP2Y3c, pueY3X, pudilY3X, &
            P2cP(I), P2nP(I), P2pP(I), 0._fp8, &
            SY3c(K), SY3n, SY3p, SP2c(I), SP2n(I), SP2p(I), dummy )
   call Y_uptake_pel ( I,K, sfP3Y3, fP3Y3c, nfP3Y3c, pueY3X, pudilY3X, &
            P3cP(I), P3nP(I), P3pP(I), 0._fp8, &
            SY3c(K), SY3n, SY3p, SP3c(I), SP3n(I), SP3p(I), dummy )
   call Y_uptake_pel ( I,K, sfR6Y3, fR6Y3c, nfR6Y3c, pueQ6Y3X, pudilY3X, &
            R6cP(I), R6nP(I), R6pP(I), R6sP(I), &
            SY3c(K), SY3n, SY3p, SR6c(I), SR6n(I), SR6p(I), SR6s(I) )
   call Y_uptake_Q6 ( K, sfQ6Y3, fQ6Y3c, nfQ6Y3c, pueQ6Y3X, pudilY3X, &
                        AQ6c, AQ6n, AQ6p, SY3c(K), SY3n, SY3p )
   call Y_uptake_fixed ( K, sfH1Y3, fH1Y3c, nfH1Y3c, pueY3X, pudilY3X, &
                      SY3c(K), SY3n, SY3p, SH1c(K), &
                      H1cP(K), H1cP(K)*qnHIcX, H1cP(K)*qpHIcX )

   ! xfr_pY3c(I) =fp1Y3c+fp2Y3c+fp3Y3c+fr6Y3c
   ! Calculate gross uptake flux .........................................
   fBTY3c = fP1Y3c + fP2Y3c + fP3Y3c + fR6Y3c + fQ6Y3c + fH1Y3c

   SP1f(I) = SP1f(I) - sfP1Y3*P1fP(I)
   SP2f(I) = SP2f(I) - sfP2Y3*P2fP(I)
   SP3f(I) = SP3f(I) - sfP3Y3*P3fP(I)
   SR6f(I) = SR6f(I) - sfR6Y3*R6fP(I)
   SN7f(I) = SN7f(I) + sfP1Y3*P1fP(I) + sfP2Y3*P2fP(I) + &
      sfP3Y3*P3fP(I) + sfR6Y3*R6fP(I)


   ! grazing on phytoplankton:
   fPXY3c(K) = fP1Y3c + fP2Y3c + fP3Y3c + fR6Y3c
   fPXY3n(K) = sfP1Y3*P1nP(I) + sfP2Y3*P1nP(I) + &
       sfP3Y3*P3nP(I) + sfR6Y3*P4nP(I)
   fPXY3p(K) = sfP1Y3*P1pP(I) + sfP2Y3*P1pP(I) + &
       sfP3Y3*P3pP(I) + sfR6Y3*P4pP(I)
   fPXY3s(K) = sfP1Y3*P1sP(I)

   ! Calculate respiration as rest respiration plus activity respiration..
   nfBTY3c = nfP1Y3c + nfP2Y3c + nfP3Y3c + nfR6Y3c + &
             nfQ6Y3c + nfH1Y3c

   fY3G3c = (srY3X * Y3cP(k) * eT) + (purY3X * nfBTY3c)
   call Respiration_fixed( K, fY3G3c, SY3c(K) )

   ! Calculate mortalities................................................
   mort_act = sdY3X * eT
   mort_ox = sdmO2Y3X * (1._fp8 - eO) * eT
   mort_cold = sdcY3X * EXP( ETW(I) * xdcY3X )

   sfY3Q6 = ( mort_act + mort_ox + mort_cold )

   sfY3Q1 = 0._fp8

   call Mortality( K, sfY3Q6, sfY3Q1, &
                      SY3c(K), SY3n, SY3p, &
                      Y3cP(K), Y3cP(K)*qnYIcX, Y3cP(K)*qpYIcX )

   ! Adjust cnp excretion to maintain fixed ratio.........................
   call Adjust_fixed_nutrients ( SY3c(K), SY3n, SY3p, qnYIcX, &
                                 qpYIcX, SK4n(K), SK1p(K), SQ6c(K) )

   ! Calculate net flux into Q6...........................................
   fY3Q6(1) = SQ6c(K) - fY3Q6(1)
   fY3Q6(2) = SQ6n(K) - fY3Q6(2)
   fY3Q6(3) = SQ6p(K) - fY3Q6(3)
   fY3Q6(4) = SQ6s(K) - fY3Q6(4)
! call Calc_cnps_flux ( fY3Q6, SQ6c(I), SQ6n(I), SQ6p(I), SQ6s(I) )

   ! take out digested chl from phytoplankton:
   SChl1(I)=SChl1(I)-sfP1Y3*Chl1P(I)/pdepth(I)
   SChl2(I)=SChl2(I)-sfP2Y3*Chl2P(I)/pdepth(I)
   SChl3(I)=SChl3(I)-sfP3Y3*Chl3P(I)/pdepth(I)

   return

   end subroutine suspension_feeder_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Meiobenthos dynamics
!
! !DESCRIPTION:
! This routine treats the processes mediated by the meiobenthos (Y4).
! These can be categorised into: Uptake (and resulting excretion etc.),
! respiration, and mortality. Four subroutines set the source terms
! for these three processes taking the process fluxes as input.\\[\baselineskip]
!
! !INTERFACE:
   subroutine meiobenthos_dynamics(I,K)
!
! !USES:
   use benthic_variables
   use pelagic_variables, only: o2o, etw
   use benthic_parameters
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i,K
!
! !LOCAL VARIABLES:
! Limitation factor due to temperature (0.0-1.0)
   real(fp8) :: eT

! Limitation factor due to oxygen
   real(fp8) :: eO

! Rate of uptake of Y4
   real(fp8) :: rate

! Pseudo source terms for nutrient content (np)
   real(fp8) :: SY4n, SY4p

! Flux terms
   real(fp8) :: fH1Y4c, fH2Y4c, fY4Y4c, fQ6Y4c, fY4G3c, &
      sfH1Y4, sfH2Y4, sfY4Y4, sfQ6Y4, sfY4Q6, sfY4Q1

! Net flux
   real(fp8) :: nfH1Y4c, nfH2Y4c, nfY4Y4c, nfQ6Y4c

! Total net flux
   real(fp8) :: nfBTY4c

   real(fp8) :: AQ6c, AQ6n, AQ6p, dummy, mort_act, mort_ox, mort_cold
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Set up pseudo source terms for nutrient content......................
   SY4n = SY4c(K) * qnYIcX
   SY4p = SY4c(K) * qpYIcX

   ! Initialise net flux into Q6 calculation..............................
   fY4Q6(1) = SQ6c(K)
   fY4Q6(2) = SQ6n(K)
   fY4Q6(3) = SQ6p(K)
   fY4Q6(4) = SQ6s(K)
! call Init_cnps_flux ( fY4Q6, SQ6c(I), SQ6n(I), SQ6p(I), SQ6s(I) )

   ! Calculate limitation factors.........................................
   et = q10Y4X**((ETW(I)-10._fp8)/10._fp8) - q10Y4X**((ETW(I)-32._fp8)/3._fp8)

   eO = eMM3( O2o(I) - rlO2Y4X, hO2Y4X - rlO2Y4X )

   ! Calculate uptake rate................................................
   rate = suY4X * Y4c(K) * eT * eO

   ! Calculate available detritus.........................................
   call AvQ6( K, 0._fp8, dQ6Y4X, AQ6c, AQ6n, AQ6p )

   ! Determine partitioning between available food sources................
   call Food_Partition ( rate, luY4X, huY4X, &
                   puH1Y4X,H1cP(K), sfH1Y4, &
                   puH2Y4X,H2cP(K), sfH2Y4, &
                   puY4Y4X,Y4cP(K), sfY4Y4, &
                   puQ6Y4X,AQ6c, sfQ6Y4, &
                   0._fp8, 0._fp8, dummy, &
                   0._fp8, 0._fp8, dummy, &
                   0._fp8, 0._fp8, dummy, &
                   0._fp8, 0._fp8, dummy, &
                   0._fp8, 0._fp8, dummy )

   ! Calculate the source terms as a result of feeding activity...........
   call Y_uptake_fixed ( K, sfH1Y4, fH1Y4c, nfH1Y4c, pueY4X, pudilY4X, &
                      SY4c(K), SY4n, SY4p, SH1c(K), &
                      H1cP(K), H1cP(K)*qnHIcX, H1cP(K)*qpHIcX )
   call Y_uptake_fixed ( K, sfH2Y4, fH2Y4c, nfH2Y4c, pueY4X, pudilY4X, &
                      SY4c(K), SY4n, SY4p, SH2c(K), &
                      H2cP(K), H2cP(K)*qnHIcX, H2cP(K)*qpHIcX )
   call Y_uptake_self ( K, sfY4Y4, fY4Y4c, nfY4Y4c, pueY4X, pudilY4X, &
           Y4cP(K),Y4cP(K)*qnYIcX,Y4cP(K)*qpYIcX,SY4c(K), SY4n, SY4p )
   call Y_uptake_Q6 ( K, sfQ6Y4, fQ6Y4c, nfQ6Y4c, pueY4X, pudilY4X, &
                               AQ6c, AQ6n, AQ6p, SY4c(K), SY4n, SY4p )

   ! Calculate gross uptake flux (used in calculation of bioturbation)....
   fBTY4c = fH1Y4c + fH2Y4c + fY4Y4c + fQ6Y4c

   ! Calculate respiration as rest respiration plus activity respiration..
   nfBTY4c = nfH1Y4c + nfH2Y4c + nfY4Y4c + nfQ6Y4c

   fY4G3c = (srY4X * Y4cP(K) * eT) + (purY4X * nfBTY4c)
   call Respiration_fixed( K, fY4G3c, SY4c(K) )

   ! Calculate mortalities................................................
   mort_act = sdY4X * eT
   mort_ox = sdmO2Y4X * (1._fp8 - eO) * eT
   mort_cold = sdcY4X * EXP( ETW(I) * xdcY4X )

   sfY4Q6 = ( mort_act + mort_ox + mort_cold )

   sfY4Q1 = 0._fp8

   call Mortality( K, sfY4Q6, sfY4Q1, &
                      SY4c(K), SY4n, SY4p, &
                      Y4cP(K), Y4cP(K)*qnYIcX, Y4cP(K)*qpYIcX )

   ! Adjust cnp excretion to maintain fixed ratio.........................
   call Adjust_fixed_nutrients ( SY4c(K), SY4n, SY4p, qnYIcX, &
                                 qpYIcX, SK4n(K), SK1p(K), SQ6c(K) )

   ! Calculate net flux into Q6...........................................
   fY4Q6(1) = SQ6c(K) - fY4Q6(1)
   fY4Q6(2) = SQ6n(K) - fY4Q6(2)
   fY4Q6(3) = SQ6p(K) - fY4Q6(3)
   fY4Q6(4) = SQ6s(K) - fY4Q6(4)
! call Calc_cnps_flux ( fY4Q6, SQ6c(I), SQ6n(I), SQ6p(I), SQ6s(I) )

   return

   end subroutine meiobenthos_dynamics
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthic zoa uptake, fixed \label{sec:YUptakeFixed}
!
! !DESCRIPTION:
! This subroutine handles uptake of one fixed nutrient ratio state
! by another fixed ratio state and the resultant excretion to detritus
! and nutrients.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Y_uptake_fixed ( K, sflux, fluxc, netfluxc, p_excX, &
      p_dilX, SYc, SYn, SYp, Sprey, dc, dn, dp )
!
! !USES:
   use benthic_variables
!
! !INPUT PARAMETERS:
! Compartment index
   integer,intent(in) :: k

! Specific gross uptake flux from prey to predator
   real(fp8),intent(in) :: sflux

! Excreted fraction of uptake
   real(fp8),intent(in) :: p_excX

! Dilution factor for excretion
   real(fp8),intent(in) :: p_dilX

! Prey biomass densities
   real(fp8),intent(in) :: dc, dn, dp
!
! !INPUT/OUTPUT PARAMETERS:
! Source terms updated for predator (cnp)
   real(fp8) :: SYc, SYn, SYp

! Source terms updated for prey
   real(fp8) :: Sprey
!
! !OUTPUT PARAMETERS:
! Gross uptake flux (c) from prey to predator
   real(fp8),intent(out) :: fluxc

! Net uptake flux (c)
   real(fp8),intent(out) :: netfluxc
!
! !LOCAL VARIABLES:
! Excretion flux (cnp)
   real(fp8) :: excfluxc, excfluxn, excfluxp
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   fluxc = sflux * dc

   ! Set net and excretion fluxes.........................................

   excfluxc = fluxc * p_excX
   netfluxc = fluxc - excfluxc

   excfluxn = sflux * p_excX * dn * p_dilX
   excfluxp = sflux * p_excX * dp * p_dilX

   ! Set source terms.....................................................
   SYc = SYc + netfluxc
   SYn = SYn + sflux * dn - excfluxn
   SYp = SYp + sflux * dp - excfluxp
   SPrey = SPrey - fluxc

   SQ6c(K) = SQ6c(K) + excfluxc
   SQ6n(K) = SQ6n(K) + excfluxn
   SQ6p(K) = SQ6p(K) + excfluxp

   ! Excess nutrient retention is dealt with in adjust_fixed_nutrients....

   return

   end subroutine Y_uptake_fixed
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthic zoa uptake, self \label{sec:YUptakeSelf}
!
! !DESCRIPTION:
! This subroutine handles uptake of one fixed nutrient ratio state
! by another fixed ratio state and the resultant excretion to detritus
! and nutrients.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Y_uptake_self (K, sflux, fluxc, netfluxc, &
      p_excX, p_dilX, dc, dn, dp, SYc, SYn, SYp )
!
! !USES:
   use benthic_variables
!
! !INPUT PARAMETERS:
! Active compartment
   integer,intent(in) :: k

! Specific gross uptake flux from prey to predator
   real(fp8),intent(in) :: sflux

! Excreted fraction of uptake
   real(fp8),intent(in) :: p_excX

! Dilution factor for excretion
   real(fp8),intent(in) :: p_dilX

   real(fp8),intent(in) :: dc, dn, dp
!
! !INPUT/OUTPUT PARAMETERS:
! Source terms updated for Predator (cnp)
   real(fp8) :: SYc, SYn, SYp
!
! !OUTPUT PARAMETERS:
! Gross uptake flux (c) from prey to predator
   real(fp8),intent(out) :: fluxc

! Net uptake flux (c)
   real(fp8),intent(out) :: netfluxc
!
! !LOCAL VARIABLES:
! Excretion flux (cnp)
   real(fp8) :: excfluxc, excfluxn, excfluxp
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   fluxc = sflux * dc

   ! Set net and excretion fluxes.........................................

   excfluxc = fluxc * p_excX
   netfluxc = fluxc - excfluxc

   excfluxn = sflux * p_excX * dn * p_dilX
   excfluxp = sflux * p_excX * dp * p_dilX

   ! Set source terms.....................................................

   SYc = SYc - excfluxc
   SYn = SYn - excfluxn
   SYp = SYp - excfluxp

   SQ6c(K) = SQ6c(K) + excfluxc
   SQ6n(K) = SQ6n(K) + excfluxn
   SQ6p(K) = SQ6p(K) + excfluxp

   ! Note excess retention of nutrients will be adjusted in adjust_fixed_n..

   return

   end subroutine Y_uptake_self
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthic zoa uptake, detritus
!
! !DESCRIPTION:
! This subroutine handles uptake of variable nutrient ratio detritus
! by a fixed ratio state with the resultant excretion to detritus
! and nutrients. All silicate uptake is excreted, hence Q6s remains
! unchanged. \\[\baselineskip]
!
! !INTERFACE:
   subroutine Y_uptake_Q6 (K, sflux, fluxc, netfluxc, p_excX, &
      p_dilX, AQ6c, AQ6n, AQ6p, SYc, SYn, SYp)
!
! !USES:
   use benthic_variables, only : sq6c,sq6n,sq6p
!
! !INPUT PARAMETERS:
! Compartment identifier
   integer,intent(in) :: k

! Specific gross uptake flux from prey to predator
   real(fp8), intent(in) :: sflux

! Excreted fraction of uptake
   real(fp8), intent(in) :: p_excX

! Dilution factor for excretion
   real(fp8), intent(in) :: p_dilX

! Q6 Biomass densities (cnp) available
   real(fp8), intent(in) :: AQ6c, AQ6n, AQ6p
!
! !INPUT/OUTPUT PARAMETERS:
! Source terms updated for predator (cnp)
   real(fp8) :: SYc, SYn, SYp
!
! !OUTPUT PARAMETERS:
! Gross uptake flux (c) from prey to predator
   real(fp8), intent(out) :: fluxc

! Net flux (c)
   real(fp8), intent(out) :: netfluxc
!
! !LOCAL VARIABLES:
! Net flux (np)
   real(fp8) :: netfluxn, netfluxp
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   fluxc = sflux * AQ6c

   ! Set net and excretion fluxes.........................................
   netfluxc = fluxc * ( 1._fp8 - p_excX )

   netfluxn = sflux * AQ6n * ( 1._fp8 - p_excX*p_dilX )
   netfluxp = sflux * AQ6p * ( 1._fp8 - p_excX*p_dilX )

   ! Set source terms.....................................................

   SYc = SYc + netfluxc
   SYn = SYn + netfluxn
   SYp = SYp + netfluxp

   SQ6c(K) = SQ6c(K) - netfluxc
   SQ6n(K) = SQ6n(K) - netfluxn
   SQ6p(K) = SQ6p(K) - netfluxp

   ! Excess nutrient retention is dealt with in adjust_fixed_nutrients....

   return

   end subroutine Y_uptake_Q6
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthic zoa uptake, pelagic \label{sec:YUptakePel}
!
! !DESCRIPTION:
! This subroutine handles uptake of a variable nutrient ratio pelagic
! food source by a fixed ratio benthic state with the resultant
! excretion to detritus and nutrients.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Y_uptake_pel (I,K, sflux, fluxc, netfluxc, p_excX, &
      p_dilX, dc, dn, dp, ds, SYc, SYn, SYp, &
      SFoodc, SFoodn, SFoodp, SFoods)
!
! !USES:
   use benthic_variables
   use pelagic_variables, only: pdepth
!
! !INPUT PARAMETERS:
! Compartment identifier
   integer,intent(in) :: i,k

! Specific gross uptake flux from prey to predator
   real(fp8), intent(in) :: sflux

! Excreted fraction of uptake
   real(fp8), intent(in) :: p_excX

! Dilution factor for excretion
   real(fp8), intent(in) :: p_dilX

! Prey biomass densities
   real(fp8), intent(in) :: dc, dn, dp, ds
!
! !INPUT/OUTPUT PARAMETERS:
! Source terms updated for Predator (cnp)
   real(fp8) :: SYc, SYn, SYp

! Food source (cnps)
   real(fp8) :: SFoodc, SFoodn, SFoodp, SFoods
!
! !OUTPUT PARAMETERS:
! Gross uptake flux (c) from prey to predator
   real(fp8),intent(out) :: fluxc

! Net uptake flux (c)
   real(fp8),intent(out) :: netfluxc
!
! !LOCAL VARIABLES:
   real(fp8) :: fluxn, fluxp, fluxs, depth, &
      excfluxc, excfluxn, excfluxp
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   fluxc = sflux * dc

   ! Set net and excretion fluxes for carbon..............................

   netfluxc = fluxc * ( 1._fp8 - p_excX )
   excfluxc = fluxc - netfluxc

   !... for nitrogen.
   fluxn = sflux * dn
   excfluxn = fluxn * p_excX * p_dilX

   !... phosphate.
   fluxp = sflux * dp
   excfluxp = fluxp * p_excX * p_dilX

   !... and silicate all of which is excreted.
   fluxs = sflux * ds

   ! Set source terms.....................................................

   depth = pdepth(I)

   SYc = SYc + netfluxc
   SYn = SYn + fluxn - excfluxn
   SYp = SYp + fluxp - excfluxp
   SFoodc = SFoodc - fluxc / depth
   SFoodn = SFoodn - fluxn / depth
   SFoodp = SFoodp - fluxp / depth
   SFoods = SFoods - fluxs / depth
   SQ6c(K) = SQ6c(K) + excfluxc
   SQ6n(K) = SQ6n(K) + excfluxn
   SQ6p(K) = SQ6p(K) + excfluxp
   SQ6s(K) = SQ6s(K) + fluxs

   return

   end subroutine Y_uptake_pel
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthic zoa uptake, variable \label{sec:YUptakeVar}
!
! !DESCRIPTION:
! This subroutine handles the uptake of a variable nutrient ratio benthic
! food source by a fixed ratio benthic state with the resultant
! excretion to detritus and nutrients.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Y_uptake_var (K, sflux, fluxc, netfluxc, p_excX, &
      p_dilX, dc, dn, dp, ds, SYc, SYn, SYp, &
      SFoodc, SFoodn, SFoodp, SFoods)
!
! !USES:
   use benthic_variables
!
! !INPUT PARAMETERS:
! Compartment identifier
   integer, intent(in) :: k

! Specific gross uptake flux from prey to predator
   real(fp8), intent(in) :: sflux

! Excreted fraction of uptake
   real(fp8), intent(in) :: p_excX

! Dilution factor for excretion
   real(fp8), intent(in) :: p_dilX

! Prey biomass densities
   real(fp8), intent(in) :: dc, dn, dp, ds
!
! !INPUT/OUTPUT PARAMETERS:
! Source terms updated for Predator (cnp)
   real(fp8) :: SYc, SYn, SYp

! Food source (cnps)
   real(fp8) :: SFoodc, SFoodn, SFoodp, SFoods
!
! !OUTPUT PARAMETERS:
! Net uptake flux (c)
   real(fp8), intent(out) :: netfluxc

! Gross uptake flux (c) from prey to predator
   real(fp8), intent(out) :: fluxc
!
! !LOCAL VARIABLES:
! Gross flux (cnp)
   real(fp8) :: fluxn, fluxp, fluxs

! Excretion flux (cnp)
   real(fp8) :: excfluxc, excfluxn, excfluxp
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   fluxc = sflux * dc

   ! Set net and excretion fluxes for carbon..............................

   netfluxc = fluxc * ( 1._fp8 - p_excX )
   excfluxc = fluxc - netfluxc

   !... for nitrogen.

   fluxn = sflux * dn
   excfluxn = fluxn * p_excX * p_dilX

   !... phosphate.

   fluxp = sflux * dp
   excfluxp = fluxp * p_excX * p_dilX

   !... and silicate all of which is excreted.

   fluxs = sflux * ds

   ! Set source terms.....................................................

   SYc = SYc + netfluxc
   SYn = SYn + fluxn - excfluxn
   SYp = SYp + fluxp - excfluxp
   SFoodc = SFoodc - fluxc
   SFoodn = SFoodn - fluxn
   SFoodp = SFoodp - fluxp
   SFoods = SFoods - fluxs
   SQ6c(K) = SQ6c(K) + excfluxc
   SQ6n(K) = SQ6n(K) + excfluxn
   SQ6p(K) = SQ6p(K) + excfluxp
   SQ6s(K) = SQ6s(K) + fluxs

   return

   end subroutine Y_uptake_var
!
!EOC
!-----------------------------------------------------------------------

   end module benthic_zoa

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
