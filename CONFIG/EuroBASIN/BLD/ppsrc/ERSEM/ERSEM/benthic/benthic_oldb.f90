

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Oldenberg benthic model
!
! !DESCRIPTION:
! Module that contains the sub-routine for the computation core of the
! benthic Oldenburg sub-model.\\[\baselineskip]
!
! !INTERFACE:
   module ben_oldb
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public EndProfile, BenthicDistribution, Prof_Parameter,&
      EquProfile, NonEquFlux
!
! !PUBLIC DATA MEMBERS:
   real(fp8), public :: diff1,diff2,diff3,cmix,d1,d2,d3
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: End profile
!
! !DESCRIPTION:
! Computes diffusion-production equilibrium profiles from layer
! productions, layer depths and benthic/pelagic interface concentration.
! Clips negativity.\\[\baselineskip]
!
! !INTERFACE:
   subroutine EndProfile(a_water,mass,p,f0)
!
! !USES:

   use ersem_variables, only: IOProcess, pidersem, eDbgU, eLogU




   use ncdfRestartErsem, only: ncdfDumpErsem

!
! !INPUT PARAMETERS:
! Concentration in water box above the seafloor (per m^3)
   real(fp8), intent(in) :: a_water

! Conent in benthic section (per m^2)
   real(fp8), intent(in) :: mass
!
! !INPUT/OUTPUT PARAMETERS:
! Working array with:
! 1: first layer production
! 2: second layer production
! 3: third layer production
! 4: concentration at seafloor (benthic/pelagic interface)
! 5: concentration at 1st/2nd layer interface
! 6: concentration at 2nd/3rd layer interface
! 7: concentraion at lower boundary of 3rd layer
! 8: 1st layer volume factor (porosity)
! 9: 2nd layer volume factor (porosity)
! 10: 3rd layer volume factor (porosity)
! 11: 1st layer content (per $m^2$)
! 12: 2nd layer content (per $m^2$)
! 13: 3rd layer content (per $m^2$)
! 14: computed difference in total benthic layer content
! 15: lower boundary of 3rd layer
   real(fp8) :: p(15)

! Flux from benthic to pelagic layer
   real(fp8) :: f0
!
! !LOCAL VARIABLES:
   real(fp8) :: f1,f2

   integer :: ierr

   logical :: case1,case2,case3,case4
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Fluxes from Sources:
   f0 = p(1)+ p(2)+ p(3)
   f1 = p(2)+ p(3)
   f2 = p(3)

   ! Surface concentration a0:
   p(4) = modconc(a_water,f0,cmix)

   ! Case differentiation:
   ! case1: oxigen
   ! case2: nitrate
   ! case4: ammonium,phosphate,silicate,co2
   case1 = ((p(1).LT.0._fp8) .and. (p(2).LE.0._fp8) .and. (p(3).eq.0._fp8))
   case2 = ((p(1).GE.0._fp8) .and. (p(2).LT.0._fp8) .and. (p(3).LE.0._fp8))
   case3 = ((p(1).GE.0._fp8) .and. (p(2).GE.0._fp8) .and. (p(3).LT.0._fp8))
   case4 = ((p(1).GE.0._fp8) .and. (p(2).GE.0._fp8) .and. (p(3).eq.0._fp8))

   ! Profile in form of up to three parabular pieces:
   if (case1) then
      call endarc(p(4),p(5), f0,f1, diff1, 0._fp8,p(15),d3,p(11))
      p(12) = 0._fp8
      p(13) = 0._fp8
   else if (case2) then
      call arc (p(4),p(5), f0,f1, diff1, 0._fp8,d1, p(11))
      call endarc(p(5),p(6), f1,f2, diff2, d1,p(15),d3, p(12))
      p(13) = 0._fp8
   else if (case3) then
      call arc (p(4),p(5), f0,f1, diff1, 0._fp8,d1, p(11))
      call arc (p(5),p(6), f1,f2, diff2, d1,d2, p(12))
      call endarc(p(6),p(7), f2,0._fp8, diff3, d2,p(15),d3, p(13))
   else if (case4) then
      call arc (p(4),p(5), f0,f1, diff1, 0._fp8,d1, p(11))
      call arc (p(5),p(6), f1,f2, diff2, d1,d2, p(12))
      call arc (p(6),p(7), f2,0._fp8, diff3, d2,d3, p(13))
      p(15) = d3
   else
      write(eDbgU,*) "ATTENTION: UNFORESEEN PROFILE in ben_nut_oldb!!!"
      write(eDbgU,*) "  Layer productions: ",p(1),p(2),p(3)
      write(eDbgU,*) "  Interface concentrations: ",p(4),p(5),p(6),p(7)
      write(eDbgU,*) "  Pelagic concentration: ",a_water

      write(eDbgU,*) "  Process: ",pidErsem

      call ncdfDumpErsem

      call flush(eDbgU)
      call flush(eLogU)
      call MPI_abort(ierr)






      call arc (p(4),p(5), f0,f1, diff1, 0._fp8,d1, p(11))
      call arc (p(5),p(6), f1,f2, diff2, d1,d2, p(12))
      call arc (p(6),p(7), f2,0._fp8, diff3, d2,d3, p(13))
      p(15) = d3
   end if

   p(14) = p(11)*p(8) + p(12)*p(9) + p(13)*p(10) - mass

   return

   end subroutine EndProfile
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Equilibrium profile
!
! !DESCRIPTION:
! Computes diffusion-production equilibrium profiles
! from layer productions, layer depths and benthic-pelagic interface
! concentration.\\[\baselineskip]
!
! !INTERFACE:
   subroutine EquProfile(a_water,mass,p,f0,k)
!
! !USES:



!
! !INPUT PARAMETERS:
! Concentration in water box above the seafloor (per m^3)
   real(fp8), intent(in) :: a_water

! Conent in benthic section (per m^2)
   real(fp8), intent(in) :: mass
!
! !INPUT/OUTPUT PARAMETERS:
! Working array with:
! 1: first layer production
! 2: second layer production
! 3: third layer production
! 4: concentration at seafloor (benthic/pelagic interface)
! 5: concentration at 1st/2nd layer interface
! 6: concentration at 2nd/3rd layer interface
! 7: concentraion at lower boundary of 3rd layer
! 8: 1st layer volume factor (porosity)
! 9: 2nd layer volume factor (porosity)
! 10: 3rd layer volume factor (porosity)
! 11: 1st layer content (per $m^2$)
! 12: 2nd layer content (per $m^2$)
! 13: 3rd layer content (per $m^2$)
! 14: computed difference in total benthic layer content
! 15: lower boundary of 3rd layer
   real(fp8) :: p(15)

! Flux from benthic to pelagic layer
   real(fp8) :: f0
!
! !LOCAL VARIABLES:
   real(fp8) :: f1,f2
   integer :: k
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Fluxes from Sources:
   f0 = p(1)+ p(2)+ p(3)
   f1 = p(2)+ p(3)
   f2 = p(3)

   ! Surface concentration a0:
   p(4) = modconc(a_water,f0,cmix)

   ! Profile in form of three parabular pieces:
   call arc (p(4),p(5), f0,f1, diff1, 0._fp8,d1, p(11))
   call arc (p(5),p(6), f1,f2, diff2, d1,d2, p(12))
   call arc (p(6),p(7), f2,0._fp8, diff3, d2,d3, p(13)) ! 0 bottom flux
   p(15) = d3

   ! Difference of equilibrium masses to real mass:
   p(14) = p(11)*p(8) + p(12)*p(9) + p(13)*p(10) - mass
   return
   end subroutine EquProfile
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Update surface concentration
!
! !DESCRIPTION:
! Modification of surface concentration by the diffusion flux.\\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function modconc(conc,flux,cmix)
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: conc,flux,cmix
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! conc mg/m3, flux mg/m2day, cmix day/m
   if (flux.GE.0._fp8) then
      modconc = conc + cmix*flux
   else
      modconc = conc*conc/(conc - cmix*flux)
   end if
   return
   end function modconc
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Non-equilibrium flux
!
! !DESCRIPTION:
! Adds non-equilibrium modification to equilibrium flux. Assumes
! homogenous distribution of flux correction.\\[\baselineskip]
!
! !INTERFACE:
   subroutine NonEquFlux(prof,profD,j_diff)
!
! !INPUT/OUTPUT PARAMETERS:
! Prof:
! (1-3): layer fluxes
! (8-10): layer volume-factors (porosity+adsorption)
! (14): Difference of eq. content to real content
! (15): flux to pelagic layer
   real(fp8) :: prof(15)
! profD:
! (1-3): layer thicknesses
! (11:13): layer equilibrium contents (per $m^2$)
   real(fp8) :: profD(15)
!
! !OUTPUT PARAMETERS:
! Outward flux on benthic surface
   real(fp8),intent(out) :: j_diff
!
!
! !LOCAL VARIABLES:
   real(fp8) :: md,factor
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Projects content difference p(14) on standard parabolic with
   ! 0 surface concentration and 0 bottom flux
   md = prof(8)*profD(11) + prof(9)*profD(12) +prof(10)*profD(13)
   factor = prof(14)/md
   prof(1) = prof(1) - profD(1)*factor
   prof(2) = prof(2) - profD(2)*factor
   prof(3) = prof(3) - profD(3)*factor
   j_diff = prof(1) + prof(2) + prof(3)
   return
   end subroutine NonEquFlux
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Compute layer lower face concentration and content
!
! !DESCRIPTION:
! Computes for a layer the lower face concentration and the layer content
! from upper face concentration and lower and upper face fluxes, assuming
! equilibrium between inner production and diffusion. Solving:
!
! \begin{equation}
! a''=\frac{(f_1-f_0)}{diff(d_1-d_0)}
! \end{equation}
!
! with $diff a'= f_0$ at face 0, and $diff a' = f_1$ at face 1, then yields:
!
! \begin{equation}
! a(d,t) = \frac{(f_1-f_0)}{diff(d_1-d_0)} \frac{(d-d0)^2}{2} + \frac{f_0}{diff(d-d_0)} + a_0
! \end{equation}\\[\baselineskip]
!
! !INTERFACE:
   subroutine arc(a0,a1,f0,f1,diff,d0,d1,m1)
!
! !INPUT PARAMETERS:
! Upper face concentration (per m^3)
   real(fp8),intent(in) :: a0
! Depth level of the upper face
   real(fp8),intent(in) :: d0
! Depth level lower face
   real(fp8),intent(in) :: d1
! Upper flace flux (outwards)
   real(fp8),intent(in) :: f0
! Lower face flux (inwards)
   real(fp8),intent(in) :: f1
   real(fp8),intent(in) :: diff
!
! !INPUT/OUTPUT PARAMETERS:
! Lower face concentration (per $m^3$)
   real(fp8),intent(out) :: a1
! Total benthic content (per $m^2$)
   real(fp8),intent(out) :: m1
!
! !LOCAL VARIABLES:
   real(fp8) :: g0,g1
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   g0 = f0/diff
   g1 = f1/diff
   a1 = a0 + (g0+g1)*(d1-d0)/2._fp8
   m1 = (a0 + (g0+g1/2._fp8)*(d1-d0)/3._fp8)*(d1-d0)
   return
   end subroutine arc
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: End arc
!
! !DESCRIPTION:
! For profiles with negative concentrations at the bottom interface cut
! the layer at the intersection. Works only for f1<=0.\\[\baselineskip]
!
! !INTERFACE:
   subroutine endarc(a0,a1,f0,f1,diff,d0,d1,dt,m1)
!
! !INPUT PARAMETERS:
!
   real(fp8),intent(in) :: diff
! Upper flace flux (outwards)
   real(fp8),intent(in) :: f0
! Lower face flux (inwards)
   real(fp8),intent(in) :: f1
! Upper face concentration (per $m^3$)
   real(fp8),intent(in) :: a0
! Depth level upper face
   real(fp8),intent(in) :: d0
!
   real(fp8),intent(in) :: dt
!
! !OUTPUT PARAMETERS:
! Lower face concentration (per $m^3$)
   real(fp8),intent(out) :: a1
! Depth level lower face
   real(fp8),intent(out) :: d1
! Total benthic content (per $m^2$)
   real(fp8),intent(out) :: m1
!
! !LOCAL VARIABLES:
   real(fp8) :: df,pf,vlow
   DATA vlow / 1.e-10_fp8 /
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   pf = f0 + f1
   if ( abs(pf).gt.vlow) then
      !df = (f0 + 2._fp8*f1)/pf
      df = 1._fp8 + f1/pf
   else
      pf = vlow * SIGN(1._fp8,pf)
      if ((abs(f0).lt.vlow).AND.(abs(f1).lt.vlow)) then
         df = 1.5_fp8
      else
         if ( abs(f0).lt.vlow ) df = 2._fp8
         if ( abs(f1).lt.vlow ) df = 1._fp8
      end if
   end if
   d1 = d0 - 2._fp8*a0/pf*diff
   if (d1.GE.d0.AND.d1.LE.dt) then ! cut profile
      m1 = a0*(d1-d0)*df/3._fp8
      a1 = 0._fp8
   else ! standard case
      call arc(a0,a1,f0,f1,diff,d0,dt,m1)
      d1=dt
   end if
   return
   end subroutine endarc
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Compute benthic distribution
!
! !DESCRIPTION:
! Computes parabolic distribution from given surface
! concentration, 0 bottom flux and total benthic content.
! Assumes homogenous distribution of flux.\\[\baselineskip]
!
! !INTERFACE:
   subroutine BenthicDistribution(K,c0,Mint,v1,v2,v3,b1,b2,b3)
!
! !USES:
   use benthic_variables, only: D1m,D2m
   use benthic_parameters, only: d_totx
!
! !INPUT PARAMETERS:
! Compartment identifier
   integer,intent(in) :: k
! Benthic surface concentration (per $m^3$)
   real(fp8),intent(in) :: c0
! Total benthic content (per $m^2$)
   real(fp8),intent(in) :: Mint
! Volume correction factors
   real(fp8),intent(in) :: v1,v2,v3
!
! !OUTPUT PARAMETERS:
! Benthic layer contents
   real(fp8),intent(out) :: b1,b2,b3
!
! !LOCAL VARIABLES:
! profD:
! (1-3): layer thicknesses
! (11-13): standard layer contents
   real(fp8) :: profD(15)
!
   real(fp8) :: md,factor,dum
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
! code.................................................................
! distributes content Mint according to standard parabolic guaranteeing
! c0 surface concentration and 0 bottom flux
!
! d1=D1m(K)
! d2=D2m(K)
! d3=d\_totX
! compute standard parabolic:
! call Prof_Parameter(profD, d1,d2-d1,d3-d2, 1.0e0,1.0e0,1.0e0)
! cmix=0.
! call EquProfile(c0/Mint,1.0e0,profD,dum,k)
! compute content distribution:
! md = v1*profD(11) + v2*profD(12) +v3*profD(13)
! factor = Mint/md
! b1 = v1*profD(11)*factor
! b2 = v2*profD(12)*factor
! b3 = v3*profD(13)*factor
   ! compute constant distribution:
   d1=D1m(K)
   d2=D2m(K)-d1
   d3=d_totX-D2m(K)
   ! mean sediment water concentration:
   factor=Mint/(v1*d1+v2*d2+v3*d3)
   ! layer contents:
   b1 = v1*d1*factor
   b2 = v2*d2*factor
   b3 = v3*d3*factor
   ! all in first layer:
   !b1=Mint
   !b2=0.
   !b3=0.
   return
   end subroutine BenthicDistribution
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Load profile parameter
!
! !DESCRIPTION:
! Load layer productions and volume factors into profile.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Prof_Parameter(prof,s1,s2,s3,v1,v2,v3)
!
! !INPUT PARAMETERS:
   real(fp8),intent(in) :: s1,s2,s3,v1,v2,v3
!
! !OUTPUT PARAMETERS:
   real(fp8),intent(out) :: prof(15)
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   prof(1) = s1 !layer fluxes
   prof(2) = s2
   prof(3) = s3
   prof(8) = v1 !layer volume factors
   prof(9) = v2
   prof(10) = v3
   return
   end subroutine Prof_Parameter
!
!EOC
!-----------------------------------------------------------------------
  end module ben_oldb
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
