

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Benthic initialisations
!
! !DESCRIPTION:
! Manage all initialisations within the benthic components of {\em ERSEM}.
! \\[\baselineskip]
!
! !INTERFACE:
   module benthic_initialisation
!
! !USES:
   use benthic_variables

   implicit none

! Default all is private
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public do_benthic_initialisations
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team

! James Clark (PML) Jan 2014 - Created module benthic_initialisations,
! and moved data members in from module set_initial_conditions.
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise benthic variables and parameters
!
! !DESCRIPTION:
! Initialise all parameters and variables specific to the benthic
! components of {\em ERSEM}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine do_benthic_initialisations
!
! !USES:
   use pelagic_variables, only: n_comp, benthicIndex, ibenx, irr_enh
   use ben_nut_oldb, only: set_benold_parameters
   use ben_gen, only: set_nut_morf_parameters, poroX, pm1x, irr_minX
   use benthic_zone, only: set_benthic_zone_parameters
!
! !LOCAL VARIABLES:
   real(fp8) :: x
   integer :: i, k
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Initialise benthic switches
   call set_benthic_switches()

   ! Initialise module parameters
   call set_benold_parameters()
   call set_benthic_zone_parameters()
   call set_nut_morf_parameters()

   ! Initialise benthic state variables
   call set_initial_benthic_conditions()

   !Initialise benthic irrigation enhancement and layer depths:
   where (ibenX.gt.0)
      Irr_enh = irr_minX
   end where

   SK11p = 0._fp8
   SK14n = 0._fp8
   SK6e = 0._fp8

   k = 0

   do i=1,n_comp

   ! Eps0X(I) = -0.033d0*Log(pdepth(I)) + 0.2583d0
   ! if(Eps0x(I) .gt. 0.09d0)then
   ! Eps0x(I) = 0.09d0
   ! endif

   ! Wave resuspension
   !off Eps0X(I)= Eps0X(I)+5.0d0*exp(-pdepth(I)/7.5d0)
   !
   !jth introduce minimum value

   ! Eps0X(I)=max(0.048d0+0.0194d0,Eps0X(I))

      if (ibenX(i) .eq. 2 .and. iswbenAnX .ne. 0) then
         k=benthicIndex(i)
         if (wdepth(k).gt.250._fp8) then
            x = 250._fp8
         else
            x = wdepth(k)
         end if

         PoroX(k) = -3.E-09_fp8*x**3 + 1.E-06_fp8*x**2 + 0.0005_fp8*x + 0.3976_fp8
         PM1X(k) = 781.62_fp8*LOG(poroX(k)) + 792.5_fp8

         Y2c(k) = 8834.4_fp8*x**(-0.1651_fp8)
         Y3c(k) = -1300._fp8*LOG(X) + 8830.8_fp8
         Y4c(k) = 0.1689_fp8*x + 55.342_fp8
         K5s(k) = -0.7588_fp8*LOG(X) + 11.271_fp8
         K3n(k) = 0.0336_fp8*x**0.595_fp8
         K4n(k) = -11.65_fp8*LOG(X) + 84.126_fp8
         K1p(k) = 0.0021_fp8*x**2 - 0.1729_fp8*x + 20.124_fp8
         D9m(k) = 0.0495_fp8*x**0.0965_fp8
         D8m(k) = 8E-06_fp8*x + 0.0155_fp8
         D7m(k) = 1E-05_fp8*x + 0.0155_fp8
         D6m(k) = 1E-05_fp8*x + 0.0232_fp8
         D5m(k) = 0.0484_fp8*x**0.1042_fp8
         H2c(k) = -156.98_fp8*LOG(X) + 1139.8_fp8
         H1c(k) = 0.61_fp8*X + 23.98_fp8
         Q6c(k) = -1069._fp8*LOG(X) + 10900._fp8
         Q6n(k) = -7.6368_fp8*LOG(X) + 78.564_fp8
         Q6p(k) = -0.545_fp8*LOG(X) + 6.0114_fp8
         Q6s(k) = -64.598_fp8*LOG(X) + 391.61_fp8
         Q1c(k) = 828.74_fp8*x**(-0.6246_fp8)
         Q1n(k) = 21.103_fp8*x**(-0.6882_fp8)
         Q1p(k) = 1.60278_fp8*x**(-0.6725_fp8)
         G2o(k) = 0.0294_fp8*x**0.5305_fp8
         D1m(k) = 0.0026_fp8*x**0.3286_fp8
         D2m(k) = 0.011_fp8*x**0.4018_fp8
         D3m(k) = 0.0486_fp8*x**0.103_fp8
         D4m(k) = 0.0484_fp8*x**0.104_fp8

      end if

   end do

   end subroutine do_benthic_initialisations
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Initialise benthic switches
!
! !DESCRIPTION:
! Initialise all switches specific to the benthic components of {\em ERSEM}
! from namelist \texttt{ersem\_benthic\_switches}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_benthic_switches
!
! !USES:
   use ersem_variables, only: IOProcess, eLogU, eDbgU, iswZeroControl
   use allocation_helpers, only:allocerr, allocateErsemDouble, &
      allocateErsemInteger
   use pelagic_variables, only: ibenX ! For the benthic submodel
!
! !LOCAL VARIABLES:
  integer :: ibenXin,ialloc
  integer :: ITRNG2o,ITRNG3c,ITRNK1p,ITRNK3n,ITRNK4n, &
             ITRNbl2c, &
             ITRNK5s, ITRNG4n, &
             ITRNQ1c,ITRNQ1n, &
             ITRNq1p,itrNq6c,itrNq6n,itrNq6p,&
             itrNq6s,itrNq7c,itrNq7n, &
             itrNq7p,itrNq17c,itrNq17n,itrNq17p,&
             itrNh1c,itrNh2c,itrNy2c, &
             itrNy3c,itrNy4c,itrNd1m,itrNd2m,&
             itrNd3m,itrNd4m,itrNd5m, &
             itrNd6m,itrNd7m,itrNd8m,itrNd9m

  integer :: itrNQ6f

  namelist/ersem_benthic_switches/ibenXin,ISWbenAnX, &
         ISWH1X,ISWH2X,ISWY2X,ISWY3X,ISWY4X,&
         ITRNG2o,ITRNG3c,ITRNK1p,ITRNK3n,&
         ITRNK4n,ITRNK5s,ITRNG4n,&
         ITRNQ1c,ITRNQ1n,&
         ITRNq1p,itrNq6c,itrNq6n,itrNq6p,itrNq6s,&
         itrNq7c,itrNq7n,itrNq7p,&
         itrNq17c,itrNq17n,itrNq17p,itrNh1c,&
         itrNh2c,itrNy2c,itrNy3c,&
         itrNy4c,itrNd1m,itrNd2m,itrNd3m,&
         itrNd4m,itrNd5m,itrNd6m,itrNd7m,&
         itrNd8m,itrNd9m, &
         itrNQ6f, &
         ITRNbl2c
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading ersem switches'

   open(99,file="include/ersem_benthic_switches.nml",status='old')
   read(99,nml=ersem_benthic_switches)

   where (ibenX.gt.0)
       ibenX=ibenXin
   end where

   ITRXG2o=ITRNG2o
   ITRXG3c=ITRNG3c
   ITRXG4n=ITRNG4n
   ITRXK1p=ITRNK1p
   ITRXK3n=ITRNK3n
   ITRXK4n=ITRNK4n
   ITRXK5s=ITRNK5s
   ITRXQ1c=ITRNQ1c
   ITRXQ1n=ITRNQ1n
   ITRXQ1p=ITRNq1p
   ITRXQ6c=itrNq6c
   ITRXQ6n=itrNq6n
   ITRXQ6p=itrNq6p
   ITRXQ6s=itrNq6s
   ITRXQ7c=itrNq7c
   ITRXQ7n=itrNq7n
   ITRXQ7p=itrNq7p
   ITRXQ17c=itrNq17c
   ITRXQ17n=itrNq17n
   ITRXQ17p=itrNq17p
   ITRXH1c=itrNh1c
   ITRXH2c=itrNh2c
   ITRXY2c=itrNy2c
   ITRXY3c=itrNy3c
   ITRXY4c=itrNy4c
   ITRXD1m=itrNd1m
   ITRXD2m=itrNd2m
   ITRXD3m=itrNd3m
   ITRXD4m=itrNd4m
   ITRXD5m=itrNd5m
   ITRXD6m=itrNd6m
   ITRXD7m=itrNd7m
   ITRXD8m=itrNd8m
   ITRXD9m=itrNd9m

   ITRXbl2c=ITRNbl2c !YYY


   ITRXQ6f=itrNQ6f

   close(99)

   ! Extra allocations according to switches
   lbben = min(1,N_COMPBEN)
   if ( ISWZeroControl.NE.0 ) then
      allocate (ccbP(lbben:N_Compben,i_stateben),stat=ialloc)
      call allocerr ('ccbP',N_Compben-lbben+1,i_stateben,ialloc)
      if ( ialloc.ne.0 ) stop
      ccbP=0._fp8
   else
      ccbP => ccb
   end if

   H1cP => ccbP(:, iH1c )
   H2cP => ccbP(:, iH2c )
   G2oP => ccbP(:, iG2o )
   G3cP => ccbP(:, iG3c )
   G4nP => ccbP(:, iG4n )
   K5sP => ccbP(:, iK5s )
   K4nP => ccbP(:, iK4n )
   K1pP => ccbP(:, iK1p )
   K3nP => ccbP(:, iK3n )
   D1mP => ccbP(:, iD1m )
   D2mP => ccbP(:, iD2m )
   D3mP => ccbP(:, iD3m )
   D4mP => ccbP(:, iD4m )
   D5mP => ccbP(:, iD5m )
   D6mP => ccbP(:, iD6m )
   D7mP => ccbP(:, iD7m )
   D8mP => ccbP(:, iD8m )
   D9mP => ccbP(:, iD9m )
   Y2cP => ccbP(:, iY2c )
   Y3cP => ccbP(:, iY3c )
   Y4cP => ccbP(:, iY4c )
   Q1cP => ccbP(:, iQ1c )
   Q1nP => ccbP(:, iQ1n )
   Q1pP => ccbP(:, iQ1p )
   Q6cP => ccbP(:, iQ6c )
   Q6nP => ccbP(:, iQ6n )
   Q6pP => ccbP(:, iQ6p )
   Q6sP => ccbP(:, iQ6s )
   Q7cP => ccbP(:, iQ7c )
   Q7nP => ccbP(:, iQ7n )
   Q7pP => ccbP(:, iQ7p )
   Q17cP => ccbP(:, iQ17c )
   Q17nP => ccbP(:, iQ17n )
   Q17pP => ccbP(:, iQ17p )

   Q6fP => ccbP(:, iQ6f )


   bl2cP=> ccbP(:, ibL2c)


   end subroutine set_benthic_switches
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Assign initial conditions
!
! !DESCRIPTION:
! Initialise benthic state variables from namelist
! \texttt{initial\_benthic\_sv\_conditions}.\\[\baselineskip]
!
! !INTERFACE:
   subroutine set_initial_benthic_conditions
!
! !USES:
   use ersem_constants, only: CMass
   use ersem_variables, only: IOProcess, eLogU, eDbgU, iswZeroControl
   use pelagic_variables, only: B1c0, R1c0, R6c0, N1p0, &
      N3n0, N4n0, N5s0, O2o0, O3c0
   use benthic_parameters, only: d_totx
   use primary_producers, only: qnRPIcX, qpRPIcX

   use primary_producers, only: qfRP1cX

!
! !LOCAL VARIABLES:
   real(fp8) :: H1cR,H2cR,Q1cR,Q1nR,Q1pR
   real(fp8) :: Q6cR,Q6nR,Q6pR,Q6sR,Q7cR,Q7nR,Q7pR
   real(fp8) :: Q17cR,Q17nR,Q17pR,G2oR,G3cR,G4nR
   real(fp8) :: K5sR,K4nR
   real(fp8) :: K1pR,K3nR
   real(fp8) :: D1mR,D2mR,D3mR,D4mR,D5mR,D6mR,D7mR,D8mR,D9mR
   real(fp8) :: Y2cR,Y3cR,Y4cR
   real(fp8), parameter :: qsRPIcX=15._fp8/106._fp8/CMass
   real(fp8), parameter :: lowMass=1.E-2_fp8
   real(fp8) :: bl2cR

   real(fp8) :: Q6fR
   namelist/initial_benthic_sv_conditions/H1cR,H2cR,Q1cR,Q1nR,&
      Q1pR,Q6cR,Q6nR,Q6pR,Q6sR,Q7cR,Q7nR,&
      Q7pR,Q17cR,Q17nR,Q17pR,G2oR,&
      G3cR,G4nR,K5sR,K4nR,K1pR,&
      K3nR,D1mR,D2mR,D3mR,D4mR,D5mR,D6mR,D7mR,&
      D8mR,D9mR,Y2cR,Y3cR,Y4cR, &
      Q6fR, &
      bl2cR

   integer :: k
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   if (IOProcess) write(eLogU,*) '       reading ersem benthic initial conditions'
   open(99,file="include/initial_benthic_sv_conditions.nml",status='old')
   read(99,nml=initial_benthic_sv_conditions)
   close(99)

   do k = 1,n_compben

       H1c(k)= H1cR
       H2c(k)= H2cR

       Q1c(k)= Q1cR
       Q1n(k)= Q1nR
       Q1p(k)= Q1pR
       Q6c(k)= Q6cR
       Q6n(k)= Q6nR
       Q6p(k)= Q6pR
       Q6s(k)= Q6sR
       Q7c(k)= Q7cR
       Q7n(k)= Q7nR
       Q7p(k)= Q7pR
       Q17c(k)= Q17cR
       Q17n(k)= Q17nR
       Q17p(k)= Q17pR

       G2o(k)= G2oR
       G3c(k)= G3cR
       G4n(k)= G4nR

       K5s(k) = K5sR
       K4n(k) = K4nR
       K1p(k) = K1pR
       K3n(k) = K3nR

       D1m(k) = D1mR
       D2m(k) = D2mR
       D3m(k) = D3mR
       D4m(k) = D4mR
       D5m(k) = D5mR
       D6m(k) = D6mR
       D7m(k) = D7mR
       D8m(k) = D8mR
       D9m(k) = D9mR

       Y2c(k) = Y2cR
       Y3c(k) = Y3cR
       Y4c(k) = Y4cR

       bl2c(k)= bl2cR


       Q6f(k) = Q6fR

   end do

   if (ISWZeroControl.NE.0) then

      D1m0=.0001_fp8
      D2m0=D1m0
      D3m0=D2m0 ! unused
      D4m0=D2m0 ! unused
      D5m0=D2m0 ! unused
      D6m0=D2m0 ! unused
      D7m0=D2m0 ! unused
      D8m0=D2m0 ! unused
      D9m0=D2m0 ! unused

      H1c0=B1c0*D1m0
      H2c0=B1c0*D2m0

      Q1c0=R1c0*d_totX
      Q1n0=Q1c0*qnRPIcX
      Q1p0=Q1c0*qpRPIcX
      Q6c0=R6c0*d_totX
      Q6n0=Q6c0*qnRPIcX
      Q6p0=Q6c0*qpRPIcX
      Q6s0=Q6c0*qsRPIcX
      Q7c0=R6c0*d_totX
      Q7n0=Q7c0*qnRPIcX
      Q7p0=Q7c0*qpRPIcX
      Q17c0=1.d-10 ! unused
      Q17n0=Q17c0*qnRPIcX ! unused
      Q17p0=Q17c0*qpRPIcX ! unused

      K5s0=N5s0*d_totX
      K4n0=N4n0*d_totX
      K1p0=N1p0*d_totX
      K3n0=N3n0*D2m0

      G2o0=O2o0*D1m0
      G3c0=O3c0*d_totX
      G4n0=K4n0

      Y2c0=.33_fp8*lowMass*d_totX
      Y3c0=.33_fp8*lowMass*d_totX
      Y4c0=.33_fp8*lowMass*d_totX

      Q6f0=Q6c0*qfRP1cX


      bl2c0=1.e-5_fp8

   end if

   end subroutine set_initial_benthic_conditions
!
!EOC
!-----------------------------------------------------------------------

   end module benthic_initialisation

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
