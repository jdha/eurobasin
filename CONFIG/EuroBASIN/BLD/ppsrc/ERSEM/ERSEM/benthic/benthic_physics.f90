

!-----------------------------------------------------------------------
! Copyright 2014 Plymouth Marine Laboratory
!
! This file is part of the SSB-ERSEM library.
!
! SSB-ERSEM is free software: you can redistribute it and/or modify it
! under the terms of the Lesser GNU General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! SSB-ERSEM is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Lesser
! GNU General Public License for more details.
!
! You should have received a copy of the Lesser GNU General Public License
! along with SSB-ERSEM. If not, see <http://www.gnu.org/licenses/>.
!
! Address:
! Plymouth Marine Laboratory
! Prospect Place, The Hoe
! Plymouth, PL1 3DH, UK
!
! Email:
! ssm@pml.ac.uk
!-----------------------------------------------------------------------
! Preprocessor definitions
!-----------------------------------------------------------------------
!BOP
!
! !MODULE: Benthic physics
!
! !DESCRIPTION:
! Module with routines handling the physical tranport processes through
! the sediments.\\[\baselineskip]
!
! !INTERFACE:
   module ben_phys
!
! !USES:
   use ersem_constants, only: fp8

   implicit none

! Default all is private.
   private
!
! !PUBLIC MEMBER FUNCTIONS:
   public settling,Resuspension,benthic_morfology,Bioturbation, &
      Burying,Refractory_Distribution,Reset_f_QX, &
      Detritus_Distribution,Calculate_Adsorption
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
! !TO DO:
! Document this module.
!
!EOP
!-----------------------------------------------------------------------

   contains

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Reset f_QX
!
! !DESCRIPTION:
! Initialisation of benthicx fluxes.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine Reset_f_QX(k)
!
! !USES:
   use benthic_variables
   use ben_gen, only: f_Q7,f_Q6,f_Q1
!
! !INPUT PARAMETERS:
   integer, intent(in) :: k
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Initialisation of f_Q7,f_Q6,f_Q1.......................................
   f_Q7(1) = wSIQ7c(K)
   f_Q7(2) = wSIQ7n(K)
   f_Q7(3) = wSIQ7p(K)
   f_Q7(4) = 0._fp8

! call Init_cnps_flux (f_Q7,wSIQ7c(I),wSIQ7n(I),wSIQ7p(I),0.0)

   f_Q6(1) = wSIQ6c(K)
   f_Q6(2) = wSIQ6n(K)
   f_Q6(3) = wSIQ6p(K)
   f_Q6(4) = wSIQ6s(K)

! call Init_cnps_flux (f_Q6,wSIQ6c(I),wSIQ6n(I),wSIQ6p(I),wSIQ6s(I))

   f_Q1(1) = wSIQ1c(K)
   f_Q1(2) = wSIQ1n(K)
   f_Q1(3) = wSIQ1p(K)
   f_Q1(4) = 0._fp8

! call Init_cnps_flux (f_Q1,wSIQ1c(I),wSIQ1n(I),wSIQ1p(I),0.0)

   return

   end subroutine Reset_f_QX
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Settling
!
! !DESCRIPTION:
! This subroutine handles the settling of P1,P2,P3,P4,R4,R8 from
! the lowest water compartments onto the sdiment and the transformation
! into the benthic state variables Q1, Q6.
! Sinking from an upper to a lower water compartment is treated
! in "sedimentaion" in the pelagic part. Settling influx is stored in
! f\_QX variables.\\[\baselineskip]
!
! !INTERFACE:
   subroutine settling(I,K)
!
! !USES:
   use pelagic_variables
   use benthic_variables
   use benthic_parameters
   use ben_gen, only: f_Q1,f_Q6,f_Q7
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i,k
!
! !LOCAL VARIABLES:
   real(fp8) :: dum
   real(fp8) :: pe_R7P3X,pe_R7P4X,drate
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   dum = 0._fp8

   pe_R7P3X = 0.05_fp8
   pe_R7P4X = 0.05_fp8

   ! P1 sinking into Q6 and Q1............................................

   call Deposit(I,K,sdP1c(I), pe_R1P1X, pe_R7P1X, &
                P1cP(I), P1nP(I), P1pP(I), P1sP(I), &
                    wSOP1c(I), wSOP1n(I), wSOP1p(I), wSOP1s(I), drate)
   call DecomposeCompound( I, drate, Chl1(I), wSOChl1(I) )

   call DepositIron(I,K,drate, P1fP(I), wSOP1f(I))


   ! P2 sinking into Q6 and Q1............................................

   call Deposit(I,K,sdP2c(I), pe_R1P2X, pe_R7P2X, &
                        P2cP(I), P2nP(I), P2pP(I), 0._fp8, &
                     wSOP2c(I), wSOP2n(I), wSOP2p(I), dum, drate)
   call DecomposeCompound( I, drate, Chl2(I), wSOChl2(I) )

   call DepositIron(I,K,drate, P2fP(I), wSOP2f(I))


   ! P3 sinking into Q6 and Q1............................................

   call Deposit(I,K,sdP3c(I), pe_R1P3X, pe_R7P3X, &
                P3cP(I), P3nP(I), P3pP(I), 0._fp8, &
                wSOP3c(I), wSOP3n(I), wSOP3p(I), dum, drate)
   call DecomposeCompound( I, drate, Chl3(I), wSOChl3(I) )

   call DepositIron(I,K,drate, P3fP(I), wSOP3f(I))


   ! P4 sinking into Q6 and Q1............................................

   call Deposit(I,K,sdP4c(I), pe_R1P4X, pe_R7P4X, &
                P4cP(I), P4nP(I), P4pP(I), 0._fp8, &
                wSOP4c(I), wSOP4n(I), wSOP4p(I), dum, drate)
   call DecomposeCompound( I, drate, Chl4(I), wSOChl4(I) )

   call DepositIron(I,K,drate, P4fP(I), wSOP4f(I))


   ! R6 sinking into Q6...................................................

   call Deposit(I,K, sdR6c(I), 0._fp8, pe_R7R6X, &
                R6cP(I), R6nP(I), R6pP(I), R6sP(I), &
                wSOR6c(I), wSOR6n(I), wSOR6p(I), wSOR6s(I), drate)

   call DepositIron(I,K,drate, R6fP(I), wSOR6f(I))


   ! R4 sinking into Q6...................................................

   call Deposit(I,K, sdR4c(I), 0._fp8, pe_R7R6X, &
                R4cP(I), R4nP(I), R4pP(I), 0._fp8, &
                wSOR4c(I), wSOR4n(I), wSOR4p(I), dum, drate)

   call DepositIron(I,K,drate, R4fP(I), wSOR4f(I))


   ! R8 sinking into Q6...................................................

   call Deposit(I,K, sdR8c(I), 0._fp8, pe_R7R6X, &
                R8cP(I), R8nP(I), R8pP(I), R8sP(I), &
                wSOR8c(I), wSOR8n(I), wSOR8p(I), wSOR8s(I), drate)

!..L2 sinking into bl2c...................................................

   call Depositbl2c(I,K,sdL2c(I), L2CP(I), wSOL2c(I))


   ! Calculation of f_Q7,f_Q6 and f_Q1.......................................

   f_Q7(1) = wSIQ7c(K) - f_Q7(1)
   f_Q7(2) = wSIQ7n(K) - f_Q7(2)
   f_Q7(3) = wSIQ7p(K) - f_Q7(3)
   f_Q7(4) = 0._fp8 - f_Q1(4)

! call Calc_cnps_flux (f_Q7,wSIQ7c(I),wSIQ7n(I),wSIQ7p(I),0.0)

   f_Q6(1) = wSIQ6c(K) - f_Q6(1)
   f_Q6(2) = wSIQ6n(K) - f_Q6(2)
   f_Q6(3) = wSIQ6p(K) - f_Q6(3)
   f_Q6(4) = wSIQ6s(K) - f_Q6(4)

! call Calc_cnps_flux (f_Q6,wSIQ6c(I),wSIQ6n(I),wSIQ6p(I),wSIQ6s(I))

   f_Q1(1) = wSIQ1c(K) - f_Q1(1)
   f_Q1(2) = wSIQ1n(K) - f_Q1(2)
   f_Q1(3) = wSIQ1p(K) - f_Q1(3)
   f_Q1(4) = 0._fp8 - f_Q1(4)

! call Calc_cnps_flux (f_Q1,wSIQ1c(I),wSIQ1n(I),wSIQ1p(I),0.0)

   end subroutine settling
!
!EOC
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Resuspension
!
! !DESCRIPTION:
! Resupension to medium size particulate matter.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Resuspension(I,K)
!
! !USES:
   use ersem_constants, only: seconds_per_day
   use ersem_variables, only: timestep
   use benthic_variables
   use pelagic_variables, only: wsiR6c, wsiR6n, wsiR6p, wsiR6s, pdepth

   use pelagic_variables, only: wsiR6f

   use ben_gen, only: f_Q6



!
! !INPUT PARAMETERS:
   integer,intent(in) :: i,k
!
! !LOCAL VARIABLES:
   real(fp8) ter, er, fac, ferC,FerN, FerP, FerS, upb
   real(fp8) bedsedXc,bedsedXn,bedsedXp,bedsedXs

   real(fp8) FerF

   real(fp8) :: dt
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!BOC
!
   ! Critical stress for erosion (m/s)
   ter=0.02_fp8**2
!
   ! Erosion constant (g/m^2/s)
   er=100._fp8*Ter * 1000._fp8*seconds_per_day ! convert to mg/day

   if (bedstress(k) .gt. ter) then

      ! Inorganic sedment (could replace by transport model)
      ! for now assume 90% is a fixed inorganic component
      bedsedXc=10._fp8*(-1069._fp8*LOG(wdepth(k)) + 10900._fp8)
      bedsedXn=10._fp8*(-7.6368_fp8*LOG(wdepth(k)) + 78.564_fp8)
      bedsedXp=10._fp8*(-0.545_fp8*LOG(wdepth(k)) + 6.0114_fp8)
      bedsedXs=10._fp8*(-64.598_fp8*LOG(wdepth(k)) + 391.61_fp8)
! fac = er*(bedstress(k)/ter - 1.0)
      fac = er*(bedstress(k)/ter - 1._fp8)/(Q6cP(k)+bedsedXc)

      ! C
! FerC=fac*Q6cP(k)/(Q6cP(k)+bedsedXc)
      FerC=fac*Q6cP(k)
      FerC=max(min(FerC,Q6cP(k)/timestep+ &
         min(SQ6c(k)-wsoQ6c(k),0._fp8)),0._fp8)

      ! N
! FerN=fac*Q6nP(k)/(Q6nP(k)+bedsedXn)
      FerN=fac*Q6nP(k)
      FerN=max(min(FerN,Q6nP(k)/timestep+ &
         min(SQ6n(k)-wsoQ6n(k),0._fp8)),0._fp8)

      ! P
! FerP=fac*Q6pP(k)/(Q6pP(k)+bedsedXp)
      FerP=fac*Q6pP(k)
      FerP=max(min(FerP,Q6pP(k)/timestep+ &
         min(SQ6p(k)-wsoQ6p(k),0._fp8)),0._fp8)

      ! S
! FerS=fac*Q6sP(k)/(Q6sP(k)+bedsedXs)
      FerS=fac*Q6sP(k)
      FerS=max(min(FerS,Q6sP(k)/timestep+ &
         min(SQ6s(k)-wsoQ6s(k),0._fp8)),0._fp8)

      ! F
      FerF=fac*Q6fP(k)
      FerF=max(min(FerF,Q6fP(k)/timestep+ &
         min(SQ6f(k)-wsoQ6f(k),0._fp8)),0._fp8)


      wSOQ6c(k) = wSOQ6c(k)+FerC
      wSOQ6n(k) = wSOQ6n(k)+FerN
      wSOQ6p(k) = wSOQ6p(k)+FerP
      wSOQ6s(k) = wSOQ6s(k)+FerS

      wSOQ6f(k) = wSOQ6f(k)+FerF

!
      uPB = 1._fp8/pdepth(I)
      wSIR6c(I) = wSIR6c(I) + FerC*uPB
      wSIR6n(I) = wSIR6n(I) + FerN*uPB
      wSIR6p(I) = wSIR6p(I) + FerP*uPB
      wSIR6s(I) = wSIR6s(I) + FerS*uPB

      wSIR6f(I) = wSIR6f(I) + FerF*uPB
   ! End of resuspension
   end if
   return
   end subroutine Resuspension
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Deposit
!
! !DESCRIPTION:
! This routine handles the division of the sedimentated material
! to benthic DOM and benthic POM and updates the sedimentation source terms.
! The fraction qQ1 goes to benthic DOM.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Deposit( I,K, fsd, qQ1c, qQ7c, Pc, Pn, Pp, Ps, &
                       wSOPc, wSOPn, wSOPp, wSOPs, sdrate )
!
! !USES:
   use ersem_variables, only: timestep
   use benthic_variables
   use pelagic_variables, only: pdepth
   use pelagic_parameters
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i,k
   real(fp8),intent(in) :: fsd, qQ1c, qQ7c, Pc, Pn, Pp, Ps
! Bedstress modified settling
   real(fp8),intent(out) :: sdrate
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8) :: wSOPc, wSOPn, wSOPp, wSOPs
!
! !LOCAL VARIABLES:
   real(fp8) :: fsdc, fsdn, fsdp, fsds, uPB
   real(fp8) :: tbed,tdep,fac
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Conversion factor for m2 to m3 units.................................
   uPB = 1._fp8/pdepth(I)
   ! Calc sdimentation fluxes............................................
   ! Maximum stress for deposition
   tbed=bedstress(k)
   ! Bed characteristics - from Puls and Sundermann 1990
   ! Critical stress for deposition (m/s)
   tdep=0.01_fp8**2
   if (tbed .lt. tdep) then
      fac=1._fp8-tbed/tdep
   else
      fac=0._fp8
   end if
   sdrate = min(fsd*fac,pdepth(I)/timestep)
   fsdc = sdrate*Pc
   fsdn = sdrate*Pn
   fsdp = sdrate*Pp
   fsds = sdrate*Ps
   ! Set 'source' terms...................................................
   wSIQ6c(K) = wSIQ6c(K) + fsdc*(1._fp8 - qQ1c- qQ7c)
   wSIQ6n(K) = wSIQ6n(K) + fsdn*(1._fp8 &
                    - MIN(1._fp8, qQ1c* xR1nX) &
                    - MIN(1._fp8, qQ7c* xR7nX))
   wSIQ6p(K) = wSIQ6p(K) + fsdp*(1._fp8 &
                    - MIN(1._fp8, qQ1c * xR1pX)&
                    - MIN(1._fp8, qQ7c * xR7pX))
   wSIQ6s(K) = wSIQ6s(K) + fsds
   wSIQ1c(K) = wSIQ1c(K) + fsdc * qQ1c
   wSIQ1n(K) = wSIQ1n(K) + fsdn * MIN(1._fp8, qQ1c * xR1nX)
   wSIQ1p(K) = wSIQ1p(K) + fsdp * MIN(1._fp8, qQ1c * xR1pX)
   wSIQ7c(K) = wSIQ7c(K) + fsdc * qQ7c
   wSIQ7n(K) = wSIQ7n(K) + fsdn * MIN(1._fp8, qQ7c * xR7nX)
   wSIQ7p(K) = wSIQ7p(K) + fsdp * MIN(1._fp8, qQ7c * xR7pX)
   ! Here the flux must be added, too, because wSO-variables
   ! will be subtracted
   wSOPc = wSOPc + fsdc*uPB
   wSOPn = wSOPn + fsdn*uPB
   wSOPp = wSOPp + fsdp*uPB
   wSOPs = wSOPs + fsds*uPB
   return
   end subroutine Deposit
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Deposit liths
!
! !DESCRIPTION:
! This routine handles the deposition of pelagic detached liths
! to benthic POC and updates the wsi/wso source terms.
! (Yuri, 02/12).\\[\baselineskip]
!
! !INTERFACE:
   subroutine Depositbl2c( I,K, fsd, Lx, wSOLx )
!
! !USES:
   use ersem_variables, only: timestep
   use benthic_variables, only: wsibl2c, bedstress
   use pelagic_variables, only: pdepth
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i,k
   real(fp8),intent(in) :: fsd, Lx
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8) :: wSOLx
!
! !LOCAL VARIABLES:
   real(fp8) :: fsdL, uPB
   real(fp8) :: tbed,tdep,fac
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Conversion factor for m2 to m3 units.................................
   uPB = 1._fp8/pdepth(I)
   ! Calc sdimentation fluxes............................................
   ! Maximum stress for deposition
   tbed=bedstress(k)
! Bed characteristics - from Puls and Sundermann 1990
! Critical stress for deposition (m/s)
   tdep=0.01_fp8**2
   if (tbed .lt. tdep) then
      fac=1._fp8-tbed/tdep
   else
      fac=0._fp8
   end if
!
   fsdL = min(fsd*fac,pdepth(I)/timestep)*Lx
   ! Set 'source' terms...................................................
   wSIbl2c(K) = wSIbl2c(K) + fsdL
   ! Here the flux must be added, too, because wSO-variables
   ! will be subtracted
   wSOLx = wSOLx + fsdL*uPB
   return
   end subroutine Depositbl2c
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Decompose compound
!
! !DESCRIPTION:
! This routine handles the decomposition of compounds connected to the
! deposition of pelagic organic material and updates the sedimentation source terms.\\[\baselineskip]
!
! !INTERFACE:
   subroutine DecomposeCompound( I, sdrate, Pcomp, wSOPcomp )
!
! !USES:
   use ersem_variables, only: timestep
   use pelagic_variables, only: pdepth
!
! !INPUT PARAMETERS:
   integer,intent(in) :: i
   real(fp8),intent(in) :: sdrate, Pcomp
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8) :: wSOPcomp
!
! !LOCAL VARIABLES:
   real(fp8) :: fsdcomp, uPB
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Conversion factor for m2 to m3 units.................................
   uPB = 1._fp8/pdepth(I)
   ! Calc sdimentation fluxes............................................
   fsdcomp = min(sdrate,pdepth(I)/timestep)*Pcomp
   ! Here the flux must be added, too, because wSO-variables
   ! will be subtracted
   wSOPcomp = wSOPcomp + fsdcomp*uPB
   return
!
   end subroutine DecomposeCompound
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Deposit iron
!
! !DESCRIPTION:
! This routine handles the deposition of pelagic organic iron
! to benthic POM and updates the sedimentation source terms.
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine DepositIron( I,K, sdrate, Pf, wSOPf )
!
! !USES:
   use ersem_variables, only: timestep
   use benthic_variables, only: wsiq6f
   use pelagic_variables, only: pdepth
!
! !INPUT PARAMETERS:
   integer, intent(in) :: i,k
   real(fp8), intent(in) :: sdrate, Pf
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8) :: wSOPf
!
! !LOCAL VARIABLES:
   real(fp8) :: fsdf, uPB
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Conversion factor for m2 to m3 units.................................
   uPB = 1._fp8/pdepth(I)
   ! Calc sdimentation fluxes............................................
   fsdf = min(sdrate,pdepth(I)/timestep)*Pf
   ! Set 'source' terms...................................................
   wSIQ6f(K) = wSIQ6f(K) + fsdf
   ! Here the flux must be added, too, because wSO-variables
   ! will be subtracted
   wSOPf = wSOPf + fsdf*uPB
   return
   end subroutine DepositIron
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Bioturbation
!
! !DESCRIPTION:
! The bioturbation determines the thickness of the layer interspersd
! with detritus and depends on the activity of the deposit feeders and
! the benthic carnivores.
!
! The bioirrigation depends on the activity of the deposit feeders
! and the meiobenthos and increases the diffusion rates.
! The diffusion-enhancement-factor varies between 1. and 1. + mirrX.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Bioturbation(I,K)
!
! !USES:
   use benthic_variables
   use pelagic_variables, only: Irr_enh, eturx
   use ben_gen, only: dturx,hirrx,hturx,irr_minx,mirrx,mturx,pirry2x, &
                      pirry4x,ptury2x,porox,pm1x
   use general, only: eMM
!
! !INPUT PARAMETERS:
   integer ::i,k
!
! !LOCAL VARIABLES:
   real(fp8) :: Ytur,Yirr,Tur_enh
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Bio-turbation........................................................
   Ytur = pturY2X * fBTY2c
   Tur_enh = 1._fp8 + mturX * eMM(Ytur, hturX)
   ! Increase of detritus penetration depth due to bioturbation...........
   SD6m(K) = SD6m(K) + EturX * Tur_enh / D6m(K)&
                                      * (1._fp8 - EXP(-dturX / D6m(K) ))
   SD7m(K) = SD7m(K) + EturX * Tur_enh / D7m(K)&
                                      * (1._fp8 - EXP(-dturX / D7m(K) ))
   SD8m(K) = SD8m(K) + EturX * Tur_enh / D8m(K)&
                                      * (1._fp8 - EXP(-dturX / D8m(K) ))
   SD9m(K) = SD9m(K) + EturX * Tur_enh / D9m(K)&
                                      * (1._fp8 - EXP(-dturX / D9m(K) ))
   SD3m(K) = SD3m(K) + EturX * Tur_enh / D3m(K)&
                                      * (1._fp8 - EXP(-dturX / D3m(K) ))
   SD4m(K) = SD4m(K) + EturX * Tur_enh / D4m(K)&
                                      * (1._fp8 - EXP(-dturX / D4m(K) ))
   SD5m(K) = SD5m(K) + EturX * Tur_enh / D5m(K)&
                                      * (1._fp8 - EXP(-dturX / D5m(K) ))
   ! Bio-irrigation.......................................................
   Yirr = pirrY2X * fBTY2c + pirrY4X * fBTY4c
   Irr_enh(I) = irr_minX + mirrX * eMM(Yirr, hirrX)
   return
   end subroutine Bioturbation
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Burying
!
! !DESCRIPTION:
! By shifting the penetration depth of Q7+Q17 a portion of
! refractory matter gets moved over the interface of Q7/Q17 at d\_totX.\\[\baselineskip]
!
! !INTERFACE:
   subroutine Burying(K)
!
! !USES:
   use benthic_variables
   use benthic_parameters, only: d_totx
   use ben_gen, only: fQ17Q7,AvQ7
!
! !INPUT PARAMETERS:
   integer :: k
!
! !LOCAL VARIABLES:
   real(fp8) :: AQ7c, AQ7n, AQ7p
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   fQ17Q7(1) = SQ7c(K)
   fQ17Q7(2) = SQ7n(K)
   fQ17Q7(3) = SQ7p(K)
   fQ17Q7(4) = 0._fp8
! call Init_cnps_flux ( fQ17Q7,&
! & SQ7c(I), SQ7n(I), SQ7p(I), 0.0 )
   call AvQ7( K, d_totX-SD3m(K), d_totX, AQ7c, AQ7n, AQ7p )
   SQ7c(K)= SQ7c(K) - AQ7c
   SQ7n(K)= SQ7n(K) - AQ7n
   SQ7p(K)= SQ7p(K) - AQ7p
   ! Only for mass balance check:
   SQ17c(K)= SQ17c(K) + AQ7c
   SQ17n(K)= SQ17n(K) + AQ7n
   SQ17p(K)= SQ17p(K) + AQ7p
   fQ17Q7(1) = SQ7c(K) - fQ17Q7(1)
   fQ17Q7(2) = SQ7n(K) - fQ17Q7(2)
   fQ17Q7(3) = SQ7p(K) - fQ17Q7(3)
   fQ17Q7(4) = 0.0e0 - fQ17Q7(4)
! call Calc_cnps_flux ( fQ17Q7, SQ7c(I), SQ7n(I), SQ7p(I), 0e0 )
   return
   end subroutine Burying
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Refactory distribution
!
! !DESCRIPTION:
! Modification of the vertical detritus distribution. \\[\baselineskip]
!
! !INTERFACE:
   subroutine Refractory_Distribution(K, fQ17Q7, fH1Q7, fH2Q7, f_Q7)
!
! !USES:
   use benthic_variables
   use benthic_parameters, only: d_totx
!
! !INPUT PARAMETERS:
   integer, intent(in) :: k
!
! !INPUT/OUTPUT PARAMETERS:
   real(fp8),intent(in) :: fQ17Q7(4),fH1Q7(4),fH2Q7(4),f_Q7(4)
!
! !LOCAL VARIABLES:
   real(fp8) :: dy
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Due to bioturbation an amount is transported beyond the 30 cm depth!
   dy=d_totX-SD3m(K)/2.0
   call Q7Distribution(K, fQ17Q7, dy)
   call Q7Distribution(K, f_Q7, 0._fp8)
   call Q7Distribution(K, fH1Q7, D1m(K)/2._fp8)
   call Q7Distribution(K, fH2Q7, D3m(K)+D1m(K))
   return
   end subroutine Refractory_Distribution
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Detritus distribution
!
! !DESCRIPTION:
! Modification of the vertical detritus distribution
! \\[\baselineskip]
!
! !INTERFACE:
   subroutine Detritus_distribution (K, fY2Q6, fY3Q6, fY4Q6, &
                                     fH1Q6, fH2Q6, f_Q6)
!
! !USES:
   use benthic_variables
   use ben_gen, only: dQ6Y3X,dQ6Y4X
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: fY2Q6(4),fY3Q6(4),fY4Q6(4)
   real(fp8), intent(in) :: fH1Q6(4),fH2Q6(4),f_Q6(4)
   integer, intent(in) :: k
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   call Q6Distribution(K, f_Q6, 0._fp8)
   call Q6Distribution(K, fH1Q6, D1m(K)/2._fp8)
   call Q6Distribution(K, fH2Q6, D6m(K)+D1m(K))
   call Q6Distribution(K, fY2Q6, D6m(K)/2._fp8)
   call Q6Distribution(K, fY3Q6, dQ6Y3X/2._fp8)
   call Q6Distribution(K, fY4Q6, dQ6Y4X/2._fp8)
   return
   end subroutine Detritus_distribution
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Q7 distribution
!
! !DESCRIPTION:
! Detritus added at depths dY changes the vertical distribution.
! Material f\_Q7 added at dY lifts average penetration depth DIm,
! if dY is above DIm and lowers it if dY is below.
! Detritus flux: fQ7 (a record of 4 components).\\[\baselineskip]
!
! !INTERFACE:
   subroutine Q7Distribution(K,f_Q7,dY)
!
! !USES:
   use benthic_variables
   use benthic_parameters, only: d_totx
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: f_Q7(4)
   real(fp8), intent(in) :: dY
   integer, intent(in) :: k
!
! !LOCAL VARIABLES:
   real(fp8) :: dY2
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   ! Sometimes DXm becomes larger than d_totX
   ! If this happens a positive feed back is the consequence which results
   ! in unrealistic values of Dim
   dY2 = dY
   if ((D3m(K) .gt. d_totX) .and. (f_Q7(1) .lt. 0._fp8)) dY2 = d_totX
   SD3m(K) = SD3m(K) + (dY2-D3m(K)) * f_Q7(1) / Q7c(K)
   if ((D4m(K) .gt. d_totX) .and. (f_Q7(2) .lt. 0._fp8)) dY2 = d_totX
   SD4m(K) = SD4m(K) + (dY2-D4m(K)) * f_Q7(2) / Q7n(K)
   if ((D5m(K) .gt. d_totX) .and. (f_Q7(3) .lt. 0._fp8)) dY2 = d_totX
   SD5m(K) = SD5m(K) + (dY2-D5m(K)) * f_Q7(3) / Q7p(K)
   return
   end subroutine Q7Distribution
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Q6 distribution
!
! !DESCRIPTION:
! Detritus added at depths dY changes the vertical distribution.
! Material f\_Q6 added at dY lifts average penetration depth DIm,
! if dY is above DIm and lowers it if dY is below.
! Detritus flux: fQ6 (a record of 4 components).\\[\baselineskip]
!
! !INTERFACE:
   subroutine Q6Distribution(K,f_Q6,dY)
!
! !USES:
   use benthic_variables
!
! !INPUT PARAMETERS:
   real(fp8), intent(in) :: f_Q6(4)
   real(fp8), intent(in) :: dY
   integer,intent(in) :: k
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   SD6m(K) = SD6m(K) + (dY-D6m(K)) * f_Q6(1) / Q6c(K)
   SD7m(K) = SD7m(K) + (dY-D7m(K)) * f_Q6(2) / Q6n(K)
   SD8m(K) = SD8m(K) + (dY-D8m(K)) * f_Q6(3) / Q6p(K)
   SD9m(K) = SD9m(K) + (dY-D9m(K)) * f_Q6(4) / Q6s(K)
   return
   end subroutine Q6Distribution
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Benthos morfology
!
! !DESCRIPTION:
! Sets the porosity.\\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function benthic_morfology(mode,boxnr)
!
! !USES:
   use ben_gen, only: pm1x,poroX
!
! !INPUT PARAMETERS:
   integer, intent(in) :: mode, boxnr
!
! !LOCAL VARIABLES:
   integer, parameter :: iporo = 1
   integer, parameter :: ipM1 = 2
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
   if (mode.eq.iporo) then ! porosity
      benthic_morfology=poroX(boxnr)
   else if (mode.eq.ipM1) then ! adsorption
      benthic_morfology=pM1X(boxnr)
   end if
!
   return
!
   end function benthic_morfology
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Calculate absorption
!
! !DESCRIPTION:
! Calculate average adsorption from depth 'from' to depth 'to' for box i
! This routine is used to calculate the adsorption according the distribution
! of detritus. The distribution of the detritus determines the distribution
! of the bacteria.\\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function calculate_adsorption(mode,K,from,to)
!
! !USES:
   use ersem_constants, only: zeroX
   use benthic_variables
   use pelagic_variables, only: n_comp, n_upperX
!
! !INPUT PARAMETERS:
   integer, intent(in) :: mode,K
   real(fp8), intent(in) :: from,to
!
! !LOCAL VARIABLES:
   real(fp8) :: r,s,a,b,alfa6c,alfa7c,pM_1,pM_2
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   if (mode.eq.1) then
      pM_1=25._fp8 +1._fp8
      if (N_COMP-N_UPPERX.gt.1) pM_1=benthic_morfology(2,K)+1
      pM_2=2._fp8 +1._fp8
   else
      pM_1=3._fp8 +1._fp8
      pM_2=3._fp8 +1._fp8
   end if
!
   alfa6c=1._fp8/D6m(K)
   alfa7c=1._fp8/D3m(K)
   a=0._fp8
   b=0._fp8
   calculate_adsorption=.5_fp8*(pM_1+pM_2)
!
   if (from .lt. D2m(K)) then
      s=min(to,D2m(K))
      r=integral_exp(-alfa6c,s-from)+integral_exp(-alfa7c,s-from)
      a=a+r
      b=b+r*pM_1
   end if
   if (to .gt. D2m(K)) then
      s=max(from,D2m(K))
      r=exp(-alfa6c*(s-from))* integral_exp(-alfa6c,to-s)&
        +exp(-alfa7c*(s-from))* integral_exp(-alfa7c,to-s)
        a=a+r
        b=b+r*pM_2
   end if
   if (a.ge.ZeroX) calculate_adsorption=b/a
   return
   end function calculate_adsorption
!
!EOC
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOP
!
! !IROUTINE: Integral exp
!
! !DESCRIPTION:
! \begin{equation}
! f(x)=\frac{\textrm{exp}(\alpha x)-1}{\alpha}
! \end{equation}
! \\[\baselineskip]
!
! !INTERFACE:
   real(fp8) function integral_exp(alfa,d)
!
! !INPUT PARAMETERS:
   real(fp8),intent(in) :: alfa,d
!
! !REVISION HISTORY:
! Original author(s): The ERSEM development team
!
!EOP
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!BOC
!
   integral_exp= (exp(alfa*d)-1._fp8)/alfa
   return
   end function integral_exp
!
!EOC
!-----------------------------------------------------------------------
   end module ben_phys
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
