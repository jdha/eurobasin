

MODULE trcsbc_PMLersem
   IMPLICIT NONE
   PRIVATE
   !!----------------------------------------------------------------------
   !! Dummy module : No TOP models
   !!----------------------------------------------------------------------
CONTAINS
   SUBROUTINE trc_sbc_PMLersem( kt ) ! Empty routine
      INTEGER, INTENT(in) :: kt
      WRITE(*,*) 'trc_sbc_PMLersem: You should not have seen this print! error?', kt
   END SUBROUTINE trc_sbc_PMLersem
END MODULE trcsbc_PMLersem
