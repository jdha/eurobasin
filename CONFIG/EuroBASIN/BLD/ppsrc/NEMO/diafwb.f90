

MODULE diafwb
   !!======================================================================
   !! *** MODULE diafwb ***
   !! Ocean diagnostics: freshwater budget
   !!======================================================================
   !! History : 8.2 ! 01-02 (E. Durand) Original code
   !! 8.5 ! 02-06 (G. Madec) F90: Free form and module
   !! 9.0 ! 05-11 (V. Garnier) Surface pressure gradient organization
   !!----------------------------------------------------------------------
   !!----------------------------------------------------------------------
   !! Default option : Dummy Module
   !!----------------------------------------------------------------------
   LOGICAL, PUBLIC, PARAMETER :: lk_diafwb = .FALSE. !: fresh water budget flag
CONTAINS
   SUBROUTINE dia_fwb( kt ) ! Empty routine
      WRITE(*,*) 'dia_fwb: : You should not have seen this print! error?', kt
   END SUBROUTINE dia_fwb
   !!======================================================================
END MODULE diafwb
