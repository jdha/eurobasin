

MODULE traadv_vppm ! to be called in all the traadv_???_vppm.F90
   !!======================================================================
   !! *** MODULE traadv_vppm ***
   !! Ocean active tracers: ppm vertical advection
   !!======================================================================
   !! History : ! 2010-01 (H. Liu) Original code
   !! ! 2013-02 (H. Liu) Modified for v3.4
   !!----------------------------------------------------------------------

   !!----------------------------------------------------------------------
   !! tra_adv_vppm : PPM scheme for tracer vertical advection
   !! To be called by any Horizontal-Vertical dimension !splitting
   !! tracer advection subroutine.
   !!----------------------------------------------------------------------
   USE oce ! ocean dynamics and active tracers
   USE dom_oce ! ocean space and time domain
   USE sbc_oce ! surface boundary condition: ocean
   USE dynspg_oce ! choice/control of key cpp for surface pressure gradient
   USE trdmod_oce ! ocean variables trends
   USE eosbn2 ! equation of state
   USE trdmod ! ocean active tracers trends
   USE closea ! closed sea
   USE trabbl ! advective term in the BBL
   USE sbcmod ! surface Boundary Condition
   USE sbcrnf ! river runoffs
   USE in_out_manager ! I/O manager
   USE iom ! IOM library
   USE lib_mpp
   USE lbclnk ! ocean lateral boundary condition (or mpp link)
   USE diaptr ! poleward transport diagnostics
   USE prtctl ! Print control
   USE zdf_oce ! ocean vertical physics
   USE restart ! ocean restart
   USE phycst, ONLY: rau0

   USE domvvl

   USE wrk_nemo
   USE timing

   IMPLICIT NONE
   PRIVATE

   PUBLIC tra_adv_vppm ! routine called by all tra advection subroutines


   !! * Substitutions
   !!----------------------------------------------------------------------
   !! *** domzgr_substitute.h90 ***
   !!----------------------------------------------------------------------
   !! ** purpose : substitute fsdep. and fse.., the vert. depth and scale
   !! factors depending on the vertical coord. used, using CPP macro.
   !!----------------------------------------------------------------------
   !! History : 1.0 ! 2005-10 (A. Beckmann, G. Madec) generalisation to all coord.
   !! 3.1 ! 2009-02 (G. Madec, M. Leclair) pure z* coordinate
   !!----------------------------------------------------------------------
! reference for s- or zps-coordinate (3D no time dependency)
! s* or z*-coordinate (3D + time dependency) + use of additional now arrays (..._1)
   !!----------------------------------------------------------------------
   !! NEMO/OPA 3.3 , NEMO Consortium (2010)
   !! $Id: domzgr_substitute.h90 2528 2010-12-27 17:33:53Z rblod $
   !! Software governed by the CeCILL licence (NEMOGCM/NEMO_CeCILL.txt)
   !!----------------------------------------------------------------------
   !!----------------------------------------------------------------------
   !! *** vectopt_loop_substitute ***
   !!----------------------------------------------------------------------
   !! ** purpose : substitute the inner loop starting and inding indices
   !! to allow unrolling of do-loop using CPP macro.
   !!----------------------------------------------------------------------
   !!----------------------------------------------------------------------
   !! NEMO/OPA 3.3 , NEMO Consortium (2010)
   !! $Id: vectopt_loop_substitute.h90 2528 2010-12-27 17:33:53Z rblod $
   !! Software governed by the CeCILL licence (NEMOGCM/NEMO_CeCILL.txt)
   !!----------------------------------------------------------------------
   !!----------------------------------------------------------------------
   !! NEMO/OPA 3.2 , LOCEAN-IPSL (2009)
   !! $Id: traadv_cen2.F90 1559 2009-07-29 14:03:14Z ctlod $
   !! Software governed by the CeCILL licence (modipsl/doc/NEMO_CeCILL.txt)
   !!----------------------------------------------------------------------
CONTAINS
   SUBROUTINE tra_adv_vppm(pta3, ptb3, htflux, dt2)
      !!----------------------------------------------------------------------
      !! *** ROUTINE tra_adv_cen2 ***
      !!
      !! ** Purpose : Compute the now trend due to the advection of tracers
      !! and add it to the general trend of passive tracer equations.
      !!
      !! ** Method : The advection is evaluated by a PPM scheme
      !!
      !! ** Action : - update pta3 with the now advective tracer trends
      !!
      !! H.Liu, Jan. 2010, POL
      !!----------------------------------------------------------------------
      !!
      IMPLICIT NONE
      REAL(wp), INTENT(in) :: dt2 ! ocean time-step
      REAL(wp), INTENT(in) , DIMENSION(jpi,jpj,jpk) :: htflux ! horizontal temperature flux
      REAL(wp), INTENT(in) , DIMENSION(jpi,jpj,jpk) :: ptb3 ! before tracer fields
      REAL(wp), INTENT(inout), DIMENSION(jpi,jpj,jpk) :: pta3 ! tracer trend
      !!
      REAL(wp), POINTER, DIMENSION(:,:,:) :: zwx, zwy
      REAL(wp), POINTER, DIMENSION(:,:) :: btr_tmp
      INTEGER :: ji, jj, jk ! dummy loop indices
      INTEGER :: jpkw
      REAL(wp) :: tmp, tmp1, tmp2
      !!----------------------------------------------------------------------
      !IF( nn_timing == 1 ) CALL timing_start('tra_adv_vppm')
      !timing for this part should have been included in its caller.
      !
      CALL wrk_alloc( jpi, jpj, jpk, zwx, zwy )
      CALL wrk_alloc( jpi, jpj, btr_tmp )
      FORALL (ji = 1:jpi, jj = 1:jpj) btr_tmp(ji,jj) = 1. / ( e1t(ji,jj) * e2t(ji,jj) )
      DO jk = jpkm1, 1, -1
      DO jj = 2, jpjm1
      DO ji = 2, jpim1 ! vector opt.
         zwx(ji,jj,jk) = - hdivn(ji,jj,jk) * e3t_1(ji,jj,jk) * dt2 + (e3t(ji,jj,jk)*(1.+sshb(ji,jj)*mut(ji,jj,jk)))
      END DO
      END DO
      END DO
      DO jj = 2, jpjm1
      DO ji = 2, jpim1 ! vector opt.
         zwx(ji,jj,1) = zwx(ji,jj,1) - 0.5_wp * (emp_b(ji,jj) + emp(ji,jj)) / rau0 * dt2
      END DO
      END DO
      !-- Check the time step limitation
      IF(MINVAL(zwx(2:jpim1,2:jpjm1,1:jpkm1)) <= 0._wp) THEN
        WRITE(ctmp1,*) 'error, Cell volume exhausted during vppm',&
                     & 'advection, ji,jj,jk --->',&
                     & MINLOC(zwx)
        CALL ctl_stop(ctmp1)
      END IF
      DO jk = jpkm1, 1, -1
      DO jj = 2, jpjm1
      DO ji = 2, jpim1 ! vector opt.
         tmp = htflux(ji,jj,jk) * btr_tmp(ji,jj) + pta3(ji,jj,jk) * e3t_1(ji,jj,jk)
         tmp = tmp * dt2
         zwy(ji,jj,jk) = (ptb3(ji,jj,jk) * (e3t(ji,jj,jk)*(1.+sshb(ji,jj)*mut(ji,jj,jk))) + tmp) / zwx(ji,jj,jk)
      END DO
      END DO
      END DO
      CALL lbc_lnk(zwx, 'T', 1.)
      CALL lbc_lnk(zwy, 'T', 1.)
      CALL PPM_1D(zwx,zwy)
      DO jj = 2, jpjm1
      DO ji = 2, jpim1
        jpkw = mbathy(ji,jj) - 1 ! get the number of layers
        If(jpkw < 2) cycle
        If(jpkw > jpkm1) call ctl_stop("error, mbathy > jpkm1")
        DO jk = 1, jpkw
           pta3(ji,jj,jk) = ( zwy(ji,jj,jk) * (e3t(ji,jj,jk)*(1.+ssha(ji,jj)*mut(ji,jj,jk))) - &
                          & ptb3(ji,jj,jk) * (e3t(ji,jj,jk)*(1.+sshb(ji,jj)*mut(ji,jj,jk)))) / &
                          & dt2 / e3t_1(ji,jj,jk)
        ENDDO
      ENDDO
      ENDDO
      CALL lbc_lnk( pta3, 'T', 1. )
      CALL wrk_dealloc( jpi, jpj, jpk, zwx, zwy )
      CALL wrk_dealloc( jpi, jpj, btr_tmp )
   END SUBROUTINE tra_adv_vppm
   SUBROUTINE PPM_1D(dz,sta)
      !!----------------------------------------------------------------------
      !! *** ROUTINE ppm_1d ***
      !!
      !! ** Purpose : PPM scheme applied in the vertical direction
      !!
      !! ** Method :
      !!
      !! ** Action : -
      !! -
      !! H.Liu, Jan. 2010, POL
      !!----------------------------------------------------------------------
      !!
      IMPLICIT NONE
      REAL(wp), INTENT(in), DIMENSION(jpi,jpj,jpk) :: dz ! delt z
      REAL(wp), INTENT(inout), DIMENSION(jpi,jpj,jpk) :: sta ! scalar to be advected
      !!
      REAL(wp), DIMENSION(-1:jpkm1+2) :: dzn, dznn, stan, stann
      REAL(wp), DIMENSION(-1:jpkm1+2) :: da, ajph
      REAL(wp), DIMENSION(-1:jpkm1+2) :: al, ar, a6
      REAL(wp), DIMENSION(jpk) :: zetan, zetann ! zeta level of old&new grid
      REAL(wp), DIMENSION(jpk) :: d2top, d2bot ! distance to the top and bottom &
                                               ! of the located old grid
      INTEGER, DIMENSION(jpk) :: ntop, nbot ! the old grid count number in &
                                               ! which the top and bottom of &
                                               ! the new gird located
      REAL(wp) :: tmp1, tmp2, tmp3, tmp4
      REAL(wp) :: tmp31, tmp32, tmp33, tmp34, tmp35
      REAL(wp) :: dalr, dbot, dtop
      INTEGER :: kbot, ktop, ktopp, kbott
      INTEGER :: jpkw, jpktmp
      INTEGER :: ji, jj, jk, jk1
      !!----------------------------------------------------------------------

      da = 0.0_wp


      DO jj = 2, jpjm1
      DO ji = 2, jpim1 ! vector opt.

         jpkw = mbathy(ji,jj) - 1 ! get the number of layers
         If(jpkw < 2) cycle
         If(jpkw > jpkm1) call ctl_stop("error, mbathy > jpkm1")

         stan(-1:jpkm1+2) = 0.0_wp

         tmp1 = sum( e3t(ji,jj,1:jpkw) )
         tmp2 = sum( dz(ji,jj,1:jpkw) )
         dzn(1:jpkw) = dz(ji,jj,1:jpkw) !thickness before adjustment
         dznn(1:jpkw) = e3t(ji,jj,1:jpkw) * tmp2 / tmp1 !thickness after adjustment
         stan(1:jpkw) = sta(ji,jj,1:jpkw)

         dzn(-1) = dz(ji,jj,2)
         dzn(0) = dz(ji,jj,1)
         dzn(jpkw+1) = dz(ji,jj,jpkw)
         dzn(jpkw+2) = dz(ji,jj,jpkw-1)

         stan(-1) = sta(ji,jj,2)
         stan(0) = sta(ji,jj,1)
         stan(jpkw+1) = sta(ji,jj,jpkw)
         stan(jpkw+2) = sta(ji,jj,jpkw-1)


! Calculate delt_a, (C&W 1.7, 1.8)
         DO jk = 1, jpkw
            tmp1 = dzn(jk) / sum(dzn(jk-1:jk+1))
            tmp2 = (2.0_wp * dzn(jk-1) + dzn(jk)) / (dzn(jk+1) + dzn(jk))
            tmp3 = (dzn(jk) + 2.0_wp * dzn(jk+1)) / (dzn(jk-1) + dzn(jk))
            da(jk) = tmp1 * ( tmp2 * (stan(jk+1) - stan(jk)) + &
                   & tmp3 * (stan(jk) - stan(jk-1)) )

            tmp1 = sign(1.0_wp, da(jk))
            tmp2 = min( abs(da(jk)), 2._wp * abs( stan(jk)-stan(jk-1) ), &
                   & 2._wp * abs( stan(jk)-stan(jk+1) ) )
            IF((stan(jk+1) - stan(jk)) * (stan(jk) - stan(jk-1)) > 0._wp) THEN
              da(jk) = tmp1 * tmp2
            ELSE
              da(jk) = 0._wp
            END IF
         END DO

! calculate aL, aR (C&W 1.6)
         DO jk = -1, jpkw
            tmp1 = stan(jk)
            tmp2 = dzn(jk) / (dzn(jk) + dzn(jk+1)) * (stan(jk+1) - stan(jk))
            tmp31 = 2._wp * dzn(jk+1) * dzn(jk) / (dzn(jk) + dzn(jk+1))
            tmp32 = (dzn(jk-1) + dzn(jk)) / (2._wp * dzn(jk) + dzn(jk+1))
            tmp33 = (dzn(jk+2) + dzn(jk+1)) / (2._wp * dzn(jk+1) + dzn(jk))

            tmp34 = dzn(jk) * (dzn(jk-1) + dzn(jk)) / &
                     & (2._wp * dzn(jk) + dzn(jk+1)) * da(jk+1)
            tmp35 = dzn(jk+1) * (dzn(jk+1) + dzn(jk+2)) / &
                     & (dzn(jk) + 2._wp * dzn(jk+1)) * da(jk)

            tmp3 = tmp31 * (tmp32 - tmp33) * (stan(jk+1) - stan(jk)) - &
                     & tmp34 + tmp35

            tmp3 = tmp3 / sum(dzn(jk-1:jk+2))

            ajph(jk) = tmp1 + tmp2 + tmp3
            ar(jk) = ajph(jk)
            al(jk+1) = ajph(jk)
         END DO

! Monotonic correction, calculate a6 (C&W 1.10)
         DO jk = 1, jpkw

            a6(jk) = 6._wp * stan(jk) - 3._wp * (al(jk) + ar(jk))

            IF((ar(jk) - stan(jk)) * (stan(jk) - al(jk)) <= 0.) THEN
              al(jk) = stan(jk); ar(jk) = stan(jk)
              a6(jk) = 6._wp * stan(jk) - 3._wp * (al(jk) + ar(jk))
              CYCLE
            END IF

            dalr = ar(jk) - al(jk)

            IF(dalr * a6(jk) > dalr * dalr) THEN
              al(jk) = 3._wp * stan(jk) - 2._wp * ar(jk)
              a6(jk) = 6._wp * stan(jk) - 3._wp * (al(jk) + ar(jk))
              cycle
            END IF

            IF(-dalr * dalr > dalr * a6(jk)) THEN
              ar(jk) = 3._wp * stan(jk) - 2._wp * al(jk)
              a6(jk) = 6._wp * stan(jk) - 3._wp * (al(jk) + ar(jk))
            END IF

         END DO

! redistribution

         ntop(:) = 0
         nbot(:) = 0
         zetan(jpkw+1) = 0.0_wp
         zetann(jpkw+1) = 0.0_wp

         DO jk = jpkw, 2, -1
           zetan(jk) = zetan(jk+1) + dzn(jk)
           zetann(jk) = zetann(jk+1) + dznn(jk)
         END DO

         zetan(1) = sum( dz(ji,jj,1:jpkw) )
         zetann(1) = zetan(1)

         jpktmp = jpkw
         DO jk = jpkw + 1, 1, -1
           DO jk1 = jpktmp, 1, -1
             tmp1 = zetann(jk) - zetan(jk1+1)
             tmp2 = zetann(jk) - zetan(jk1)
             If(tmp1 * tmp2 <= 0.0) then
               ntop(jk) = jk1
               d2bot(jk) = abs(tmp2) ! distance to level jk1
               d2top(jk) = abs(tmp1) ! distance to level jk1+1
               EXIT
             End If
           ENDDO
           If(ntop(jk) == 0) THEN
             WRITE(ctmp1,*) 'error,vppm redistribution failed at(ji,jj,jk)...', ji, jj, jk, tmp1, tmp2
             CALL ctl_stop(ctmp1)
           ENDIF
           jpktmp = ntop(jk)
         ENDDO

         DO jk = jpkw, 1, -1

            ktopp = ntop(jk+1)
            kbott = ntop(jk)


            IF(ktopp == kbott) THEN
              dtop = d2top(jk)
              tmp31 = ppm_integl( al(ktopp), ar(ktopp), a6(ktopp), dtop, dzn(ktopp) )
              tmp31 = tmp31 * dtop

              dtop = d2top(jk+1)
              tmp32 = ppm_integl( al(ktopp), ar(ktopp), a6(ktopp), dtop, dzn(ktopp) )
              tmp32 = tmp32 * dtop

              stann(jk) = (tmp31 - tmp32) / (d2top(jk) - d2top(jk+1))
             ELSE IF(ktopp > kbott) THEN
              dbot = d2top(jk)
              tmp31 = ppm_integl( al(kbott), ar(kbott), a6(kbott), dbot, dzn(kbott) )
              tmp31 = tmp31 * dbot
              tmp35 = dbot
              DO jk1 = kbott + 1, ktopp - 1
                 tmp31 = tmp31 + stan(jk1) * dzn(jk1)
                 tmp35 = tmp35 + dzn(jk1)
              END DO
              dtop = d2bot(jk+1)
              tmp31 = tmp31 + ppm_integr( al(ktopp), ar(ktopp), a6(ktopp), dtop, dzn(ktopp))&
                            * dtop
              tmp35 = tmp35 + dtop
              stann(jk) = tmp31 / tmp35
            ELSE
              CALL ctl_stop('error,attention: ktopp > kbott')
            END IF

            kbot = ktopp

         END DO

        sta(ji,jj,1:jpkw) = stann(1:jpkw)

      END DO
      END DO

      CALL lbc_lnk(sta, 'T', 1.)



   END SUBROUTINE PPM_1D


   FUNCTION ppm_integl(al, ar, a6, dx, dz) RESULT(f)
      !!----------------------------------------------------------------------
      !! *** ROUTINE interp1 ***
      !!
      !! ** Purpose : 1-d ppm integration
      !!
      !! ** Method :
      !!
      !! H.Liu, Jan. 2010, POL
      !!----------------------------------------------------------------------
      IMPLICIT NONE
      REAL(wp), INTENT(in) :: al, ar, a6, dx, dz
      REAL(wp) :: x, da
      REAL(wp) :: f ! value from the interpolation (extrapolation)
      !!----------------------------------------------------------------------

      x = dx / dz
      da = ar - al
      f = ar - 0.5_wp * x * ( da - ( 1._wp - 2._wp * x / 3._wp ) * a6 )

   END FUNCTION ppm_integl

   FUNCTION ppm_integr(al, ar, a6, dx, dz) RESULT(f)
      !!----------------------------------------------------------------------
      !! *** ROUTINE interp1 ***
      !!
      !! ** Purpose : 1-d ppm integration
      !!
      !! ** Method :
      !!
      !! H.Liu, Jan. 2010, POL
      !!----------------------------------------------------------------------
      IMPLICIT NONE
      REAL(wp), INTENT(in) :: al, ar, a6, dx, dz
      REAL(wp) :: x, da
      REAL(wp) :: f ! value from the interpolation (extrapolation)
      !!----------------------------------------------------------------------

      x = dx / dz
      da = ar - al
      f = al + 0.5_wp * x * ( da + ( 1._wp - 2._wp * x / 3._wp ) * a6 )

   END FUNCTION ppm_integr

   !!======================================================================

END MODULE traadv_vppm
