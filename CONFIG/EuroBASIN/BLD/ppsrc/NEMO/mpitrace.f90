

module mpitrace
  INTEGER,parameter :: VTcheck_buffer=91
  INTEGER,parameter :: VTprocess_event=92
  INTEGER,parameter :: VTbuffer_full=93

  INTEGER,parameter :: nb_inst=7
  INTEGER :: MPE_begin(nb_inst)
  INTEGER :: MPE_end(nb_inst)

contains

  subroutine InitVampir
    implicit none
  end subroutine InitVampir
  subroutine VTb(number)
    implicit none
    INTEGER :: number
  end subroutine VTb
  subroutine VTe(number)
    implicit none
    INTEGER :: Number
  end subroutine VTe
end module mpitrace
