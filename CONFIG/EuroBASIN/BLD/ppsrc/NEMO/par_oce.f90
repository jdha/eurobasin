

MODULE par_oce
   !!======================================================================
   !! *** par_oce ***
   !! Ocean : set the ocean parameters
   !!======================================================================
   !! History : OPA ! 1991 (Imbard, Levy, Madec) Original code
   !! NEMO 1.0 ! 2004-01 (G. Madec, J.-M. Molines) Free form and module
   !! 3.3 ! 2010-09 (C. Ethe) TRA-TRC merge: add jpts, jp_tem & jp_sal
   !!----------------------------------------------------------------------
   USE par_kind ! kind parameters

   IMPLICIT NONE
   PUBLIC

   !!----------------------------------------------------------------------
   !! Domain decomposition
   !!----------------------------------------------------------------------
   !! if we dont use massively parallel computer (parameters jpni=jpnj=1) so jpiglo=jpi and jpjglo=jpj
   INTEGER, PUBLIC :: jpni !: number of processors following i
   INTEGER, PUBLIC :: jpnj !: number of processors following j
   INTEGER, PUBLIC :: jpnij !: nb of local domain = nb of processors ( <= jpni x jpnj )
   INTEGER, PUBLIC, PARAMETER :: jpr2di = 0 !: number of columns for extra outer halo
   INTEGER, PUBLIC, PARAMETER :: jpr2dj = 0 !: number of rows for extra outer halo
   INTEGER, PUBLIC, PARAMETER :: jpreci = 1 !: number of columns for overlap
   INTEGER, PUBLIC, PARAMETER :: jprecj = 1 !: number of rows for overlap

   !! Ocean Domain sizes
   !! ------------------
   !! data domain (jpidta,jpjdta)
   !! global or zoom domain (jpiglo,jpjglo)
   !! local domain ( jpi , jpj )
   !!---------------------------------------------------------------------
   !! 'key_NNA_r12' : regional basin : NNA
   !!---------------------------------------------------------------------
   !!---------------------------------------------------------------------
   !! *** par_NNA_R12.h90 ***
   !! Ocean Domain : 1/12th degree resolution regional ocean
   !! (0RCA_R12 configuration)
   !!---------------------------------------------------------------------
   !!----------------------------------------------------------------------
   !! NEMO/OPA 3.3.1 , NEMO Consortium (2010)
   !! $Id: par_NNA_R12.h90 63 2013-04-29 19:59:55Z jdha $
   !! Software governed by the CeCILL licence (NEMOGCM/NEMO_CeCILL.txt)
   !! James Harle, 17/05/2011.
   !! Parameters appropriate to ORCA1
   !! Use: key_orca_r12=75 to set 75 levels
   !!----------------------------------------------------------------------
   CHARACTER (len=16) &
      , PARAMETER &
      :: &
      cp_cfg = "orca" !: name of the configuration
   INTEGER &
      , PARAMETER &
      :: &
      jp_cfg = 0.0833 , & !: resolution of the configuration (degrees)
      ! Original data size
      ! ORCA025 global grid size
      jpiglo_ORCA0083 = 4322, &
      jpjglo_ORCA0253 = 3059, & ! not used currently
      ! NNA "global" domain localisation in the ORCA0083 global grid
      jpi_iw = 2345, &
      jpi_ie = 3945, &
      jpj_js = 1810, &
      jpj_jn = 2740, &
      jpidta = ( jpi_ie - jpi_iw + 1 ), & !:first horizontal dimension > or = to jpi
      jpjdta = ( jpj_jn - jpj_js + 1 ), & !:second > or = to jpj
      jpkdta = 75 , & !: number of levels > or = to jpk
      ! total domain matrix size
      jpiglo = jpidta, & !: first dimension of global domain --> i
      jpjglo = jpjdta, & !: second dimension of global domain --> j
      ! starting position of the zoom
      jpizoom = 1 , & !: left bottom (i,j) indices of the zoom
      jpjzoom = 1 , & !: in data indices
      ! Domain characteristics
      jperio = 0 !: lateral cond. type (between 0 and 6)
   !! Values set to pp_not_used indicates that this parameter is not used in THIS config.
   !! Values set to pp_to_be_computed indicates that variables will be computed in domzgr
   REAL,PARAMETER :: pp_not_used = 999999_wp , &
      & pp_to_be_computed = 0._wp
   !!
   !! Coefficients associated with the horizontal coordinate system (jphgr_msh /= 0 )
   !!
   INTEGER, PARAMETER :: & !
      jphgr_msh = 0 !: type of horizontal mesh
      ! ! = 0 curvilinear coordinate on the sphere
      ! ! read in coordinate.nc file
      ! ! = 1 geographical mesh on the sphere
      ! ! with regular grid-spacing
      ! ! = 2 f-plane with regular grid-spacing
      ! ! = 3 beta-plane with regular grid-spacing
      ! ! = 4 Mercator grid with T/U point at the equator with
      ! ! isotropic resolution (e1_deg)
      ! ppglam0 , ppgphi0: coordinates of the lower leftmost T point of the grid.
      ! The mercator grid starts only approximately at gphi0 because
      ! of the constraint that the equator be a T point.
   REAL(wp), PARAMETER :: & !
      ppglam0 = pp_not_used, & !: longitude of first raw and column T-point (jphgr_msh = 1)
      ppgphi0 = pp_not_used, & !: latitude of first raw and column T-point (jphgr_msh = 1)
      ! ! latitude for the Coriolis or Beta parameter (jphgr_msh = 2 or 3)
      ppe1_deg = pp_not_used, & !: zonal grid-spacing (degrees)
      ppe2_deg = pp_not_used, & !: meridional grid-spacing (degrees)
      !
      ppe1_m = pp_not_used, & !: zonal grid-spacing (meters )
      ppe2_m = pp_not_used !: meridional grid-spacing (meters )
   !! Coefficients associated with the vertical coordinate system
   REAL(wp), PARAMETER :: &
      & ppsur = -3958.951371276829_wp , & !: ORCA r1 coefficients
      & ppa0 = 103.9530096000000_wp , & !: (75 levels case)
      & ppa1 = 2.415951269000000_wp , & !:
      & ppkth = 15.35101370000000_wp , & !: (non dimensional): gives the approximate
      ! !: layer number above which stretching will
      ! !: be maximum. Usually of order jpk/2.
      & ppacr = 7.00000000000_wp !: (non dimensional): stretching factor
      ! !: for the grid. The higher zacr, the smaller
      ! !: the stretching.
   !!
   !! If both ppa0 ppa1 and ppsur are specified to pp_to_be_computed, then
   !! they are computed from ppdzmin, pphmax , ppkth, ppacr in dom_zgr
   !!
   REAL(wp), PARAMETER :: &
      & ppdzmin = pp_not_used , & !: (meters) vertical thickness of the top layer
      & pphmax = pp_not_used !: (meters) Maximum depth of the ocean gdepw(jpk)
   !!
   LOGICAL, PARAMETER :: &
      & ldbletanh = .TRUE. !: Use/do not use double tanf function for vertical coordinates
   REAL(wp), PARAMETER :: &
      & ppa2 = 100.7609285000000_wp , & !: Double tanh function parameters
      & ppkth2= 48.02989372000000_wp , & !:
      & ppacr2= 13.00000000000_wp !:
      !
   !!---------------------------------------------------------------------
   !!---------------------------------------------------------------------
   !! Active tracer parameters
   !!---------------------------------------------------------------------
   INTEGER, PUBLIC, PARAMETER :: jpts = 2 !: Number of active tracers (=2, i.e. T & S )
   INTEGER, PUBLIC, PARAMETER :: jp_tem = 1 !: indice for temperature
   INTEGER, PUBLIC, PARAMETER :: jp_sal = 2 !: indice for salinity
   !!---------------------------------------------------------------------
   !! Domain Matrix size (if AGRIF, they are not all parameters)
   !!---------------------------------------------------------------------
   INTEGER, PUBLIC :: jpi ! = ( jpiglo-2*jpreci + (jpni-1) ) / jpni + 2*jpreci !: first dimension
   INTEGER, PUBLIC :: jpj ! = ( jpjglo-2*jprecj + (jpnj-1) ) / jpnj + 2*jprecj !: second dimension
   INTEGER, PUBLIC :: jpk ! = jpkdta
   INTEGER, PUBLIC :: jpim1 ! = jpi-1 !: inner domain indices
   INTEGER, PUBLIC :: jpjm1 ! = jpj-1 !: - - -
   INTEGER, PUBLIC :: jpkm1 ! = jpk-1 !: - - -
   INTEGER, PUBLIC :: jpij ! = jpi*jpj !: jpi x jpj
   !!---------------------------------------------------------------------
   !! Optimization/control flags
   !!---------------------------------------------------------------------
   LOGICAL, PUBLIC, PARAMETER :: lk_esopa = .FALSE. !: flag to activate the all options
   LOGICAL, PUBLIC, PARAMETER :: lk_vopt_loop = .FALSE. !: vector optimization flag
   !!----------------------------------------------------------------------
   !! NEMO/OPA 3.3 , NEMO Consortium (2010)
   !! $Id: par_oce.F90 3294 2012-01-28 16:44:18Z rblod $
   !! Software governed by the CeCILL licence (NEMOGCM/NEMO_CeCILL.txt)
   !!======================================================================
END MODULE par_oce
