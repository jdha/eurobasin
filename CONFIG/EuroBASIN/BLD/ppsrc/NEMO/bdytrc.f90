

MODULE bdytrc
   !!======================================================================
   !! *** MODULE bdytrc ***
   !! Ocean tracers: Apply boundary conditions for passive tracers
   !!======================================================================
   !! History : 3.4 ! 2013 (D. Ford) adapt from bdytra for use with ERSEM
   !!----------------------------------------------------------------------
   !!----------------------------------------------------------------------
   !! Dummy module NO Unstruct Open Boundary Conditions
   !!----------------------------------------------------------------------
CONTAINS
   SUBROUTINE bdy_trc(kt) ! Empty routine
      WRITE(*,*) 'bdy_trc: You should not have seen this print! error?', kt
   END SUBROUTINE bdy_trc
   !!======================================================================
END MODULE bdytrc
