

! Preprocessor definitions
 MODULE trcini_my_trc
   !!======================================================================
   !! *** MODULE trcini_my_trc ***
   !! TOP : initialisation of the MY_TRC tracers
   !!======================================================================
   !! History : 2.0 ! 2007-12 (C. Ethe, G. Madec) Original code
   !! History : Modifications for PML-ERSEM, Oct 2012, M. Butenschoen (momm@pml.ac.uk) !!----------------------------------------------------------------------
   !!----------------------------------------------------------------------
   !! Dummy module No MY_TRC model
   !!----------------------------------------------------------------------
CONTAINS
   SUBROUTINE trc_ini_my_trc ! Empty routine
   END SUBROUTINE trc_ini_my_trc
   !!======================================================================
END MODULE trcini_my_trc
