

MODULE trcnam_my_trc
   !!======================================================================
   !! *** MODULE trcnam_my_trc ***
   !! TOP : initialisation of some run parameters for ERSEM bio-model
   !!======================================================================
   !! History : 2.0 ! 2007-12 (C. Ethe, G. Madec) Original code
   !! History : Modifications for PML-ERSEM, Oct 2012, M. Butenschoen (momm@pml.ac.uk) !!----------------------------------------------------------------------
   !!----------------------------------------------------------------------
   !! Dummy module : No MY_TRC
   !!----------------------------------------------------------------------
CONTAINS
   SUBROUTINE trc_nam_my_trc ! Empty routine
   END SUBROUTINE trc_nam_my_trc
   !!======================================================================
END MODULE trcnam_my_trc
