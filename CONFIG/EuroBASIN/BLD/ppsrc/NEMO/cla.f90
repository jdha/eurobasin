

MODULE cla
   !!======================================================================
   !! *** MODULE cla ***
   !! Cross Land Advection : specific update of the horizontal divergence,
   !! tracer trends and after velocity
   !!
   !! --- Specific to ORCA_R2 ---
   !!
   !!======================================================================
   !! History : 1.0 ! 2002-11 (A. Bozec) Original code
   !! 3.2 ! 2009-07 (G. Madec) merge cla, cla_div, tra_cla, cla_dynspg
   !! ! and correct a mpp bug reported by A.R. Porter
   !!----------------------------------------------------------------------
   !!----------------------------------------------------------------------
   !! Default key Dummy module
   !!----------------------------------------------------------------------
   USE lib_mpp, ONLY: ctl_stop
CONTAINS
   SUBROUTINE cla_init
      CALL ctl_stop( 'cla_init: Cross Land Advection hard coded for ORCA_R2 with 31 levels' )
   END SUBROUTINE cla_init
   SUBROUTINE cla_div( kt )
      WRITE(*,*) 'cla_div: You should have not see this print! error?', kt
   END SUBROUTINE cla_div
   SUBROUTINE cla_traadv( kt )
      WRITE(*,*) 'cla_traadv: You should have not see this print! error?', kt
   END SUBROUTINE cla_traadv
   SUBROUTINE cla_dynspg( kt )
      WRITE(*,*) 'dyn_spg_cla: You should have not see this print! error?', kt
   END SUBROUTINE cla_dynspg
   !!======================================================================
END MODULE cla
