MODULE trcrst_my_trc
   !!======================================================================
   !!                       ***  MODULE trcrst_my_trc  ***
   !! TOP :   create, write, read the restart files of MY_TRC tracer
   !!======================================================================
   !! History :   1.0  !  2010-01 (C. Ethe)  Original
   !! History :   Modifications for PML-ERSEM, Oct 2012, M. Butenschoen (momm@pml.ac.uk)
   !!----------------------------------------------------------------------
#if defined key_PMLersem
   !!----------------------------------------------------------------------
   !!   'key_my_trc'                                               CFC tracers
   !!----------------------------------------------------------------------
   !!   trc_rst_read_my_trc   : read  restart file
   !!   trc_rst_wri_my_trc    : write restart file
   !!----------------------------------------------------------------------

   USE iom
   USE par_trc
   USE par_my_trc
   USE trc
   USE ersem_constants, ONLY: fp8
   USE pelagic_variables, ONLY: ccc,SeaFloor,Water,iBenX
   USE benthic_variables, ONLY: ccb

   USE NemoErsem, ONLY: ccbm1,SetERSEMEnvironment

   IMPLICIT NONE
   PRIVATE

   PUBLIC  trc_rst_read_my_trc   ! called by trcini.F90 module
   PUBLIC  trc_rst_wri_my_trc   ! called by trcini.F90 module

CONTAINS
   
   SUBROUTINE trc_rst_read_my_trc( knum ) 
     INTEGER, INTENT(in)  :: knum
     INTEGER :: n0,jn
# if defined key_PMLersem
      DO jn = 1, jptra2d
         CALL iom_get( knum, jpdom_autoglo, 'TR2DN'//ctrcnm2d(jn), tr2dn(:,:,jn) ) 
         ccb(:,jn) = pack(tr2dn(:,:,jn),SeaFloor>0)         
      END DO
      DO jn = 1, jptra2d
         CALL iom_get( knum, jpdom_autoglo, 'TR2DB'//ctrcnm2d(jn), tr2db(:,:,jn) ) 
         ccbm1(:,jn) = pack(tr2db(:,:,jn),SeaFloor>0)         
      END DO
      n0=jp_myt0-1
      DO jn=jp_myt0,jp_myt1
         ccc(:,jn-n0)=pack(trn(:,:,:,jn),Water)
      END DO
      CALL SetERSEMEnvironment       ! set environment and ERSEM calendar
#endif

   END SUBROUTINE trc_rst_read_my_trc

   SUBROUTINE trc_rst_wri_my_trc( kt, kirst, knum )
     INTEGER, INTENT(in)  :: kt, kirst, knum
     INTEGER :: jn

      DO jn = 1, jptra2d

         tr2dn(:,:,jn) = unpack(ccb(:,jn),SeaFloor>0,0._wp)         
         CALL iom_rstput( kt, kirst,knum, 'TR2DN'//ctrcnm2d(jn), tr2dn(:,:,jn) ) 
         !tr2db(:,:,jn) = tr2dn(:,:,jn) ! Euler
         tr2db(:,:,jn) = unpack(ccbm1(:,jn),SeaFloor>0,0._wp) !leap-frog
         CALL iom_rstput( kt, kirst,knum, 'TR2DB'//ctrcnm2d(jn), tr2db(:,:,jn) ) 
      END DO

   END SUBROUTINE trc_rst_wri_my_trc

#else
   !!----------------------------------------------------------------------
   !!  Dummy module :                                     No passive tracer
   !!----------------------------------------------------------------------
CONTAINS
   SUBROUTINE trc_rst_read_my_trc( knum )
     INTEGER, INTENT(in)  :: knum
     WRITE(*,*) 'trc_rst_read_my_trc: You should not have seen this print! error?', knum
   END SUBROUTINE trc_rst_read_my_trc

   SUBROUTINE trc_rst_wri_my_trc( kt, kirst, knum )
     INTEGER, INTENT(in)  :: kt, kirst, knum
     WRITE(*,*) 'trc_rst_wri_my_trc: You should not have seen this print! error?', kt, kirst, knum
   END SUBROUTINE trc_rst_wri_my_trc
#endif

   !!======================================================================
END MODULE trcrst_my_trc
