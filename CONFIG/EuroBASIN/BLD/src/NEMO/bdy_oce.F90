MODULE bdy_oce
   !!======================================================================
   !!                       ***  MODULE bdy_oce   ***
   !! Unstructured Open Boundary Cond. :   define related variables
   !!======================================================================
   !! History :  1.0  !  2001-05  (J. Chanut, A. Sellar)  Original code
   !!            3.0  !  2008-04  (NEMO team)  add in the reference version     
   !!            3.3  !  2010-09  (D. Storkey) add ice boundary conditions
   !!            3.4  !  2011     (D. Storkey) rewrite in preparation for OBC-BDY merge
   !!----------------------------------------------------------------------
#if defined key_bdy 
   !!----------------------------------------------------------------------
   !!   'key_bdy'                      Unstructured Open Boundary Condition
   !!----------------------------------------------------------------------
   USE par_oce         ! ocean parameters
   USE bdy_par         ! Unstructured boundary parameters
   USE lib_mpp         ! distributed memory computing
#if defined key_top
   USE par_trc, ONLY: jptra
#endif
   IMPLICIT NONE
   PUBLIC

   TYPE, PUBLIC ::   OBC_INDEX    !: Indices and weights which define the open boundary
      INTEGER,          DIMENSION(jpbgrd) ::  nblen
      INTEGER,          DIMENSION(jpbgrd) ::  nblenrim
      INTEGER, POINTER, DIMENSION(:,:)   ::  nbi
      INTEGER, POINTER, DIMENSION(:,:)   ::  nbj
      INTEGER, POINTER, DIMENSION(:,:)   ::  nbr
      INTEGER, POINTER, DIMENSION(:,:)   ::  nbmap
      REAL   , POINTER, DIMENSION(:,:)   ::  nbw
      REAL   , POINTER, DIMENSION(:,:,:)   ::  nbz
      REAL   , POINTER, DIMENSION(:)     ::  flagu
      REAL   , POINTER, DIMENSION(:)     ::  flagv
   END TYPE OBC_INDEX

   TYPE, PUBLIC ::   OBC_DATA     !: Storage for external data
      REAL, POINTER, DIMENSION(:)     ::  ssh
      REAL, POINTER, DIMENSION(:)     ::  u2d
      REAL, POINTER, DIMENSION(:)     ::  v2d
      REAL, POINTER, DIMENSION(:,:)   ::  u3d
      REAL, POINTER, DIMENSION(:,:)   ::  v3d
      REAL, POINTER, DIMENSION(:,:)   ::  tem
      REAL, POINTER, DIMENSION(:,:)   ::  sal
#if defined key_lim2
      REAL, POINTER, DIMENSION(:)     ::  frld
      REAL, POINTER, DIMENSION(:)     ::  hicif
      REAL, POINTER, DIMENSION(:)     ::  hsnif
#endif
#if defined key_top
      REAL, POINTER, DIMENSION(:,:)   ::  no3
      REAL, POINTER, DIMENSION(:,:)   ::  po4
      REAL, POINTER, DIMENSION(:,:)   ::  si
      REAL, POINTER, DIMENSION(:,:,:) ::  trc
#endif
   END TYPE OBC_DATA

   !!----------------------------------------------------------------------
   !! Namelist variables
   !!----------------------------------------------------------------------
   CHARACTER(len=80), DIMENSION(jp_bdy) ::   cn_coords_file !: Name of bdy coordinates file
   CHARACTER(len=80)                    ::   cn_mask_file   !: Name of bdy mask file
   !
   LOGICAL, DIMENSION(jp_bdy) ::   ln_coords_file           !: =T read bdy coordinates from file; 
   !                                                        !: =F read bdy coordinates from namelist
   LOGICAL                    ::   ln_mask_file             !: =T read bdymask from file
   LOGICAL                    ::   ln_vol                   !: =T volume correction             
   !
   INTEGER                    ::   nb_bdy                   !: number of open boundary sets
   INTEGER, DIMENSION(jp_bdy) ::   nn_rimwidth              !: boundary rim width for Flow Relaxation Scheme
   INTEGER                    ::   nn_volctl                !: = 0 the total volume will have the variability of the surface Flux E-P 
   !                                                        !  = 1 the volume will be constant during all the integration.
   INTEGER, DIMENSION(jp_bdy) ::   nn_dyn2d                 ! Choice of boundary condition for barotropic variables (U,V,SSH)
   INTEGER, DIMENSION(jp_bdy) ::   nn_dyn2d_dta      	    !: = 0 use the initial state as bdy dta ; 
                                                            !: = 1 read it in a NetCDF file
                                                            !: = 2 read tidal harmonic forcing from a NetCDF file
                                                            !: = 3 read external data AND tidal harmonic forcing from NetCDF files
   INTEGER, DIMENSION(jp_bdy) ::   nn_dyn3d                 ! Choice of boundary condition for baroclinic velocities 
   INTEGER, DIMENSION(jp_bdy) ::   nn_dyn3d_dta      	    !: = 0 use the initial state as bdy dta ; 
                                                            !: = 1 read it in a NetCDF file
   INTEGER, DIMENSION(jp_bdy) ::   nn_tra                   ! Choice of boundary condition for active tracers (T and S)
   INTEGER, DIMENSION(jp_bdy) ::   nn_tra_dta      	    !: = 0 use the initial state as bdy dta ; 
                                                            !: = 1 read it in a NetCDF file
   INTEGER                    ::   nb_jpk       	    ! Number of levels in the bdy data (set < 0 if consistent with planned run)
#if defined key_lim2
   INTEGER, DIMENSION(jp_bdy) ::   nn_ice_lim2              ! Choice of boundary condition for sea ice variables 
   INTEGER, DIMENSION(jp_bdy) ::   nn_ice_lim2_dta          !: = 0 use the initial state as bdy dta ; 
                                                            !: = 1 read it in a NetCDF file
#endif
#if defined key_top
!!! Have these always declared so that it doesn't matter if they're in the
!!! namelist for a physics only run
   INTEGER, DIMENSION(jp_bdy) ::   nn_trc                   ! Choice of boundary condition for passive tracers
   INTEGER, DIMENSION(jp_bdy) ::   nn_trc_dta               !: = 0 use the initial state as bdy dta ;
                                                            !: = 1 read it in a NetCDF file
#endif
   !
   
   !!----------------------------------------------------------------------
   !! Global variables
   !!----------------------------------------------------------------------
   REAL(wp), ALLOCATABLE, SAVE, DIMENSION(:,:) ::   bdytmask   !: Mask defining computational domain at T-points
   REAL(wp), ALLOCATABLE, SAVE, DIMENSION(:,:) ::   bdyumask   !: Mask defining computational domain at U-points
   REAL(wp), ALLOCATABLE, SAVE, DIMENSION(:,:) ::   bdyvmask   !: Mask defining computational domain at V-points

   REAL(wp)                                    ::   bdysurftot !: Lateral surface of unstructured open boundary

   REAL(wp), POINTER, DIMENSION(:,:)           ::   pssh       !: 
   REAL(wp), POINTER, DIMENSION(:,:)           ::   phur       !: 
   REAL(wp), POINTER, DIMENSION(:,:)           ::   phvr       !: Pointers for barotropic fields 
   REAL(wp), POINTER, DIMENSION(:,:)           ::   pu2d       !: 
   REAL(wp), POINTER, DIMENSION(:,:)           ::   pv2d       !: 

   !!----------------------------------------------------------------------
   !! open boundary data variables
   !!----------------------------------------------------------------------

   INTEGER,  DIMENSION(jp_bdy)                     ::   nn_dta            !: =0 => *all* data is set to initial conditions
                                                                          !: =1 => some data to be read in from data files
   REAL(wp), ALLOCATABLE, DIMENSION(:,:,:), TARGET ::   dta_global        !: workspace for reading in global data arrays
   REAL(wp), ALLOCATABLE, DIMENSION(:,:,:), TARGET ::   dta_global_1      !: workspace for reading in global data arrays
   REAL(wp), ALLOCATABLE, DIMENSION(:,:  ), TARGET ::   dta_global_2      !: workspace for reading in global data arrays
   TYPE(OBC_INDEX), DIMENSION(jp_bdy), TARGET      ::   idx_bdy           !: bdy indices (local process)
   TYPE(OBC_DATA) , DIMENSION(jp_bdy)              ::   dta_bdy           !: bdy external data (local process)

#if defined key_top
   !
   ! At vn3.2 it was found that advection caused a problem for the
   ! passive tracers at the boundaries. Exact details and memories
   ! are hazy, but it seems that the way the boundaries are implemented
   ! can cause large vertical velocities. These are confined to the
   ! boundary regions, so are of little consequence for the physics,
   ! but can have a large detrimental impact on passive tracers. The
   ! solution at vn3.2, which is being reproduced here (in slightly
   ! different technical form, but same scientific intent [assuming
   ! the issue has been remembered/understood correctly]), is to take
   ! trn plus the non-advective tra trends at each time step, and apply
   ! these as boundary conditions (in the absence of external data).
   !
   ! Following the approach of vn3.2, with slight adaptation, we define:
   !

   REAL(wp), ALLOCATABLE, DIMENSION(:,:,:,:)       ::   adv_tra           !: Advective tracer trends

   !
   ! adv_tra(jpi,jpj,jpk,jn) contains the advective trends for each
   ! tracer. This can be subtracted from tra to give the updated
   ! tracer field as if no advection had occurred. This is just done
   ! in the boundary regions, and is stored in dta_bdy%trc (defined above),
   ! which is then applied as a boundary condition in the normal manner
   ! (unless external data is available for a tracer, in which case that
   ! is of course used instead).
   !
   ! In this implementation, adv_tra is populated each time step by the
   ! advection routine (tra_adv_muscl or tra_adv_tvd) and everything
   ! else is done in bdytrc.F90. At vn3.2 the boundaries were populated in trc_trp
   ! (or trc_trp_spm if running SPM without ERSEM).
   !
   ! UPDATE: The same needs to be done for the diffusion too. This has been
   ! implemented, but could do with some tidying up and generalising to other
   ! diffusion routines (it's only in traldf_iso.F90 for now).
   !
   ! The %trc attribute of the OBC_DATA type is defined in addition to %no3 etc,
   ! rather than just having %trc, in order to have consistent population of
   ! %trc, and avoid pointer-based confusion. If we're using external nutrient
   ! data, these elements of %trc are just not applied (see bdy_trc).
   !
#endif

   !!----------------------------------------------------------------------
   !! NEMO/OPA 4.0 , NEMO Consortium (2011)
   !! $Id: bdy_oce.F90 68 2013-04-30 07:02:47Z jdha $ 
   !! Software governed by the CeCILL licence     (NEMOGCM/NEMO_CeCILL.txt)
   !!----------------------------------------------------------------------
CONTAINS

   FUNCTION bdy_oce_alloc()
      !!----------------------------------------------------------------------
      USE lib_mpp, ONLY: ctl_warn, mpp_sum
      !
      INTEGER :: bdy_oce_alloc
#if defined key_top
      INTEGER :: bdy_oce_trc_alloc
#endif
      !!----------------------------------------------------------------------
      !
      ALLOCATE( bdytmask(jpi,jpj) , bdyumask(jpi,jpj), bdyvmask(jpi,jpj),                    &  
         &      STAT=bdy_oce_alloc )
         !
#if defined key_top
      ! Allocate adv_tra separately to avoid conflicts
      ALLOCATE( adv_tra(jpi,jpj,jpk,jptra), STAT=bdy_oce_trc_alloc )
      IF ( bdy_oce_trc_alloc /= 0 ) bdy_oce_alloc = bdy_oce_trc_alloc
#endif
         !
      IF( lk_mpp             )   CALL mpp_sum ( bdy_oce_alloc )
      IF( bdy_oce_alloc /= 0 )   CALL ctl_warn('bdy_oce_alloc: failed to allocate arrays.')
      !
   END FUNCTION bdy_oce_alloc

#else
   !!----------------------------------------------------------------------
   !!   Dummy module                NO Unstructured Open Boundary Condition
   !!----------------------------------------------------------------------
   LOGICAL ::   ln_tides = .false.  !: =T apply tidal harmonic forcing along open boundaries
#endif

   !!======================================================================
END MODULE bdy_oce

