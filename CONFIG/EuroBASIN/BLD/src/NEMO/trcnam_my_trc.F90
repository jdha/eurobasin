MODULE trcnam_my_trc
   !!======================================================================
   !!                      ***  MODULE trcnam_my_trc  ***
   !! TOP :   initialisation of some run parameters for ERSEM bio-model
   !!======================================================================
   !! History :   2.0  !  2007-12  (C. Ethe, G. Madec) Original code
   !! History :   Modifications for PML-ERSEM, Oct 2012, M. Butenschoen (momm@pml.ac.uk)   !!----------------------------------------------------------------------
#if defined key_PMLersem
   !!----------------------------------------------------------------------
   !!   'key_my_trc'   :                                       MY_TRC model
   !!----------------------------------------------------------------------
   !! trc_nam_my_trc      : MY_TRC model initialisation
   !!----------------------------------------------------------------------
   USE oce_trc         ! Ocean variables
   USE par_trc         ! TOP parameters
   USE trc             ! TOP variables

   IMPLICIT NONE
   PRIVATE

   INTEGER,PUBLIC :: numnatl
   PUBLIC   trc_nam_my_trc   ! called by trcnam.F90 module

   !!----------------------------------------------------------------------
   !! NEMO/TOP 3.3 , NEMO Consortium (2010)
   !! $Id: trcnam_my_trc.F90 2528 2010-12-27 17:33:53Z rblod $ 
   !! Software governed by the CeCILL licence (NEMOGCM/NEMO_CeCILL.txt)
   !!----------------------------------------------------------------------

CONTAINS

   SUBROUTINE trc_nam_my_trc
      !!----------------------------------------------------------------------
      !!                     ***  trc_nam_my_trc  ***  
      !!
      !! ** Purpose :   read MY_TRC namelist
      !!
      !!----------------------------------------------------------------------
      INTEGER :: jl,jn
#if defined key_trc_diaadd && ! defined key_iomput
      TYPE(DIAG) , DIMENSION(jp_my_trc_2d) :: ersdia2d
      TYPE(DIAG) , DIMENSION(jp_my_trc_3d) :: ersdia3d

      NAMELIST/namersdia/nwritedia, ersdia3d, ersdia2d     ! additional diagnostics

#endif
      !
      IF(lwp) WRITE(numout,*)
      IF(lwp) WRITE(numout,*) ' trc_nam_my_trc : read ERSEM diagnostics namelists'
      IF(lwp) WRITE(numout,*) ' ~~~~~~~~~~~~~~~'
      !
      CALL ctl_opn( numnatl, 'namelist_ersem', 'OLD', 'FORMATTED', 'SEQUENTIAL', 1, numout, .FALSE. )

#if defined key_trc_diaadd && ! defined key_iomput

      ! Namelist namersdia
      ! -------------------
      nwritedia = 12                   ! default values
      DO jl = 1, jp_my_trc_2d
         jn = jp_myt0_2d + jl - 1
         WRITE(ctrc2d(jn),'("2D_",I1)') jn                      ! short name
         WRITE(ctrc2l(jn),'("2D DIAGNOSTIC NUMBER ",I2)') jn    ! long name
         ctrc2u(jn) = ' '                                       ! units
      END DO
      !                                 ! 3D output arrays
      DO jl = 1, jp_my_trc_3d
         jn = jp_myt0_3d + jl - 1
         WRITE(ctrc3d(jn),'("3D_",I1)') jn                      ! short name
         WRITE(ctrc3l(jn),'("3D DIAGNOSTIC NUMBER ",I2)') jn    ! long name
         ctrc3u(jn) = ' '                                       ! units
      END DO

      REWIND( numnatl )               ! read natrtd
      READ  ( numnatl, namersdia )

      DO jl = 1, jp_my_trc_2d
         jn = jp_myt0_2d + jl - 1
         ctrc2d(jn) = ersdia2d(jl)%snamedia
         ctrc2l(jn) = ersdia2d(jl)%lnamedia
         ctrc2u(jn) = ersdia2d(jl)%unitdia
      END DO

      DO jl = 1, jp_my_trc_3d
         jn = jp_myt0_3d + jl - 1
         ctrc3d(jn) = ersdia3d(jl)%snamedia
         ctrc3l(jn) = ersdia3d(jl)%lnamedia
         ctrc3u(jn) = ersdia3d(jl)%unitdia
      END DO

      IF(lwp) THEN                   ! control print
         WRITE(numout,*)
         WRITE(numout,*) ' Namelist : natadd'
         WRITE(numout,*) '    frequency of outputs for additional arrays nwritedia = ', nwritedia
         DO jl = 1, jp_my_trc_3d
            jn = jp_myt0_3d + jl - 1
            WRITE(numout,*) '   3d output field No : ',jn
            WRITE(numout,*) '   short name         : ', TRIM(ctrc3d(jn))
            WRITE(numout,*) '   long name          : ', TRIM(ctrc3l(jn))
            WRITE(numout,*) '   unit               : ', TRIM(ctrc3u(jn))
            WRITE(numout,*) ' '
         END DO
 
         DO jl = 1, jp_my_trc_2d
            jn = jp_myt0_2d + jl - 1
            WRITE(numout,*) '   2d output field No : ',jn
            WRITE(numout,*) '   short name         : ', TRIM(ctrc2d(jn))
            WRITE(numout,*) '   long name          : ', TRIM(ctrc2l(jn))
            WRITE(numout,*) '   unit               : ', TRIM(ctrc2u(jn))
            WRITE(numout,*) ' '
         END DO
      ENDIF
#endif
      CLOSE( numnatl )

   END SUBROUTINE trc_nam_my_trc
   
#else
   !!----------------------------------------------------------------------
   !!  Dummy module :                                             No MY_TRC
   !!----------------------------------------------------------------------
CONTAINS
   SUBROUTINE trc_nam_my_trc                      ! Empty routine
   END  SUBROUTINE  trc_nam_my_trc
#endif  

   !!======================================================================
END MODULE trcnam_my_trc
