MODULE bdytrc
   !!======================================================================
   !!                       ***  MODULE  bdytrc  ***
   !! Ocean tracers:   Apply boundary conditions for passive tracers
   !!======================================================================
   !! History :  3.4  !  2013     (D. Ford) adapt from bdytra for use with ERSEM
   !!----------------------------------------------------------------------
#if defined key_bdy && defined key_top 
   !!----------------------------------------------------------------------
   !!   'key_bdy'                     Unstructured Open Boundary Conditions
   !!----------------------------------------------------------------------
   !!   bdy_trc            : Apply open boundary conditions to passive tracers
   !!   bdy_trc_frs        : Apply Flow Relaxation Scheme
   !!----------------------------------------------------------------------
   USE timing          ! Timing
   USE oce             ! ocean dynamics and tracers variables
   USE dom_oce         ! ocean space and time domain variables 
   USE bdy_oce         ! ocean open boundary conditions
   USE bdydta, ONLY:   bf
   USE lbclnk          ! ocean lateral boundary conditions (or mpp link)
   USE in_out_manager  ! I/O manager
   USE trc, ONLY: tra, rdttrc, nn_dttrc, nittrc000, ln_trc_bdy
   USE par_trc, ONLY: jptra
   USE par_my_trc, ONLY: jp_myt0
#ifdef key_fix_ecobc
   USE global_declarations, ONLY:  iN1p, iN3n, iN5s, iR6c, iR6n, iR6p, iR6s, &
             iR4c, iR4n, iR4p, iR8c, iR8n, iR8p, iR8s, iO3c, iR1p, iP1p, iP2p, &
             iP3p, iP4p, iZ5p, iZ6p, iB1p, iR1n, iP1n, iP2n, iP3n, iP4n, iZ5n, &
             iZ6n, iB1n, iN4n
#else
   USE global_declarations, ONLY:  iN1p, iN3n, iN5s,iO3c
#endif

   IMPLICIT NONE
   PRIVATE
   REAL(wp), ALLOCATABLE, SAVE, DIMENSION(:) ::   r2dt



   PUBLIC bdy_trc      ! routine called in trcnxt.F90 

   !!----------------------------------------------------------------------
   !! NEMO/OPA 3.3 , NEMO Consortium (2010)
   !! $Id: bdytra.F90 3294 2012-01-28 16:44:18Z rblod $ 
   !! Software governed by the CeCILL licence (NEMOGCM/NEMO_CeCILL.txt)
   !!----------------------------------------------------------------------
CONTAINS

   INTEGER FUNCTION bdytrc_alloc()
      !!----------------------------------------------------------------------
      !!                   ***  ROUTINE trc_nxt_alloc  ***
      !!----------------------------------------------------------------------
      ALLOCATE( r2dt(jpk), STAT=bdytrc_alloc )
      !
      IF( bdytrc_alloc /= 0 )   CALL ctl_warn('bdytrc_alloc : failed to allocate array')
      !
   END FUNCTION bdytrc_alloc

   SUBROUTINE bdy_trc( kt )
      !!----------------------------------------------------------------------
      !!                  ***  SUBROUTINE bdy_trc  ***
      !!
      !! ** Purpose : - Apply open boundary conditions for passive tracers
      !!
      !!----------------------------------------------------------------------
      INTEGER, INTENT( in )    :: kt     ! Main time step counter
      !!
      INTEGER                  :: ib_bdy, igrd, jn, ib, ii, ij, ik
   !  REAL(wp), DIMENSION(jpk) :: r2dt   ! Time step size
      
      ! Set trbdy equal to tra without the advective trends (see comments in bdy_oce)
      ! At this point tra is the actual after field, rather than a trend (this is done
      ! in the call to trc_zdf, which is called before trc_nxt, which calls bdy_trc).
      ! Therefore we went tra minus adv_tra multiplied by the time step information.

      ! Set time step size (Euler/Leapfrog)
      ! SHOULD REALLY BACK-PORT THE VN3.5 NAMELIST SWITCH!
      
       IF( kt ==  nittrc000 ) THEN  
          IF( bdytrc_alloc() /= 0 )   CALL ctl_stop( 'STOP', 'bdytrc_init : unable to allocate arrays' )
       ENDIF

       IF( neuler == 0 .AND. kt ==  nittrc000 ) THEN  ;  r2dt(:) =     rdttrc(:)   ! at nittrc000             (Euler)
       ELSEIF( kt <= nittrc000 + 1 )            THEN  ;  r2dt(:) = 2.* rdttrc(:)   ! at nit000 or nit000+1 (Leapfrog)
       ENDIF
!      IF (lwp) write(numout,*) 'BDYTIME', neuler, kt, nittrc000, r2dt(1), narea
   !  r2dt(:) = rdttrc(:)
      
      ! Populate trbdy (or not if the namelist is set to use initial conditions for this
      ! boundary, bearing in mind this could get confusing)
      DO ib_bdy = 1, nb_bdy  
         IF(nn_trc_dta(ib_bdy) == 1) THEN !nn_trc_dta(ib_bdy) == 0 --> use initial conditions
            igrd = 1            ! T-points data
            DO ib = 1, idx_bdy(ib_bdy)%nblen(igrd)
               ii = idx_bdy(ib_bdy)%nbi(ib,igrd)
               ij = idx_bdy(ib_bdy)%nbj(ib,igrd)
               DO ik = 1, jpkm1
                  DO jn = 1, jptra
                     dta_bdy(ib_bdy)%trc(ib,ik,jn) = tra(ii,ij,ik,jn) - adv_tra(ii,ij,ik,jn) * r2dt(ik)
!                   IF (narea.eq.2 .and. ii.eq.14 .and. ij.lt.4 .and. ik.eq.1 .and. jn.eq.2 ) write(*,*) &
!                                       'bdytrc0 ',  tra(ii,ij,ik,jn), &
!                                        adv_tra(ii,ij,ik,jn), ij, kt
                  END DO
               END DO
            END DO
         ENDIF
#ifdef key_fix_ecobc_poc
              !dta_bdy(ib_bdy)%trc(:,:,iR6c+jp_pmlers0-1) = 0._wp
              !dta_bdy(ib_bdy)%trc(:,:,iR6n+jp_pmlers0-1) = 0._wp
              !dta_bdy(ib_bdy)%trc(:,:,iR6p+jp_pmlers0-1) = 0._wp
              !dta_bdy(ib_bdy)%trc(:,:,iR6s+jp_pmlers0-1) = 0._wp
              !dta_bdy(ib_bdy)%trc(:,:,iR4c+jp_pmlers0-1) = 0._wp
              !dta_bdy(ib_bdy)%trc(:,:,iR4n+jp_pmlers0-1) = 0._wp
              !dta_bdy(ib_bdy)%trc(:,:,iR4p+jp_pmlers0-1) = 0._wp
              !dta_bdy(ib_bdy)%trc(:,:,iR8c+jp_pmlers0-1) = 0._wp
              !dta_bdy(ib_bdy)%trc(:,:,iR8n+jp_pmlers0-1) = 0._wp
              !dta_bdy(ib_bdy)%trc(:,:,iR8p+jp_pmlers0-1) = 0._wp
              !dta_bdy(ib_bdy)%trc(:,:,iR8s+jp_pmlers0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR6c+jp_myt0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR6n+jp_myt0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR6p+jp_myt0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR6s+jp_myt0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR4c+jp_myt0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR4n+jp_myt0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR4p+jp_myt0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR8c+jp_myt0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR8n+jp_myt0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR8p+jp_myt0-1) = 0._wp
               dta_bdy(ib_bdy)%trc(:,:,iR8s+jp_myt0-1) = 0._wp
#endif
      END DO

      ! Apply boundary conditions
      DO ib_bdy=1, nb_bdy

         SELECT CASE( nn_trc(ib_bdy) )
         CASE(jp_none)
            CYCLE
         CASE(jp_frs)
            CALL bdy_trc_frs( idx_bdy(ib_bdy), dta_bdy(ib_bdy), kt )
         CASE DEFAULT
            CALL ctl_stop( 'bdy_trc : unrecognised option for open boundaries for passive tracers' )
         END SELECT
      ENDDO

   END SUBROUTINE bdy_trc

   SUBROUTINE bdy_trc_frs( idx, dta, kt )
      !!----------------------------------------------------------------------
      !!                 ***  SUBROUTINE bdy_trc_frs  ***
      !!                    
      !! ** Purpose : Apply the Flow Relaxation Scheme for tracers at open boundaries.
      !! 
      !! Reference : Engedahl H., 1995, Tellus, 365-382.
      !!----------------------------------------------------------------------
      INTEGER,         INTENT(in) ::   kt
      TYPE(OBC_INDEX), INTENT(in) ::   idx  ! OBC indices
      TYPE(OBC_DATA),  INTENT(in) ::   dta  ! OBC external data
      !! 
      REAL(wp) ::   zwgt           ! boundary weight
      INTEGER  ::   ib, ik, igrd   ! dummy loop indices
      INTEGER  ::   ii, ij, jn     ! 2D addresses
      INTEGER  ::   iim1, iip1, ijm1, ijp1   ! 2D addresses
#ifdef key_fix_ecobc
      REAL(wp) ::   r,r1          ! multiplier for relaxation
#endif
      !!----------------------------------------------------------------------
      !
      IF( nn_timing == 1 ) CALL timing_start('bdy_trc_frs')
      !
!     IF (lwp) write(numout,*) 'jp_myt0 = ', jp_myt0
      igrd = 1                       ! Everything is at T-points here
#ifdef key_fix_ecobc
! timescale for relaxation is one day
      r = rdt*nn_dttrc/86400.d0
! try shorter - 12 hours
      !r = rdt*rdttrc/43200.d0
#endif

      DO ib = 1, idx%nblen(igrd)
            ii = idx%nbi(ib,igrd)
            ij = idx%nbj(ib,igrd)
         DO ik = 1, jpkm1
#ifdef key_fix_ecobc
!           r1 = rdt*nn_dttrc/MAX((e2t(ii,ij)/MAX(ABS(un(ii,ij,ik)*umask(ii,ij,ik)),ABS(un(ii-1,ij,ik)*umask(ii-1,ij,ik)))), &
!                                 (e1t(ii,ij)/MAX(ABS(vn(ii,ij,ik)*vmask(ii,ij,ik)),ABS(vn(ii,ij-1,ik)*vmask(ii,ij-1,ik)))))
            r1 = 2 * rdt*nn_dttrc * MAX( (MAX(ABS(un(ii,ij,ik)*umask(ii,ij,ik)),ABS(un(ii-1,ij,ik)*umask(ii-1,ij,ik)))/e2t(ii,ij)), &
                                  (MAX(ABS(vn(ii,ij,ik)*vmask(ii,ij,ik)),ABS(vn(ii,ij-1,ik)*vmask(ii,ij-1,ik)))/e1t(ii,ij)))
             r1 = MIN(r1,1._wp)
#endif
!           IF (lwp .and. ik == 1) write(numout,*) 'R1 ',r1, ABS(un(ii,ij,ik)*umask(ii,ij,ik)), ABS(un(ii-1,ij,ik)*umask(ii-1,ij,ik)), &
!                                                  ABS(vn(ii,ij,ik)*vmask(ii,ij,ik)),ABS(vn(ii,ij-1,ik)*vmask(ii,ij-1,ik)), &
!                                                  e1t(ii,ij), e2t(ii,ij), iN3n+jp_myt0-1
            DO jn = 1, jptra
            zwgt = idx%nbw(ib,igrd)
         !   zwgt = 1._wp
!#ifdef key_fix_ecobc
!            IF(ln_trc_bdy(jn)) zwgt=zwgt*MAX(r,r1)
!#endif
           !IF (lwp .and. ii.eq.14 .and. ij.eq.4 .and. ik.eq.1 .and. jn.eq.2 ) write(numout,*) &
!                   IF (narea.eq.2 .and. ii.eq.14 .and. ij.lt.4 .and. ik.eq.1 .and. jn.eq.2 ) write(*,*) &
!                         'bdytrc1 ',  tra(ii,ij,ik,jn), dta%trc(ib,ik,jn), zwgt
               IF ( (jn /= iN3n+jp_myt0-1) .AND. (jn /= iN1p+jp_myt0-1) .AND. (jn /= iN5s+jp_myt0-1) ) THEN
                  tra(ii,ij,ik,jn) = ( tra(ii,ij,ik,jn) + zwgt * ( dta%trc(ib,ik,jn) - tra(ii,ij,ik,jn) ) ) * tmask(ii,ij,ik)
                ! IF ( idx%nbw(ib,igrd) == 1._wp ) tra(ii,ij,ik,jn) = MIN(tra(ii,ij,ik,jn), dta%trc(ib,ik,jn)) * tmask(ii,ij,ik)
               ENDIF
           !IF (lwp .and. ii.eq.14 .and. ij.eq.4 .and. ik.eq.1 .and. jn.eq.2 ) write(numout,*) &
!                   IF (narea.eq.2 .and. ii.eq.14 .and. ij.lt.4 .and. ik.eq.1 .and. jn.eq.2 ) write(*,*) &
!            'bdytrc2 ',  tra(ii,ij,ik,jn), dta%trc(ib,ik,jn), zwgt
            END DO


!!!!!!!!!!!1 TODO sort out this mess
            zwgt = idx%nbw(ib,igrd)
#ifdef key_fix_ecobc 
            IF(ln_trc_bdy(iN3n+jp_myt0-1)) zwgt=zwgt*MAX(r,r1)
#endif
            tra(ii,ij,ik,iN3n+jp_myt0-1) = ( tra(ii,ij,ik,iN3n+jp_myt0-1) + zwgt * &
                                                  ( dta%no3(ib,ik) - tra(ii,ij,ik,iN3n+jp_myt0-1) ) ) * tmask(ii,ij,ik)
            IF ( idx%nbw(ib,igrd) == 1._wp )  &
                     tra(ii,ij,ik,iN3n+jp_myt0-1) = MIN(tra(ii,ij,ik,iN3n+jp_myt0-1), dta%no3(ib,ik)) * tmask(ii,ij,ik)
            tra(ii,ij,ik,iN1p+jp_myt0-1) = ( tra(ii,ij,ik,iN1p+jp_myt0-1) + zwgt * &
                                                  ( dta%po4(ib,ik) - tra(ii,ij,ik,iN1p+jp_myt0-1) ) ) * tmask(ii,ij,ik)
            IF ( idx%nbw(ib,igrd) == 1._wp )  &
                     tra(ii,ij,ik,iN1p+jp_myt0-1) = MIN(tra(ii,ij,ik,iN1p+jp_myt0-1), dta%po4(ib,ik)) * tmask(ii,ij,ik)
            tra(ii,ij,ik,iN5s+jp_myt0-1) = ( tra(ii,ij,ik,iN5s+jp_myt0-1) + zwgt * &
                                                  ( dta%si(ib,ik)  - tra(ii,ij,ik,iN5s+jp_myt0-1) ) ) * tmask(ii,ij,ik)
            IF ( idx%nbw(ib,igrd) == 1._wp )  &
                     tra(ii,ij,ik,iN5s+jp_myt0-1) = MIN(tra(ii,ij,ik,iN5s+jp_myt0-1), dta%si(ib,ik)) * tmask(ii,ij,ik)
         END DO
      END DO 
      !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! replace outer most grid cells as outside the dynamic boundary

      DO jn = 1, jptra
        CALL lbc_lnk( tra(:,:,:,jn), 'T', 1., 'no0' )    ! Boundary points should be updated
        !CALL lbc_lnk( tra(:,:,:,jn), 'T', 1.)    ! Boundary points should be updated
      END DO
      DO jn = 1, jptra
   !  IF ( (jn /= iN3n+jp_myt0-1) .AND. (jn /= iN1p+jp_myt0-1) .AND. (jn /= iN5s+jp_myt0-1) ) THEN
   !  igrd = 1      ! Flather bc on u-velocity; 
   !  DO ib = 1, idx%nblenrim(igrd)
   !     ii  = idx%nbi(ib,igrd)
   !     ij  = idx%nbj(ib,igrd)
   !     DO ik = 1, jpkm1
   !      IF (lwp .and. ii.eq.14 .and. ij.eq.4 .and. ik.eq.1 .and. jn.eq.2 ) write(numout,*) &
   !        'bdytrc3 ',  tra(ii,ij,ik,jn), dta%trc(ib,ik,jn), zwgt
   !     ENDDO
   !  ENDDO
   !  ELSE
      igrd = 2      ! Flather bc on u-velocity; 
      !             ! remember that flagu=-1 if normal velocity direction is outward
      !             ! I think we should rather use after ssh ?
      DO ib = 1, idx%nblenrim(igrd)
         ii  = idx%nbi(ib,igrd)
         ij  = idx%nbj(ib,igrd)
         iim1 = ii + MAX( 0, INT( idx%flagu(ib) ) )   ! T pts i-indice inside the boundary
         iip1 = ii - MIN( 0, INT( idx%flagu(ib) ) )   ! T pts i-indice outside the boundary 
         !
!                   IF (narea.eq.2 .and. jn.eq.2 .and.  kt ==  nittrc000 ) write(*,*) 'FLAGU ', idx%flagu(ib)
         DO ik = 1, jpkm1
         tra(iip1,ij,ik,jn) = tra(iim1,ij,ik,jn)
         ENDDO
      END DO
!                   IF (narea.eq.2 .and. jn.eq.2 ) write(*,*) 'TRA AFTER U ', tra(14,3,1,jn), tra(14,2,1,jn)
      !
      igrd = 3      ! Flather bc on v-velocity
      !             ! remember that flagv=-1 if normal velocity direction is outward
      DO ib = 1, idx%nblenrim(igrd)
         ii  = idx%nbi(ib,igrd)
         ij  = idx%nbj(ib,igrd)
         ijm1 = ij + MAX( 0, INT( idx%flagv(ib) ) )   ! T pts j-indice inside the boundary
         ijp1 = ij - MIN( 0, INT( idx%flagv(ib) ) )   ! T pts j-indice outside the boundary 
         !
!                   IF (narea.eq.2 .and. jn.eq.2 .and.  kt ==  nittrc000 ) write(*,*) 'FLAGV ', idx%flagv(ib)
         DO ik = 1, jpkm1
         tra(ii,ijp1,ik,jn) = tra(ii,ijm1,ik,jn)
         ENDDO
      END DO
   !  ENDIF
!                   IF (narea.eq.2 .and. jn.eq.2 ) write(*,*) 'TRA AFTER V ', tra(14,3,1,jn), tra(14,2,1,jn)
      END DO
      


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      DO jn = 1, jptra
         CALL lbc_lnk( tra(:,:,:,jn), 'T', 1., 'no0' )    ! Boundary points should be updated
      END DO
               !    IF (narea.eq.2 ) write(*,*) 'TRA AFTER LNK ', tra(14,3,1,2), tra(14,2,1,2)
      !
      IF( nn_timing == 1 ) CALL timing_stop('bdy_trc_frs')
      !
   END SUBROUTINE bdy_trc_frs
   
#else
   !!----------------------------------------------------------------------
   !!   Dummy module                   NO Unstruct Open Boundary Conditions
   !!----------------------------------------------------------------------
CONTAINS
   SUBROUTINE bdy_trc(kt)      ! Empty routine
      WRITE(*,*) 'bdy_trc: You should not have seen this print! error?', kt
   END SUBROUTINE bdy_trc
#endif

   !!======================================================================
END MODULE bdytrc
