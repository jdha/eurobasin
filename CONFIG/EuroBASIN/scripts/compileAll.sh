#!/bin/bash
# 1: compiler

COMPILER=${1:-pgi}
./compile.sh debug nemo clean $COMPILER >& compile.nemo.$COMPILER.log
./compile.sh debug nemoersem clean $COMPILER >& compile.nemo.$COMPILER.log
./compile.sh production nemo clean $COMPILER >& compile.nemo.$COMPILER.log
./compile.sh production nemoersem clean $COMPILER >& compile.nemo.$COMPILER.log
#
