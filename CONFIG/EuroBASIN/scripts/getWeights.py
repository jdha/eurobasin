from numpy import arange,zeros
#no leap-year centres:
nlc=arange(.5/365.,1.,1./365.)
#leap-year centres:
lc=arange(.5/366.,1.,1./366.)
#left and right weights:
lw=zeros(lc.shape);rw=zeros(lc.shape)
rw[0]=0
d=lc[1:]-lc[:-1]
rw[1:]=(nlc-lc[:-1])/d
lw[:-1]=(lc[1:]-nlc)/d
from netCDF4 import Dataset
nc=Dataset('DailyLeapWeights.nc','w')
nc.createDimension('time',None)
nc.createVariable('lw','f4',('time',))
nc.createVariable('rw','f4',('time',))
nc.variables['lw'][:]=lw
nc.variables['rw'][:]=rw
nc.close()
lw8=zeros([lw.shape[0]*8])
rw8=zeros([lw.shape[0]*8])
nc=Dataset('4HourlyLeapWeights.nc','w')
for n in arange(8):
  lw8[n::8]=lw
  rw8[n::8]=rw
nc.createDimension('time',None)
nc.createVariable('lw','f4',('time',))
nc.createVariable('rw','f4',('time',))
nc.variables['lw'][:]=lw8
nc.variables['rw'][:]=rw8
nc.close()
