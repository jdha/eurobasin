#!/bin/bash

FORCDIR=/data/medusa/momm/NEMO-NAtl/FORC/ATM

#radsw,radlw,precip,snow
#VSTR=snow
#bn=drowned_radsw_DFS5.1_y
#bn=drowned_radlw_DFS5.1_y
#bn=drowned_precip_ERAinterim_dtrd_corr_astorto_notimintrp_y
#bn=drowned_snow_ERAinterim_y
#WFILE=DailyLeapWeights.nc
#TOFFSET=1
#TEND=365

#t2,q2,u10,v10
VSTR=v10
#bn=drowned_t2_DFS5.1_y
#bn=drowned_q2_DFS5.1_y
#bn=drowned_u10_ERAinterim_corr_QuikSCAT_v2_y
bn=drowned_v10_ERAinterim_corr_QuikSCAT_v2_y
WFILE=4HourlyLeapWeights.nc
TOFFSET=8
TEND=2927

TMPDIR=/tmp/nc/$VSTR
mkdir -p $TMPDIR
rm -rf $TMPDIR/*

year=1978
while [ $year -le 2011 ]
do
  echo "Retrieving year" $year  
  FILE=$bn$year.nc  
  if [ $(( $year%4 )) -eq 0 ]
  then
    ncrename -O -v lw,$VSTR $WFILE $TMPDIR/$WFILE 
    ncbo -O -y'*' -v$VSTR $FORCDIR/$FILE $TMPDIR/$WFILE $TMPDIR/${bn}_l.nc  
    ncks -O -d time,0,$(( $TEND-$TOFFSET )) $TMPDIR/${bn}_l.nc $TMPDIR/${bn}_l.nc
    ncrename -O -v rw,$VSTR $WFILE $TMPDIR/$WFILE 
    ncbo -O -y'*' -v$VSTR $FORCDIR/$FILE $TMPDIR/$WFILE $TMPDIR/${bn}_r.nc  
    ncks -O -d time,$TOFFSET,$TEND $TMPDIR/${bn}_r.nc $TMPDIR/${bn}_r.nc
    ncbo -O -y'+' -v$VSTR $TMPDIR/${bn}_l.nc $TMPDIR/${bn}_r.nc $TMPDIR/${bn}$year.nc
  else  
    ln -sf $FORCDIR/$FILE $TMPDIR/
  fi    
  year=$(( $year+1 ))
done
echo "Processing..."
ncea -O -v $VSTR `ls $TMPDIR/${bn}????.nc` $FORCDIR/${bn}clim.nc
echo "Done."
