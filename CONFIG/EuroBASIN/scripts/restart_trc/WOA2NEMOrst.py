from ncdfView import ncdfView
from netCDF4 import Dataset
from numpy import where

nc=Dataset('BASN25_trc_IC.nc','a')
ncw=ncdfView('NATL25-IC.nc',Quiet=True)
ncv=nc.variables
for vstr,v in ncw.variables.items():
    ncv['TRN'+vstr][:]=where(v[:].mask,ncv['TRB'+vstr][:],v[:].data)
    ncv['TRB'+vstr][:]=where(v[:].mask,ncv['TRB'+vstr][:],v[:].data)
nc.close()
ncw.close()
