#!/bin/bash --login
#
# command line arguments:
# 1 compilation mode (production/debug)
# 2 model name
# 3 clean before compilation (clean/noclean)
# 4 compiler (pgi/cray)
#

BLDDIR=$HOME/work/eurobasin/port/NEMO-ERSEM-NAtl/BLD_new/cfg
ERSEMDIR=$HOME/work/eurobasin/port/NEMO-ERSEM-NAtl/BLD_new/src/ERSEM/ERSEM
RUNDIR=/work/n01/n01/jdha/eurobasin/port/NEMO-ERSEM-NAtl/EXP00
#export MPI_HOME=/usr/local
#export NCDFDIR=/opt/cray/netcdf/default/pgi/119
MODE=${1:-production}
MODEL=${2:-nemoersem}
CCLEAN=${3:-noclean}
PENV=PrgEnv-intel
if [ $PENV == PrgEnv-cray ] ; then
module swap $(module list 2>&1 | grep PrgEnv | cut -f2 -d")" | cut -d"/" -f1) $PENV
COMPILER=cray
#flags for cray
FFLAGS="-em -s real64 -s integer32 -eZ -e0"
if [ $MODE == debug ] ; then
FFLAGS="-G0 -K trap=ovf  $FFLAGS"
fi
fi
#module swap $(module list 2>&1 | grep PrgEnv | cut -f2 -d")" | cut -d"/" -f1) $PENV
if [ $PENV == PrgEnv-intel ] ; then
COMPILER=intel
#LD="ifort -L/opt/cray/mpt/default/gni/mpich2-intel/130/lib -L/opt/cray/netcdf/default/intel/130/lib -L/opt/cray/hdf5/default/intel/130/lib -lnetcdff -lfmpich_intel -lmpich_intel -lmpl -lhdf5_hl -lhdf5 -static"
#flags for ifort
FFLAGS="-integer-size 32 -real-size 64 -fp-model source -zero -fpp -warn all "
if [ $MODE = debug ] ; then
FFLAGS="-g -O0 -traceback $FFLAGS"
elif [ $MODE = traceback ] ; then
FFLAGS="-O3 -traceback $FFLAGS"
elif [ $MODE = production ] ; then
FFLAGS="-O3 $FFLAGS"
fi
fi
#rsync -a ncdfErsem?D.list $ERSEMDIR/src/output/
cd $ERSEMDIR/src/output
python parseBudgets.py
python parseNCDFErsem.py
cd $BLDDIR
echo "Compiling $MODE in $BLDDIR..."
#if [ $MODE = "production" ] ; then
# cat bld_ersem_template.cfg \
#     | sed "s,__FFLAGS__,-O3 $FFLAGS,g" \
#     > /tmp/bld_ersem.cfg
#elif [ $MODE = "traceback" ] ; then
# cat bld_ersem_template.cfg \
#     | sed "s,__FFLAGS__,-O3 $FFLAGS,g" \
#     > /tmp/bld_ersem.cfg
#else
# cat bld_ersem_template.cfg \
#     | sed "s,__FFLAGS__,-g -O0 $FFLAGS,g" \
#     > /tmp/bld_ersem.cfg
#fi
cat bld_ersem_template.cfg \
     | sed "s,__FFLAGS__,$FFLAGS,g" \
     > /tmp/bld_ersem.cfg
if [ $MODEL = "nemoersem" ] ; then
 cat /tmp/bld_ersem.cfg \
     | sed "s,__PPNEMO__,key_top key_lim2 key_PMLersem key_nosignedzero ADYTRACER key_tide key_melange key_limit_bfr key_bdy key_jth_fix key_fix_ecobc_poc key_fix_ecobc key_netcdf4 PPNCDFERSEM PPNCDFRESTART PARALLELERSEM,g" \
     | sed "s,__PPERSEM__,key_PMLersem key_iomput IOPMODEL DOCDYN IRON ADYTRACER SAVEFLX CALC CO2SERIES PPNCDFERSEM PPNCDFRESTART PARALLELERSEM,g" \
     > bld_ersem.cfg
else
 cat /tmp/bld_ersem.cfg \
     | sed "s,__PPNEMO__,key_limit_bfr key_lim2 key_tide key_bdy key_jth_fix key_netcdf4 key_nosignedzero,g" \
     | sed "s,__PPERSEM__, IOPMODEL DOCDYN IRON ADYTRACER SAVEFLX CALC CO2SERIES PPNCDFERSEM PPNCDFRESTART PARALLELERSEM,g" \
     > bld_ersem.cfg
fi
if [ $CCLEAN = "clean" ]; then
     fcm build --clean bld_ersem.cfg
fi
fcm build bld_ersem.cfg
#rsync -a ../bin/nemo.exe $RUNDIR/$MODEL-$COMPILER-$MODE
rsync -a ../bin/nemo.exe $RUNDIR/opa.test.181214
