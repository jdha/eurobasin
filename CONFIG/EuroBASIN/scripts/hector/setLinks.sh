INPUTBASE=/work/n04/n04/momme/NEMO-NAtl
ln -sf $INPUTBASE/FORC/bathy/coordinates.nc .
ln -sf $INPUTBASE/FORC/bathy/bathy_meter.nc.new.corrected bathy_meter.nc
ln -sf $INPUTBASE/FORC/levitus/data_1m_potential_temperature_nomask.nc data_1m_potential_temperature_nomask.nc
ln -sf $INPUTBASE/FORC/levitus/data_1m_salinity_nomask.nc data_1m_salinity_nomask.nc
ln -sf $INPUTBASE/FORC/reshape* .
ln -sf $INPUTBASE/FORC/levitus/sss_1m.nc .
ln -sf $INPUTBASE/FORC/levitus/sst_1m.nc .
ln -sf $INPUTBASE/FORC/BIO/chlorophyll.nc .
ln -sf $INPUTBASE/FORC/RNF/runoff_1m_nomask.nc .
#iomdef.xml
