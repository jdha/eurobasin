#!/bin/bash --login
#
# command line arguments:
# 1 compilation mode (production/debug)
# 2 model name
# 3 clean before compilation (clean/noclean)
# 4 compiler (pgi/cray)
#

COMPILER=${4:-pgi}
ARCH=$(module list 2>&1 | grep craype | grep -v network | cut -f2 -d")") 
#module swap $ARCH craype-interlagos
ARCH=$(module list 2>&1 | grep craype | grep -v network | cut -f2 -d")") 
echo "Compiling for architecture" $ARCH "."
PENV=$(module list 2>&1 | grep PrgEnv | cut -f2 -d")" | cut -d"/" -f1)
#module swap $PENV PrgEnv-$COMPILER
PENV=$(module list 2>&1 | grep PrgEnv | cut -f2 -d")" | cut -d"/" -f1)
echo "Compilation environment" $PENV "."
#module add netcdf
BLDDIR=$HOME/work/eurobasin/port/NEMO-ERSEM-NAtl/BLD/cfg
ERSEMDIR=$HOME/work/eurobasin/port/NEMO-ERSEM-NAtl/BLD/src/ERSEM/ERSEM
RUNDIR=/work/n01/n01/jdha/eurobasin/port/NEMO-ERSEM-NAtl/EXP00
#export MPI_HOME=/usr/local
#export NCDFDIR=/opt/cray/netcdf/default/pgi/119
MODE=${1:-production}
MODEL=${2:-nemoersem}
CCLEAN=${3:-noclean}
if [ $PENV == PrgEnv-cray ] ; then
#flags for cray
FFLAGS="-em -s integer32 -s real64 -eZ -e0"
if [ $MODE == debug ] ; then
FFLAGS="-G0 -K trap=ovf -eZ -e0 $FFLAGS"
fi
fi
if [ $PENV == PrgEnv-pgi ] ; then
module swap pgi pgi/12.10.0
export PGI_TERM=trace
#flags for pgi
FFLAGS="-r8 -i4"
if [ $MODE == debug ] ; then
FFLAGS=" -Mbounds -Mlist -Minfo -traceback -Ktrap=fp $FFLAGS"
fi
fi
rsync -a $RUNDIR/ncdfErsem?D.list $ERSEMDIR
cd $ERSEMDIR
python parseBudgets.py
python parseNCDFErsem.py
cd $BLDDIR
echo "Compiling $MODE in $BLDDIR..."
if [ $MODE = "production" ] ; then
 cat bld_ersem_template.cfg \
     | sed "s,__FFLAGS__,-O3 $FFLAGS,g" \
     > /tmp/bld_ersem.cfg
else
 cat bld_ersem_template.cfg \
     | sed "s,__FFLAGS__,-O0 -g $FFLAGS,g" \
     > /tmp/bld_ersem.cfg
fi
#     | sed "s,__PPNEMO__,key_top key_PMLersem key_netcdf4 key_nosignedzero,g" \
if [ $MODEL = "nemoersem" ] ; then
 cat /tmp/bld_ersem.cfg \
     | sed "s,__PPNEMO__,key_PMLersem key_top key_tide key_melange key_limit_bfr key_bdy key_jth_fix key_fix_ecobc_poc key_fix_ecobc key_PMLersem key_netcdf4 key_nosignedzero  PPNCDFERSEM PPNCDFRESTART PARALLELERSEM,g" \
     | sed "s,__PPERSEM__,key_PMLersem key_PMLersem key_bdy key_fix_ecobc_poc key_fix_ecobc key_iomput IRON SAVEFLX CALC CO2SERIES PPNCDFERSEM PPNCDFRESTART PARALLELERSEM,g" \
     > bld_ersem.cfg
else
 cat /tmp/bld_ersem.cfg \
     | sed "s,__PPNEMO__,key_limit_bfr key_tide key_jth_fix key_melange key_netcdf4 key_bdy key_nosignedzero,g" \
     | sed "s,__PPERSEM__, IRON SAVEFLX CALC CO2SERIES PPNCDFERSEM PPNCDFRESTART PARALLELERSEM,g" \
     > bld_ersem.cfg
fi
if [ $CCLEAN = "clean" ]; then
     fcm build --clean bld_ersem.cfg
fi
fcm build bld_ersem.cfg
rsync -a ../bin/nemo.exe $RUNDIR/$MODEL-$COMPILER-$MODE
