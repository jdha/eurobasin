INPUTBASE=/work/n04/n04/momme/NEMO-NAtl

# ATMOS FORCING
#3 hourly
ln -sf $INPUTBASE/FORC/ATM/drowned_u10_ERAinterim_corr_QuikSCAT_v2_yclim.nc drowned_u10_ERAinterim_corr_QuikSCAT_v2_y2222.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_u10_ERAinterim_corr_QuikSCAT_v2_yclim.nc drowned_u10_ERAinterim_corr_QuikSCAT_v2_y2221.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_u10_ERAinterim_corr_QuikSCAT_v2_yclim.nc drowned_u10_ERAinterim_corr_QuikSCAT_v2_y2223.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_v10_ERAinterim_corr_QuikSCAT_v2_yclim.nc drowned_v10_ERAinterim_corr_QuikSCAT_v2_y2222.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_v10_ERAinterim_corr_QuikSCAT_v2_yclim.nc drowned_v10_ERAinterim_corr_QuikSCAT_v2_y2221.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_v10_ERAinterim_corr_QuikSCAT_v2_yclim.nc drowned_v10_ERAinterim_corr_QuikSCAT_v2_y2223.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_q2_DFS5.1_yclim.nc drowned_q2_DFS5.1_y2222.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_q2_DFS5.1_yclim.nc drowned_q2_DFS5.1_y2221.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_q2_DFS5.1_yclim.nc drowned_q2_DFS5.1_y2223.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_t2_DFS5.1_yclim.nc drowned_t2_DFS5.1_y2222.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_t2_DFS5.1_yclim.nc drowned_t2_DFS5.1_y2221.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_t2_DFS5.1_yclim.nc drowned_t2_DFS5.1_y2223.nc
#daily
ln -sf $INPUTBASE/FORC/ATM/drowned_radsw_DFS5.1_yclim.nc drowned_radsw_DFS5.1_y2222.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_radsw_DFS5.1_yclim.nc drowned_radsw_DFS5.1_y2221.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_radsw_DFS5.1_yclim.nc drowned_radsw_DFS5.1_y2223.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_radlw_DFS5.1_yclim.nc drowned_radlw_DFS5.1_y2222.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_radlw_DFS5.1_yclim.nc drowned_radlw_DFS5.1_y2221.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_radlw_DFS5.1_yclim.nc drowned_radlw_DFS5.1_y2223.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_precip_ERAinterim_dtrd_corr_astorto_notimintrp_yclim.nc drowned_precip_ERAinterim_dtrd_corr_astorto_notimintrp_y2222.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_precip_ERAinterim_dtrd_corr_astorto_notimintrp_yclim.nc drowned_precip_ERAinterim_dtrd_corr_astorto_notimintrp_y2221.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_precip_ERAinterim_dtrd_corr_astorto_notimintrp_yclim.nc drowned_precip_ERAinterim_dtrd_corr_astorto_notimintrp_y2223.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_snow_ERAinterim_yclim.nc drowned_snow_ERAinterim_y2222.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_snow_ERAinterim_yclim.nc drowned_snow_ERAinterim_y2221.nc
ln -sf $INPUTBASE/FORC/ATM/drowned_snow_ERAinterim_yclim.nc drowned_snow_ERAinterim_y2223.nc
