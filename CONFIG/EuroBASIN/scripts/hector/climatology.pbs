#!/bin/bash --login
#PBS -N NATL.momm
#PBS -l mppwidth=1024
#PBS -l mppnppn=32
#PBS -l walltime=00:20:00
#PBS -o NATL.momm
#PBS -j oe
#PBS -A n04-o2025
##PBS -v DISPLAY

export PGI_TERM=trace
export ATP_ENABLED=1

ARCH=$(module list 2>&1 | grep craype | grep -v network | cut -f2 -d")") 
module swap $(module list 2>&1 | grep craype | grep -v network | cut -f2 -d")") craype-interlagos
ARCH=$(module list 2>&1 | grep craype | grep -v network | cut -f2 -d")") 
echo "Compiling for architecture" $ARCH "."
PENV=$(module list 2>&1 | grep PrgEnv | cut -f2 -d")" | cut -d"/" -f1)
module swap $PENV PrgEnv-pgi
PENV=$(module list 2>&1 | grep PrgEnv | cut -f2 -d")" | cut -d"/" -f1)
echo "Compilation environment" $PENV "."
module add netcdf
module load stat

TMPDIR=/work/n04/n04/momme/tmp # for fortran scratch files, /tmp doesn't exist on computing nodes

TPN=32
I=8
J=4
NTASKS=1024
NNODES=$(( NTASKS/$TPN ))
#EXE="nemoersem-pgi-production"
EXE="nemoersem-pgi-debug"
#EXE="nemo-pgi-debug"

RUNDIR=/work/n04/n04/momme/NEMO-NAtl/HINDCAST

cd $RUNDIR

y0=2222
ym1=$(($y0-1))
yp1=$(($y0+1))

#link atmospheric forcing:
rm drowned_*_y????.nc
./setClimatologyLinks.sh

#physics restart

if [ -f restart_0000.nc ]
then
rst=".true."
else
rst=".false."
fi

#biogeochemistry restart

if [ ${EXE: 0:9} = nemoersem ]   
then 
  lbio=.true.
  lrgb=.false.
else
  lbio=.false.
  lrgb=.true.
fi
cat namelist.template \
                | sed "s,__YEAR__,$y0,g" \
                | sed "s,__RST__,$rst,g" \
                | sed "s,__II__,0,g" \
                | sed "s,__JJ__,0,g" \
                | sed "s,__IIJJ__,$NTASKS,g" \
                | sed "s,__LBIO__,$lbio,g" \
                | sed "s,__LRGB__,$lrgb,g" \
                > namelist
cat NemoErsem.nml.template \
                | sed "s,__RSTERSEMOUT__,hindcast.$y0.RSTERSEM,g" \
                | sed "s,__RSTERSEMIN__,hindcast/$ym1/hindcast.$ym1.RSTERSEM,g" \
                | sed "s,__SPIN__,0,g" \
                > NemoErsem.nml
rm -f mesh_mask_00*nc
echo "=== running nemo on $NTASKS cores ($NNODES nodes)"
date >> runs.log
aprun -N$TPN -n$NTASKS $EXE >& out.log
#totalview aprun -a -b -N$TPN -n$NTASKS $EXE > out.log

# prepare restart for next cycle:
rstfiles=BASN25_00017520_restart_????.nc
for file in $rstfiles
do
  fn=${file: -7:4} #file number
  mv $file restart_$fn.nc
done
rstfiles=BASN25_00017520_restart_ice_????.nc
for file in $rstfiles
do
  fn=${file: -7:4} #file number
  mv $file restart_ice_$fn.nc
done
rstfiles=BASN25_00017520_rst_trc_????.nc
for file in $rstfiles
do
  fn=${file: -7:4} #file number
  mv $file restart_trc_$fn.nc
done

# move output files:

mv BASN25_1m_${y0}0101_${y0}1231_grid_?_????.nc /work/n04/n04/momme/NEMO-NAtl/HINDCAST/clim/
mv BASN25_1m_${y0}0101_${y0}1231_diad_?_????.nc /work/n04/n04/momme/NEMO-NAtl/HINDCAST/clim/
mv BASN25_1m_${y0}0101_${y0}1231_ptrc_?_????.nc /work/n04/n04/momme/NEMO-NAtl/HINDCAST/clim/
mv BASN25_1d_${y0}0101_${y0}1231_grid_T_*.nc /work/n04/n04/momme/NEMO-NAtl/HINDCAST/clim/

# resubmit:
#qsub climatology.pbs

# merge on LMS:
cat chainMerge.rst.lms.template.pbs \
                | sed "s,__YEAR__,$y0,g" \
                | sed "s,__ORIGPATH__,$RUNDIR,g" \
                | sed "s,__DESTPATH__,$RUNDIR/clim,g" \
                > chainMerge.rst.pbs
cat chainMerge.grid.lms.template.pbs \
                | sed "s,__YEAR__,$y0,g" \
                | sed "s,__ORIGPATH__,$RUNDIR,g" \
                | sed "s,__DESTPATH__,$RUNDIR/clim,g" \
                > chainMerge.grid.pbs
cat chainMerge.ptrc.lms.template.pbs \
                | sed "s,__YEAR__,$y0,g" \
                | sed "s,__ORIGPATH__,$RUNDIR,g" \
                | sed "s,__DESTPATH__,$RUNDIR/clim,g" \
                > chainMerge.ptrc.pbs
cat chainMerge.diad.lms.template.pbs \
                | sed "s,__YEAR__,$y0,g" \
                | sed "s,__ORIGPATH__,$RUNDIR,g" \
                | sed "s,__DESTPATH__,$RUNDIR/clim,g" \
                > chainMerge.diad.pbs
ssh -f lms "qsub $RUNDIR/chainMerge.rst.pbs"
ssh -f lms "qsub $RUNDIR/chainMerge.grid.pbs"
ssh -f lms "qsub $RUNDIR/chainMerge.ptrc.pbs"
ssh -f lms "qsub $RUNDIR/chainMerge.diad.pbs"
rm -f output.*_????.nc
rm -f mesh_mask*_????.nc
rm -f $TMPDIR/*
