INPUTBASE=/work/n04/n04/momme/NEMO-NAtl

# ATMOS FORCING
#3 hourly
ln -sf $INPUTBASE/FORC/ATM/drowned_u10_ERAinterim_corr_QuikSCAT_v2_y$1.nc .
ln -sf $INPUTBASE/FORC/ATM/drowned_v10_ERAinterim_corr_QuikSCAT_v2_y$1.nc .
ln -sf $INPUTBASE/FORC/ATM/drowned_q2_DFS5.1_y$1.nc .
ln -sf $INPUTBASE/FORC/ATM/drowned_t2_DFS5.1_y$1.nc .
#daily
ln -sf $INPUTBASE/FORC/ATM/drowned_radsw_DFS5.1_y$1.nc .
ln -sf $INPUTBASE/FORC/ATM/drowned_radlw_DFS5.1_y$1.nc .
ln -sf $INPUTBASE/FORC/ATM/drowned_precip_ERAinterim_dtrd_corr_astorto_notimintrp_y$1.nc .
ln -sf $INPUTBASE/FORC/ATM/drowned_snow_ERAinterim_y$1.nc . daily
