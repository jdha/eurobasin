from ncdfView import ncdfView
from numpy import array,meshgrid,where,reshape
from numpy.ma import zeros as mzeros
from numpy.ma import where as mwhere
from numpy.ma import ones as mones
from numpy.ma import array as marray
from numpy.ma import masked_where,masked_greater,masked_less_equal
from scipy.interpolate import splrep,splev,interp1d,bisplrep,bisplev
from UTMgrid import UTMgrid,createIPTable
from netCDF4 import _default_fillvals,Dataset
nc=Dataset('NATL25-WOA.nc','w',format='NETCDF4')
nc.title='NEMO-Nutrient Climatology'
bn='0112an1'
ncORCA=ncdfView('NATL25-grid_T.nc')
ncWOA=ncdfView('/data/modellers/archived/WOAdata/'+bn+'.nc')
oMask=ncORCA('vosaline').max(0)<=0.
WOAMask=ncWOA('n'+bn+'').mask[0,:,:,:]
WOAMask[:,168:,:]=True
WOAMask[:,:120,22:50]=True
WOAMask[:,155:168,250:277]=True
WOAMask[:,:97,250:290]=True
WOAMask[:,97:103,255:277]=True
WOAMask[:,97:107,255:269]=True
WOAMask[:,97:99,255:278]=True
WOAMask[:,:,43:258]=True
lon=ncWOA('lon').ravel()
lat=ncWOA('lat').ravel()
lon,lat=meshgrid(lon,lat)
lon=masked_where(WOAMask[0],lon)
lat=masked_where(WOAMask[0],lat)
lonlat=marray([lon.ravel(),lat.ravel()]).transpose()
lon=masked_where(oMask[0],ncORCA('nav_lon')).ravel()
lat=masked_where(oMask[0],ncORCA('nav_lat')).ravel()
lonlatOut=marray([lon,lat]).transpose()
print "Creatin Look-up table for horizontal interpolation first..."
ipTable=createIPTable(lonlat,lonlatOut)
orcaShape=oMask.shape
tShape=ncWOA('n'+bn).shape[0]
#write ncdf header:
print "No. of depth levels for vertical interpolation: ",orcaShape[0]
nc.history='Restart file created by PML-ERSEM'
nc.institution='Plymouth Marine Laboratory - UK'
nc.source='World Ocean Atlas'
nc.createDimension('buffer',tShape)
nc.createDimension('z',orcaShape[0])
nc.createDimension('y',orcaShape[1])
nc.createDimension('x',orcaShape[2])
N3n=nc.createVariable('N3n','f8',('buffer','z','y','x'))
nc.variables['N3n'].set_auto_maskandscale(True)
N1P=nc.createVariable('N1p','f8',('buffer','z','y','x'))
nc.variables['N1p'].set_auto_maskandscale(True)
N5s=nc.createVariable('N5s','f8',('buffer','z','y','x'))
nc.variables['N5s'].set_auto_maskandscale(True)
O2o=nc.createVariable('O2o','f8',('buffer','z','y','x'))
nc.variables['O2o'].set_auto_maskandscale(True)
# fill in data:
d={'n'+bn:'N3n','p'+bn:'N1p','i'+bn:'N5s','o'+bn:'O2o'}
eps={'n'+bn:1.e-2*7.86e-4,'p'+bn:1.e-2*7.86e-4,'i'+bn:1.e-2*.3,'o'+bn:.01}
for dkey,dvar in d.items():
 VWOA=ncWOA(dkey)   
 print "interpolating "+dvar+"..."
 for m in xrange(VWOA.shape[0]):
  # getting data from origin:
  N=VWOA[m,:,:,:]
  woaShape=[N.shape[0],N.shape[1],N.shape[2]]
  N=masked_greater(N,999998.)
  #transform in [vertical levels,horizontal points] shape:
  N=reshape(N,[woaShape[0],woaShape[1]*woaShape[2]])
  print "horizontal interpolation..."
  #intermediate shape horizontal dimension from ORCA, vertical from WOA:
  iShape=[woaShape[0],orcaShape[1],orcaShape[2]]
  iN=mzeros([iShape[0],iShape[1]*iShape[2]])
  #fill Look-up table with 0 in irrelevant points:
  Pos=where(ipTable.mask[:,:,0],0,ipTable.data[:,:,0])
  w=ipTable[:,:,1]
  for n in xrange(iN.shape[1]):
    pos=Pos[n,:]
    iN[:,n]=w[n,0]*N[:,pos[0]]+w[n,1]*N[:,pos[1]]+w[n,2]*N[:,pos[2]]
  #iN=iN.transpose()
  print "vertical interpolation..."
  oN=mones([orcaShape[1]*orcaShape[2],orcaShape[0]])*_default_fillvals['f8']
  Wz=ncWOA('depth')
  for n,el in enumerate(iN.transpose()):
    if any(el.mask==False):
      z=masked_where(el.mask,Wz)
      zmax=z.max()
      Oz=ncORCA('deptht')[:]
      Oz=where(Oz>zmax,zmax,Oz)
      korder=min(len(z.compressed())-1,1)
      if korder==0:
	oN[n,:]=el[0]
      else:
	s=splrep(z.compressed(),el.compressed(),s=0,k=korder)
	oN[n,:]=splev(Oz,s)
	#s=interp1d(z.compressed(),el.compressed())
	#oN[n,:]=s(-Oz)
      oN[n,:]=where(Oz>z.compressed()[-1],-1.e7,oN[n,:])
  oN=reshape(oN,[orcaShape[1],orcaShape[2],orcaShape[0]])
  iN=reshape(iN,iShape)
  oN=oN.transpose([2,0,1])
  oN=masked_where(oMask,oN)
  print "Filling data below WOA levels..."
  fillMask=where(oN.data<=-1.e7,-oN.mask,False)
  oN[0,:,:]=mwhere(fillMask[0,:,:],iN[0,:,:],oN[0,:,:]) #fill surface wholes
  # fill in data outside WOA range by horizontal interpolation:
  for k in xrange(orcaShape[0]):
   for j in xrange(orcaShape[1]):
    for i in xrange(orcaShape[2]):
      if fillMask[k,j,i]:
          kmin=max(0,k-1)
          kmax=min(orcaShape[0],k+2)
          jmin=max(0,j-1)
          jmax=min(orcaShape[1],j+2)
          imin=max(0,i-1)
	  imax=min(orcaShape[2],i+2)
	  oN[k,j,i]=oN[kmin:kmax,jmin:jmax,imin:imax].mean()
  oN=where(oN>eps[dkey],oN,eps[dkey])
  oN=where(oMask,_default_fillvals['f8'],oN)
  oN=masked_where(oMask,oN)
  if dvar=='N3n':
	print 'writing '+dvar+'...'
	nc.variables['N3n'][m]=1.*oN
  elif dvar=='O2o':
	print 'writing '+dvar+'...'
	nc.variables['O2o'][m]=44.614*oN
  else:
	print 'writing '+dvar+'...'
	nc.variables[dvar][m]=1.*oN
nc.close()
ncORCA.close()
ncWOA.close()
